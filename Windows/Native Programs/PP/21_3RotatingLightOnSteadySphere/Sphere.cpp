#include<Windows.h>
#include<stdio.h>
#include"vmath.h"

#include<GL/glew.h>
#pragma comment (lib,"glew32.lib")
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#include"Sphere.h"
#pragma comment (lib,"Sphere.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600


//namespace
using namespace vmath;

//Global variables
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];

//light values
float lightAmbiantRed[] = { 0.0f,0.0f,0.0f,1.0f };
float lightDiffuseRed[] = { 1.0f,0.0f,0.0f,1.0f };
float lightSpecularRed[] = { 1.0f,0.0f,0.0f,1.0f };
float lightPositionRed[] = { 0.0f,0.0f,0.0f,1.0f };

float lightAmbiantBlue[] = { 0.0f,0.0f,0.0f,1.0f };
float lightDiffuseBlue[] = { 0.0f,1.0f,0.0f,1.0f };
float lightSpecularBlue[] = { 0.0f,1.0f,0.0f,1.0f };
float lightPositionBlue[] = { 0.0f,0.0f,0.0f,1.0f };

float lightAmbiantGreen[] = { 0.0f,0.0f,0.0f,1.0f };
float lightDiffuseGreen[] = { 0.0f,0.0f,1.0f,1.0f };
float lightSpecularGreen[] = { 0.0f,0.0f,1.0f,1.0f };
float lightPositionGreen[] = { 0.0f,0.0f,0.0f,1.0f };
//material values
float materialAmbiant[4] = { 0.0f,0.0f,0.0f,0.0f };
float materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialShinyness = 128.0f;

bool gbFullScreen = false;
bool gbLighting = false;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

//variables for PP
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_sphere;//vertex array object for rect
GLuint vbo_position_sphere;//vertex buffer object(position) for rect
GLuint vbo_normal_sphere;//vertex buffer object(color) for rect
GLuint vbo_elements_sphere;

//matrices
GLuint mUniform; // model matrix
GLuint vUniform; // view matrix
GLuint pUniform; //projection matrix

//lights
//lights
GLuint laUniformRed;
GLuint ldUniformRed;
GLuint lsUniformRed;


GLuint laUniformBlue;
GLuint ldUniformBlue;
GLuint lsUniformBlue;

GLuint laUniformGreen;
GLuint ldUniformGreen;
GLuint lsUniformGreen;
//material
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShinynessUniform;


GLuint lightPositionUniformRed;
GLuint lightPositionUniformBlue;
GLuint lightPositionUniformGreen;

GLuint lKeyIsPressedUniform;

//sphere variables
unsigned int gNumVertices;
unsigned int gNumElements;

mat4 perspectiveProjectionMatrix;//This is from vmath


//Animation varibales
GLfloat lightAngleRed = 0.0f;
GLfloat lightAngleBlue = 0.0f;
GLfloat lightAngleGreen = 0.0f;


//enum decleration
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};



//function
void update(void);
void vertexShaderCode(void);
void fragmentShaderCode(void);
void createProgramCode(void);
void geomentryCode(void);
void uninitialize(void);

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w+") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("RSK_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0X46:

			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (gbLighting == false)
			{
				gbLighting = true;
			}
			else {
				gbLighting = false;
			}
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	// variables
	GLenum result;
	/*GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	*///functions
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	//openGL extensions of PP
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit Failed!!\n");
		uninitialize();
	}


	//GLEW initialization Code for GLSL (IMPORTANT : It must be here Means After 
	// creating OpenGL context But before using	any OpenGL function

	GLenum glew_error = glewInit();
	fprintf(gpFile, "GL_Version : %s \n", (char*)glGetString(GL_VERSION));

	fprintf(gpFile, "GL_Shading_Language_Version : %s \n", (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));


	//call vertex shader function 
	vertexShaderCode();

	//call fragment shader function
	fragmentShaderCode();

	//call create Program code function
	createProgramCode();

	//Postlinking Retriving Uniform locations
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniformRed = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	ldUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
	lsUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	lightPositionUniformRed = glGetUniformLocation(gShaderProgramObject, "u_lightPosition_red");
	laUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	ldUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
	lsUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	lightPositionUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_lightPosition_blue");
	laUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_la_green");
	ldUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_ld_green");
	lsUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_ls_green");
	lightPositionUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_lightPosition_green");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShine");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");

	//call geomentry code function
	geomentryCode();


	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//ADD CULL FACE
	//glDisable(GL_CULL_FACE);

	//Background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//Give identity toorthographicProjectMatrix
	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);

}


void vertexShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec4 u_lightPosition_red;" \
		"uniform vec4 u_lightPosition_blue;" \
		"uniform vec4 u_lightPosition_green;" \
		"out vec3 tNorm;" \
		"out vec3 lightDirectionRed;" \
		"out vec3 lightDirectionBlue;" \
		"out vec3 lightDirectionGreen;" \
		"out vec3 viwerVector;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
		"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" \
		"lightDirectionRed = vec3(u_lightPosition_red - eye_Coordinate);" \
		"lightDirectionBlue = vec3(u_lightPosition_blue - eye_Coordinate);" \
		"lightDirectionGreen = vec3(u_lightPosition_green - eye_Coordinate);" \
		"viwerVector = -eye_Coordinate.xyz;" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}

void fragmentShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 lightDirectionRed;" \
		"in vec3 lightDirectionBlue;" \
		"in vec3 lightDirectionGreen;" \
		"in vec3 viwerVector;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShine;" \
		"uniform vec4 u_lightPosition_red;" \
		"uniform vec4 u_lightPosition_blue;" \
		"uniform vec4 u_lightPosition_green;" \
		"uniform int u_lKeyIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec3 normalizeTNorm = normalize(tNorm);" \
		"vec3 normalizeLightDirectionRed = normalize(lightDirectionRed);" \
		"vec3 normalizeLightDirectionBlue = normalize(lightDirectionBlue);" \
		"vec3 normalizeLightDirectionGreen = normalize(lightDirectionGreen);" \
		"vec3 normalizeViwerVector = normalize(viwerVector);" \
		"float tn_dot_ldRed = max(dot(normalizeLightDirectionRed,normalizeTNorm),0.0f);" \
		"float tn_dot_ldBlue = max(dot(normalizeLightDirectionBlue,normalizeTNorm),0.0f);" \
		"float tn_dot_ldGreen = max(dot(normalizeLightDirectionGreen,normalizeTNorm),0.0f);" \
		"vec3 reflectionVectorRed = reflect(-normalizeLightDirectionRed , normalizeTNorm);" \
		"vec3 reflectionVectorBlue = reflect(-normalizeLightDirectionBlue , normalizeTNorm);" \
		"vec3 reflectionVectorGreen = reflect(-normalizeLightDirectionGreen , normalizeTNorm);" \
		"vec3 ambiantRed = vec3(u_la_red * u_ka);" \
		"vec3 ambiantBlue = vec3(u_la_blue * u_ka);" \
		"vec3 ambiantGreen = vec3(u_la_green * u_ka);" \
		"vec3 diffuseRed = vec3(u_ld_red * u_kd * tn_dot_ldRed);" \
		"vec3 diffuseBlue = vec3(u_ld_blue * u_kd * tn_dot_ldBlue);" \
		"vec3 diffuseGreen = vec3(u_ld_green * u_kd * tn_dot_ldGreen);" \
		"vec3 specularRed = vec3(u_ls_red * u_ks * pow(max(dot(reflectionVectorRed , normalizeViwerVector),0.0f),u_materialShine));" \
		"vec3 specularBlue = vec3(u_ls_blue * u_ks * pow(max(dot(reflectionVectorBlue , normalizeViwerVector),0.0f),u_materialShine));" \
		"vec3 specularGreen = vec3(u_ls_green * u_ks * pow(max(dot(reflectionVectorGreen , normalizeViwerVector),0.0f),u_materialShine));" \
		"vec3 phong_ads_light_red = ambiantRed + diffuseRed + specularRed;" \
		"vec3 phong_ads_light_blue = ambiantBlue + diffuseBlue + specularBlue;" \
		"vec3 phong_ads_light_green = ambiantGreen + diffuseGreen + specularGreen;" \
		"FragColor = vec4(phong_ads_light_red + phong_ads_light_blue + phong_ads_light_green ,1.0f);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
		"}" \
		"}"
	};
	//*******************************//"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		
	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}




void createProgramCode(void)
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding of vertex shader

	//----- Position
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	// ---- Normal
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking For Program Object

	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}


void geomentryCode(void)
{

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//****************************** CUBE ****************************
	//Create vao for rect
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//################### POSITION ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and arra for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//################### NORMAL ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);


	//########################## Elements ###############
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_elements_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

	////how many slots my array is break
	//glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	////Enabling the position
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);

}
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}



void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Using program object 
	glUseProgram(gShaderProgramObject);

	//declerations of matrix
	mat4 viewMatrix;
	mat4 modelMatrix;
	mat4 translationMatrix;
	/*mat4 rotationMatrix;
	mat4 scaleMatrix;*/
	mat4 projectionMatrix;
	//*********************************** SPHERE *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -3.0f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;
	//Send neccessary matrices to shader in respective to uniforms-
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	
	if (gbLighting == true)
	{
		lightPositionRed[1] = cos(lightAngleRed) * 10.0f;
		lightPositionRed[2] = -sin(lightAngleRed) * 10.0f;
		
		lightPositionBlue[0] = cos(lightAngleBlue) * 10.0f;
		lightPositionBlue[2] = -sin(lightAngleBlue) * 10.0f;
		
		lightPositionGreen[0] = cos(lightAngleGreen) * 10.0f;
		lightPositionGreen[1] = sin(lightAngleGreen) * 10.0f;

		glUniform1i(lKeyIsPressedUniform, 1);

		glUniform3fv(laUniformRed, 1, lightAmbiantRed);
		glUniform3fv(ldUniformRed, 1, lightDiffuseRed);
		glUniform3fv(lsUniformRed, 1, lightSpecularRed);
		glUniform4fv(lightPositionUniformRed, 1, lightPositionRed);

		glUniform3fv(laUniformBlue, 1, lightAmbiantBlue);
		glUniform3fv(ldUniformBlue, 1, lightDiffuseBlue);
		glUniform3fv(lsUniformBlue, 1, lightSpecularBlue);
		glUniform4fv(lightPositionUniformBlue, 1, lightPositionBlue);

		glUniform3fv(laUniformGreen, 1, lightAmbiantGreen);
		glUniform3fv(ldUniformGreen, 1, lightDiffuseGreen);
		glUniform3fv(lsUniformGreen, 1, lightSpecularGreen);
		glUniform4fv(lightPositionUniformGreen, 1, lightPositionGreen);

		glUniform3fv(kaUniform, 1, materialAmbiant);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(materialShinynessUniform, materialShinyness);

		
	}
	else {
		glUniform1i(lKeyIsPressedUniform, 0);
	}



	//Bind with vao of rectangle
	glBindVertexArray(vao_sphere);

	//Bind texture if any

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
	//glDrawArrays(GL_TRIANGLES, 0, gNumElements);
	//Unbind vao of rectangle
	glBindVertexArray(0);


	//Unused Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{

	lightAngleRed = lightAngleRed + 0.003f;
	if (lightAngleRed >= 360.0f)
	{
		lightAngleRed = 0.0f;
	}
	lightAngleBlue = lightAngleBlue + 0.003f;
	if (lightAngleBlue >= 360.0f)
	{
		lightAngleBlue = 0.0f;
	}
	lightAngleGreen = lightAngleGreen + 0.003f;
	if (lightAngleGreen >= 360.0f)
	{
		lightAngleGreen = 0.0f;
	}


}


void uninitialize(void)
{
	if (vbo_elements_sphere)
	{
		glDeleteBuffers(1, &vbo_elements_sphere);
		vbo_elements_sphere = 0;
	}

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}

	if (vbo_position_sphere)
	{
		glDeleteVertexArrays(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}



	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

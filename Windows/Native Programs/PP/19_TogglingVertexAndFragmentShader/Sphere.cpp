#include<Windows.h>
#include<stdio.h>
#include"vmath.h"

#include<GL/glew.h>
#pragma comment (lib,"glew32.lib")
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#include"Sphere.h"
#pragma comment (lib,"Sphere.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600


//namespace
using namespace vmath;

//Global variables
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];



//light values
float lightAmbiant[4] = { 0.0f,0.0f,0.0f,0.0f };
float lightDiffuse[4] = { 0.5f,0.2f,0.7f,1.0f };
float lightSpecular[4] = { 0.5f,0.2f,0.7f,1.0f };
float lightPosition[4] = { 100.0f,100.0f,100.0f,1.0f };
//material values
float materialAmbiant[4] = { 0.0f,0.0f,0.0f,0.0f };
float materialDiffuse[4] = { 0.5f,0.2f,0.7f,1.0f };
float materialSpecular[4] = { 0.5f,0.2f,0.7f,1.0f };
float materialShinyness = 128.0f;


bool gbFullScreen = false;
bool gbLighting = false;
bool isPerVertex = false;
bool isPerFragment = false;


DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

//variables for PP
GLuint gVertexShaderObjectForPerVertex;
GLuint gFragmentShaderObjectForPerVertex;
GLuint gShaderProgramObjectForPerVertex;

GLuint gVertexShaderObjectForPerFragment;
GLuint gFragmentShaderObjectForPerFragment;
GLuint gShaderProgramObjectForPerFragment;

GLuint vao_sphere;//vertex array object for rect
GLuint vbo_position_sphere;//vertex buffer object(position) for rect
GLuint vbo_normal_sphere;//vertex buffer object(color) for rect
GLuint vbo_elements_sphere;

//matrices
GLuint mUniformVertex; // model matrix
GLuint vUniformVertex; // view matrix
GLuint pUniformVertex; //projection matrix

//lights
GLuint laUniformVertex;
GLuint ldUniformVertex;
GLuint lsUniformVertex;

//material
GLuint kaUniformVertex;
GLuint kdUniformVertex;
GLuint ksUniformVertex;
GLuint materialShinynessUniformVertex;


GLuint lightPositionUniformVertex;
GLuint lKeyIsPressedUniformVertex;

//matrices
GLuint mUniformFragment; // model matrix
GLuint vUniformFragment; // view matrix
GLuint pUniformFragment; //projection matrix

//lights
GLuint laUniformFragment;
GLuint ldUniformFragment;
GLuint lsUniformFragment;

//material
GLuint kaUniformFragment;
GLuint kdUniformFragment;
GLuint ksUniformFragment;
GLuint materialShinynessUniformFragment;


GLuint lightPositionUniformFragment;
GLuint lKeyIsPressedUniformFragment;

//sphere variables
unsigned int gNumVertices;
unsigned int gNumElements;

mat4 perspectiveProjectionMatrix;//This is from vmath


//Animation varibales
GLfloat angleCube = 0.0f;


//enum decleration
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};



//function
void update(void);
void vertexShaderCodPerVertex(void);
void frgmentShaderCodPerVertex(void);
void vertexShaderCodPerFragment(void);
void fragmentShaderCodePerFragment(void);
void createProgramCodePerVertex(void);
void createProgramCodePerFragment(void);
void geomentryCode(void);
void uninitialize(void);


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w+") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("RSK_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0x54:
			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		
		case 'f':
		case 'F':
			if (isPerFragment == false)
			{
				isPerFragment = true;
			}
			else {
				isPerFragment = false;
			}
			break;
		case 'v':
		case 'V':
			if (isPerVertex == false)
			{
				isPerVertex = true;
			}
			else {
				isPerVertex = false;
			}
			break;
		case 'L':
		case 'l':
			if (gbLighting == false)
			{
				gbLighting = true;
			}
			else {
				gbLighting = false;
			}
			break;
		}
		break;
	
	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	// variables
	GLenum result;
	/*GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	*///functions
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	//openGL extensions of PP
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit Failed!!\n");
		uninitialize();
	}


	//GLEW initialization Code for GLSL (IMPORTANT : It must be here0 Means After 
	// creating OpenGL context But before using	any OpenGL function

	GLenum glew_error = glewInit();
	fprintf(gpFile, "GL_Version : %s \n", (char*)glGetString(GL_VERSION));

	fprintf(gpFile, "GL_Shading_Language_Version : %s \n", (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));


	//call vertex shader function 

	vertexShaderCodPerVertex();
	frgmentShaderCodPerVertex();
	//call fragment shader function
	vertexShaderCodPerFragment();
	fragmentShaderCodePerFragment();


	//call create Program code function
	createProgramCodePerVertex();
	createProgramCodePerFragment();

	//Postlinking Retriving Uniform locations
	fprintf(gpFile, " VERT POSTLINKING");
	mUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_m_matrix");
	vUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_v_matrix");
	pUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_p_matrix");
	laUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_la");
	ldUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ld");
	lsUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ls");
	kaUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ka");
	kdUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_kd");
	ksUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_ks");
	materialShinynessUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_materialShine");
	lightPositionUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_lightPosition");
	lKeyIsPressedUniformVertex = glGetUniformLocation(gShaderProgramObjectForPerVertex, "u_lKeyIsPressed");

	fprintf(gpFile, "FRAG POSTLINKING");
	mUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_m_matrix");
	vUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_v_matrix");
	pUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_p_matrix");
	laUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_la");
	ldUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ld");
	lsUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ls");
	kaUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ka");
	kdUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_kd");
	ksUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_ks");
	materialShinynessUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_materialShine");
	lightPositionUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_lightPosition");
	lKeyIsPressedUniformFragment = glGetUniformLocation(gShaderProgramObjectForPerFragment, "u_lKeyIsPressed");

	
	//call geomentry code function
	geomentryCode();


	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//ADD CULL FACE
	//glDisable(GL_CULL_FACE);

	//Background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//Give identity toorthographicProjectMatrix
	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);

}


void vertexShaderCodPerVertex(void)
{
	fprintf(gpFile, "I AM IN V PRESSED VERT PER VERT");
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObjectForPerVertex = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \

		"uniform int u_lKeyIsPressed;" \

		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShine;" \
		"uniform vec4 u_lightPosition;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
		"vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix ) * vNormal);" \
		"vec3 lightDirection = normalize(vec3(u_lightPosition - eye_Coordinate));" \
		"float tn_dot_ld = max(dot(lightDirection,tNorm),0.0f);" \
		"vec3 reflectionVector = reflect(-lightDirection , tNorm);" \
		"vec3 viwerVector = normalize(-eye_Coordinate.xyz);" \
		"vec3 ambiant = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector , viwerVector),0.0f),u_materialShine);" \
		"phong_ads_light = ambiant + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0f,1.0f,1.0f);" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObjectForPerVertex, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObjectForPerVertex);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObjectForPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectForPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectForPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}
}

void frgmentShaderCodPerVertex(void)
{
	fprintf(gpFile, "I AM IN V PRESSED FRAG PER VERT");
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObjectForPerVertex = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"uniform int u_lKeyIsPressed;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"FragColor = vec4(phong_ads_light,1.0f);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
		"}" \
		"}"
	};
	//*******************************//"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		
	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObjectForPerVertex, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObjectForPerVertex);

	//Error checking For Fragment Shader Object

	glGetShaderiv(gFragmentShaderObjectForPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectForPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectForPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}
}

void vertexShaderCodPerFragment(void)
{
	
	
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObjectForPerFragment = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec4 u_lightPosition;" \
		"out vec3 tNorm;" \
		"out vec3 lightDirection;" \
		"out vec3 viwerVector;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
		"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" \
		"lightDirection = vec3(u_lightPosition - eye_Coordinate);" \
		"viwerVector = -eye_Coordinate.xyz;" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObjectForPerFragment, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObjectForPerFragment);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObjectForPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectForPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectForPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}
}

void fragmentShaderCodePerFragment(void)
{
	
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObjectForPerFragment = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 lightDirection;" \
		"in vec3 viwerVector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShine;" \
		"uniform vec4 u_lightPosition;" \
		"uniform int u_lKeyIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec3 normalizeTNorm = normalize(tNorm);" \
		"vec3 normalizeLightDirection = normalize(lightDirection);" \
		"vec3 normalizeViwerVector = normalize(viwerVector);" \
		"float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" \
		"vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" \
		"vec3 ambiant = vec3(u_la * u_ka);" \
		"vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" \
		"vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" \
		"vec3 phong_ads_light = ambiant + diffuse + specular;" \
		"FragColor = vec4(phong_ads_light,1.0f);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
		"}" \
		"}"
	};
	//*******************************//"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		
	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObjectForPerFragment, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObjectForPerFragment);

	//Error checking For Fragment Shader Object

	glGetShaderiv(gFragmentShaderObjectForPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectForPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectForPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}
}


void createProgramCodePerVertex(void)
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObjectForPerVertex = glCreateProgram();
	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObjectForPerVertex, gVertexShaderObjectForPerVertex);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObjectForPerVertex, gFragmentShaderObjectForPerVertex);

	//Prelinking binding of vertex shader

	//----- Position
	glBindAttribLocation(gShaderProgramObjectForPerVertex, AMC_ATTRIBUTE_POSITION, "vPosition");
	// ---- Normal
	glBindAttribLocation(gShaderProgramObjectForPerVertex, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObjectForPerVertex);

	//Error Checking For Program Object

	glGetProgramiv(gShaderProgramObjectForPerVertex, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectForPerVertex, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectForPerVertex, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}


void createProgramCodePerFragment(void)
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObjectForPerFragment = glCreateProgram();
	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObjectForPerFragment, gVertexShaderObjectForPerFragment);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObjectForPerFragment, gFragmentShaderObjectForPerFragment);

	//Prelinking binding of vertex shader

	//----- Position
	glBindAttribLocation(gShaderProgramObjectForPerFragment, AMC_ATTRIBUTE_POSITION, "vPosition");
	// ---- Normal
	glBindAttribLocation(gShaderProgramObjectForPerFragment, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObjectForPerFragment);

	//Error Checking For Program Object

	glGetProgramiv(gShaderProgramObjectForPerFragment, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectForPerFragment, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectForPerFragment, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}


void geomentryCode(void)
{

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//****************************** CUBE ****************************
	//Create vao for rect
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//################### POSITION ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and arra for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//################### NORMAL ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);


	//########################## Elements ###############
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_elements_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

	////how many slots my array is break
	//glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	////Enabling the position
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);

}
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.91f,
		100.0f);
}



void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Using program object 

	if (isPerVertex == true)
	{
		
		glUseProgram(gShaderProgramObjectForPerVertex);

		//declerations of matrix
		mat4 viewMatrix;
		mat4 modelMatrix;
		mat4 translationMatrix;
		/*mat4 rotationMatrix;
		mat4 scaleMatrix;*/
		mat4 projectionMatrix;
		//*********************************** SPHERE *****************************
		//Initialize above matrix to identity
		translationMatrix = mat4::identity();
		//rotationMatrix = mat4::identity();
		//scaleMatrix = mat4::identity();
		modelMatrix = mat4::identity();
		viewMatrix = mat4::identity();
		projectionMatrix = mat4::identity();
		//Do neccessary transformation

		translationMatrix = translate(0.0f, 0.0f, -3.0f);
		//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

		//Do neccessary Matrix Multilication
		modelMatrix = translationMatrix;
		//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;
		//Send neccessary matrices to shader in respective to uniforms-
		glUniformMatrix4fv(mUniformVertex, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(vUniformVertex, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(pUniformVertex, 1, GL_FALSE, projectionMatrix);


		if (gbLighting == true)
		{
			glUniform1i(lKeyIsPressedUniformVertex, 1);

			glUniform3fv(laUniformVertex, 1, lightAmbiant);
			glUniform3fv(ldUniformVertex, 1, lightDiffuse);
			glUniform3fv(lsUniformVertex, 1, lightSpecular);

			glUniform3fv(kaUniformVertex, 1, materialAmbiant);
			glUniform3fv(kdUniformVertex, 1, materialDiffuse);
			glUniform3fv(ksUniformVertex, 1, materialSpecular);
			glUniform1f(materialShinynessUniformVertex, materialShinyness);

			glUniform4fv(lightPositionUniformVertex, 1, lightPosition);
		}
		else {
			glUniform1i(lKeyIsPressedUniformVertex, 0);
		}



		//Bind with vao of rectangle
		glBindVertexArray(vao_sphere);

		//Bind texture if any

		//Draw function
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		//glDrawArrays(GL_TRIANGLES, 0, gNumElements);
		//Unbind vao of rectangle
		glBindVertexArray(0);


		//Unused Program
		glUseProgram(0);
	}


	if (isPerFragment == true)
	{
		glUseProgram(gShaderProgramObjectForPerFragment);



		//declerations of matrix
		mat4 viewMatrix;
		mat4 modelMatrix;
		mat4 translationMatrix;
		/*mat4 rotationMatrix;
		mat4 scaleMatrix;*/
		mat4 projectionMatrix;
		//*********************************** SPHERE *****************************
		//Initialize above matrix to identity
		translationMatrix = mat4::identity();
		//rotationMatrix = mat4::identity();
		//scaleMatrix = mat4::identity();
		modelMatrix = mat4::identity();
		viewMatrix = mat4::identity();
		projectionMatrix = mat4::identity();
		//Do neccessary transformation

		translationMatrix = translate(0.0f, 0.0f, -3.0f);
		//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

		//Do neccessary Matrix Multilication
		modelMatrix = translationMatrix;
		//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
		projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;
		//Send neccessary matrices to shader in respective to uniforms-
		glUniformMatrix4fv(mUniformFragment, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(vUniformFragment, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(pUniformFragment, 1, GL_FALSE, projectionMatrix);


		if (gbLighting == true)
		{
			glUniform1i(lKeyIsPressedUniformFragment, 1);

			glUniform3fv(laUniformFragment, 1, lightAmbiant);
			glUniform3fv(ldUniformFragment, 1, lightDiffuse);
			glUniform3fv(lsUniformFragment, 1, lightSpecular);

			glUniform3fv(kaUniformFragment, 1, materialAmbiant);
			glUniform3fv(kdUniformFragment, 1, materialDiffuse);
			glUniform3fv(ksUniformFragment, 1, materialSpecular);
			glUniform1f(materialShinynessUniformFragment, materialShinyness);

			glUniform4fv(lightPositionUniformFragment, 1, lightPosition);
		}
		else {
			glUniform1i(lKeyIsPressedUniformFragment, 0);
		}



		//Bind with vao of rectangle
		glBindVertexArray(vao_sphere);

		//Bind texture if any

		//Draw function
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
		//glDrawArrays(GL_TRIANGLES, 0, gNumElements);
		//Unbind vao of rectangle
		glBindVertexArray(0);


		//Unused Program
		glUseProgram(0);
	}
	SwapBuffers(ghdc);
}

void update(void)
{

	/*angleCube = angleCube + 0.01f;
	if (angleCube >= 360)
	{
		angleCube = 0.0f;
	}*/


}


void uninitialize(void)
{
	if (vbo_elements_sphere)
	{
		glDeleteBuffers(1, &vbo_elements_sphere);
		vbo_elements_sphere = 0;
	}

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}

	if (vbo_position_sphere)
	{
		glDeleteVertexArrays(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}



	if (gShaderProgramObjectForPerVertex)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObjectForPerVertex);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObjectForPerVertex, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObjectForPerVertex, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObjectForPerVertex, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObjectForPerVertex);
		gShaderProgramObjectForPerVertex = 0;
		glUseProgram(0);
	}

	if (gShaderProgramObjectForPerFragment)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObjectForPerFragment);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObjectForPerFragment, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObjectForPerFragment, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObjectForPerFragment, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObjectForPerFragment);
		gShaderProgramObjectForPerFragment = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

#include<Windows.h>
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#include<stdio.h>
#include<gl/GLU.h>
#pragma comment (lib,"glu32.lib")
#include"Tree.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

GLuint texture_Age51;
GLuint texture_Age28;
GLuint texture_Age62;
GLuint texture_Age12;
GLuint texture_Age33;
GLuint texture_Age59;
GLuint texture_Age78;
GLuint texture_Age18;
GLuint texture_Age61;

using namespace std;
typedef struct node
{
	int value;
	node * pLeft;
	node * pRight;
	node(int val = 0)
	{
		value = val;
		pRight = NULL;
		pLeft = NULL;
	}
}node;
bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
bool bLight = false;


GLuint fontOffset;
bool gEdgeEnable = false;
GLint keyPress = 0;




GLfloat lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,0.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,0.0f,1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };
GLUquadric *quadric[9];

GLfloat light_model_ambiant[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat light_model_local_viewer[] = { 0.0f };



//font Rendring
GLubyte space[] = { 0x00,0x00,0x00,0x00 ,0x00,0x00 ,0x00,0x00 ,0x00,0x00 ,0x00,0x00 ,0x00 };

GLubyte letters[][13] = {

	{0xc3,0xc3,0xc3,0xc3 ,0xc3 ,0xc3,0xff,0xc3 ,0xc3 ,0xc3 ,0x66,0x3c ,0x18} ,
	{0x00,0x00,0xfe,0xc7,0xc3,0xc3,0xc7,0xfe,0xc7,0xc3,0xc3,0xc7,0xfe},
	{0x00,0x00,0x7e,0xe7,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0,0xe7,0x7e},
	{0x00,0x00,0xfc,0xce,0xc7,0xc3,0xc3,0xc3,0xc3,0xc3,0xc7,0xce,0xfc},
	{0x00,0x00,0xff,0xc0,0xc0,0xc0,0xc0,0xfc,0xc0,0xc0,0xc0,0xc0,0xff},
	{0x00,0x00,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0,0xfc,0xc0,0xc0,0xc0,0xff},
	{0x00,0x00,0x7e,0xe7,0xc3,0xc3,0xcf,0xc0,0xc0,0xc0,0xc0,0xe7,0x7e},
	{0x00,0x00,0xc3,0xc3,0xc3,0xc3,0xc3,0xff,0xc3,0xc3,0xc3,0xc3,0xc3},
	{0x00,0x00,0x7e,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x7e},
	{0x00,0x00,0x7c,0xee,0xc6,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06},
	{0x00,0x00,0xc3,0xc6,0xcc,0xd8,0xf0,0xe0,0xf0,0xd8,0xcc,0xc6,0xc3},
	{0x00,0x00,0xff,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0,0xc0},
	{0x00,0x00,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3,0xdb,0xff,0xff,0xe7,0xc3},
	{0x00,0x00,0xc7,0xc7,0xcf,0xcf,0xdf,0xdb,0xfb,0xf3,0xf3,0xe3,0xe3},
	{0x00,0x00,0x7e,0xe7,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3,0xe7,0x7e},
	{0x00,0x00,0xc0,0xc0,0xc0,0xc0,0xc0,0xfe,0xc7,0xc3,0xc3,0xc7,0xfe},
	{0x00,0x00,0x3f,0x6e,0xdf,0xdb,0xc3,0xc3,0xc3,0xc3,0xc3,0x66,0x3c},
	{0x00,0x00,0xc3,0xc6,0xcc,0xd8,0xf0,0xfe,0xc7,0xc3,0xc3,0xc7,0xfe},
	{0x00,0x00,0x7e,0xe7,0x03,0x03,0x07,0x7e,0xe0,0xc0,0xc0,0xe7,0x7e},
	{0x00,0x00,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0xff},
	{0x00,0x00,0x7e,0xe7,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3},
	{0x00,0x00,0x18,0xc3,0xc3,0x66,0x66,0xc3,0xc3,0xc3,0xc3,0xc3,0xc3},
	{0x00,0x00,0xc3,0xe7,0xff,0xff,0xdb,0xdb,0xc3,0xc3,0xc3,0xc3,0xc3},
	{0x00,0x00,0xc3,0x66,0x66,0x3c,0x3c,0x18,0x3c,0x3c,0x66,0x66,0xc3},
	{0x00,0x00,0x18,0x18,0x18,0x18,0x18,0x18,0x3c,0x3c,0x66,0x66,0xc3},
	{0x00,0x00,0xff,0xc0,0xc0,0x60,0x30,0x7e,0x0c,0x06,0x03,0x03,0xff},

};

//GLubyte numbers[][13] = {
//	{0x4,0x4,0x4,0x4,0x4,0x4,0x4,0x4,0x4,0x3C,0x3C,0x4,0x4},
//	{0x00,0xF0,0xF0,0xC0,0xC0,0xC0,0xF0,0xF0,0x30,0x30,0x30,0xF0,0XF0},
//	{0xFE,0xFE,0xE,0xE,0xE,0xE,0xFE,0xFE,0xE,0xE,0xE,0xFE,0xFE},
//	{0x00,0xC,0xC,0xC,0xC,0x7C,0x7C,0x7C,0x6C,0x6C,0x6C,0x60,0x60},
//	{0x00,0x3C,0x3C,0xC,0xC,0xC,0x3C,0x3C,0x30,0x30,0x3C,0x3C,0x3C},
//	{0x3F,0x3F,0x33,0x33,0x33,0x33,0x3F,0x3F,0x30,0x30,0x30,0x30,0x30},
//	{0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0x18,0xF8,0xF8},
//	{ 0x7C,0x7C,0x64,0x64,0x64,0x64,0x7C,0x64,0x64,0x64,0x64,0x7C,0x7C},
//	{0x3C,0x3C,0x4,0x4,0x4,0x3C,0x3C,0x24,0x24,0x24,0x3C,0x3C,0x3C}
//};


//function
void update(void);
void drawNodes();
void list();
void printString(char *);
void insert(node ** pRoot, int val);
node * getBST(GLuint * arr, int size);

//functions for edges
void E1();
void E2();
void E3();
void E4();
void E5();
void E6();
void E7();
void E8();
//
//initialize
GLuint arr[] = { 51,28,62,12,33,59,78,18,61 };
node * pRoot = getBST(arr, sizeof(arr) / sizeof(int));

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		/*case 0X46:
			ToggleFullScreen();
			break;
		*/}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();

			break;
		case 'E':
		case 'e':
			gEdgeEnable = true;
			keyPress = 1;
			break;
		case 'L':
		case 'l':
			if (bLight == false)
			{
				bLight = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);
			}
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	void resize(int, int);
	void makeRasterFont(void);
	//void ToggleFullScreen();

	BOOL loadTexture(GLuint *, TCHAR[]);


	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	
	
	glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambiant);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);
	glEnable(GL_LIGHT0);
	
	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glEnable(GL_TEXTURE_GEN_S);
	glEnable(GL_TEXTURE_GEN_T);

	glEnable(GL_TEXTURE_2D);
	loadTexture(&texture_Age51, MAKEINTRESOURCE(IDBITMAP_AGEFIFTYONE));
	loadTexture(&texture_Age28, MAKEINTRESOURCE(IDBITMAP_AGETWENTEIGHT));
	loadTexture(&texture_Age62, MAKEINTRESOURCE(IDBITMAP_AGESIXTYTWO));
	loadTexture(&texture_Age12, MAKEINTRESOURCE(IDBITMAP_AGETWELVE));
	loadTexture(&texture_Age33, MAKEINTRESOURCE(IDBITMAP_AGETHIRTYTHREE));
	loadTexture(&texture_Age59, MAKEINTRESOURCE(IDBITMAP_AGEFIFTYNINE));
	loadTexture(&texture_Age78, MAKEINTRESOURCE(IDBITMAP_AGESEVENTYEIGHT));
	loadTexture(&texture_Age18, MAKEINTRESOURCE(IDBITMAP_AGEEIGHTEEN));
	loadTexture(&texture_Age61, MAKEINTRESOURCE(IDBITMAP_AGESIXTYONE));

	makeRasterFont();
	//ToggleFullScreen();

	resize(WIN_WIDTH, WIN_HEIGHT);

	for (int i = 0; i < 9; i++)
	{
		quadric[i] = gluNewQuadric();
	}
	return(0);

	}

BOOL loadTexture(GLuint *texture, TCHAR imgResourceID[])
{
	HBITMAP hBitmap = NULL;
	BITMAP bmp;

	BOOL bStatus = FALSE;
	//code
	hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imgResourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		bStatus = TRUE;
		GetObject(hBitmap, sizeof(BITMAP), &bmp);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		DeleteObject(hBitmap);
	}
	return(bStatus);
}

void insert(node ** pRoot, int val)
{
	if (*pRoot == NULL)
		*pRoot = new node(val);
	else if
		((*pRoot)->value <= val){
			insert(&((*pRoot)->pRight), val);
			if (gEdgeEnable == true)
			{
				E2();
				E5();
				E6();
				E8();
			}
	}
	else if ((*pRoot)->value > val){
		insert(&((*pRoot)->pLeft), val);
		if (gEdgeEnable == true)
		{

			E1();
			E3();
			E4();
			E7();
		}
	}
}


//for root
node * getBST(GLuint * arr, int size)
{
	node * pRoot = NULL;
	for (int i = 0; i < size; i++)
	{
		insert(&pRoot, arr[i]);
	}
	return pRoot;
}
//struct treeNode *insert(struct treeNode *t)
//{
//	int i = 1;
//	if (quadric[0])
//	{
//		drawNodes();
//	}
//	else
//	{
//
//		if (quadric[i]<quadric[i-1])
//		{
//			t->left = insert(t->left);
//			drawNodes();
//		}
//		else if (quadric[i]>quadric[i-1])
//		{
//			t->right = insert(t->right);
//			drawNodes();
//		}
//	}
//	return(t);
//}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

}

char str1[] = "FIFTY ONE ";
char str2[] = "TWENTY EIGHT";
char str3[] = "SIXTY TWO";
char str4[] = "TWELVE";
char str5[] = "THIRTY THREE";
char str6[] = "FIFTY NINE";
char str7[] = "SEVENTY EIGHT";
char str8[] = "EIGHTEEN";
char str9[] = "SIXTY ONE";
char str10[] = "THE AGES OF MEMBERS";
char str11[] = "PRESS E TO TREE CONNECTION";
void list()
{
	glColor3f(0.0f,0.0f,0.0f);

	glRasterPos2f(-7.0f, 5.0f);
	printString(str1);

	glRasterPos2f(-6.5f, 4.6f);
	printString(str2);

	glRasterPos2f(-7.0f, 4.2f);
	printString(str3);

	glRasterPos2f(-6.5f, 3.8f);
	printString(str4);

	glRasterPos2f(-7.0f, 3.4f);
	printString(str5);

	glRasterPos2f(-6.5f, 3.0f);
	printString(str6);

	glRasterPos2f(-7.0f, 2.6f);
	printString(str7);

	glRasterPos2f(-6.5f, 2.2f);
	printString(str8);

	glRasterPos2f(-7.0f, 1.8f);
	printString(str9);

	glRasterPos2f(-7.5f, 5.5f);
	printString(str10);


	glRasterPos2f(0.5f, 5.5f);
	printString(str11);

}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	drawNodes();

	list();
	
	glColor3f(0.0f,0.0f,0.0f);
	
	glRasterPos2f(0.3f, 4.0f);
	printString(str1);

	glRasterPos2f(-2.6f, 2.5f);
	printString(str2);

	glRasterPos2f(2.6f,2.5f);
	printString(str3);

	glRasterPos2f(-3.0f, 0.5f);
	printString(str4);

	glRasterPos2f(-0.8f, -0.8f);
	printString(str5);
	
	glRasterPos2f(0.6f, 0.5f);
	printString(str6);

	glRasterPos2f(4.2f, 0.5f);
	printString(str7);

	glRasterPos2f(-3.2f, -1.8f);
	printString(str8);

	glRasterPos2f(3.0f, -1.8f);
	printString(str9);


	if (keyPress == 1)
	{
		 E1();
	}
	SwapBuffers(ghdc);
}


void drawNodes()
{
	
	glColor3f(1.0f, 1.0f, 0.0f);
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 2.5f, -12.0f);
	glBindTexture(GL_TEXTURE_2D, texture_Age51);
	gluSphere(quadric[0], 0.4f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-2.0f, 1.0f, -12.0f);
	glBindTexture(GL_TEXTURE_2D, texture_Age28);
	gluSphere(quadric[1], 0.4f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(2.0f, 1.0f, -12.0f);
	glBindTexture(GL_TEXTURE_2D, texture_Age62);
	gluSphere(quadric[2], 0.4f, 30, 30);
	glPopMatrix();
	
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-3.0f, -1.0f, -12.0f);
	glBindTexture(GL_TEXTURE_2D, texture_Age12);
	gluSphere(quadric[3], 0.4f, 30, 30);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D,texture_Age33);
	glTranslatef(-1.0f, -1.0f, -12.0f);
	gluSphere(quadric[4], 0.4f, 30, 30);
	
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.0f, -1.0f, -12.0f);
	glBindTexture(GL_TEXTURE_2D, texture_Age59);
	gluSphere(quadric[5], 0.4f, 30, 30);
	glPopMatrix();


	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D, texture_Age78);
	glTranslatef(3.0f, -1.0f, -12.0f);
	gluSphere(quadric[6], 0.4f, 30, 30);
	glPopMatrix();
	
	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D, texture_Age18);
	glTranslatef(-2.5f, -2.5f, -12.0f);
	gluSphere(quadric[7], 0.4f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glBindTexture(GL_TEXTURE_2D, texture_Age61);
	glTranslatef(1.5f, -2.5f, -12.0f);
	gluSphere(quadric[8], 0.4f, 30, 30);
	glPopMatrix();

}
void makeRasterFont(void)
{
	GLuint i, j;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	fontOffset = glGenLists(128);

	for (i = 0, j = 'A'; i < 27; i++, j++)
	{
		glNewList(fontOffset + j, GL_COMPILE);
		glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, letters[i]);
		glEndList();
	}

	/*for (i = 0, j = '1'; i< 10; i++, j++) {
	glNewList(fontOffset + j, GL_COMPILE);
	glBitmap(8, 12, 0.0, 2.1, 10.0, 0.0, numbers[i]);
	glEndList();
	}*/
	glNewList(fontOffset + ' ', GL_COMPILE);
	glBitmap(8, 13, 0.0, 2.0, 10.0, 0.0, space);
	glEndList();

}
void printString(char *s)
{
	glPushAttrib(GL_LIST_BIT);
	glListBase(fontOffset);

	glCallLists(strlen(s), GL_UNSIGNED_BYTE, (GLbyte *)s);
	glPopAttrib();
}
GLfloat xE1LeftEdge = 0.0f;
GLfloat yE1LeftEdge = 2.5f;

void E1()
{
	
	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(0.0f, 2.5f);
	glVertex2f(xE1LeftEdge, yE1LeftEdge);
	glEnd();

	xE1LeftEdge = xE1LeftEdge - 0.00132f;
	if (xE1LeftEdge <= -2.0f)
	{
		xE1LeftEdge = -2.0f;
	}
	yE1LeftEdge = yE1LeftEdge - 0.001f;
	if (yE1LeftEdge <= 1.0f)
	{
		yE1LeftEdge = 1.0f;
	}
	if (xE1LeftEdge == -2.0f) {
		E2();
	}
}
GLfloat xE2RightEdge = 0.0f;
GLfloat yE2RightEdge = 2.5f;

void E2()
{
	
	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(0.0f, 2.5f);
	glVertex2f(xE2RightEdge, yE2RightEdge);
	glEnd();

	xE2RightEdge = xE2RightEdge + 0.00132f;
	if (xE2RightEdge >= 2.0f)
	{
		xE2RightEdge = 2.0f;
	}
	yE2RightEdge = yE2RightEdge - 0.001f;
	if (yE2RightEdge <= 1.0f)
	{
		yE2RightEdge = 1.0f;
	}
	if (xE2RightEdge == 2.0f) {
		E3();
	}
}
GLfloat xE3LeftEdge = -2.0f;
GLfloat yE3LeftEdge = 1.0f;

void E3()
{

	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(-2.0f, 1.0f);
	glVertex2f(xE3LeftEdge, yE3LeftEdge);
	glEnd();

	xE3LeftEdge = xE3LeftEdge - 0.001f;
	if (xE3LeftEdge <= -3.0f)
	{
		xE3LeftEdge = -3.0f;
	}
	yE3LeftEdge = yE3LeftEdge - 0.002f;
	if (yE3LeftEdge <= -1.0f)
	{
		yE3LeftEdge = -1.0f;
	}
	if (xE3LeftEdge == -3.0f) {
		E4();
	}
}

GLfloat xE4LeftEdge = -2.0f;
GLfloat yE4LeftEdge = 1.0f;

void E4()
{

	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(-2.0f, 1.0f);
	glVertex2f(xE4LeftEdge, yE4LeftEdge);
	glEnd();

	xE4LeftEdge = xE4LeftEdge + 0.001f;
	if (xE4LeftEdge >= -1.0f)
	{
		xE4LeftEdge = -1.0f;
	}
	yE4LeftEdge = yE4LeftEdge - 0.002f;
	if (yE4LeftEdge <= -1.0f)
	{
		yE4LeftEdge = -1.0f;
	}
	if (xE4LeftEdge == -1.0f) {
		E5();
	}
}

GLfloat xE5RightEdge = 2.0f;
GLfloat yE5RightEdge = 1.0f;

void E5()
{

	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(2.0f, 1.0f);
	glVertex2f(xE5RightEdge, yE5RightEdge);
	glEnd();

	xE5RightEdge = xE5RightEdge - 0.001f;
	if (xE5RightEdge <= 1.0f)
	{
		xE5RightEdge = 1.0f;
	}
	yE5RightEdge = yE5RightEdge - 0.002f;
	if (yE5RightEdge <= -1.0f)
	{
		yE5RightEdge = -1.0f;
	}
	if (xE5RightEdge == 1.0f) {
		E6();
	}
}

GLfloat xE6RightEdge = 2.0f;
GLfloat yE6RightEdge = 1.0f;

void E6()
{

	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(2.0f, 1.0f);
	glVertex2f(xE6RightEdge, yE6RightEdge);
	glEnd();

	xE6RightEdge = xE6RightEdge + 0.001f;
	if (xE6RightEdge >= 3.0f)
	{
		xE6RightEdge = 3.0f;
	}
	yE6RightEdge = yE6RightEdge - 0.002f;
	if (yE6RightEdge <= -1.0f)
	{
		yE6RightEdge = -1.0f;
	}
	if (xE6RightEdge == 3.0f) {
		E7();
	}
}


GLfloat xE7LeftEdge = -3.2f;
GLfloat yE7LeftEdge = -1.0f;

void E7()
{

	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(-3.2f, -1.0f);
	glVertex2f(xE7LeftEdge, yE7LeftEdge);
	glEnd();

	xE7LeftEdge = xE7LeftEdge + 0.004f;
	if (xE7LeftEdge >= -2.5f)
	{
		xE7LeftEdge = -2.5f;
	}
	yE7LeftEdge = yE7LeftEdge - 0.009f;
	if (yE7LeftEdge <= -2.5f)
	{
		yE7LeftEdge = -2.5f;
	}
	if (xE7LeftEdge == -2.5f) {
		E8();
	}
}
GLfloat xE8RightEdge = 0.8f;
GLfloat yE8RightEdge = -1.0f;

void E8()
{

	glLoadIdentity();
	glLineWidth(5.0f);
	glTranslatef(0.0f, 0.0f, -12.0f);

	glBegin(GL_LINES);
	glVertex2f(0.8f, -1.0f);
	glVertex2f(xE8RightEdge, yE8RightEdge);
	glEnd();

	xE8RightEdge = xE8RightEdge + 0.004f;
	if (xE8RightEdge >= 1.5f)
	{
		xE8RightEdge = 1.5f;
	}
	yE8RightEdge = yE8RightEdge - 0.009f;
	if (yE8RightEdge <= -2.5f)
	{
		yE8RightEdge = -2.5f;
	}
}
//void printNumber(GLuint *i)
//{
//	/*static int count = 0;
//	GLuint oldNum[10];
//	oldNum[count-1] = i[count];
//	*/
//	glPushAttrib(GL_LIST_BIT);
//	glListBase(fontOffset);
//	//while (i[count] != 0)
//	//{
//	//	// n = n/10
//	//	i[count] /= 10;
//	//	++count;
//	//}
//	glCallLists(strlen(i), GL_UNSIGNED_INT, (GLuint*)oldNum);
//	glPopAttrib();
//}
void update(void)
{
	/*angleOfXRotation = angleOfXRotation + 0.08f;
	if (angleOfXRotation >= 360.0f)
	{
		angleOfXRotation = 0.0f;
	}
	angleOfYRotation = angleOfYRotation + 0.08f;
	if (angleOfYRotation >= 360.0f)
	{
		angleOfYRotation = 0.0f;
	}
	angleOfZRotation = angleOfZRotation + 0.08f;
	if (angleOfZRotation >= 360.0f)
	{
		angleOfZRotation = 0.0f;
	}*/

}


void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
	for (int i = 0; i < 9; i++)
	{
		gluDeleteQuadric(quadric[i]);

	}
	
	glDisable(GL_TEXTURE_GEN_S);

	glDisable(GL_TEXTURE_GEN_T);

}

void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}
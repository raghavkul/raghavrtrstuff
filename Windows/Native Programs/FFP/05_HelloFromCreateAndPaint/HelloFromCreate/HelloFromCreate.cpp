#include<Windows.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG Msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindow(szAppName,
		TEXT("My Window-Raghvendra"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ShowWindow(hWnd, iCmdShow);
	UpdateWindow(hWnd);

	while (GetMessage(&Msg, NULL, 0, 0))
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return((int)Msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HFONT hFont;

	HDC hdc;
	RECT rs;
	switch (iMsg)
	{
	case WM_CREATE:
		GetClientRect(hWnd, &rs);
		hdc = GetDC(hWnd);
		hFont = CreateFont(66, 20, 0, 0, FW_REGULAR, FALSE, TRUE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
			CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, TEXT("Arials"));
		SetTextColor(hdc, RGB(0, 0, 255));
		SetBkColor(hdc, RGB(0, 0, 0));
		SelectObject(hdc, hFont);
		TextOut(hdc, 360, 360, TEXT("Hello OpenGL"), 15);
		ReleaseDC(hWnd, hdc);


		break;

	/*case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		GetClientRect(hWnd, &rs);

		hFont = CreateFont(66, 20, 0, 0, FW_REGULAR, FALSE, TRUE, FALSE, DEFAULT_CHARSET, OUT_OUTLINE_PRECIS,
			CLIP_DEFAULT_PRECIS, ANTIALIASED_QUALITY, VARIABLE_PITCH, TEXT("Arials"));
		SetTextColor(hdc, RGB(0, 0, 255));
		SetBkColor(hdc, RGB(0, 0, 0));
		SelectObject(hdc, hFont);
		TextOut(hdc, 360, 360, TEXT("Hello OpenGL"), 15);
		EndPaint(hWnd, &ps);

		break;*/
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}



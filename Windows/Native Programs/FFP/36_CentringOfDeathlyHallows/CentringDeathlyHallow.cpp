#include<Windows.h>
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#include<stdio.h>
#include<gl/GLU.h>
#pragma comment (lib,"glu32.lib")
#define _USE_MATH_DEFINES 1
#include<math.h>

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0X46:

			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;


	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}


	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);

}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

}

void display(void)
{
	GLfloat x1, y1, x2, y2, x3, y3;
	GLfloat dist_a = 0.0f;
	GLfloat dist_b = 0.0f;
	GLfloat dist_c = 0.0f;
	static GLfloat k1 = -4.0f;
	static GLfloat k2 = 4.0f;
	static GLfloat k3 = 3.0f;

	GLfloat perimeter = 0.0f;

	GLfloat areaOfTriangle = 0.0f;

	GLfloat radious = 0.0f;

	GLfloat angle = 0.0f;
	static GLfloat rot = 0.0f;
	static GLfloat rot1 = 0.0f;
	static GLfloat rot2 = 0.0f;

	GLfloat xCentroid = 0.0f, yCentroid = 0.0f;
	
	x1 = 0.0f;
	y1 = 1.0f;
	x2 = -1.0f;
	y2 = -1.0f;
	x3 = 1.0f;
	y3 = -1.0f;
	
		glClear(GL_COLOR_BUFFER_BIT);
				
					// For Triangle
					glMatrixMode(GL_MODELVIEW);
					glLoadIdentity(); 
					glTranslatef(k1, 0.0f, -6.0f);
					glColor3f(1.0f, 0.5f, 0.001f);
					glRotatef(rot, 0.0f, 1.0f, 0.0f);
										
							glBegin(GL_LINES);
							glVertex2f(x1, y1);
							glVertex2f(x2 , y2);

							glVertex2f(x2 , y2);
							glVertex2f(x3 , y3);

							glVertex2f(x3 , y3);
							glVertex2f(x1 , y1);

							glEnd();
							if ((k1 < 0.0f) || (k1 == 0.0f))
							{
								k1 = k1 + 0.001f;
								
							}
							rot = rot + 0.1f;

					// For Line	
					glMatrixMode(GL_MODELVIEW);
					glLoadIdentity();
					glTranslatef(0.0f, k3, -6.0f);
					glColor3f(1.0f, 1.0f, 1.0f);

					glRotatef(rot1, 0.0f, 1.0f, 0.0f);
				
					glBegin(GL_LINES);

					glVertex2f(0.0f, 1.0f);
					glVertex2f(0.0f, -1.0f);

					glEnd();
					if ((k3 > 0.0f) || (k3 == 0.0f))
					{
						k3 = k3 - 0.00075f;

					}
					rot1 = rot1 + 0.1f;
	

		dist_a = sqrtf(((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));
		dist_b = sqrtf(((x3 - x2)*(x3 - x2)) + ((y3 - y2)*(y3 - y2)));
		dist_c = sqrtf(((x3 - x1)*(x3 - x1)) + ((y3 - y1)*(y3 - y1)));

		perimeter = ((dist_a + dist_b + dist_c) / 2);

		areaOfTriangle = sqrtf(perimeter*(perimeter - dist_a)*(perimeter - dist_b)*(perimeter - dist_c));

		radious = (areaOfTriangle / perimeter);

		//For Circle
	
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(k2, 0.0f, -6.0f);
		glColor3f(0.0f, 0.9f, 0.0f);

		glRotatef(rot2, 0.0f, 1.0f, 0.0f);

		xCentroid = (((dist_b*x1) + (dist_c*x2) + (dist_a*x3)) / (2 * perimeter));
		yCentroid = (((dist_b*y1) + (dist_c*y2) + (dist_a*y3)) / (2 * perimeter));

		glBegin(GL_POINTS);

		for (GLfloat angle = 0.0f; angle < 2.0f*M_PI; angle += 0.001f)
		{
			glVertex2f(xCentroid + cosf(angle)*radious, yCentroid + sinf(angle)*radious);

		}
		glEnd();
		
		if ((k2 > 0.0f) || (k2 == 0.0f))
		{
			k2 = k2 - 0.001f;

		}
		rot2 = rot2 + 0.1f;

	SwapBuffers(ghdc);

}




void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}
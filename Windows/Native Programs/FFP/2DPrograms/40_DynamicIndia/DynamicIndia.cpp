#include<Windows.h>
#include"dynamicIndia.h"
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#include<stdio.h>
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<Mmsystem.h>
#pragma comment (lib,"glu32.lib")
#pragma comment (lib,"Winmm.lib")

#define WIN_WIDTH 1280
#define WIN_HEIGHT 720

bool gbFullScreen = true;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;


//Functions
void leftI();
void N();
void D();
void rightI();
void A();
void topPlane();
void middlePlane();
void bottomPlane();
void WholeFlag();
void AFlag();
//static flags
static bool ifLI = false;
static bool ifA = false;
static bool ifN = false;
static bool ifRI = false;
static bool ifD = false;
static bool movingPlane = false;
static bool Airostopper = false;
static bool splitter = false;


LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("OGL_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		1280,
		720,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	switch (iMsg)
	{
	
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	void resize(int, int);
	void ToggleFullScreen();

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;


	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}


	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	ToggleFullScreen();

	PlaySoundW(MAKEINTRESOURCE(MY_SONG), NULL, SND_ASYNC | SND_FILENAME | SND_RESOURCE);
	return(0);

}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

}



void display(void)
{
	
	//static GLfloat AiroAngle = 90.0f, rx = 0.0f, ry = 0.0f, rz = 1.0f;



	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	if (ifLI == false)
	{
		leftI();
			}
	if (ifA == true)
	{
		A();
	}
	if (ifN == true)
	{
		N();
	}
	 if (ifRI == true)
	{
		rightI();
	}
	if (ifD == true)
	{
		D();
	}
	 if (movingPlane == true)
	{
		
		topPlane();
		middlePlane();
		bottomPlane();
	}
	 WholeFlag();

	SwapBuffers(ghdc);
}

void leftI()
{
	static GLfloat Litx = -3.0f, Lity = -0.7f, Litz = -8.0f;

	////Left Leter I
	glLineWidth(5.0f);

	glLoadIdentity();
	glTranslatef(Litx, Lity, Litz);

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(-2.5f, 1.5f);
	glVertex2f(-3.5f, 1.5f);

	glVertex2f(-3.0f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-3.0f, 0.0f);

	glVertex2f(-2.5f, 0.0f);
	glVertex2f(-3.5f, 0.0f);

	glEnd();
	if ((Litx < 0.0f) || (Litx == 0.0f))
	{
		Litx = Litx + 0.0009f;
	}
	if (Litx >= 0.0f)
	{
		A();
	}
}
void N()
{
	static GLfloat ntx = 0.0f, nty = 4.0f, ntz = -8.0f;
	glLineWidth(5.0f);

	// Letter N
	glLoadIdentity();
	glTranslatef(ntx, nty, ntz);

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-2.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);

	glEnd();
	if ((nty >= -0.7f) || (nty == -0.7f))
	{
		nty = nty - 0.002f;

	}
	if (nty <= -0.7f)
	{
		rightI();
	}
}
	

void D()
{
	static GLfloat dtx = 0.0f, dty = -0.7f, dtz = -8.0f;
	static GLfloat i = 0.0f, j = 0.0f, k = 0.0f, l = 0.0f, m = 0.0f, n = 0.0f;  //For D

	glLineWidth(5.0f);

	// Letter D

	glLoadIdentity();
	glTranslatef(dtx, dty, dtz);

	glBegin(GL_LINES);

	glColor3f(i, j, k);
	glVertex2f(-0.5f, 1.5f);

	glColor3f(l, m, n);
	glVertex2f(-0.5f, 0.0f);


	glColor3f(i, j, k);
	glVertex2f(-0.5f, 1.5f);
	glVertex2f(0.5f, 1.2f);


	glColor3f(i, j, k);
	glVertex2f(0.5f, 1.2f);

	glColor3f(l, m, n);
	glVertex2f(0.5f, 0.3f);

	glColor3f(l, m, n);
	glVertex2f(-0.5f, 0.0f);
	glVertex2f(0.5f, 0.3f);

	glEnd();
	if (i <= 1.0f && j < 0.6f && k < 0.2f && l< 0.07f && m < 0.533f && n < 0.027f)
	{
		i = i + 0.0001f;
		j = j + 0.00006f;
		k = k + 0.00002f;

		l = l + 0.000007f;
		m = m + 0.0000533f;
		n = n + 0.0000027f;
	}
	if (n >= 0.027f)
	{
		topPlane();
		middlePlane();
		bottomPlane();
	}
	
	
}
void rightI()
{
	static GLfloat Ritx = 0.0f, Rity = -6.0f, Ritz = -8.0f;
	glLineWidth(5.0f);

	//Right Letter I

	glLoadIdentity();
	glTranslatef(Ritx, Rity, Ritz);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(1.0f, 1.5f);
	glVertex2f(2.0f, 1.5f);

	glVertex2f(1.5f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(1.5f, 0.0f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(2.0f, 0.0f);
	glEnd();
	 if ((Rity <= -0.7f) || (Rity == -0.7f))
	{
		Rity = Rity + 0.002f;

	}
	 if (Rity >= -0.7f)
	 {
		 D();
	 }
	


}
void A()
{
	static GLfloat x = 0.0f, y = 0.0f, z = 0.0f, a = 0.0f, b = 0.0f, c = 0.0f, d = 0.0f, e = 0.0f, f = 0.0f; //For  middle color of A
	
	static GLfloat atx = 4.0f, aty = -0.7f, atz = -8.0f, aty1 = 0.01f;
	
	glLineWidth(5.0f);

	//Letter A

	glLoadIdentity();
	glTranslatef(atx, aty, atz);

	glBegin(GL_LINES);

	glColor3f(0.07f, 0.533f, 0.027f);

	glVertex2f(2.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(3.0f, 1.5f);

	glVertex2f(3.0f, 1.5f);
	//glVertex2f(0.95f, 0.3f);

	//glVertex2f(0.95f, 0.3f);
	//glVertex2f(1.05f, 0.18f);

	//glVertex2f(1.05f, 0.18f);

	glColor3f(0.07f, 0.533f, 0.027f);

	glVertex2f(3.5f, 0.0f);


	glEnd();
	//middle flag of A

	 if ((atx > 0.0) || (atx == 0.0f))
	{
		atx = atx - 0.0009f;

	}
	 if (atx <= 0.0f)
	 {
		 N();
	 }
	 


}
void topPlane()
{
	static GLfloat t2x = -18.0f, t2y = 6.0f, t2z = -22.0f, Airoangale2 = -90.0f, r2x = 0.0f, r2y = 0.0f, r2z = 1.0f;
	static bool  splitter = false;

	//top plane
	glColor3f(0.7265f, 0.8828f, 0.9296f);

	glLoadIdentity();
	glTranslatef(t2x, t2y, t2z);
	glRotatef(Airoangale2, r2x, r2y, r2z);
	glLineWidth(3.0f);
	glBegin(GL_QUADS);

	glVertex2f(0.4f, 0.2f);
	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-0.6f, -0.2f);
	glVertex2f(0.4f, -0.2f);

	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-1.0f, 0.4f);
	glVertex2f(-1.0f, -0.4f);
	glVertex2f(-0.6f, -0.2f);

	glVertex2f(0.2f, 0.2f);
	glVertex2f(-0.3f, 1.0f);
	glVertex2f(-0.3f, -1.0f);
	glVertex2f(0.2f, -0.2f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(0.4f, 0.2f);
	glVertex2f(0.4f, -0.2f);
	glVertex2f(1.0f, 0.0f);

	glEnd();
	//IAF for Top Plane

	glLoadIdentity();
	glTranslatef(t2x, t2y, t2z);
	glRotatef(Airoangale2, r2x, r2y, r2z);
	

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.3f, 0.0f);
	glVertex2f(0.1f, 0.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(-0.1f, 0.05f);
	glVertex2f(-0.1f, -0.05f);

	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.5f, 0.1f);
	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.3f, -0.1f);
	glVertex2f(-0.4f, 0.1f);
	glVertex2f(-0.4f, -0.05f);



	glEnd();
	//Back Flag Of Top plane
	glLoadIdentity();
	glTranslatef(t2x, t2y, t2z);
	glRotatef(Airoangale2, r2x, r2y, r2z);

	glBegin(GL_LINES);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.05f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 0.05f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(-1.0f, 0.0f);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, -0.05f);
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, -0.05f);
	glEnd();
	if (splitter == false)
	{

		Airoangale2 = Airoangale2 + 0.014f;
		if (Airoangale2 >= 0.0f)
		{
			Airoangale2 = 0.0f;

		}

	}
	
	if (splitter == false)
	{

		t2y = t2y - 0.0009f;
		if (t2y <= 0.0f)
		{
			t2y = 0.0f;
			if (t2y <= 0.0f)
			{
				splitter = true;
			}
		}

	}

	t2x = t2x + 0.0009f;
	if (t2x >= 18.0f)
	{
		t2x = 18.0f;
	}

	if (t2x >= 11.0f)
	{
		Airoangale2 = Airoangale2 + 0.013f;
		if (Airoangale2 >= 90.0f)
		{
			Airoangale2 = 90.0f;
		}
	}


	if (t2x >= 11.0f)
	{
		t2y = t2y + 0.0009f;
		if (t2y >= 7.0f)
		{
			t2y = 7.0f;
		}
	}
	
}

void middlePlane()
{
	static GLfloat t1x = -18.0f, t1y = 0.0f, t1z = -22.0f, Airoangale1, r1x = 0.0f, r1y = 0.0f, r1z = 1.0f;
	//middle plane
//glColor3f(1.0f, 1.0f, 1.0f);
	glColor3f(0.7265f, 0.8828f, 0.9296f);

	glLoadIdentity();
	glTranslatef(t1x, t1y, t1z);

	glLineWidth(3.0f);
	glBegin(GL_QUADS);

	glVertex2f(0.4f, 0.2f);
	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-0.6f, -0.2f);
	glVertex2f(0.4f, -0.2f);

	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-1.0f, 0.4f);
	glVertex2f(-1.0f, -0.4f);
	glVertex2f(-0.6f, -0.2f);

	glVertex2f(0.2f, 0.2f);
	glVertex2f(-0.3f, 1.0f);
	glVertex2f(-0.3f, -1.0f);
	glVertex2f(0.2f, -0.2f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(0.4f, 0.2f);
	glVertex2f(0.4f, -0.2f);
	glVertex2f(1.0f, 0.0f);

	glEnd();
	//IAF for Middle Plane
	glLoadIdentity();
	glTranslatef(t1x, t1y, t1z);

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.3f, 0.0f);
	glVertex2f(0.1f, 0.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(-0.1f, 0.05f);
	glVertex2f(-0.1f, -0.05f);

	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.5f, 0.1f);
	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.3f, -0.1f);
	glVertex2f(-0.4f, 0.1f);
	glVertex2f(-0.4f, -0.05f);



	glEnd();

	//Back Flag Of Middle plane
	glLoadIdentity();
	glTranslatef(t1x, t1y, t1z);
	glBegin(GL_LINES);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.05f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 0.05f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(-1.0f, 0.0f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, -0.05f);
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, -0.05f);


	glEnd();
	t1x = t1x + 0.0009f;
	if (t1x >= 22.0f)
	{
		t1x = 22.0f;
	}
	if (t1x >= 15.0f)
	{
		Airostopper = true ;
	//	AFlag();
	}
}
void bottomPlane()
{
	static GLfloat t3x = -18.0f, t3y = -6.0f, t3z = -22.0f, Airoangale3 = 90.0f, r3x = 0.0f, r3y = 0.0f, r3z = 1.0f;
	static bool Airostopper = false;
	static bool splitter = false;

	//bottom plane
	glColor3f(0.7265f, 0.8828f, 0.9296f);

	glLoadIdentity();
	glTranslatef(t3x, t3y, t3z);
	glRotatef(Airoangale3, r3x, r3y, r3z);
	glLineWidth(3.0f);
	glBegin(GL_QUADS);

	glVertex2f(0.4f, 0.2f);
	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-0.6f, -0.2f);
	glVertex2f(0.4f, -0.2f);

	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-1.0f, 0.4f);
	glVertex2f(-1.0f, -0.4f);
	glVertex2f(-0.6f, -0.2f);

	glVertex2f(0.2f, 0.2f);
	glVertex2f(-0.3f, 1.0f);
	glVertex2f(-0.3f, -1.0f);
	glVertex2f(0.2f, -0.2f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(0.4f, 0.2f);
	glVertex2f(0.4f, -0.2f);
	glVertex2f(1.0f, 0.0f);

	glEnd();
	//IAF for Bottom Plane
	glLoadIdentity();
	glTranslatef(t3x, t3y, t3z);
	glRotatef(Airoangale3, r3x, r3y, r3z);

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.3f, 0.0f);
	glVertex2f(0.1f, 0.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(-0.1f, 0.05f);
	glVertex2f(-0.1f, -0.05f);

	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.5f, 0.1f);
	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.3f, -0.1f);
	glVertex2f(-0.4f, 0.1f);
	glVertex2f(-0.4f, -0.05f);



	glEnd();

	//Back Flag Of Bottom plane
	glLoadIdentity();
	glTranslatef(t3x, t3y, t3z);
	glRotatef(Airoangale3, r3x, r3y, r3z);

	glBegin(GL_LINES);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.05f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 0.05f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(-1.0f, 0.0f);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, -0.05f);
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, -0.05f);


	glEnd();

	
	if (splitter == false)
	{

		Airoangale3 = Airoangale3 - 0.014f;
		if (Airoangale3 <= 0.0f)
		{
			Airoangale3 = 0.0f;

		}
	}
	

	if (splitter == false)
	{


		t3y = t3y + 0.0009f;
		if (t3y >= 0.0f)
		{
			t3y = 0.0f;
			if (t3y >= 0.0f)
			{
				splitter = true;
			}
		}
	}


	t3x = t3x + 0.0009f;
	if (t3x >= 18.0f)
	{
		t3x = 18.0f;
	}


	if (t3x >= 11.0f)
	{
		Airoangale3 = Airoangale3 - 0.013f;
		if (Airoangale3 <= -90.0f)
		{
			Airoangale3 = -90.0f;
		}
	}

	if (t3x >= 11.0f)
	{
		t3y = t3y - 0.0009f;
		if (t3y <= -7.0f)
		{
			t3y = -7.0f;
		}
	}
}

void WholeFlag()
{
	static GLfloat x = 1.0f, y = 0.6f, z = 0.2f, a = 1.0f, b = 1.0f, c = 1.0f, d = 0.07f, e = 0.533f, f = 0.027f; //For  middle color flag
	static GLfloat t = -11.0f;
	static GLfloat tx = -4.0f,ty=0.0f, tz=-9.0f;
	static GLfloat p = -1.0f, q = 0.03f, p1 = -1.0f, q1 = 0.0f, p2 = -1.0f, q2 = -0.03f;
	static GLfloat atx = 4.0f, aty = -0.7f, atz = -8.0f, aty1 = 0.01f;

	glLoadIdentity();
	glTranslatef(tx, ty, tz);
	glLineWidth(2.5f);

	glBegin(GL_LINES);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.03f);

	glColor3f(x, y, z);
	glVertex2f(p, q);//-2.0, 0.025
	//glVertex2f(2.77f, 0.025f);
	//glVertex2f(3.23f, 0.025f);

	//glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);

	glColor3f(a, b, c);
	glVertex2f(p1, q1);//-2.0 , 0.0

	//glVertex2f(2.76f, 0.0f);
	//glVertex2f(3.24f, 0.0f);

	//glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, -0.03f);

	glColor3f(d, e, f);
	glVertex2f(p2, q2);//-2.0f,  -0.02f

	//glVertex2f(2.75f, -0.02f);
	//glVertex2f(3.23f, -0.02f);
	glEnd();

	
	//if (t >= 40.0f)
	//{

	//	glLoadIdentity();
	//	glTranslatef(8.0f, 0.0f, -6.0f);
	//	glLineWidth(2.5f);
	//	glBegin(GL_LINES);


	//	glColor3f(1.0f, 0.6f, 0.2f);
	//	glVertex2f(8.0f, 0.03f);
	//	glVertex2f(11.0f, 0.03f);

	//	//glLineWidth(1.0f);

	//	glColor3f(1.0f, 1.0f, 1.0f);
	//	glVertex2f(8.0f, 0.0f);
	//	glVertex2f(11.0f, 0.0f);

	//	//glLineWidth(1.0f);

	//	glColor3f(0.07f, 0.533f, 0.027f);
	//	glVertex2f(8.0f, -0.03f);
	//	glVertex2f(11.0f, -0.03f);
	//	glEnd();

	//}

	t = t + 0.0009f;
	if (t >= 50.0f)
	{
		t = 50.0f;
	}

	if (t >= 19.7f)
	{
		p = p + 0.00043f;
		p1 = p1 + 0.00043f;
		p2 = p2 + 0.00043f;
		if (p2 >= 8.0f)
		{
			p = 8.0f;
			p1 = 8.0f;
			p2 = 8.0f;
		}

	}
	
	
		if(t>=40.0f)
		{
			x = x - 0.0001f;
			y = y - 0.00006f;
			z = z - 0.00002f;
			a = a - 0.0001f;
			b = b - 0.0001f;
			c = c - 0.0001f;
			d = d - 0.000007f;
			e = e - 0.0000533f;
			f = f - 0.0000027f;
			if (f <= 0.00f)
			{
				x = 0.0f;
				y = 0.0f;
				z = 0.0f;
				a = 0.0f;
				b = 0.0f;
				c = 0.0f;
				d = 0.0f;
				e = 0.0f;
				f = 0.0f;
			}
		}

		if (t >= 40)
		{
			AFlag();
		}
	
}
void AFlag()
{
	glLoadIdentity();
	glTranslatef(-0.97f,0.0f,-3.0f);
	glLineWidth(2.5f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(1.97f,0.01f);
	glVertex2f(2.23f,0.01f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(1.97f,0.0f);
	glVertex2f(2.23f,0.0f);
	
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(1.97f,-0.01f);
	glVertex2f(2.23f,-0.01f);
	glEnd();
	//LeftI
	glLineWidth(5.0f);

	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(-3.0f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-3.0f, 0.0f);
	glEnd();
	//RightI
	glLineWidth(5.0f);
	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);

	
	glVertex2f(1.5f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(1.5f, 0.0f);
	glEnd();
	//D
	glLineWidth(5.0f);
	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-0.5f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(0.5f, 1.2f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(0.5f, 0.3f);
	glEnd();
	//N
	glLineWidth(5.0f);
	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);
	
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-2.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);


	glEnd();
}
void uninitialize(void)
{
	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}


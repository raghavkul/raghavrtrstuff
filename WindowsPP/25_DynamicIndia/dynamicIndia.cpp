#include<Windows.h>
#include"dynamicIndia.h"
#include<stdio.h>
#include"vmath.h"
#include<GL/glew.h>
#pragma comment (lib,"glew32.lib")
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#include<Mmsystem.h>

#pragma comment (lib,"Winmm.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//namespace
using namespace vmath;

//Global variables
bool gbFullScreen = false;

static bool ifLI = false;
static bool ifA = false;
static bool ifN = false;
static bool ifRI = false;
static bool ifD = false;
static bool movingPlane = false;
static bool movingtopPlane = false;
static bool movingbottomPlane = false;
static bool flag = false;
static bool AFlag = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

//variables for PP
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_LeftI;
GLuint vao_N;
GLuint vao_D;
GLuint vao_RightI;
GLuint vao_A;
GLuint vao_plane;
GLuint vao_topplane;
GLuint vao_bottomplane;
GLuint vao_AFlag;
//GLuint vao_planeFlag;
GLuint vao_flag;


GLuint vbo_position_LeftI;//vertex buffer object for tri
GLuint vbo_color_LeftI;

GLuint vbo_position_N;//vertex buffer object for tri
GLuint vbo_color_N;

GLuint vbo_position_D;//vertex buffer object for tri
GLuint vbo_color_D;

GLuint vbo_position_RightI;//vertex buffer object for tri
GLuint vbo_color_RightI;

GLuint vbo_position_A;//vertex buffer object for tri
GLuint vbo_color_A;

GLuint vbo_position_plane;//vertex buffer object for tri
GLuint vbo_color_plane;

GLuint vbo_position_topplane;//vertex buffer object for tri
GLuint vbo_color_topplane;

GLuint vbo_position_bottomplane;//vertex buffer object for tri
GLuint vbo_color_bottomplane;


GLuint vbo_position_AFlag;//vertex buffer object for tri
GLuint vbo_color_AFlag;

//GLuint vbo_position_planeFlag;//vertex buffer object for tri
//GLuint vbo_color_planeFlag;


GLuint vbo_position_flag;//vertex buffer object for tri
GLuint vbo_color_flag;

GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;//This is from vmath

//enum decleration
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};



//function
void update(void);
void vertexShaderCode(void);
void fragmentShaderCode(void);
void createProgramCode(void);
void geomentryCode(void);
void uninitialize(void);

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w+") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("RSK_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0X46:

			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	// variables
	GLenum result;
	/*GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	*///functions
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	//openGL extensions of PP
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit Failed!!\n");
		uninitialize();
	}


	//GLEW initialization Code for GLSL (IMPORTANT : It must be here Means After 
	// creating OpenGL context But before using	any OpenGL function

	GLenum glew_error = glewInit();
	fprintf(gpFile, "GL_Version : %s \n", (char*)glGetString(GL_VERSION));

	fprintf(gpFile, "GL_Shading_Language_Version : %s \n", (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));


	//call vertex shader function 
	vertexShaderCode();

	//call fragment shader function
	fragmentShaderCode();

	//call create Program code function
	createProgramCode();

	//Postlinking Retriving Uniform locations
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	//call geomentry code function
	geomentryCode();


	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//Background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//Give identity toorthographicProjectMatrix
	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize
	resize(WIN_WIDTH, WIN_HEIGHT);
	PlaySoundW(MAKEINTRESOURCE(MY_SONG), NULL, SND_ASYNC | SND_FILENAME | SND_RESOURCE);

	return(0);

}


void vertexShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"out vec4 out_color;" \
		"uniform mat4 u_mvp_matrix;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}

void fragmentShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code


	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}"
	};

	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}




void createProgramCode(void)
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding of vertex shader
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking For Program Object

	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}


void geomentryCode(void)
{
	//Here we give vertex,color,texcoord,normals in arrays 
	const GLfloat leftIVertices[] = {
		-3.30f,1.70f,0.0f,
		-3.30f,-1.70f,0.0f
	};
	const GLfloat leftIColor[] = {
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f
	};
	const GLfloat nVertices[] = {
		-2.10f,1.70f,0.0f,
		-2.10f,-1.70f,0.0f,
		-2.10f,1.70f,0.0f,
		-1.60f,-1.70f,0.0f,
		-1.60f,1.70f,0.0f,
		-1.60f,-1.70f,0.0f
	};
	const GLfloat nColor[] = {
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f
	};
	const GLfloat dVertices[] = {
		-0.4f,1.70f,0.0f,
		-0.4f,-1.70f,0.0f,
		-0.50f,1.70f,0.0f,
		1.1f,1.70f,0.0f,
		1.1f,1.70f,0.0f,
		1.1f,-1.70f,0.0f,
		-0.50f,-1.70f,0.0f,
		1.1f,-1.70f,0.0f
	};
	/*const GLfloat dColor[] = {
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		0.07f, 0.533f, 0.027f,
		0.07f, 0.533f, 0.027f
	};*/
	const GLfloat rightIVertices[] = {
		2.3f,1.70f,0.0f,
		2.3f,-1.70f,0.0f
	};
	const GLfloat rightIColor[] = {
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f
	};
	const GLfloat aVertices[] = {
		3.8f,1.70f,0.0f,
		3.50f,-1.70f,0.0f,
		3.80f,1.70f,0.0f,
		4.1f,-1.70f,0.0f
		/*3.66f,0.02f,0.0f,
		3.94f,0.02f,0.0f,
		3.66f,0.0f,0.0f,
		3.94f,0.0f,0.0f,
		3.66f,-0.02f,0.0f,
		3.94f,-0.02f,0.0f*/
	};
	const GLfloat aColor[] = {
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
	/*	1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		0.07f, 0.533f, 0.027f,
		0.07f, 0.533f, 0.027f*/
	};

	const GLfloat planePosition[] = {
		/*0.4f, 0.2f,0.0f,
		-0.6f, 0.2f,0.0f,
		
		0.4f, -0.2f,0.0f,
		-0.6f, -0.2f,0.0f,

		0.4f, 0.2f,0.0f,
		0.6f,0.0f,0.0f,

		0.4f, -0.2f,0.0f,
		0.6f,0.0f,0.0f,

		-0.6f, 0.2f,0.0f,
		-0.8f,0.0f,0.0f,

		-0.6f, -0.2f,0.0f,
		-0.8f,0.0f,0.0f,

		-0.8f,0.0f,0.0f,
		-0.12f,0.2f,0.0f,

		-0.8f,0.0f,0.0f,
		-0.12f,-0.2f,0.0f

		-0.12f,0.2f,0.0f,
		-0.12f,-0.2f,0.0f,

		0.0f,0.2f,0.0f,
		0.0f,0.4f,0.0f,

		0.0f,0.4f,0.0f,
		0.3,0.2f,0.0f,
		
		0.0f,-0.2f,0.0f,
		0.0f,-0.4f,0.0f,
	
		0.0f,-0.4f,0.0f,
		0.3,-0.2f,0.0f*/

		0.4f, 0.2f,0.0f,
		1.0f, -0.3f,0.0f,
		0.4f, -0.2f,0.0f,

		-0.6f, 0.2f,0.0f,
//		-1.0f, 0.6f,0.0f,
		-0.6f, -0.2f,0.0f,
		
		
		0.4f, 0.2f,0.0f,
		0.4f, -0.2f,0.0f,
		
	
		-0.6f, -0.2f,0.0f,
		-1.0f, -0.2f,0.0f,
		-0.6f, 0.2f,0.0f,
		-1.0f, 0.8f,0.0f,
		- 0.6f, -0.2f,0.0f

		//-1.0f, -0.2f,0.0f,
		//-1.0f, 0.6f,0.0f

		/*-0.6f, 0.2f,0.0f,
		-1.0f, 0.6f,0.0f,
		
		-0.6f, -0.2f,0.0f,
		-1.0f, -0.2f,0.0f,
		-1.0f, 0.6f,0.0f*/


		/*0.4f, 0.2f,0.0f,
		-0.6f, 0.2f,0.0f,
		-0.6f, -0.2f,0.0f,
		0.4f, -0.2f,0.0f,

		-0.6f, 0.2f,0.0f,
		-1.0f, 0.4f,0.0f,
		-1.0f, -0.4f,0.0f,
		-0.6f, -0.2f,0.0f,

		0.2f, 0.2f,0.0f,
		-0.3f, 1.0f,0.0f,
		-0.3f, -1.0f,0.0f,
		0.2f, -0.2f,0.0f,

		1.0f, 0.0f,0.0f,
		0.4f, 0.2f,0.0f,
		0.4f, -0.2f,0.0f,
		1.0f, 0.0f,0.0f*/
	};

	const GLfloat planeColor[] = {
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f
		//0.7265f, 0.8828f, 0.9296f
		/*0.7265f, 0.8828f, 0.9296f,
		
		
		
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f*/
		
		/*0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f,
		0.7265f, 0.8828f, 0.9296f*/
	};
	
	/*const GLfloat planeFlagPosition[] = {
		-2.5f,0.05f,0.0f,
		-1.0f,0.05f,0.0f,
		-2.5f,0.0f,0.0f,
		-1.0f,0.0f,0.0f
		- 2.5f,-0.05f,0.0f,
		-1.0f,-0.05f,0.0f

	};
	const GLfloat planeFlagColor[] = {
		0.0f,0.0f,0.0f,
		1.0f,0.6f,0.2f,
		0.0f,0.0f,0.0f,
		1.0f,1.0f,1.0f,
		0.0f,0.0f,0.0f,
		0.07f,0.533f,0.027f
	};*/


	const GLfloat aFlagPosition[] = {
		3.67f,0.01f,0.0f,
		3.93f,0.01f,0.0f,
		3.67f,0.0f,0.0f,
		3.93f,0.01f,0.0f,
		3.67f,-0.01f,0.0f,
		3.93f,-0.01f,0.0f,
	
		- 3.30f,1.70f,0.0f,
		-3.30f,-1.70f,0.0f,

		- 2.10f,1.70f,0.0f,
		-2.10f,-1.70f,0.0f,
		-2.10f,1.70f,0.0f,
		-1.60f,-1.70f,0.0f,
		-1.60f,1.70f,0.0f,
		-1.60f,-1.70f,0.0f,

		- 0.4f,1.70f,0.0f,
		-0.4f,-1.70f,0.0f,
		-0.50f,1.70f,0.0f,
		1.1f,1.70f,0.0f,
		1.1f,1.70f,0.0f,
		1.1f,-1.70f,0.0f,
		-0.50f,-1.70f,0.0f,
		1.1f,-1.70f,0.0f,


		2.3f,1.70f,0.0f,
		2.3f,-1.70f,0.0f,


		3.8f,1.70f,0.0f,
		3.50f,-1.70f,0.0f,
		3.80f,1.70f,0.0f,
		4.1f,-1.70f,0.0f


		/*-3.0f,1.5f,0.0f,
		-3.0f,0.0f,0.0f,
		1.5f, 1.5f,0.0f,
		1.5f, 0.0f,0.0f,
		-0.5f, 1.5f,0.0f,
		-0.5f, 0.0f,0.0f,
		0.5f, 1.2f,0.0f,
		0.5f, 0.3f,0.0f,
		-2.0f, 1.5f,0.0f,
		-2.0f, 0.0f,0.0f,
		-1.0f, 0.0f,0.0f,
		-1.0f, 0.0f,0.0f,
		- 2.0f, 1.5f,0.0f*/


	};
	const GLfloat aFlagColor[] = {
		1.0f, 0.6f, 0.2f,
		1.0f, 1.0f, 1.0f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,

		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,

		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		0.07f, 0.533f, 0.027f,
		0.07f, 0.533f, 0.027f,
		
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,


		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f


		/*0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f*/

	};
	//leftI
	glGenVertexArrays(1, &vao_LeftI);
	glBindVertexArray(vao_LeftI);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_LeftI);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_LeftI);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftIVertices), leftIVertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_LeftI);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_LeftI);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(leftIColor), leftIColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


	//N
	//Create vao for triangle
	glGenVertexArrays(1, &vao_N);
	glBindVertexArray(vao_N);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_N);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(nVertices), nVertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_N);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_N);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//D
	//Create vao for triangle
	glGenVertexArrays(1, &vao_D);
	glBindVertexArray(vao_D);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_D);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(dVertices), dVertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_D);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(4 * 3 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//RightI
	//Create vao for triangle
	glGenVertexArrays(1, &vao_RightI);
	glBindVertexArray(vao_RightI);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_RightI);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_RightI);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(rightIVertices), rightIVertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_RightI);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_RightI);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(rightIColor), rightIColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//A
	//Create vao for triangle
	glGenVertexArrays(1, &vao_A);
	glBindVertexArray(vao_A);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_A);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(aVertices), aVertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_A);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_A);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);

	//plane
	//Create vao for triangle
	glGenVertexArrays(1, &vao_plane);
	glBindVertexArray(vao_plane);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_plane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_plane);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(planePosition), planePosition, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_plane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_plane);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeColor), planeColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


	//topplane
	//Create vao for triangle
	glGenVertexArrays(1, &vao_topplane);
	glBindVertexArray(vao_topplane);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_topplane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_topplane);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(planePosition), planePosition, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_topplane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_topplane);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeColor), planeColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


	//bottomplane
	//Create vao for triangle
	glGenVertexArrays(1, &vao_bottomplane);
	glBindVertexArray(vao_bottomplane);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_bottomplane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_bottomplane);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(planePosition), planePosition, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_bottomplane);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_bottomplane);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(planeColor), planeColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


	////planeFlag
	////Create vao for triangle
	//glGenVertexArrays(1, &vao_planeFlag);
	//glBindVertexArray(vao_planeFlag);

	////Generating Buffer for triangle
	//glGenBuffers(1, &vbo_position_planeFlag);
	//glBindBuffer(GL_ARRAY_BUFFER, vbo_position_planeFlag);
	////push data into buffers immediate
	//glBufferData(GL_ARRAY_BUFFER, sizeof(planeFlagPosition), planeFlagPosition, GL_STATIC_DRAW);
	////how many slots my array is break
	//glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	////Enabling the position
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	////Unbinding buffer and array for triangle
	//glBindBuffer(GL_ARRAY_BUFFER, 0);


	////Generating Buffer for triangle
	//glGenBuffers(1, &vbo_color_planeFlag);
	//glBindBuffer(GL_ARRAY_BUFFER, vbo_color_planeFlag);
	////push data into buffers immediate
	//glBufferData(GL_ARRAY_BUFFER, sizeof(planeColor), planeColor, GL_STATIC_DRAW);
	////how many slots my array is break
	//glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	////Enabling the position
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	////Unbinding buffer and array for triangle
	//glBindBuffer(GL_ARRAY_BUFFER, 0);


	//glBindVertexArray(0);

	//flag
	//Create vao for triangle
	glGenVertexArrays(1, &vao_flag);
	glBindVertexArray(vao_flag);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_flag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_flag);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(4 * 3 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_flag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_flag);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(4 * 3 * sizeof(GLfloat)), NULL, GL_DYNAMIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


	//aFlag
	//Create vao for triangle
	glGenVertexArrays(1, &vao_AFlag);
	glBindVertexArray(vao_AFlag);

	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_AFlag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_AFlag);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(aFlagPosition), aFlagPosition, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_color_AFlag);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_AFlag);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(aFlagColor), aFlagColor, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

	//Unbinding buffer and array for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	glBindVertexArray(0);


}
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}



void display(void)
{

	

	//LI
	static GLfloat Litx = -3.0f, Lity = 0.0f, Litz = -24.0f;
	
	//A
	static GLfloat x = 0.0f, y = 0.0f, z = 0.0f, a = 0.0f, b = 0.0f, c = 0.0f, d = 0.0f, e = 0.0f, f = 0.0f; //For  middle color of A
	static GLfloat atx = 4.0f, aty = 0.0f, atz = -24.0f, aty1 = 0.01f;
	//RI
	static GLfloat Ritx = 0.0f, Rity = -6.0f, Ritz = -24.0f;

	//D
	static GLfloat dtx = 0.0f, dty = 0.0f, dtz = -24.0f;
	static GLfloat i = 0.0f, j = 0.0f, k = 0.0f, l = 0.0f, m = 0.0f, n = 0.0f;
	
	//N
	static GLfloat ntx = 0.0f, nty = 4.0f, ntz = -24.0f;

	//plane
	static GLfloat t1x = -18.0f, t1y = 0.0f, t1z = -22.0f;
	

	//flag
	static GLfloat fx = 1.0f, fy = 0.6f, fz = 0.2f, fa = 1.0f, fb = 1.0f, fc = 1.0f, fd = 0.07f, fe = 0.533f, ff = 0.027f; //For  middle color flag
	static GLfloat ft = -11.0f;
	static GLfloat ftx = -10.0f, fty = 0.0f, ftz = -24.0f;
	static GLfloat fp = -1.0f, fq = 0.03f, fp1 = -1.0f, fq1 = 0.0f, fp2 = -1.0f, fq2 = -0.03f;
	static GLfloat fatx = 4.0f, faty = -0.7f, fatz = -8.0f, faty1 = 0.01f;


	const GLfloat dColor[] = {
		i, j, k,
		l, m, n,
		i, j, k,
		i, j, k,
		i, j, k,
		l, m, n,
		l, m, n,
		l, m, n
	};
	
	const GLfloat flagPosition[] = {
		-1.0f, 0.03f,0.0f,
		fp, fq,0.0f,
		-1.0f,0.0f,0.0f,
		fp1,fq1,0.0f,
		-1.0f,-0.03f,0.0f,
		fp2,fq1,0.0f

	};
	const GLfloat flagColor[] = {
		0.0f,0.0f,0.0f,
		fx,fy,fz,
		0.0f,0.0f,0.0f,
		fa,fb,fc,
		0.0f,0.0f,0.0f,
		fd,fe,ff
	};

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Using program object 
	glUseProgram(gShaderProgramObject);

	glLineWidth(5.0f);
	//declerations of matrix
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;
	mat4 translateMatrix;

	

	if (ifLI == false)
	{
		//leftI
	//Initialize above matrix to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(Litx, Lity, Litz);


		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_LeftI);


		//Draw function
		glDrawArrays(GL_LINES, 0, 2);

		//Unbind vao of triangle
		glBindVertexArray(0);

		if ((Litx < 0.0f) || (Litx == 0.0f))
		{
			Litx = Litx + 0.0009f;
		}
		if (Litx >= 0.0f)
		{
			ifA = true;
			//A();
		}
	}
	if (ifA == true)
	{
		//A
	//Initialize above matrix to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(atx, aty, atz);


		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_A);


		//Draw function
		glDrawArrays(GL_LINES, 0, 10);

		//Unbind vao of triangle
		glBindVertexArray(0);
		if ((atx > 0.0) || (atx == 0.0f))
		{
			atx = atx - 0.0009f;

		}
		if (atx <= 0.0f)
		{
			//N();
			ifN = true;
		}

	}
	if (ifN == true)
	{
		//N
	//Initialize above matrix to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(ntx, nty, ntz);


		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_N);


		//Draw function
		glDrawArrays(GL_LINES, 0, 6);

		//Unbind vao of triangle
		glBindVertexArray(0);

		if ((nty >= 0.0f) || (nty == 0.0f))
		{
			nty = nty - 0.002f;

		}
		if (nty <= 0.0f)
		{
			//rightI();
			ifRI = true;
		}
	}
	if (ifRI == true)
	{
		//rightI
	//Initialize above matrix to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(Ritx, Rity, Ritz);


		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_RightI);


		//Draw function
		glDrawArrays(GL_LINES, 0, 2);

		//Unbind vao of triangle
		glBindVertexArray(0);

		if ((Rity <= 0.0f) || (Rity == 0.0f))
		{
			Rity = Rity + 0.002f;

		}
		if (Rity >= 0.0f)
		{
			//D();
			ifD = true;
		}
	}
	if (ifD == true)
	{
		//D
	//Initialize above matrix to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(dtx, dty, dtz);


		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_D);

		glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D);
		glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//Draw function
		glDrawArrays(GL_LINES, 0, 8);

		//Unbind vao of triangle
		glBindVertexArray(0);

		if (i <= 1.0f && j < 0.6f && k < 0.2f && l < 0.07f && m < 0.533f && n < 0.027f)
		{
			i = i + 0.0001f;
			j = j + 0.00006f;
			k = k + 0.00002f;

			l = l + 0.000007f;
			m = m + 0.0000533f;
			n = n + 0.0000027f;
		}

		if (n >= 0.027f)
		{
			movingPlane = true;
			movingtopPlane = true;
			movingbottomPlane = true;
		}
	}
	

	if (movingPlane == true)
	{
		glLineWidth(3.0f);

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(t1x, t1y, t1z);


		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_plane);


		//Draw function
		glDrawArrays(GL_TRIANGLE_FAN, 0, 12);

		//Unbind vao of triangle
		glBindVertexArray(0);
		
		
		
			
		t1x = t1x + 0.0009f;
		if (t1x >= 22.0f)
		{
			t1x = 22.0f;
		}
	}

	if (movingtopPlane == true)
	{
		static GLfloat t2x = -18.0f, t2y = 6.0f, t2z = -22.0f, Airoangale2 = -90.0f, r2x = 0.0f, r2y = 0.0f, r2z = 1.0f;
		static bool  splitter = false;

		glLineWidth(3.0f);

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();
		mat4 rotatiomMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(t2x, t2y, t2z);
		rotatiomMatrix = rotate(Airoangale2, r2x, r2y, r2z);

		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix * rotatiomMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_plane);


		//Draw function
		glDrawArrays(GL_TRIANGLE_FAN, 0, 12);

		//Unbind vao of triangle
		glBindVertexArray(0);




		if (splitter == false)
		{

			Airoangale2 = Airoangale2 + 0.014f;
			if (Airoangale2 >= 0.0f)
			{
				Airoangale2 = 0.0f;

			}

		}

		if (splitter == false)
		{

			t2y = t2y - 0.0009f;
			if (t2y <= 0.0f)
			{
				t2y = 0.0f;
				if (t2y <= 0.0f)
				{
					splitter = true;
				}
			}

		}

		t2x = t2x + 0.0009f;
		if (t2x >= 18.0f)
		{
			t2x = 18.0f;
		}

		if (t2x >= 11.0f)
		{
			Airoangale2 = Airoangale2 + 0.013f;
			if (Airoangale2 >= 90.0f)
			{
				Airoangale2 = 90.0f;
			}
		}


		if (t2x >= 11.0f)
		{
			t2y = t2y + 0.0009f;
			if (t2y >= 7.0f)
			{
				t2y = 7.0f;
			}
		}
	}

	if (movingbottomPlane == true)
	{
		static GLfloat t3x = -18.0f, t3y = -6.0f, t3z = -22.0f, Airoangale3 = 90.0f, r3x = 0.0f, r3y = 0.0f, r3z = 1.0f;
		static bool Airostopper = false;
		static bool splitter = false;
		glLineWidth(3.0f);

		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();
		mat4 rotatiomMatrix = mat4::identity();
		//Do neccessary transformation

		translateMatrix = translate(t3x, t3y, t3z);
		rotatiomMatrix = rotate(Airoangale3, r3x, r3y, r3z);

		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix * rotatiomMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_plane);


		//Draw function
		glDrawArrays(GL_TRIANGLE_FAN, 0, 12);

		//Unbind vao of triangle
		glBindVertexArray(0);

		if (splitter == false)
		{

			Airoangale3 = Airoangale3 - 0.014f;
			if (Airoangale3 <= 0.0f)
			{
				Airoangale3 = 0.0f;

			}
		}


		if (splitter == false)
		{


			t3y = t3y + 0.0009f;
			if (t3y >= 0.0f)
			{
				t3y = 0.0f;
				if (t3y >= 0.0f)
				{
					splitter = true;
				}
			}
		}


		t3x = t3x + 0.0009f;
		if (t3x >= 18.0f)
		{
			t3x = 18.0f;
		}


		if (t3x >= 11.0f)
		{
			Airoangale3 = Airoangale3 - 0.013f;
			if (Airoangale3 <= -90.0f)
			{
				Airoangale3 = -90.0f;
			}
		}

		if (t3x >= 11.0f)
		{
			t3y = t3y - 0.0009f;
			if (t3y <= -7.0f)
			{
				t3y = -7.0f;
			}
		}
	}



	if (flag == false)
	{
		//Initialize above matrix to identity
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(ftx, fty, ftz);
		glLineWidth(2.5f);

		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_flag);

		glBindBuffer(GL_ARRAY_BUFFER, vbo_position_flag);
		glBufferData(GL_ARRAY_BUFFER, sizeof(flagPosition), flagPosition, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, vbo_color_flag);
		glBufferData(GL_ARRAY_BUFFER, sizeof(flagColor), flagColor, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		//Draw function
		glDrawArrays(GL_LINES, 0, 6);

		//Unbind vao of triangle
		glBindVertexArray(0);
	
		ft = ft + 0.00095f;
		if (ft >= 50.0f)
		{
			ft = 50.0f;
		}

		if (ft >= 19.7f)
		{
			fp = fp + 0.00099f;
			fp1 = fp1 + 0.00099f;
			fp2 = fp2 + 0.00099f;
			if (fp2 >= 18.0f)
			{
				fp = 18.0f;
				fp1 = 18.0f;
				fp2 = 18.0f;
			}

		}


		if (ft >= 42.0f)
		{
			fx = fx - 0.0001f;
			fy = fy - 0.00006f;
			fz = fz - 0.00002f;
			fa = fa - 0.0001f;
			fb = fb - 0.0001f;
			fc = fc - 0.0001f;
			fd = fd - 0.000007f;
			fe = fe - 0.0000533f;
			ff = ff - 0.0000027f;
			if (ff <= 0.00f)
			{
				fx = 0.0f;
				fy = 0.0f;
				fz = 0.0f;
				fa = 0.0f;
				fb = 0.0f;
				fc = 0.0f;
				fd = 0.0f;
				fe = 0.0f;
				ff = 0.0f;
			}
		}
		if (ft >= 42.0f)
		{
			AFlag = true;
		}
	}


	if (AFlag == true)
	{
		modelViewMatrix = mat4::identity();
		modelViewProjectionMatrix = mat4::identity();
		translateMatrix = mat4::identity();

		//Do neccessary transformation

		translateMatrix = translate(0.0f, 0.0f, -24.0f);
		glLineWidth(5.0f);

		//Do neccessary Matrix Multilication
		modelViewMatrix = translateMatrix;
		modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		//Send neccessary matrices to shader in respective to uniforms
		glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//Bind with vao of triangle
		glBindVertexArray(vao_AFlag);

		//Draw function
		glDrawArrays(GL_LINES, 0, 28);

		//Unbind vao of triangle
		glBindVertexArray(0);



		//modelViewMatrix = mat4::identity();
		//modelViewProjectionMatrix = mat4::identity();
		//translateMatrix = mat4::identity();

		////Do neccessary transformation

		//translateMatrix = translate(Litx, Lity, Litz);
		//glLineWidth(2.5f);

		////Do neccessary Matrix Multilication
		//modelViewMatrix = translateMatrix;
		//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		////Send neccessary matrices to shader in respective to uniforms
		//glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		//glBindVertexArray(vao_LeftI);


		////Draw function
		//glDrawArrays(GL_LINES, 0, 2);

		////Unbind vao of triangle
		//glBindVertexArray(0);

		//modelViewMatrix = mat4::identity();
		//modelViewProjectionMatrix = mat4::identity();
		//translateMatrix = mat4::identity();

		////Do neccessary transformation

		//translateMatrix = translate(atx, aty, atz);
		//glLineWidth(2.5f);

		////Do neccessary Matrix Multilication
		//modelViewMatrix = translateMatrix;
		//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		////Send neccessary matrices to shader in respective to uniforms
		//glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		////Bind with vao of triangle
		//glBindVertexArray(vao_A);


		////Draw function
		//glDrawArrays(GL_LINES, 0, 10);

		////Unbind vao of triangle
		//glBindVertexArray(0);

		//modelViewMatrix = mat4::identity();
		//modelViewProjectionMatrix = mat4::identity();
		//translateMatrix = mat4::identity();

		////Do neccessary transformation

		//translateMatrix = translate(ntx, nty, ntz);
		//glLineWidth(2.5f);

		////Do neccessary Matrix Multilication
		//modelViewMatrix = translateMatrix;
		//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		////Send neccessary matrices to shader in respective to uniforms
		//glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		////Bind with vao of triangle
		//glBindVertexArray(vao_N);


		////Draw function
		//glDrawArrays(GL_LINES, 0, 6);

		////Unbind vao of triangle
		//glBindVertexArray(0);


		//modelViewMatrix = mat4::identity();
		//modelViewProjectionMatrix = mat4::identity();
		//translateMatrix = mat4::identity();

		////Do neccessary transformation

		//translateMatrix = translate(Ritx, Rity, Ritz);
		//glLineWidth(2.5f);

		////Do neccessary Matrix Multilication
		//modelViewMatrix = translateMatrix;
		//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		////Send neccessary matrices to shader in respective to uniforms
		//glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

		////Bind with vao of triangle
		//glBindVertexArray(vao_RightI);


		////Draw function
		//glDrawArrays(GL_LINES, 0, 2);

		////Unbind vao of triangle
		//glBindVertexArray(0);

		//modelViewMatrix = mat4::identity();
		//modelViewProjectionMatrix = mat4::identity();
		//translateMatrix = mat4::identity();

		////Do neccessary transformation

		//translateMatrix = translate(dtx, dty, dtz);
		//glLineWidth(2.5f);

		////Do neccessary Matrix Multilication
		//modelViewMatrix = translateMatrix;
		//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

		////Send neccessary matrices to shader in respective to uniforms
		//glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
		////Bind with vao of triangle
		//glBindVertexArray(vao_D);

		//glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D);
		//glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_DYNAMIC_DRAW);
		//glBindBuffer(GL_ARRAY_BUFFER, 0);
		////Draw function
		//glDrawArrays(GL_LINES, 0, 8);

		////Unbind vao of triangle
		//glBindVertexArray(0);


	}
	//Unused Program
	glUseProgram(0);

	SwapBuffers(ghdc);


}

void update(void)
{
	//code
}


void uninitialize(void)
{
	if (vbo_color_A)
	{
		glDeleteBuffers(1, &vbo_color_A);
		vbo_color_A = 0;
	}
	if (vbo_color_D)
	{
		glDeleteBuffers(1, &vbo_color_D);
		vbo_color_D = 0;
	}
	if (vbo_color_LeftI)
	{
		glDeleteBuffers(1, &vbo_color_LeftI);
		vbo_color_LeftI = 0;
	}
	if (vbo_color_N)
	{
		glDeleteBuffers(1, &vbo_color_N);
		vbo_color_N = 0;
	}
	if (vbo_color_RightI)
	{
		glDeleteBuffers(1, &vbo_color_RightI);
		vbo_color_RightI = 0;
	}

	if (vbo_position_A)
	{
		glDeleteBuffers(1, &vbo_position_A);
		vbo_position_A = 0;
	}
	if (vbo_position_D)
	{
		glDeleteBuffers(1, &vbo_position_D);
		vbo_position_D = 0;
	}
	if (vbo_position_LeftI)
	{
		glDeleteBuffers(1, &vbo_position_LeftI);
		vbo_position_LeftI = 0;
	}
	if (vbo_position_N)
	{
		glDeleteBuffers(1, &vbo_position_N);
		vbo_position_N = 0;
	}
	if (vbo_position_RightI)
	{
		glDeleteBuffers(1, &vbo_position_RightI);
		vbo_position_RightI = 0;
	}

	if (vao_A)
	{
		glDeleteVertexArrays(1, &vao_A);
		vao_A = 0;
	}
	if (vao_D)
	{
		glDeleteVertexArrays(1, &vao_D);
		vao_D = 0;
	}
	if (vao_LeftI)
	{
		glDeleteVertexArrays(1, &vao_LeftI);
		vao_LeftI = 0;
	}
	if (vao_RightI)
	{
		glDeleteVertexArrays(1, &vao_RightI);
		vao_RightI = 0;
	}
	if (vao_N)
	{
		glDeleteVertexArrays(1, &vao_N);
		vao_N = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}
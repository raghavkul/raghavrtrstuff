#include<Windows.h>
#include<stdio.h>
#include"vmath.h"

#include<GL/glew.h>
#pragma comment (lib,"glew32.lib")
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#include"Sphere.h"
#pragma comment (lib,"Sphere.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600





//namespace
using namespace vmath;

//Global variables
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];

//light values
GLfloat lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };


bool gbFullScreen = false;
bool gbLighting = false;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

//variables for PP
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_sphere;//vertex array object for rect
GLuint vbo_position_sphere;//vertex buffer object(position) for rect
GLuint vbo_normal_sphere;//vertex buffer object(color) for rect
GLuint vbo_elements_sphere;

//matrices
GLuint mUniform; // model matrix
GLuint vUniform; // view matrix
GLuint pUniform; //projection matrix

//lights
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;

//material
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShinynessUniform;


GLuint lightPositionUniform;
GLuint lKeyIsPressedUniform;

//sphere variables
unsigned int gNumVertices;
unsigned int gNumElements;

mat4 perspectiveProjectionMatrix;//This is from vmath

GLint keyPress = 0;

//Animation varibales
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;

//enum decleration
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};



//function
void update(void);
void vertexShaderCode(void);
void fragmentShaderCode(void);
void createProgramCode(void);
void geomentryCode(void);
void uninitialize(void);

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w+") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("RSK_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0X46:

			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (gbLighting == false)
			{
				gbLighting = true;
			}
			else {
				gbLighting = false;
			}
			break;
		case'x':
		case'X':
			keyPress = 1;
			angleOfXRotation = 0.0f;
			break;
		case'y':
		case'Y':
			keyPress = 2;
			angleOfXRotation = 0.0f;
			break;
		case'z':
		case'Z':
			keyPress = 3;
			angleOfXRotation = 0.0f;
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	
	
	// variables
	GLenum result;
	/*GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	*///functions
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	//openGL extensions of PP
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit Failed!!\n");
		uninitialize();
	}


	//GLEW initialization Code for GLSL (IMPORTANT : It must be here Means After 
	// creating OpenGL context But before using	any OpenGL function

	GLenum glew_error = glewInit();
	fprintf(gpFile, "GL_Version : %s \n", (char*)glGetString(GL_VERSION));

	fprintf(gpFile, "GL_Shading_Language_Version : %s \n", (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));


	//call vertex shader function 
	vertexShaderCode();

	//call fragment shader function
	fragmentShaderCode();

	//call create Program code function
	createProgramCode();

	//Postlinking Retriving Uniform locations
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShine");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");

	//call geomentry code function
	geomentryCode();


	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//ADD CULL FACE
	//glDisable(GL_CULL_FACE);

	//Background color
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	//Give identity toorthographicProjectMatrix
	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);

}


void vertexShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec4 u_lightPosition;" \
		"out vec3 tNorm;" \
		"out vec3 lightDirection;" \
		"out vec3 viwerVector;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
		"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" \
		"lightDirection = vec3(u_lightPosition - eye_Coordinate);" \
		"viwerVector = -eye_Coordinate.xyz;" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}

void fragmentShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 lightDirection;" \
		"in vec3 viwerVector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShine;" \
		"uniform vec4 u_lightPosition;" \
		"uniform int u_lKeyIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec3 normalizeTNorm = normalize(tNorm);" \
		"vec3 normalizeLightDirection = normalize(lightDirection);" \
		"vec3 normalizeViwerVector = normalize(viwerVector);" \
		"float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" \
		"vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" \
		"vec3 ambiant = vec3(u_la * u_ka);" \
		"vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" \
		"vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" \
		"vec3 phong_ads_light = ambiant + diffuse + specular;" \
		"FragColor = vec4(phong_ads_light,1.0f);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
		"}" \
		"}"
	};
	//*******************************//"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		
	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}




void createProgramCode(void)
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding of vertex shader

	//----- Position
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	// ---- Normal
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking For Program Object

	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}


void geomentryCode(void)
{
	
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	//****************************** CUBE ****************************
	//Create vao for rect
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//################### POSITION ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and arra for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//################### NORMAL ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);


	//########################## Elements ###############
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_elements_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

	////how many slots my array is break
	//glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	////Enabling the position
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);

}
void resize(int width, int height)
{
	int xWindowCenter = GetSystemMetrics(SM_CXSCREEN) / 2;
	fprintf(gpFile, " width : %d\n", xWindowCenter);
	int yWindowCenter = GetSystemMetrics(SM_CYSCREEN) / 2;
	fprintf(gpFile, " height : %d\n", yWindowCenter);

	int distorasionX = GetSystemMetrics(SM_CXSCREEN)/6;
	fprintf(gpFile, "  distorsion width : %d\n", distorasionX);
	int distorasionY = GetSystemMetrics(SM_CYSCREEN)/8;
	fprintf(gpFile, " distorsion height : %d\n", distorasionY);

	
	if (height == 0)
	{
		height = 1;
	}

	
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)distorasionX / (GLfloat)distorasionY,
		0.1f,
		100.0f);
}



void display(void)
{
	void draw24Sphers();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Using program object 
	glUseProgram(gShaderProgramObject);
	
	if (gbLighting == true)
	{
		glUniform1i(lKeyIsPressedUniform, 1);
		glUniform3fv(laUniform, 1, lightAmbiant);
		glUniform3fv(ldUniform, 1, lightDiffuse);
		glUniform3fv(lsUniform, 1, lightSpecular);
		if (keyPress == 1)
		{
			lightPosition[1] = cos(angleOfXRotation) * 100.0f;
			lightPosition[2] = -sin(angleOfXRotation) * 100.0f;
			
			glUniform4fv(lightPositionUniform, 1, lightPosition);
		}else if(keyPress == 2)
		{
			lightPosition[0] = cos(angleOfYRotation) * 100.0f;
			lightPosition[2] = -sin(angleOfYRotation) * 100.0f;
		
			glUniform4fv(lightPositionUniform, 1, lightPosition);
		}else if(keyPress==3) {

			lightPosition[0] = cos(angleOfZRotation) * 100.0f;
			lightPosition[1] = sin(angleOfZRotation) * 100.0f;
			
			glUniform4fv(lightPositionUniform, 1, lightPosition);
		}
		
		
	}
	else {
		glUniform1i(lKeyIsPressedUniform, 0);
	}

	

	draw24Sphers();

	//Unused Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void draw24Sphers()
{
	int xWindowCenter = GetSystemMetrics(SM_CXSCREEN) / 2;
	int yWindowCenter = GetSystemMetrics(SM_CYSCREEN) / 2;


	int distorasionX = GetSystemMetrics(SM_CXSCREEN) / 6;
	int distorasionY = GetSystemMetrics(SM_CYSCREEN) / 8;

	int xTransOffset = distorasionX;
	int yTransOffset = distorasionY;

	int currentViewportX;
	int currentViewportY;

	mat4 viewMatrix;
	mat4 modelMatrix;
	mat4 translationMatrix;
	/*mat4 rotationMatrix;
	mat4 scaleMatrix;*/
	mat4 projectionMatrix;


	GLfloat MaterialAmbiant[4];
	GLfloat MaterialDiffuse[4];
	GLfloat MaterialSpecular[4];
	GLfloat MaterialShininess[1];

	//*********************************** SPHERE 1 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	
	

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;

	
	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);

	
	MaterialAmbiant[0] = 0.0215f;
	MaterialAmbiant[1] = 0.1745f;
	MaterialAmbiant[2] = 0.0215f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.07568f;
	MaterialDiffuse[1] = 0.61424f;
	MaterialDiffuse[2] = 0.0215f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.633f;
	MaterialSpecular[1] = 0.727811f;
	MaterialSpecular[2] = 0.633f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);


	//*********************************** SPHERE 2 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);

	
	MaterialAmbiant[0] = 0.135f;
	MaterialAmbiant[1] = 0.2225f;
	MaterialAmbiant[2] = 0.1575f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.54f;
	MaterialDiffuse[1] = 0.89f;
	MaterialDiffuse[2] = 0.63f;
	MaterialDiffuse[3] = 1.0f;
	

	MaterialSpecular[0] = 0.316228f;
	MaterialSpecular[1] = 0.316228f;
	MaterialSpecular[2] = 0.316228f;
	MaterialSpecular[3] = 1.0f;
	

	MaterialShininess[0] = 0.1f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 3 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.5375f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.06625f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.18275f;
	MaterialDiffuse[1] = 0.17f;
	MaterialDiffuse[2] = 0.22525f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.332741f;
	MaterialSpecular[1] = 0.328634f;
	MaterialSpecular[2] = 0.346435f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.3f * 128.0f;
	
	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);


	//*********************************** SPHERE 4 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.20725f;
	MaterialAmbiant[2] = 0.20725f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 1.0f;
	MaterialDiffuse[1] = 0.829f;
	MaterialDiffuse[2] = 0.829f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.296648f;
	MaterialSpecular[1] = 0.296648f;
	MaterialSpecular[2] = 0.296648f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.088f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 5 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.1745f;
	MaterialAmbiant[1] = 0.01175f;
	MaterialAmbiant[2] = 0.01175f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.61424f;
	MaterialDiffuse[1] = 0.04136f;
	MaterialDiffuse[2] = 0.04136f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.727811f;
	MaterialSpecular[1] = 0.626959f;
	MaterialSpecular[2] = 0.626959f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.6f * 128.0f;
	

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 6 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.1f;
	MaterialAmbiant[1] = 0.18725f;
	MaterialAmbiant[2] = 0.1745f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.396f;
	MaterialDiffuse[1] = 0.74151f;
	MaterialDiffuse[2] = 0.69102f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.297254f;
	MaterialSpecular[1] = 0.30829f;
	MaterialSpecular[2] = 0.306678f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.1f * 128.0f;
	

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 7 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.329412f;
	MaterialAmbiant[1] = 0.223529f;
	MaterialAmbiant[2] = 0.27451f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.780392f;
	MaterialDiffuse[1] = 0.568627f;
	MaterialDiffuse[2] = 0.113725f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.992157f;
	MaterialSpecular[1] = 0.941176f;
	MaterialSpecular[2] = 0.807843f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.21794872f * 128.0f;
	

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 8 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.2125f;
	MaterialAmbiant[1] = 0.1275f;
	MaterialAmbiant[2] = 0.054f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.714f;
	MaterialDiffuse[1] = 0.4284f;
	MaterialDiffuse[2] = 0.18144f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.393548f;
	MaterialSpecular[1] = 0.271906f;
	MaterialSpecular[2] = 0.166721f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.2f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 9 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.25f;
	MaterialAmbiant[2] = 0.25f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.774597f;
	MaterialSpecular[1] = 0.774597f;
	MaterialSpecular[2] = 0.774597f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 10 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.19125f;
	MaterialAmbiant[1] = 0.0735f;
	MaterialAmbiant[2] = 0.02225f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.7038f;
	MaterialDiffuse[1] = 0.27048f;
	MaterialDiffuse[2] = 0.0828f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.256777f;
	MaterialSpecular[1] = 0.137622f;
	MaterialSpecular[2] = 0.086014f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.1f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 11 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.24725f;
	MaterialAmbiant[1] = 0.1995f;
	MaterialAmbiant[2] = 0.0745f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.75164f;
	MaterialDiffuse[1] = 0.60648f;
	MaterialDiffuse[2] = 0.22648f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.628281f;
	MaterialSpecular[1] = 0.555802f;
	MaterialSpecular[2] = 0.366065f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.4f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 12 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.19225f;
	MaterialAmbiant[1] = 0.19225f;
	MaterialAmbiant[2] = 0.19225f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.50754f;
	MaterialDiffuse[1] = 0.50754f;
	MaterialDiffuse[2] = 0.50754f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.508273f;
	MaterialSpecular[1] = 0.508273f;
	MaterialSpecular[2] = 0.508273f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.4f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 13 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.50f;
	MaterialSpecular[1] = 0.50f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 14 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.1f;
	MaterialAmbiant[2] = 0.06f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.0f;
	MaterialDiffuse[1] = 0.50980392f;
	MaterialDiffuse[2] = 0.50980392f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.50196078f;
	MaterialSpecular[1] = 0.50196078f;
	MaterialSpecular[2] = 0.50196078f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 15 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.1f;
	MaterialDiffuse[1] = 0.35f;
	MaterialDiffuse[2] = 0.1f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.45f;
	MaterialSpecular[1] = 0.55f;
	MaterialSpecular[2] = 0.45f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 16 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.0f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.6f;
	MaterialSpecular[2] = 0.6f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;
	
	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 17 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.55f;
	MaterialDiffuse[1] = 0.55f;
	MaterialDiffuse[2] = 0.55f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.70f;
	MaterialSpecular[1] = 0.70f;
	MaterialSpecular[2] = 0.70f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 18 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.60f;
	MaterialSpecular[1] = 0.60f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 19 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.02f;
	MaterialAmbiant[1] = 0.02f;
	MaterialAmbiant[2] = 0.02f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.4f;
	MaterialSpecular[1] = 0.4f;
	MaterialSpecular[2] = 0.4f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;
	
	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 20 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 21 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 22 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.04f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 23 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 24 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

}
void update(void)
{
	angleOfXRotation = angleOfXRotation + 0.001f;
	if (angleOfXRotation >= 360.0f)
	{
		angleOfXRotation = 0.0f;
	}
	angleOfYRotation = angleOfYRotation + 0.001f;
	if (angleOfYRotation >= 360.0f)
	{
		angleOfYRotation = 0.0f;
	}
	angleOfZRotation = angleOfZRotation + 0.001f;
	if (angleOfZRotation >= 360.0f)
	{
		angleOfZRotation = 0.0f;
	}

	/*angleCube = angleCube + 0.01f;
	if (angleCube >= 360)
	{
		angleCube = 0.0f;
	}*/


}


void uninitialize(void)
{
	if (vbo_elements_sphere)
	{
		glDeleteBuffers(1, &vbo_elements_sphere);
		vbo_elements_sphere = 0;
	}

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}

	if (vbo_position_sphere)
	{
		glDeleteVertexArrays(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}



	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

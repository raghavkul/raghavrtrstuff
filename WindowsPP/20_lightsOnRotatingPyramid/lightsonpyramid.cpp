#include<Windows.h>
#include<stdio.h>
#include"vmath.h"
#include<GL/glew.h>
#pragma comment (lib,"glew32.lib")
#include<gl/GL.h>
#pragma comment (lib,"opengl32.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//namespace
using namespace vmath;

//Global variables

bool gbFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

//variables for PP
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;
GLuint vao_pyramid;//vertex array object for tri
GLuint vbo_position_pyramid;//vertex buffer object(position) for tri
GLuint vbo_normal_pyramid;//vertex buffer object(color) for tri
mat4 perspectiveProjectionMatrix;//This is from vmath


//light values
struct Light
{
	GLfloat ambiant[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position[4];
};

struct Light lights[2];
//material values
float materialAmbiant[4] = { 0.0f,0.0f,0.0f,0.0f };
float materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialShinyness = 50.0f;


bool gbLighting = false;

//Animation varibales
GLfloat anglePyramid = 0.0f;


//matrices
GLuint mUniform; // model matrix
GLuint vUniform; // view matrix
GLuint pUniform; //projection matrix

//lights
GLuint laUniformRed;
GLuint ldUniformRed;
GLuint lsUniformRed;


GLuint laUniformBlue;
GLuint ldUniformBlue;
GLuint lsUniformBlue;

//material
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShinynessUniform;


GLuint lightPositionUniformRed;
GLuint lightPositionUniformBlue;

GLuint lKeyIsPressedUniform;


//enum decleration
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};



//function
void update(void);
void vertexShaderCode(void);
void fragmentShaderCode(void);
void createProgramCode(void);
void geomentryCode(void);
void uninitialize(void);

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);

	if (fopen_s(&gpFile, "log.txt", "w+") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "LogFile Successfull created \n");
	}
	int iRet = 0;
	bool bDone = false;

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("My App");

	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("RSK_WINDOW"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed\n");
		DestroyWindow(hWnd);
	}
	if (iRet == -4)
	{
		fprintf(gpFile, "wglMakeCurrent Failed\n");
		DestroyWindow(hWnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Successfull \n");

	}
	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0X46:

			ToggleFullScreen();
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (gbLighting == false)
			{
				gbLighting = true;
			}
			else {
				gbLighting = false;
			}
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}


int initialize(void)
{
	// variables
	GLenum result;
	/*GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;
	*///functions
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghWnd);
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		return(-4);
	}

	//openGL extensions of PP
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf(gpFile, "glewInit Failed!!\n");
		uninitialize();
	}


	//GLEW initialization Code for GLSL (IMPORTANT : It must be here Means After 
	// creating OpenGL context But before using	any OpenGL function

	GLenum glew_error = glewInit();
	fprintf(gpFile, "GL_Version : %s \n", (char*)glGetString(GL_VERSION));

	fprintf(gpFile, "GL_Shading_Language_Version : %s \n", (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));


	//peace meal array initialization
	//for light 0
	lights[0].ambiant[0] = { 0.0f };
	lights[0].ambiant[1] = { 0.0f };
	lights[0].ambiant[2] = { 0.0f };
	lights[0].ambiant[3] = { 0.0f };

	lights[0].diffuse[0] = { 1.0f };
	lights[0].diffuse[1] = { 0.0f };
	lights[0].diffuse[2] = { 0.0f };
	lights[0].diffuse[3] = { 0.0f };

	lights[0].specular[0] = { 1.0f };
	lights[0].specular[1] = { 0.0f };
	lights[0].specular[2] = { 0.0f };
	lights[0].specular[3] = { 0.0f };

	lights[0].position[0] = { -2.0f };
	lights[0].position[1] = { 0.0f };
	lights[0].position[2] = { 0.0f };
	lights[0].position[3] = { 0.0f };

	//for light 1
	lights[1].ambiant[0] = { 0.0f };
	lights[1].ambiant[1] = { 0.0f };
	lights[1].ambiant[2] = { 0.0f };
	lights[1].ambiant[3] = { 0.0f };

	lights[1].diffuse[0] = { 0.0f };
	lights[1].diffuse[1] = { 0.0f };
	lights[1].diffuse[2] = { 1.0f };
	lights[1].diffuse[3] = { 0.0f };

	lights[1].specular[0] = { 0.0f };
	lights[1].specular[1] = { 0.0f };
	lights[1].specular[2] = { 1.0f };
	lights[1].specular[3] = { 0.0f };

	lights[1].position[0] = { 2.0f };
	lights[1].position[1] = { 0.0f };
	lights[1].position[2] = { 0.0f };
	lights[1].position[3] = { 0.0f };




	//call vertex shader function 
	vertexShaderCode();

	//call fragment shader function
	fragmentShaderCode();

	//call create Program code function
	createProgramCode();

	//Postlinking Retriving Uniform locations
	//Postlinking Retriving Uniform locations
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniformRed = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	ldUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
	lsUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	lightPositionUniformRed = glGetUniformLocation(gShaderProgramObject, "u_lightPosition_red");
	laUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	ldUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
	lsUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	lightPositionUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_lightPosition_blue");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShine");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");

	//call geomentry code function
	geomentryCode();


	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	//ADD CULL FACE
	//glDisable(GL_CULL_FACE);

	//Background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//Give identity toorthographicProjectMatrix
	perspectiveProjectionMatrix = mat4::identity();

	//warmup resize
	resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);

}


void vertexShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \

		"uniform int u_lKeyIsPressed;" \

		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShine;" \
		"uniform vec4 u_lightPosition_red;" \
		"uniform vec4 u_lightPosition_blue;" \
		"out vec3 phong_ads_light1;" \
		"out vec3 phong_ads_light2;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
		"vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix ) * vNormal);" \
		"vec3 lightDirection1 = normalize(vec3(u_lightPosition_red - eye_Coordinate));" \
		"vec3 lightDirection2 = normalize(vec3(u_lightPosition_blue - eye_Coordinate));" \
		"float tn_dot_ld1 = max(dot(lightDirection1,tNorm),0.0f);" \
		"float tn_dot_ld2 = max(dot(lightDirection2,tNorm),0.0f);" \
		"vec3 reflectionVector1 = reflect(-lightDirection1 , tNorm);" \
		"vec3 reflectionVector2 = reflect(-lightDirection2 , tNorm);" \
		"vec3 viwerVector = normalize(-eye_Coordinate.xyz);" \
		"vec3 ambiant1 = u_la_red * u_ka;" \
		"vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld1;" \
		"vec3 specular1 = u_ls_red * u_ks * pow(max(dot(reflectionVector1 , viwerVector),0.0f),u_materialShine);" \
		"vec3 ambiant2 = u_la_blue * u_ka;" \
		"vec3 diffuse2 = u_ld_blue * u_kd * tn_dot_ld2;" \
		"vec3 specular2 = u_ls_blue * u_ks * pow(max(dot(reflectionVector2 , viwerVector),0.0f),u_materialShine);" \
		"phong_ads_light1 = ambiant1 + diffuse1 + specular1;" \
		"phong_ads_light2 = ambiant2 + diffuse2 + specular2;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light1 = vec3(1.0f,1.0f,1.0f);" \
		"phong_ads_light2 = vec3(1.0f,1.0f,1.0f);" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}

void fragmentShaderCode(void)
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec3 phong_ads_light1;" \
		"in vec3 phong_ads_light2;" \
		"out vec4 FragColor;" \
		"uniform int u_lKeyIsPressed;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"FragColor = vec4(phong_ads_light1 + phong_ads_light2,1.0f);" \
		
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
		"}" \
		"}"
	};
	//*******************************//"FragColor = vec4(1.0,1.0,1.0,1.0);" \
		
	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}




void createProgramCode(void)
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding of vertex shader

	//----- Position
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	// ---- Normal
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking For Program Object

	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(ghWnd);
				exit(0);
			}
		}
	}

}


void geomentryCode(void)
{
	//Here we give vertex,color,texcoord,normals in arrays 
	//for triangle position
	const GLfloat pyramidVertices[] = {
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
	};
	//for triangle color
	const GLfloat pyramidNormal[] = {
		0.0f, 0.447214f, 0.89442f,
		0.0f, 0.447214f, 0.89442f,
		0.0f, 0.447214f, 0.89442f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f

	};





	//****************************** PYRAMID ****************************
	//Create vao for triangle
	glGenVertexArrays(1, &vao_pyramid);
	glBindVertexArray(vao_pyramid);

	//########### POSITION ##############
	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_position_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//########### COLOR ##############
	//Generating Buffer for triangle
	glGenBuffers(1, &vbo_normal_pyramid);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for triangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for triangle

	glBindVertexArray(0);

}
void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}



void display(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Using program object 
	glUseProgram(gShaderProgramObject);

	//declerations of matrix
	mat4 viewMatrix;
	mat4 modelMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix;
	//mat4 scaleMatrix;
	mat4 projectionMatrix;
	//*********************************** SPHERE *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -4.0f);
	rotationMatrix = rotate(0.0f, anglePyramid, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix * rotationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;
	//Send neccessary matrices to shader in respective to uniforms-
	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);



	if (gbLighting == true)
	{
		glUniform1i(lKeyIsPressedUniform, 1);

		glUniform3fv(laUniformRed, 1, lights[0].ambiant);
		glUniform3fv(ldUniformRed, 1, lights[0].diffuse);
		glUniform3fv(lsUniformRed, 1, lights[0].specular);
		glUniform4fv(lightPositionUniformRed, 1, lights[0].position);

		glUniform3fv(laUniformBlue, 1, lights[1].ambiant);
		glUniform3fv(ldUniformBlue, 1, lights[1].diffuse);
		glUniform3fv(lsUniformBlue, 1, lights[1].specular);
		glUniform4fv(lightPositionUniformBlue, 1, lights[1].position);
		
		
		glUniform3fv(kaUniform, 1, materialAmbiant);
		glUniform3fv(kdUniform, 1, materialDiffuse);
		glUniform3fv(ksUniform, 1, materialSpecular);
		glUniform1f(materialShinynessUniform, materialShinyness);

		
	}
	else {
		glUniform1i(lKeyIsPressedUniform, 0);
	}

	//Bind with vao of triangle
	glBindVertexArray(vao_pyramid);

	//Bind texture if any

	//Draw function
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glDrawArrays(GL_TRIANGLES, 3, 3);
	glDrawArrays(GL_TRIANGLES, 6, 3);
	glDrawArrays(GL_TRIANGLES, 9, 3);
	//Unbind vao of triangle
	glBindVertexArray(0);

	

	//Unused Program
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void update(void)
{
	anglePyramid = anglePyramid + 0.03f;
	if (anglePyramid >= 360)
	{
		anglePyramid = 0.0f;
	}

}


void uninitialize(void)
{
	
	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);
		vao_pyramid = 0;
	}
	if (vbo_position_pyramid)
	{
		glDeleteBuffers(1, &vbo_position_pyramid);
		vbo_position_pyramid = 0;
	}
	if (vbo_normal_pyramid)
	{
		glDeleteBuffers(1, &vbo_normal_pyramid);
		vbo_normal_pyramid = 0;
	}
	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	if (gbFullScreen == true)
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (ghdc)
	{
		ReleaseDC(ghWnd, ghdc);
		ghdc = NULL;

	}
	if (gpFile)
	{
		fprintf(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}
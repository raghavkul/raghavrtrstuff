#include<Windows.h>
#include<stdio.h>
#include<d3d11.h>
#include<d3dcompiler.h>
#pragma warning(disable:4838)
#include"XNAMath/xnamath.h" 
#include"Sphere.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

bool gbFullScreen = false;
bool gbEscapekeyIsPressed = false;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE* gpFile = NULL;
char gszLogFilename[] = "Log.txt";
HRESULT hr;

float gClearColor[4]; //RGBA
IDXGISwapChain* gpIDXGISwapChain_raghav = NULL;
ID3D11Device* gpID3D11Device_raghav = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext_raghav = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView_raghav = NULL;

ID3D11VertexShader* gpID3D11VertexShader_raghav = NULL;
ID3D11PixelShader* gpID3D11PixelShader_raghav = NULL;

ID3D11Buffer* gpID3D11Buffer_vertexBuffer_sphere_position_raghav = NULL;
ID3D11Buffer* gpID3D11Buffer_vertexBuffer_sphere_normal_raghav = NULL;

ID3D11InputLayout* gpID3D11InputLayout_raghav = NULL;
ID3D11Buffer* gpID3D11Buffer_constantBuffer_raghav = NULL;

ID3D11DepthStencilView* gpID3D11DepthStencilView_raghav = NULL;

ID3D11RasterizerState* gpID3D11RasterizerState_raghav = NULL;

//Sphere variables
ID3D11Buffer* gpID3D11Buffer_IndexBuffer = NULL;

float sphere_vertices_raghav[1146];
float sphere_normals_raghav[1146];
float sphere_texture_raghav[764];
unsigned short sphere_element_raghav[2280];
unsigned int gNumElements_raghav;
unsigned int gNumVertices_raghav;

bool gbLight = false;

struct CBUFFER
{
	XMMATRIX WorldViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR Ld;
	XMVECTOR Kd;
	XMVECTOR LightPosition;
	unsigned int KeyPressed;
};

XMMATRIX gPerspectiveProjectionMatrix;

//function
void update(void);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);
	void unitialize(void);

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("DirectX");

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "LogFile Successfull created \n");
		fclose(gpFile);
	}

	bool bDone = false;



	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("D3DWindow"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);


	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "Initialization Failed\n");
		fclose(gpFile);
		DestroyWindow(hWnd);
		hWnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "Initialization Successfull \n");
		fclose(gpFile);
	}


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_ACTIVATE:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		if (gpID3D11DeviceContext_raghav)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "resize Failed\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "resize Successfull \n");
				fclose(gpFile);
			}
		}
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0X46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else {
				ToggleFullScreen();
				gbFullScreen = false;
			}

			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (gbLight == false)
			{
				gbLight = true;
			}
			else {
				gbLight = false;
			}
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}


int initialize(void)
{
	//functions
	HRESULT resize(int, int);
	void uninitialize(void);
	HRESULT LoadD3DTexture(const wchar_t*, ID3D11ShaderResourceView * *);

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_requried = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquried = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	//code

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)& dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghWnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_requried,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain_raghav,
			&gpID3D11Device_raghav,
			&d3dFeatureLevel_acquried,
			&gpID3D11DeviceContext_raghav
		);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain Successfull \n");
			fprintf_s(gpFile, "The Chosen File is \n");
			if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
			{
				fprintf_s(gpFile, "Hardware type \n");
			}
			else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
			{
				fprintf_s(gpFile, "Wrap type \n");
			}
			else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
			{
				fprintf_s(gpFile, "Software type \n");
			}
			else {
				fprintf_s(gpFile, "Unknown Type \n");
			}

			fprintf_s(gpFile, "The support Highest level is \n");
			if (d3dFeatureLevel_acquried == D3D_FEATURE_LEVEL_11_0)
			{
				fprintf_s(gpFile, "11.0 \n");
			}
			else if (d3dFeatureLevel_acquried == D3D_FEATURE_LEVEL_10_0)
			{
				fprintf_s(gpFile, "10.0 \n");
			}
			else {
				fprintf_s(gpFile, "Unknown Type \n");
			}
			fclose(gpFile);
		}


		//initialize shader,input layout and constant buffer
		//##### Vertex Shader #######

		const char* vertexShaderSourceCode =
			"cbuffer ConstantBuffer" \
			"{" \
			"float4x4 worldViewMatrix;" \
			"float4x4 projectionMatrix;" \
			"float4 ld;" \
			"float4 kd;" \
			"float4 lightPosition;" \
			"uint keyPressed;" \
			"}" \
			"struct vertex_output" \
			"{" \
			"float4 position:SV_POSITION;" \
			"float4 diffuse_light:COLOR;" \
			"};" \
			"vertex_output main(float4 pos : POSITION,float4 normal:NORMAL)" \
			"{" \
			"vertex_output output;" \
			"if(keyPressed==1)" \
			"{" \
			"float4 eyeCoordinates=mul(worldViewMatrix,pos);" \
			"float3 tnorm = normalize(mul((float3x3)worldViewMatrix,(float3)normal));" \
			"float3 s=(float3)normalize(lightPosition-eyeCoordinates);" \
			"output.diffuse_light=ld*kd*max(dot(s,tnorm),0.0);" \
			"}" \
			"else" \
			"{" \
			"output.diffuse_light=float4(1.0,1.0,1.0,1.0);" \
			"}" \
			"output.position=mul(projectionMatrix,mul(worldViewMatrix , pos));" \
			"return(output);" \
			"}";

		ID3DBlob* pID3DBlob_vertexShaderSourceCode = NULL;
		ID3DBlob* pID3DBlob_Error = NULL;

		hr = D3DCompile(vertexShaderSourceCode,
			lstrlenA(vertexShaderSourceCode) + 1,
			"VS",
			NULL,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			"main",
			"vs_5_0",
			0,
			0,
			&pID3DBlob_vertexShaderSourceCode,
			&pID3DBlob_Error);

		if (FAILED(hr))
		{
			if (pID3DBlob_Error != NULL)
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "D3DCompile Failed in vertex shader: %s\n", (char*)pID3DBlob_Error->GetBufferPointer());
				fclose(gpFile);
				pID3DBlob_Error->Release();
				pID3DBlob_Error = NULL;
				return(hr);
			}
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3DCompile Successfull in vertex shader\n");
			fclose(gpFile);
		}

		hr = gpID3D11Device_raghav->CreateVertexShader(pID3DBlob_vertexShaderSourceCode->GetBufferPointer(),
			pID3DBlob_vertexShaderSourceCode->GetBufferSize(),
			NULL,
			&gpID3D11VertexShader_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateVertexShader Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateVertexShader Successfull in vertex shader\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->VSSetShader(gpID3D11VertexShader_raghav, 0, 0);


		//##### Pixel Shader #######

		const char* pixelShaderSourceCode =
			"float4 main(float4 position:SV_POSITION,float4 diffuse_light_color:COLOR) : SV_TARGET" \
			"{" \
			"float4 color = diffuse_light_color;" \
			"return(color);" \
			"}";

		ID3DBlob* pID3DBlob_pixelShaderSourceCode = NULL;

		hr = D3DCompile(pixelShaderSourceCode,
			lstrlenA(pixelShaderSourceCode) + 1,
			"PS",
			NULL,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			"main",
			"ps_5_0",
			0,
			0,
			&pID3DBlob_pixelShaderSourceCode,
			&pID3DBlob_Error);

		if (FAILED(hr))
		{
			if (pID3DBlob_Error != NULL)
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "D3DCompile Failed in pixel shader: %s\n", (char*)pID3DBlob_Error->GetBufferPointer());
				fclose(gpFile);
				pID3DBlob_Error->Release();
				pID3DBlob_Error = NULL;
				return(hr);
			}
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3DCompile Successfull in pixel shader\n");
			fclose(gpFile);
		}

		hr = gpID3D11Device_raghav->CreatePixelShader(pID3DBlob_pixelShaderSourceCode->GetBufferPointer(),
			pID3DBlob_pixelShaderSourceCode->GetBufferSize(),
			NULL,
			&gpID3D11PixelShader_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreatePixelShader Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreatePixelShader Successfull in pixel shader\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->PSSetShader(gpID3D11PixelShader_raghav, 0, 0);

		//######### Input Layout ##########


		D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
		inputElementDesc[0].SemanticName = "POSITION";
		inputElementDesc[0].SemanticIndex = 0;
		inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[0].AlignedByteOffset = 0;
		inputElementDesc[0].InputSlot = 0;
		inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		inputElementDesc[0].InstanceDataStepRate = 0;

		inputElementDesc[1].SemanticName = "NORMAL";
		inputElementDesc[1].SemanticIndex = 0;
		inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[1].AlignedByteOffset = 0;
		inputElementDesc[1].InputSlot = 1;
		inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		inputElementDesc[1].InstanceDataStepRate = 0;

		hr = gpID3D11Device_raghav->CreateInputLayout(inputElementDesc, _ARRAYSIZE(inputElementDesc),
			pID3DBlob_vertexShaderSourceCode->GetBufferPointer(),
			pID3DBlob_vertexShaderSourceCode->GetBufferSize(),
			&gpID3D11InputLayout_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateInputLayout Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateInputLayout Successfull \n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->IASetInputLayout(gpID3D11InputLayout_raghav);
		pID3DBlob_vertexShaderSourceCode->Release();
		pID3DBlob_vertexShaderSourceCode = NULL;
		pID3DBlob_pixelShaderSourceCode->Release();
		pID3DBlob_pixelShaderSourceCode = NULL;

		//###### Pass Arrays ########
		getSphereVertexData(sphere_vertices_raghav, sphere_normals_raghav, sphere_texture_raghav, sphere_element_raghav);
		gNumVertices_raghav = getNumberOfSphereVertices();
		gNumElements_raghav = getNumberOfSphereElements();
		//create vertex buffer

		D3D11_BUFFER_DESC bufferDesc_vertexBuffer;

		////Rectangle
		ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc_vertexBuffer.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices_raghav);
		bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_vertexBuffer_sphere_position_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
		}

		//copy vertices in buffer
		D3D11_MAPPED_SUBRESOURCE mappedSubResource;

		ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext_raghav->Map(gpID3D11Buffer_vertexBuffer_sphere_position_raghav, 0,
			D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
		memcpy(mappedSubResource.pData, sphere_vertices_raghav, sizeof(sphere_vertices_raghav));
		gpID3D11DeviceContext_raghav->Unmap(gpID3D11Buffer_vertexBuffer_sphere_position_raghav, 0);


		//for normal
		ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc_vertexBuffer.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals_raghav);
		bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_vertexBuffer_sphere_normal_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
		}

		//cpoy vertices in buffer
		ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext_raghav->Map(gpID3D11Buffer_vertexBuffer_sphere_normal_raghav, 0,
			D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
		memcpy(mappedSubResource.pData, sphere_normals_raghav, sizeof(sphere_normals_raghav));
		gpID3D11DeviceContext_raghav->Unmap(gpID3D11Buffer_vertexBuffer_sphere_normal_raghav, 0);


		//index buffer for elements
		ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc_vertexBuffer.ByteWidth = gNumElements_raghav * sizeof(short);
		bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_IndexBuffer);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
		}

		//cpoy vertices in buffer
		ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext_raghav->Map(gpID3D11Buffer_IndexBuffer, 0,
			D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
		memcpy(mappedSubResource.pData, sphere_element_raghav, gNumElements_raghav * sizeof(short));
		gpID3D11DeviceContext_raghav->Unmap(gpID3D11Buffer_IndexBuffer, 0);


		//define and set constant buffer
		D3D11_BUFFER_DESC bufferDesc_constantBuffer;
		ZeroMemory(&bufferDesc_constantBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_constantBuffer.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc_constantBuffer.ByteWidth = sizeof(CBUFFER);
		bufferDesc_constantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_constantBuffer, 0, &gpID3D11Buffer_constantBuffer_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_constantBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_constantBuffer \n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_constantBuffer_raghav);

		D3D11_RASTERIZER_DESC rasterizerDesc;
		ZeroMemory((void*)& rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.CullMode = D3D11_CULL_NONE;
		rasterizerDesc.DepthBias = 0;
		rasterizerDesc.DepthBiasClamp = 0.0f;
		rasterizerDesc.SlopeScaledDepthBias = 0.0f;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.FillMode = D3D11_FILL_SOLID;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.MultisampleEnable = FALSE;
		rasterizerDesc.ScissorEnable = FALSE;

		hr = gpID3D11Device_raghav->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateRasterizerState Failed for bufferDesc_constantBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateRasterizerState Successfull for bufferDesc_constantBuffer \n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->RSSetState(gpID3D11RasterizerState_raghav);

		gClearColor[0] = 0.0f;
		gClearColor[1] = 0.0f;
		gClearColor[2] = 0.0f;
		gClearColor[3] = 1.0f;
	}

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "resize Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "resize Successfull \n");
		fclose(gpFile);
	}
	return(S_OK);

}

HRESULT resize(int width, int height)
{
	if (gpID3D11DepthStencilView_raghav)
	{
		gpID3D11DepthStencilView_raghav->Release();
		gpID3D11DepthStencilView_raghav = NULL;
	}

	//free any size-depedant resource
	if (gpID3D11RenderTargetView_raghav)
	{
		gpID3D11RenderTargetView_raghav->Release();
		gpID3D11RenderTargetView_raghav = NULL;
	}

	//resize swap chain buffer
	gpIDXGISwapChain_raghav->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//agian get back buffer to swap chain
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain_raghav->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)& pID3D11Texture2D_BackBuffer);

	//again set render target view from d3d11 device using above back buffer
	hr = gpID3D11Device_raghav->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_raghav);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreaterenderTargetView Failed\n");
		fclose(gpFile);
		return(S_OK);
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "resize Successfull \n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;


	//create depth stencil buffer
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MipLevels = 0;

	ID3D11Texture2D* pID3D11Texture2D_DepthBuffer;
	gpID3D11Device_raghav->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device_raghav->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView_raghav);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView Failed\n");
		fclose(gpFile);
		return(S_OK);
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "CreateDepthStencilView Successfull \n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;
	//set render target view
	gpID3D11DeviceContext_raghav->OMSetRenderTargets(1, &gpID3D11RenderTargetView_raghav, gpID3D11DepthStencilView_raghav);


	//set Viewport
	D3D11_VIEWPORT d3d11ViewPort;
	d3d11ViewPort.TopLeftX = 0;
	d3d11ViewPort.TopLeftY = 0;
	d3d11ViewPort.Width = (float)width;
	d3d11ViewPort.Height = (float)height;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);

	//set ortho matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);
	return(S_OK);
}

void display(void)
{
	//code
	gpID3D11DeviceContext_raghav->ClearRenderTargetView(gpID3D11RenderTargetView_raghav, gClearColor);

	gpID3D11DeviceContext_raghav->ClearDepthStencilView(gpID3D11DepthStencilView_raghav, D3D11_CLEAR_DEPTH, 1.0f, 0);


	//transformatiion
	XMMATRIX worlViewdMatrix = XMMatrixIdentity();
	XMMATRIX trnslationMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();

	//vertex buffer to set
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext_raghav->IASetVertexBuffers(0, 1, &gpID3D11Buffer_vertexBuffer_sphere_position_raghav, &stride, &offset);

	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext_raghav->IASetVertexBuffers(1, 1, &gpID3D11Buffer_vertexBuffer_sphere_normal_raghav, &stride, &offset);

	gpID3D11DeviceContext_raghav->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	//select primitive topology
	gpID3D11DeviceContext_raghav->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	//transformatiion
	worlViewdMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 5.0f);
	worlViewdMatrix = trnslationMatrix;

	//load data into constant buffer
	CBUFFER constantBuffer;
	//load data into constant buffer
	constantBuffer.WorldViewMatrix = worlViewdMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	if (gbLight == true)
	{
		constantBuffer.KeyPressed = 1;
		constantBuffer.Ld = XMVectorSet(1.0, 1.0, 1.0, 1.0);
		constantBuffer.Kd = XMVectorSet(0.5, 0.5, 0.5, 1.0);
		constantBuffer.LightPosition = XMVectorSet(0.0, 0.0, -2.0, 1.0);
	}
	else {
		constantBuffer.KeyPressed = 0;
	}
	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);

	//switch between front and back buffer
	gpIDXGISwapChain_raghav->Present(0, 0);
}

void update(void)
{
	//code
}


void uninitialize(void)
{
	//code
	if (gpID3D11Buffer_IndexBuffer)
	{
		gpID3D11Buffer_IndexBuffer->Release();
		gpID3D11Buffer_IndexBuffer = NULL;
	}
	if (gpID3D11Buffer_constantBuffer_raghav)
	{
		gpID3D11Buffer_constantBuffer_raghav->Release();
		gpID3D11Buffer_constantBuffer_raghav = NULL;
	}

	if (gpID3D11Buffer_vertexBuffer_sphere_position_raghav)
	{
		gpID3D11Buffer_vertexBuffer_sphere_position_raghav->Release();
		gpID3D11Buffer_vertexBuffer_sphere_position_raghav = NULL;
	}

	if (gpID3D11Buffer_vertexBuffer_sphere_normal_raghav)
	{
		gpID3D11Buffer_vertexBuffer_sphere_normal_raghav->Release();
		gpID3D11Buffer_vertexBuffer_sphere_normal_raghav = NULL;
	}
	if (gpID3D11InputLayout_raghav)
	{
		gpID3D11InputLayout_raghav->Release();
		gpID3D11InputLayout_raghav = NULL;
	}
	if (gpID3D11PixelShader_raghav)
	{
		gpID3D11PixelShader_raghav->Release();
		gpID3D11PixelShader_raghav = NULL;
	}
	if (gpID3D11VertexShader_raghav)
	{
		gpID3D11VertexShader_raghav->Release();
		gpID3D11VertexShader_raghav = NULL;
	}
	if (gpID3D11RenderTargetView_raghav)
	{
		gpID3D11RenderTargetView_raghav->Release();
		gpID3D11RenderTargetView_raghav = NULL;
	}
	if (gpID3D11RasterizerState_raghav)
	{
		gpID3D11RasterizerState_raghav->Release();
		gpID3D11RasterizerState_raghav = NULL;
	}
	if (gpID3D11DepthStencilView_raghav)
	{
		gpID3D11DepthStencilView_raghav->Release();
		gpID3D11DepthStencilView_raghav = NULL;
	}
	if (gpIDXGISwapChain_raghav)
	{
		gpIDXGISwapChain_raghav->Release();
		gpIDXGISwapChain_raghav = NULL;
	}
	if (gpID3D11DeviceContext_raghav)
	{
		gpID3D11DeviceContext_raghav->Release();
		gpID3D11DeviceContext_raghav = NULL;
	}
	if (gpID3D11Device_raghav)
	{
		gpID3D11Device_raghav->Release();
		gpID3D11Device_raghav = NULL;
	}
	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

#include<Windows.h>
#include<stdio.h>
#include<d3d11.h>
#include<d3dcompiler.h>
#pragma warning(disable:4838)
#include"XNAMath/xnamath.h" 
#include"Sphere.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

bool gbFullScreen = false;
bool gbEscapekeyIsPressed = false;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE* gpFile = NULL;
char gszLogFilename[] = "Log.txt";
HRESULT hr;

float gClearColor[4]; //RGBA
IDXGISwapChain* gpIDXGISwapChain_raghav = NULL;
ID3D11Device* gpID3D11Device_raghav = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext_raghav = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView_raghav = NULL;

ID3D11VertexShader* gpID3D11VertexShader_raghav = NULL;
ID3D11PixelShader* gpID3D11PixelShader_raghav = NULL;

ID3D11Buffer* gpID3D11Buffer_vertexBuffer_sphere_position_raghav = NULL;
ID3D11Buffer* gpID3D11Buffer_vertexBuffer_sphere_normal_raghav = NULL;

ID3D11InputLayout* gpID3D11InputLayout_raghav = NULL;
ID3D11Buffer* gpID3D11Buffer_constantBuffer_raghav = NULL;

ID3D11DepthStencilView* gpID3D11DepthStencilView_raghav = NULL;

ID3D11RasterizerState* gpID3D11RasterizerState_raghav = NULL;

//Sphere variables
ID3D11Buffer* gpID3D11Buffer_IndexBuffer = NULL;

float sphere_vertices_raghav[1146];
float sphere_normals_raghav[1146];
float sphere_texture_raghav[764];
unsigned short sphere_element_raghav[2280];
unsigned int gNumElements_raghav;
unsigned int gNumVertices_raghav;

bool gbLight = false;


int xWindowCenter = GetSystemMetrics(SM_CXSCREEN) / 2;
int yWindowCenter = GetSystemMetrics(SM_CYSCREEN) / 2;


int distorasionX = GetSystemMetrics(SM_CXSCREEN) / 6;
int distorasionY = GetSystemMetrics(SM_CYSCREEN) / 8;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;

	XMVECTOR La;
	XMVECTOR Ld;
	XMVECTOR Ls;
	XMVECTOR LightPosition;

	XMVECTOR Ka;
	XMVECTOR Kd;
	XMVECTOR Ks;
	float Matrial_Shinyness;

	unsigned int KeyPressed;
};

XMMATRIX gPerspectiveProjectionMatrix;

float lightAmbiant[] = { 0.0,0.0,0.0,1.0 };
float lightDiffuse[] = { 1.0,1.0,1.0,1.0 };
float lightSpecular[] = { 1.0,1.0,1.0,1.0 };
float lightPosition[] = { 0.0,0.0,0.0,1.0 };


//Animation varibales
float angleOfXRotation = 0.0f;
float angleOfYRotation = 0.0f;
float angleOfZRotation = 0.0f;

int keyPress = 0;

//function
void update(void);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	int initialize(void);
	void display(void);
	void unitialize(void);

	WNDCLASSEX WndClass;
	HWND hWnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("DirectX");

	if (fopen_s(&gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("LogFile can not be Created!!"), TEXT("ERROR"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "LogFile Successfull created \n");
		fclose(gpFile);
	}

	bool bDone = false;



	WndClass.cbSize = sizeof(WNDCLASSEX);
	WndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WndClass.cbClsExtra = 0;
	WndClass.cbWndExtra = 0;
	WndClass.lpfnWndProc = WndProc;
	WndClass.hInstance = hInstance;
	WndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	WndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	WndClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	WndClass.lpszClassName = szAppName;
	WndClass.lpszMenuName = NULL;

	RegisterClassEx(&WndClass);

	hWnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("D3DWindow"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		100,
		100,
		800,
		600,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	if (hWnd == NULL)
	{
		MessageBox(NULL, TEXT("Error in Creating Window"), TEXT("Error"), MB_OK);
		exit(0);
	}
	ghWnd = hWnd;

	ShowWindow(hWnd, iCmdShow);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);


	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "Initialization Failed\n");
		fclose(gpFile);
		DestroyWindow(hWnd);
		hWnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "Initialization Successfull \n");
		fclose(gpFile);
	}


	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
			}
			display();

		}
	}
	return((int)msg.wParam);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HRESULT resize(int, int);
	void display(void);
	void uninitialize(void);
	void ToggleFullScreen();
	switch (iMsg)
	{
	case WM_ACTIVATE:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		if (gpID3D11DeviceContext_raghav)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "resize Failed\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "resize Successfull \n");
				fclose(gpFile);
			}
		}
		break;
	case WM_ERASEBKGND:
		return(0);
		break;
	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hWnd);
			break;

		case 0X46:
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else {
				ToggleFullScreen();
				gbFullScreen = false;
			}

			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			if (gbLight == false)
			{
				gbLight = true;
			}
			else {
				gbLight = false;
			}
			break;
		case'x':
		case'X':
			keyPress = 1;
			angleOfXRotation = 0.0f;
			break;
		case'y':
		case'Y':
			keyPress = 2;
			angleOfXRotation = 0.0f;
			break;
		case'z':
		case'Z':
			keyPress = 3;
			angleOfXRotation = 0.0f;
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hWnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO mi;
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER |
					SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}


int initialize(void)
{
	//functions
	HRESULT resize(int, int);
	void uninitialize(void);
	HRESULT LoadD3DTexture(const wchar_t*, ID3D11ShaderResourceView * *);

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {
		D3D_DRIVER_TYPE_HARDWARE,D3D_DRIVER_TYPE_WARP,D3D_DRIVER_TYPE_REFERENCE,
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_requried = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquried = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	//code

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void*)& dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghWnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_requried,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain_raghav,
			&gpID3D11Device_raghav,
			&d3dFeatureLevel_acquried,
			&gpID3D11DeviceContext_raghav
		);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain Successfull \n");
			fprintf_s(gpFile, "The Chosen File is \n");
			if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
			{
				fprintf_s(gpFile, "Hardware type \n");
			}
			else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
			{
				fprintf_s(gpFile, "Wrap type \n");
			}
			else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
			{
				fprintf_s(gpFile, "Software type \n");
			}
			else {
				fprintf_s(gpFile, "Unknown Type \n");
			}

			fprintf_s(gpFile, "The support Highest level is \n");
			if (d3dFeatureLevel_acquried == D3D_FEATURE_LEVEL_11_0)
			{
				fprintf_s(gpFile, "11.0 \n");
			}
			else if (d3dFeatureLevel_acquried == D3D_FEATURE_LEVEL_10_0)
			{
				fprintf_s(gpFile, "10.0 \n");
			}
			else {
				fprintf_s(gpFile, "Unknown Type \n");
			}
			fclose(gpFile);
		}


		//initialize shader,input layout and constant buffer
		//##### Vertex Shader #######

		const char* vertexShaderSourceCode =
			"cbuffer ConstantBuffer" \
			"{" \
			"float4x4 worldMatrix;" \
			"float4x4 viewMatrix;" \
			"float4x4 projectionMatrix;" \
			"float4 la;" \
			"float4 ld;" \
			"float4 ls;" \
			"float4 lightPosition;" \
			"float4 ka;" \
			"float4 kd;" \
			"float4 ks;" \
			"float material_shinyness;" \
			"uint keyPressed;" \
			"}" \
			"struct vertex_output" \
			"{" \
			"float4 Position:SV_POSITION;" \
			"float3 transformedNormal:NORMAL0;" \
			"float3 lightDirection:NORMAL1;" \
			"float3 viewerVector:NORMAL2;" \
			"};" \
			"vertex_output main(float4 pos : POSITION,float4 normal:NORMAL)" \
			"{" \
			"vertex_output output;" \
			"if(keyPressed==1)" \
			"{" \
			"float4 eyeCoordinates=mul(worldMatrix,pos);" \
			"eyeCoordinates=mul(viewMatrix,eyeCoordinates);" \
			"output.transformedNormal = mul(mul((float3x3)worldMatrix,(float3x3)viewMatrix),(float3)normal);" \
			"output.lightDirection=(float3)lightPosition-(float3)eyeCoordinates;" \
			"output.viewerVector=-eyeCoordinates.xyz;" \
			"}" \
			"float4 position = mul(worldMatrix , pos);" \
			"position = mul(viewMatrix , position);" \
			"position = mul(projectionMatrix , position);" \
			"output.Position=position;" \
			"return(output);" \
			"}";

		ID3DBlob* pID3DBlob_vertexShaderSourceCode = NULL;
		ID3DBlob* pID3DBlob_Error = NULL;

		hr = D3DCompile(vertexShaderSourceCode,
			lstrlenA(vertexShaderSourceCode) + 1,
			"VS",
			NULL,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			"main",
			"vs_5_0",
			0,
			0,
			&pID3DBlob_vertexShaderSourceCode,
			&pID3DBlob_Error);

		if (FAILED(hr))
		{
			if (pID3DBlob_Error != NULL)
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "D3DCompile Failed in vertex shader: %s\n", (char*)pID3DBlob_Error->GetBufferPointer());
				fclose(gpFile);
				pID3DBlob_Error->Release();
				pID3DBlob_Error = NULL;
				return(hr);
			}
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3DCompile Successfull in vertex shader\n");
			fclose(gpFile);
		}

		hr = gpID3D11Device_raghav->CreateVertexShader(pID3DBlob_vertexShaderSourceCode->GetBufferPointer(),
			pID3DBlob_vertexShaderSourceCode->GetBufferSize(),
			NULL,
			&gpID3D11VertexShader_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateVertexShader Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateVertexShader Successfull in vertex shader\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->VSSetShader(gpID3D11VertexShader_raghav, 0, 0);


		//##### Pixel Shader #######

		const char* pixelShaderSourceCode =
			"cbuffer ConstantBuffer" \
			"{" \
			"float4x4 worldMatrix;" \
			"float4x4 viewMatrix;" \
			"float4x4 projectionMatrix;" \
			"float4 la;" \
			"float4 ld;" \
			"float4 ls;" \
			"float4 lightPosition;" \
			"float4 ka;" \
			"float4 kd;" \
			"float4 ks;" \
			"float material_shinyness;" \
			"uint keyPressed;" \
			"}" \
			"struct vertex_output" \
			"{" \
			"float4 Position:SV_POSITION;" \
			"float3 transformedNormal:NORMAL0;" \
			"float3 lightDirection:NORMAL1;" \
			"float3 viewerVector:NORMAL2;" \
			"};" \
			"float4 main(float4 position:SV_POSITION,float3 transformedNormal:NORMAL0,float3 lightDirection:NORMAL1,float3 viewerVector:NORMAL2) : SV_TARGET" \
			"{" \
			"float4 color;" \
			"if(keyPressed==1)" \
			"{" \
			"float3 tNorm = normalize(transformedNormal);" \
			"float3 normalizeLightDirection = normalize(lightDirection);" \
			"float3 normalizeViwerVector = normalize(viewerVector);" \
			"float tn_dot_ld=max(dot(normalizeLightDirection,tNorm),0.0);" \
			"float4 ambiant=la*ka;" \
			"float4 diffuse=ld*kd*tn_dot_ld;" \
			"float3 reflectionVector=reflect(-normalizeLightDirection,tNorm);" \
			"float4 specular=ls*ks*pow(max(dot(reflectionVector,normalizeViwerVector),0.0),material_shinyness);" \
			"float4 phong_ads_color=ambiant + diffuse +specular;" \
			"return color=phong_ads_color;" \
			"}" \
			"else" \
			"{" \
			"return color=float4(1.0,1.0,1.0,1.0);" \
			"}" \
			"}";

		ID3DBlob* pID3DBlob_pixelShaderSourceCode = NULL;

		hr = D3DCompile(pixelShaderSourceCode,
			lstrlenA(pixelShaderSourceCode) + 1,
			"PS",
			NULL,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			"main",
			"ps_5_0",
			0,
			0,
			&pID3DBlob_pixelShaderSourceCode,
			&pID3DBlob_Error);

		if (FAILED(hr))
		{
			if (pID3DBlob_Error != NULL)
			{
				fopen_s(&gpFile, gszLogFilename, "a+");
				fprintf_s(gpFile, "D3DCompile Failed in pixel shader: %s\n", (char*)pID3DBlob_Error->GetBufferPointer());
				fclose(gpFile);
				pID3DBlob_Error->Release();
				pID3DBlob_Error = NULL;
				return(hr);
			}
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "D3DCompile Successfull in pixel shader\n");
			fclose(gpFile);
		}

		hr = gpID3D11Device_raghav->CreatePixelShader(pID3DBlob_pixelShaderSourceCode->GetBufferPointer(),
			pID3DBlob_pixelShaderSourceCode->GetBufferSize(),
			NULL,
			&gpID3D11PixelShader_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreatePixelShader Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreatePixelShader Successfull in pixel shader\n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->PSSetShader(gpID3D11PixelShader_raghav, 0, 0);

		//######### Input Layout ##########


		D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
		inputElementDesc[0].SemanticName = "POSITION";
		inputElementDesc[0].SemanticIndex = 0;
		inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[0].AlignedByteOffset = 0;
		inputElementDesc[0].InputSlot = 0;
		inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		inputElementDesc[0].InstanceDataStepRate = 0;

		inputElementDesc[1].SemanticName = "NORMAL";
		inputElementDesc[1].SemanticIndex = 0;
		inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
		inputElementDesc[1].AlignedByteOffset = 0;
		inputElementDesc[1].InputSlot = 1;
		inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		inputElementDesc[1].InstanceDataStepRate = 0;

		hr = gpID3D11Device_raghav->CreateInputLayout(inputElementDesc, _ARRAYSIZE(inputElementDesc),
			pID3DBlob_vertexShaderSourceCode->GetBufferPointer(),
			pID3DBlob_vertexShaderSourceCode->GetBufferSize(),
			&gpID3D11InputLayout_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateInputLayout Failed\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateInputLayout Successfull \n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->IASetInputLayout(gpID3D11InputLayout_raghav);
		pID3DBlob_vertexShaderSourceCode->Release();
		pID3DBlob_vertexShaderSourceCode = NULL;
		pID3DBlob_pixelShaderSourceCode->Release();
		pID3DBlob_pixelShaderSourceCode = NULL;

		//###### Pass Arrays ########
		getSphereVertexData(sphere_vertices_raghav, sphere_normals_raghav, sphere_texture_raghav, sphere_element_raghav);
		gNumVertices_raghav = getNumberOfSphereVertices();
		gNumElements_raghav = getNumberOfSphereElements();
		//create vertex buffer

		D3D11_BUFFER_DESC bufferDesc_vertexBuffer;

		////Rectangle
		ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc_vertexBuffer.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices_raghav);
		bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_vertexBuffer_sphere_position_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
		}

		//copy vertices in buffer
		D3D11_MAPPED_SUBRESOURCE mappedSubResource;

		ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext_raghav->Map(gpID3D11Buffer_vertexBuffer_sphere_position_raghav, 0,
			D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
		memcpy(mappedSubResource.pData, sphere_vertices_raghav, sizeof(sphere_vertices_raghav));
		gpID3D11DeviceContext_raghav->Unmap(gpID3D11Buffer_vertexBuffer_sphere_position_raghav, 0);


		//for normal
		ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc_vertexBuffer.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals_raghav);
		bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_vertexBuffer_sphere_normal_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
		}

		//cpoy vertices in buffer
		ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext_raghav->Map(gpID3D11Buffer_vertexBuffer_sphere_normal_raghav, 0,
			D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
		memcpy(mappedSubResource.pData, sphere_normals_raghav, sizeof(sphere_normals_raghav));
		gpID3D11DeviceContext_raghav->Unmap(gpID3D11Buffer_vertexBuffer_sphere_normal_raghav, 0);


		//index buffer for elements
		ZeroMemory(&bufferDesc_vertexBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_vertexBuffer.Usage = D3D11_USAGE_DYNAMIC;
		bufferDesc_vertexBuffer.ByteWidth = gNumElements_raghav * sizeof(short);
		bufferDesc_vertexBuffer.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bufferDesc_vertexBuffer.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_vertexBuffer, 0, &gpID3D11Buffer_IndexBuffer);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_vertexBuffer\n");
			fclose(gpFile);
		}

		//cpoy vertices in buffer
		ZeroMemory(&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext_raghav->Map(gpID3D11Buffer_IndexBuffer, 0,
			D3D11_MAP_WRITE_DISCARD, 0, &mappedSubResource);
		memcpy(mappedSubResource.pData, sphere_element_raghav, gNumElements_raghav * sizeof(short));
		gpID3D11DeviceContext_raghav->Unmap(gpID3D11Buffer_IndexBuffer, 0);


		//define and set constant buffer
		D3D11_BUFFER_DESC bufferDesc_constantBuffer;
		ZeroMemory(&bufferDesc_constantBuffer, sizeof(D3D11_BUFFER_DESC));
		bufferDesc_constantBuffer.Usage = D3D11_USAGE_DEFAULT;
		bufferDesc_constantBuffer.ByteWidth = sizeof(CBUFFER);
		bufferDesc_constantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

		hr = gpID3D11Device_raghav->CreateBuffer(&bufferDesc_constantBuffer, 0, &gpID3D11Buffer_constantBuffer_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Failed for bufferDesc_constantBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateBuffer Successfull for bufferDesc_constantBuffer \n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_constantBuffer_raghav);
		gpID3D11DeviceContext_raghav->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_constantBuffer_raghav);

		D3D11_RASTERIZER_DESC rasterizerDesc;
		ZeroMemory((void*)& rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
		rasterizerDesc.AntialiasedLineEnable = FALSE;
		rasterizerDesc.CullMode = D3D11_CULL_NONE;
		rasterizerDesc.DepthBias = 0;
		rasterizerDesc.DepthBiasClamp = 0.0f;
		rasterizerDesc.SlopeScaledDepthBias = 0.0f;
		rasterizerDesc.DepthClipEnable = TRUE;
		rasterizerDesc.FillMode = D3D11_FILL_SOLID;
		rasterizerDesc.FrontCounterClockwise = FALSE;
		rasterizerDesc.MultisampleEnable = FALSE;
		rasterizerDesc.ScissorEnable = FALSE;

		hr = gpID3D11Device_raghav->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState_raghav);

		if (FAILED(hr))
		{
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateRasterizerState Failed for bufferDesc_constantBuffer\n");
			fclose(gpFile);
			return(hr);
		}
		else {
			fopen_s(&gpFile, gszLogFilename, "a+");
			fprintf_s(gpFile, "gpID3D11Device_raghav:CreateRasterizerState Successfull for bufferDesc_constantBuffer \n");
			fclose(gpFile);
		}

		gpID3D11DeviceContext_raghav->RSSetState(gpID3D11RasterizerState_raghav);

		gClearColor[0] = 0.0f;
		gClearColor[1] = 0.0f;
		gClearColor[2] = 0.0f;
		gClearColor[3] = 1.0f;
	}

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "resize Failed\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "resize Successfull \n");
		fclose(gpFile);
	}
	return(S_OK);

}

HRESULT resize(int width, int height)
{
	/*int xWindowCenter = GetSystemMetrics(SM_CXSCREEN) / 2;
	fprintf(gpFile, " width : %d\n", xWindowCenter);
	int yWindowCenter = GetSystemMetrics(SM_CYSCREEN) / 2;
	fprintf(gpFile, " height : %d\n", yWindowCenter);

	int distorasionX = GetSystemMetrics(SM_CXSCREEN) / 6;
	fprintf(gpFile, "  distorsion width : %d\n", distorasionX);
	int distorasionY = GetSystemMetrics(SM_CYSCREEN) / 8;
	fprintf(gpFile, " distorsion height : %d\n", distorasionY);
	*/

	if (gpID3D11DepthStencilView_raghav)
	{
		gpID3D11DepthStencilView_raghav->Release();
		gpID3D11DepthStencilView_raghav = NULL;
	}

	//free any size-depedant resource
	if (gpID3D11RenderTargetView_raghav)
	{
		gpID3D11RenderTargetView_raghav->Release();
		gpID3D11RenderTargetView_raghav = NULL;
	}

	//resize swap chain buffer
	gpIDXGISwapChain_raghav->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//agian get back buffer to swap chain
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain_raghav->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)& pID3D11Texture2D_BackBuffer);

	//again set render target view from d3d11 device using above back buffer
	hr = gpID3D11Device_raghav->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView_raghav);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreaterenderTargetView Failed\n");
		fclose(gpFile);
		return(S_OK);
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "resize Successfull \n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;


	//create depth stencil buffer
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MipLevels = 0;

	ID3D11Texture2D* pID3D11Texture2D_DepthBuffer;
	gpID3D11Device_raghav->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;

	hr = gpID3D11Device_raghav->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView_raghav);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView Failed\n");
		fclose(gpFile);
		return(S_OK);
	}
	else
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "CreateDepthStencilView Successfull \n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;
	//set render target view
	gpID3D11DeviceContext_raghav->OMSetRenderTargets(1, &gpID3D11RenderTargetView_raghav, gpID3D11DepthStencilView_raghav);


	//set Viewport
	D3D11_VIEWPORT d3d11ViewPort;
	d3d11ViewPort.TopLeftX = 0;
	d3d11ViewPort.TopLeftY = 0;
	d3d11ViewPort.Width = (float)width;
	d3d11ViewPort.Height = (float)height;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);

	//set ortho matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)distorasionX / (float)distorasionY, 0.1f, 100.0f);
	return(S_OK);
}

void display(void)
{



	int xTransOffset = distorasionX;
	int yTransOffset = distorasionY;

	float currentViewportX = 0.0f;
	float currentViewportY = 0.0f;

	float MaterialAmbiant[4];
	float MaterialDiffuse[4];
	float MaterialSpecular[4];
	float MaterialShininess[1];


	//code
	gpID3D11DeviceContext_raghav->ClearRenderTargetView(gpID3D11RenderTargetView_raghav, gClearColor);

	gpID3D11DeviceContext_raghav->ClearDepthStencilView(gpID3D11DepthStencilView_raghav, D3D11_CLEAR_DEPTH, 1.0f, 0);


	//vertex buffer to set
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext_raghav->IASetVertexBuffers(0, 1, &gpID3D11Buffer_vertexBuffer_sphere_position_raghav, &stride, &offset);

	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext_raghav->IASetVertexBuffers(1, 1, &gpID3D11Buffer_vertexBuffer_sphere_normal_raghav, &stride, &offset);

	gpID3D11DeviceContext_raghav->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	//select primitive topology
	gpID3D11DeviceContext_raghav->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	//load data into constant buffer
	CBUFFER constantBuffer;

	if (gbLight == true)
	{
		constantBuffer.KeyPressed = 1;

		constantBuffer.La = XMVectorSet(lightAmbiant[0], lightAmbiant[1], lightAmbiant[2], lightAmbiant[3]);
		constantBuffer.Ld = XMVectorSet(lightDiffuse[0], lightDiffuse[1], lightDiffuse[2], lightDiffuse[3]);
		constantBuffer.Ls = XMVectorSet(lightSpecular[0], lightSpecular[1], lightSpecular[2], lightSpecular[3]);
		if (keyPress == 1)
		{
			lightPosition[1] = cos(angleOfXRotation) * 100.0f;
			lightPosition[2] = -sin(angleOfXRotation) * 100.0f;
			constantBuffer.LightPosition = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);
		}
		else if (keyPress == 2)
		{
			lightPosition[0] = cos(angleOfYRotation) * 100.0f;
			lightPosition[2] = -sin(angleOfYRotation) * 100.0f;

			constantBuffer.LightPosition = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);
		}
		else if (keyPress == 3)
		{
			lightPosition[0] = cos(angleOfZRotation) * 100.0f;
			lightPosition[1] = sin(angleOfZRotation) * 100.0f;
			constantBuffer.LightPosition = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);
		}
	}
	else {
		constantBuffer.KeyPressed = 0;
	}

	//1st coloum
	//transformatiion
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX projectionMatrix = XMMatrixIdentity();
	XMMATRIX trnslationMatrix = XMMatrixIdentity();
	XMMATRIX rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	//set Viewport
	D3D11_VIEWPORT d3d11ViewPort;

	currentViewportX = xWindowCenter - xTransOffset * 2.5f;
	currentViewportY = yWindowCenter + yTransOffset * -2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0215f;
	MaterialAmbiant[1] = 0.1745f;
	MaterialAmbiant[2] = 0.0215f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.07568f;
	MaterialDiffuse[1] = 0.61424f;
	MaterialDiffuse[2] = 0.0215f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.633f;
	MaterialSpecular[1] = 0.727811f;
	MaterialSpecular[2] = 0.633f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);



	//transformatiion
	 worldMatrix = XMMatrixIdentity();
	 viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	 trnslationMatrix = XMMatrixIdentity();
	 rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 2.5f;
	currentViewportY = yWindowCenter + yTransOffset * -1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.135f;
	MaterialAmbiant[1] = 0.2225f;
	MaterialAmbiant[2] = 0.1575f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.54f;
	MaterialDiffuse[1] = 0.89f;
	MaterialDiffuse[2] = 0.63f;
	MaterialDiffuse[3] = 1.0f;


	MaterialSpecular[0] = 0.316228f;
	MaterialSpecular[1] = 0.316228f;
	MaterialSpecular[2] = 0.316228f;
	MaterialSpecular[3] = 1.0f;


	MaterialShininess[0] = 0.1f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 2.5f;
	currentViewportY = yWindowCenter + yTransOffset * -0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.5375f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.06625f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.18275f;
	MaterialDiffuse[1] = 0.17f;
	MaterialDiffuse[2] = 0.22525f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.332741f;
	MaterialSpecular[1] = 0.328634f;
	MaterialSpecular[2] = 0.346435f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.3f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 2.5f;
	currentViewportY = yWindowCenter + yTransOffset * 0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.20725f;
	MaterialAmbiant[2] = 0.20725f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 1.0f;
	MaterialDiffuse[1] = 0.829f;
	MaterialDiffuse[2] = 0.829f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.296648f;
	MaterialSpecular[1] = 0.296648f;
	MaterialSpecular[2] = 0.296648f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.088f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 2.5f;
	currentViewportY = yWindowCenter + yTransOffset * 1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.1745f;
	MaterialAmbiant[1] = 0.01175f;
	MaterialAmbiant[2] = 0.01175f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.61424f;
	MaterialDiffuse[1] = 0.04136f;
	MaterialDiffuse[2] = 0.04136f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.727811f;
	MaterialSpecular[1] = 0.626959f;
	MaterialSpecular[2] = 0.626959f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 2.5f;
	currentViewportY = yWindowCenter + yTransOffset * 2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.1f;
	MaterialAmbiant[1] = 0.18725f;
	MaterialAmbiant[2] = 0.1745f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.396f;
	MaterialDiffuse[1] = 0.74151f;
	MaterialDiffuse[2] = 0.69102f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.297254f;
	MaterialSpecular[1] = 0.30829f;
	MaterialSpecular[2] = 0.306678f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.1f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//2nd coloum
	 worldMatrix = XMMatrixIdentity();
	 viewMatrix = XMMatrixIdentity();
	 projectionMatrix = XMMatrixIdentity();
	 trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 1.0f;
	currentViewportY = yWindowCenter + yTransOffset * -2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.329412f;
	MaterialAmbiant[1] = 0.223529f;
	MaterialAmbiant[2] = 0.27451f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.780392f;
	MaterialDiffuse[1] = 0.568627f;
	MaterialDiffuse[2] = 0.113725f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.992157f;
	MaterialSpecular[1] = 0.941176f;
	MaterialSpecular[2] = 0.807843f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.21794872f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);



	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 1.0f;
	currentViewportY = yWindowCenter + yTransOffset * -1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.2125f;
	MaterialAmbiant[1] = 0.1275f;
	MaterialAmbiant[2] = 0.054f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.714f;
	MaterialDiffuse[1] = 0.4284f;
	MaterialDiffuse[2] = 0.18144f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.393548f;
	MaterialSpecular[1] = 0.271906f;
	MaterialSpecular[2] = 0.166721f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.2f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 1.0f;
	currentViewportY = yWindowCenter + yTransOffset * -0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.25f;
	MaterialAmbiant[2] = 0.25f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.774597f;
	MaterialSpecular[1] = 0.774597f;
	MaterialSpecular[2] = 0.774597f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 1.0f;
	currentViewportY = yWindowCenter + yTransOffset * 0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);

	MaterialAmbiant[0] = 0.19125f;
	MaterialAmbiant[1] = 0.0735f;
	MaterialAmbiant[2] = 0.02225f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.7038f;
	MaterialDiffuse[1] = 0.27048f;
	MaterialDiffuse[2] = 0.0828f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.256777f;
	MaterialSpecular[1] = 0.137622f;
	MaterialSpecular[2] = 0.086014f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.1f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 1.0f;
	currentViewportY = yWindowCenter + yTransOffset * 1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.24725f;
	MaterialAmbiant[1] = 0.1995f;
	MaterialAmbiant[2] = 0.0745f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.75164f;
	MaterialDiffuse[1] = 0.60648f;
	MaterialDiffuse[2] = 0.22648f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.628281f;
	MaterialSpecular[1] = 0.555802f;
	MaterialSpecular[2] = 0.366065f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.4f * 128.0f;



	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * 1.0f;
	currentViewportY = yWindowCenter + yTransOffset * 2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.19225f;
	MaterialAmbiant[1] = 0.19225f;
	MaterialAmbiant[2] = 0.19225f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.50754f;
	MaterialDiffuse[1] = 0.50754f;
	MaterialDiffuse[2] = 0.50754f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.508273f;
	MaterialSpecular[1] = 0.508273f;
	MaterialSpecular[2] = 0.508273f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.4f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//3rd coloum
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -0.5f;
	currentViewportY = yWindowCenter + yTransOffset * -2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.50f;
	MaterialSpecular[1] = 0.50f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.25f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);



	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -0.5f;
	currentViewportY = yWindowCenter + yTransOffset * -1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.1f;
	MaterialAmbiant[2] = 0.06f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.0f;
	MaterialDiffuse[1] = 0.50980392f;
	MaterialDiffuse[2] = 0.50980392f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.50196078f;
	MaterialSpecular[1] = 0.50196078f;
	MaterialSpecular[2] = 0.50196078f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.25f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -0.5f;
	currentViewportY = yWindowCenter + yTransOffset * -0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.1f;
	MaterialDiffuse[1] = 0.35f;
	MaterialDiffuse[2] = 0.1f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.45f;
	MaterialSpecular[1] = 0.55f;
	MaterialSpecular[2] = 0.45f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.25f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -0.5f;
	currentViewportY = yWindowCenter + yTransOffset * 0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);

	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.0f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.6f;
	MaterialSpecular[2] = 0.6f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.25f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -0.5f;
	currentViewportY = yWindowCenter + yTransOffset * 1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.55f;
	MaterialDiffuse[1] = 0.55f;
	MaterialDiffuse[2] = 0.55f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.70f;
	MaterialSpecular[1] = 0.70f;
	MaterialSpecular[2] = 0.70f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.25f * 128.0f;



	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -0.5f;
	currentViewportY = yWindowCenter + yTransOffset * 2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.60f;
	MaterialSpecular[1] = 0.60f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.25f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//3rd coloum
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -2.0f;
	currentViewportY = yWindowCenter + yTransOffset * -2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.02f;
	MaterialAmbiant[1] = 0.02f;
	MaterialAmbiant[2] = 0.02f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.4f;
	MaterialSpecular[1] = 0.4f;
	MaterialSpecular[2] = 0.4f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);



	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -2.0f;
	currentViewportY = yWindowCenter + yTransOffset * -1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -2.0f;
	currentViewportY = yWindowCenter + yTransOffset * -0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -2.0f;
	currentViewportY = yWindowCenter + yTransOffset * 0.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);

	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.04f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -2.0f;
	currentViewportY = yWindowCenter + yTransOffset * 1.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;



	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);


	//transformatiion
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();
	projectionMatrix = XMMatrixIdentity();
	trnslationMatrix = XMMatrixIdentity();
	rotationMatrix = XMMatrixIdentity();

	trnslationMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.8f);
	worldMatrix = trnslationMatrix;

	//load data into constant buffer
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;


	currentViewportX = xWindowCenter - xTransOffset * -2.0f;
	currentViewportY = yWindowCenter + yTransOffset * 2.5f;

	d3d11ViewPort.TopLeftX = currentViewportX;
	d3d11ViewPort.TopLeftY = currentViewportY;
	d3d11ViewPort.Width = (float)distorasionX;
	d3d11ViewPort.Height = (float)distorasionY;
	d3d11ViewPort.MinDepth = 0.0f;
	d3d11ViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext_raghav->RSSetViewports(1, &d3d11ViewPort);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;


	constantBuffer.Ka = XMVectorSet(MaterialAmbiant[0], MaterialAmbiant[1], MaterialAmbiant[2], MaterialAmbiant[3]);
	constantBuffer.Kd = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.Ks = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.Matrial_Shinyness = MaterialShininess[0];


	gpID3D11DeviceContext_raghav->UpdateSubresource(gpID3D11Buffer_constantBuffer_raghav, 0, NULL, &constantBuffer, 0, 0);
	//draw
	gpID3D11DeviceContext_raghav->DrawIndexed(gNumElements_raghav, 0, 0);

	//switch between front and back buffer
	gpIDXGISwapChain_raghav->Present(0, 0);
}

void update(void)
{
	//code
	angleOfXRotation = angleOfXRotation + 0.01f;
	if (angleOfXRotation >= 360.0f)
	{
		angleOfXRotation = 0.0f;
	}
	angleOfYRotation = angleOfYRotation + 0.01f;
	if (angleOfYRotation >= 360.0f)
	{
		angleOfYRotation = 0.0f;
	}
	angleOfZRotation = angleOfZRotation + 0.01f;
	if (angleOfZRotation >= 360.0f)
	{
		angleOfZRotation = 0.0f;
	}
}


void uninitialize(void)
{
	//code
	if (gpID3D11Buffer_IndexBuffer)
	{
		gpID3D11Buffer_IndexBuffer->Release();
		gpID3D11Buffer_IndexBuffer = NULL;
	}
	if (gpID3D11Buffer_constantBuffer_raghav)
	{
		gpID3D11Buffer_constantBuffer_raghav->Release();
		gpID3D11Buffer_constantBuffer_raghav = NULL;
	}

	if (gpID3D11Buffer_vertexBuffer_sphere_position_raghav)
	{
		gpID3D11Buffer_vertexBuffer_sphere_position_raghav->Release();
		gpID3D11Buffer_vertexBuffer_sphere_position_raghav = NULL;
	}

	if (gpID3D11Buffer_vertexBuffer_sphere_normal_raghav)
	{
		gpID3D11Buffer_vertexBuffer_sphere_normal_raghav->Release();
		gpID3D11Buffer_vertexBuffer_sphere_normal_raghav = NULL;
	}
	if (gpID3D11InputLayout_raghav)
	{
		gpID3D11InputLayout_raghav->Release();
		gpID3D11InputLayout_raghav = NULL;
	}
	if (gpID3D11PixelShader_raghav)
	{
		gpID3D11PixelShader_raghav->Release();
		gpID3D11PixelShader_raghav = NULL;
	}
	if (gpID3D11VertexShader_raghav)
	{
		gpID3D11VertexShader_raghav->Release();
		gpID3D11VertexShader_raghav = NULL;
	}
	if (gpID3D11RenderTargetView_raghav)
	{
		gpID3D11RenderTargetView_raghav->Release();
		gpID3D11RenderTargetView_raghav = NULL;
	}
	if (gpID3D11RasterizerState_raghav)
	{
		gpID3D11RasterizerState_raghav->Release();
		gpID3D11RasterizerState_raghav = NULL;
	}
	if (gpID3D11DepthStencilView_raghav)
	{
		gpID3D11DepthStencilView_raghav->Release();
		gpID3D11DepthStencilView_raghav = NULL;
	}
	if (gpIDXGISwapChain_raghav)
	{
		gpIDXGISwapChain_raghav->Release();
		gpIDXGISwapChain_raghav = NULL;
	}
	if (gpID3D11DeviceContext_raghav)
	{
		gpID3D11DeviceContext_raghav->Release();
		gpID3D11DeviceContext_raghav = NULL;
	}
	if (gpID3D11Device_raghav)
	{
		gpID3D11Device_raghav->Release();
		gpID3D11Device_raghav = NULL;
	}
	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFilename, "a+");
		fprintf_s(gpFile, "LogFile closed Successfully\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

package com.example.MulViewport;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo = new int[1];
	
	private int mvpUniform;
	
	private int keyPress = 0;
	
	 private int key=0;

	 private int gWidth,gHeight;
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
    if(key==0)
        keyPress=0;
    else if(key==1)
        keyPress=1;
    else if(key==2)
        keyPress=2;
    else if(key==3)
        keyPress=3;
    else if(key==4)
        keyPress=4;
    else if(key==5)
        keyPress=5;
    else if(key==6)
        keyPress=6;
    else if(key==7)
        keyPress=7;
    else if(key==8)
        keyPress=8;
    if(key>9)
    {
        keyPress=0;
        key=0;
    }
    key++;
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"uniform mat4 u_mvp_matrix;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = vec4(1.0,1.0,0.0,1.0);" +
		"}"
		
		);
		
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status   " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		
		
		//Triangle vertices
		
		final float[] triangleVertices = new float[]
		{
			0.0f,1.0f,0.0f,-1.0f,-1.0f,0.0f,1.0f,-1.0f,0.0f
		};
		
		//create vao
		GLES30.glGenVertexArrays(1,vao,0);
		
		GLES30.glBindVertexArray(vao[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer.put(triangleVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,triangleVertices.length * 4, positionBuffer, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		GLES30.glBindVertexArray(0);
		
		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

	
		 GLES30.glViewport(0, 0, width, height);
		 gWidth=width;
		 gHeight=height;
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							width / height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
	
	if (keyPress ==0) {
		
				GLES30.glViewport(0, 0, gWidth, gHeight);
	}else if (keyPress == 1) {
		
				GLES30.glViewport(0, 0, gWidth/2, gHeight/2);
	}else if (keyPress == 2) {

		GLES30.glViewport(gWidth / 2, 0, gWidth / 2, gHeight / 2);
	}
	else if (keyPress == 3) {

		GLES30.glViewport(0, gHeight / 2, gWidth / 2, gHeight / 2);
	}
	else if (keyPress == 4) {

		GLES30.glViewport(gWidth / 2, gHeight / 2, gWidth / 2, gHeight / 2);
	}
	else if (keyPress == 5) {

		GLES30.glViewport(0, 0, gWidth / 2, gHeight);
	}
	else if (keyPress == 6) {

		GLES30.glViewport(gWidth / 2, 0, gWidth / 2, gHeight);
	}
	else if (keyPress == 7) {

		GLES30.glViewport(0, gHeight / 2, gWidth, gHeight / 2);
	}
	else if (keyPress == 8) {

		GLES30.glViewport(0, 0, gWidth, gHeight / 2);
	}

		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);
		
		//declerations of matrix
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f,-5.0f);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,0, 3);

		//Unbind vao
		GLES30.glBindVertexArray(0);

		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
	}
	
	
	void uninitialize()
	{
		if (vbo[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo,0);
			vbo[0] = 0;
		}
		if (vao[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao,0);
			vao[0] = 0;
		}

		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



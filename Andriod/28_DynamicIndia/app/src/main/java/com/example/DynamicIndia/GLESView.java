package com.example.DynamicIndia;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_LeftI = new int[1];
	private int[] vao_N = new int[1];
	private int[] vao_D = new int[1];
	private int[] vao_RightI = new int[1];
	private int[] vao_A = new int[1];


	private int[] vbo_position_LeftI = new int[1];
	private int[] vbo_color_LeftI = new int[1];

	private int[] vbo_position_N = new int[1];
	private int[] vbo_color_N = new int[1];

	private int[] vbo_position_D = new int[1];
	private int[] vbo_color_D = new int[1];

	private int[] vbo_position_RightI = new int[1];
	private int[] vbo_color_RightI = new int[1];

	private int[] vbo_position_A = new int[1];
	private int[] vbo_color_A = new int[1];
	
		//LI
	static float Litx = -3.0f, Lity = 0.0f, Litz = -4.0f;
	
	//A
	static float x = 0.0f, y = 0.0f, z = 0.0f, a = 0.0f, b = 0.0f, c = 0.0f, d = 0.0f, e = 0.0f, f = 0.0f; //For  middle color of A
	static float atx = 4.0f, aty = 0.0f, atz = -4.0f, aty1 = 0.01f;
	//RI
	static float Ritx = 0.0f, Rity = -8.0f, Ritz = -4.0f;

	//D
	static float dtx = 0.0f, dty = 0.0f, dtz = -4.0f;
	static float i = 0.0f, j = 0.0f, k = 0.0f, l = 0.0f, m = 0.0f, n = 0.0f;
	
	//N
	static float ntx = 0.0f, nty = 4.0f, ntz = -4.0f;



	boolean  ifLI= false;
	boolean  ifA= false;
	boolean  ifN= false;
	boolean  ifRI= false;
	boolean  ifD= false;

	private int mvpUniform;
	
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_color;" +
		"}"
		
		);
		
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		// ----position
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		// ---- color
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
		
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status   " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		
		
		//Triangle vertices
		
		final float[] leftIVertices = new float[]
		{
			-1.30f,0.70f,0.0f,
		-1.30f,-0.70f,0.0f
		};

		final float[] leftIColor = new float[]
		{
			1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f
		};

		final float[] nVertices = new float[]
		{
			-1.10f,0.70f,0.0f,
		-1.10f,-0.70f,0.0f,
		-1.10f,0.70f,0.0f,
		-0.60f,-0.70f,0.0f,
		-0.60f,0.70f,0.0f,
		-0.60f,-0.70f,0.0f
		};

		final float[] nColor = new float[]
		{
			1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f
		};

		final float[] dVertices = new float[]
		{
			-0.4f,0.70f,0.0f,
		-0.4f,-0.70f,0.0f,
		-0.50f,0.70f,0.0f,
		0.1f,0.70f,0.0f,
		0.1f,0.70f,0.0f,
		0.1f,-0.70f,0.0f,
		-0.50f,-0.70f,0.0f,
		0.1f,-0.70f,0.0f
		};

		

		final float[] rightIVertices = new float[]
		{
			0.3f,0.70f,0.0f,
		0.3f,-0.70f,0.0f
		};

		final float[] rightIColor = new float[]
		{
			1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f
		};

		final float[] aVertices = new float[]
		{
			0.8f,0.70f,0.0f,
		0.50f,-0.70f,0.0f,
		0.80f,0.70f,0.0f,
		1.1f,-0.70f,0.0f,
		0.66f,0.02f,0.0f,
		0.94f,0.02f,0.0f,
		0.66f,0.0f,0.0f,
		0.94f,0.0f,0.0f,
		0.66f,-0.02f,0.0f,
		0.94f,-0.02f,0.0f
		};

		final float[] aColor = new float[]
		{
			1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		0.07f, 0.533f, 0.027f,
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f,
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		0.07f, 0.533f, 0.027f,
		0.07f, 0.533f, 0.027f
		};
		
		//LeftI
		//create vao
		GLES30.glGenVertexArrays(1,vao_LeftI,0);
		
		GLES30.glBindVertexArray(vao_LeftI[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_LeftI,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_LeftI[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(leftIVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer.put(leftIVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,leftIVertices.length * 4, positionBuffer, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		//create vbo
		GLES30.glGenBuffers(1,vbo_color_LeftI,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_LeftI[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(leftIColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer1.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer1.put(leftIColor);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer1.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,leftIColor.length * 4, positionBuffer1, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		GLES30.glBindVertexArray(0);



		//N

		//create vao
		GLES30.glGenVertexArrays(1,vao_N,0);
		
		GLES30.glBindVertexArray(vao_N[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_N,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_N[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(nVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer2.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer2 = byteBuffer2.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer2.put(nVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer2.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,nVertices.length * 4, positionBuffer2, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		//create vbo
		GLES30.glGenBuffers(1,vbo_color_N,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_N[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(nColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer3.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer3 = byteBuffer3.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer3.put(nColor);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer3.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,nColor.length * 4, positionBuffer3, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		GLES30.glBindVertexArray(0);

		//D

		//create vao
		GLES30.glGenVertexArrays(1,vao_D,0);
		
		GLES30.glBindVertexArray(vao_D[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_D,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_D[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(dVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer4.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer4 = byteBuffer4.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer4.put(dVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer4.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,dVertices.length * 4, positionBuffer4, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		//create vbo
		GLES30.glGenBuffers(1,vbo_color_D,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_D[0]);	
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,4 * 3 * 4, null, GLES30.GL_DYNAMIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		GLES30.glBindVertexArray(0);		

		//RightI

		//create vao
		GLES30.glGenVertexArrays(1,vao_RightI,0);
		
		GLES30.glBindVertexArray(vao_RightI[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_RightI,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_RightI[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer6 = ByteBuffer.allocateDirect(rightIVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer6.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer6 = byteBuffer6.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer6.put(rightIVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer6.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,rightIVertices.length * 4, positionBuffer6, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		//create vbo
		GLES30.glGenBuffers(1,vbo_color_RightI,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_RightI[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer7 = ByteBuffer.allocateDirect(rightIColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer7.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer7 = byteBuffer7.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer7.put(rightIColor);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer7.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,rightIColor.length * 4, positionBuffer7, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		GLES30.glBindVertexArray(0);

		//A

		//create vao
		GLES30.glGenVertexArrays(1,vao_A,0);
		
		GLES30.glBindVertexArray(vao_A[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_A,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_A[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer8 = ByteBuffer.allocateDirect(aVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer8.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer8 = byteBuffer8.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer8.put(aVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer8.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,aVertices.length * 4, positionBuffer8, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		//create vbo
		GLES30.glGenBuffers(1,vbo_color_A,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_A[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer9 = ByteBuffer.allocateDirect(aColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer9.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer9 = byteBuffer9.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer9.put(aColor);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer9.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,aColor.length * 4, positionBuffer9, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		GLES30.glBindVertexArray(0);



		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							width / height,
							0.1f,
							100.0f);
	}
	
	
	private void display()
	{
	


	final float[] dColor = new float[]
		{
			i, j, k,
			l, m, n,
			i, j, k,
			i, j, k,
			i, j, k,
			l, m, n,
			l, m, n,
			l, m, n
		};


		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		
		GLES30.glUseProgram(shaderProgramObject);
		
		//declerations of matrix
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		//Initialize above matrix to identity
		
		GLES30.glLineWidth(5.0f);

		if(ifLI == false)
		{
			//LeftI
		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,Litx, Lity, Litz);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao_LeftI[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_LINES,0, 2);

		//Unbind vao
		GLES30.glBindVertexArray(0);	

		if ((Litx < 0.0f) || (Litx == 0.0f))
		{
			Litx = Litx + 0.009f;
		}
		if (Litx >= 0.0f)
		{
			ifA = true;
			//A();
		}
	
	}
		
	if(ifA == true)

	{
			//A

		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,atx, aty, atz);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao_A[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_LINES,0, 10);

		//Unbind vao
		GLES30.glBindVertexArray(0);
		
		if ((atx > 0.0) || (atx == 0.0f))
		{
			atx = atx - 0.009f;

		}
		if (atx <= 0.0f)
		{
			//N();
			ifN = true;
		}

	}

	if(ifN == true)
	{
		//N

		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,ntx, nty, ntz);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao_N[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_LINES,0, 6);

		//Unbind vao
		GLES30.glBindVertexArray(0);

		if ((nty >= 0.0f) || (nty == 0.0f))
		{
			nty = nty - 0.005f;

		}
		if (nty <= 0.0f)
		{
			//rightI();
			ifRI = true;
		}

	}	
	if(ifRI = true)
	{
			//RightI

		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,Ritx, Rity, Ritz);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao_RightI[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_LINES,0, 2);

		//Unbind vao
		GLES30.glBindVertexArray(0);
		
			if ((Rity <= 0.0f) || (Rity == 0.0f))
		{
			Rity = Rity + 0.005f;

		}
		if (Rity >= 0.0f)
		{
			//D();
			ifD = true;
		}
	
	}

	if(ifD == true)
	{
		//D

		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,dtx, dty, dtz);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao_D[0]);

		//Bind texture if any

		//Allocate buffer from native memory
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(dColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer2.order(ByteOrder.nativeOrder());

		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer posBuffer2 = byteBuffer2.asFloatBuffer();
		
		//Now fill the data of array
		posBuffer2.put(dColor);
		
		//set the array to zero th postion (In case of interleaved )
		posBuffer2.position(0);
		
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_D[0]);
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,dColor.length * 4, posBuffer2, GLES30.GL_DYNAMIC_DRAW);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, 0);

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_LINES,0, 8);

		//Unbind vao
		GLES30.glBindVertexArray(0);

		if (i <= 1.0f && j < 0.6f && k < 0.2f && l < 0.07f && m < 0.533f && n < 0.027f)
		{
			i = i + 0.001f;
			j = j + 0.0006f;
			k = k + 0.0002f;

			l = l + 0.00007f;
			m = m + 0.000533f;
			n = n + 0.000027f;
		}
	

	}
		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
	}
	
	
	void uninitialize()
	{
		if (vbo_color_A[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_color_A,0);
			vbo_color_A[0] = 0;
		}
		if (vbo_color_D[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_color_D,0);
			vbo_color_D[0] = 0;
		}
		if (vbo_color_LeftI[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_color_LeftI,0);
			vbo_color_LeftI[0] = 0;
		}
		if (vbo_color_N[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_color_N,0);
			vbo_color_N[0] = 0;
		}
		if (vbo_color_RightI[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_color_RightI,0);
			vbo_color_RightI[0] = 0;
		}
		if (vbo_position_A[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_A,0);
			vbo_position_A[0] = 0;
		}
		if (vbo_position_D[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_D,0);
			vbo_position_D[0] = 0;
		}
		if (vbo_position_LeftI[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_LeftI,0);
			vbo_position_LeftI[0] = 0;
		}
		if (vbo_position_N[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_N,0);
			vbo_position_N[0] = 0;
		}
		if (vbo_position_RightI[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_RightI,0);
			vbo_position_RightI[0] = 0;
		}
		if (vao_A[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_A,0);
			vao_A[0] = 0;
		}
		if (vao_D[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_D,0);
			vao_D[0] = 0;
		}
		if (vao_LeftI[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_LeftI,0);
			vao_LeftI[0] = 0;
		}
		if (vao_RightI[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_RightI,0);
			vao_RightI[0] = 0;
		}
		if (vao_N[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_N,0);
			vao_N[0] = 0;
		}

		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



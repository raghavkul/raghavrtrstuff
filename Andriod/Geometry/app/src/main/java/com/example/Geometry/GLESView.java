package com.example.Geometry;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int geometryShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo = new int[1];
	private int[] vboColor = new int[1];
	
	private int mvpUniform;

	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES32.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES32.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}"
		
		);
		
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		geometryShaderObject = GLES32.glCreateShader(GLES32.GL_GEOMETRY_SHADER);
		final String geometryShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"layout(triangles)in;" +
		"layout(triangle_strip,max_vertices = 9)out;" +
		"uniform mat4 u_mvp_matrix;" +
		"in vec4 out_color[];" +
		"out vec4 out_colorTri;" +
		"void main(void)" +
		"{" +
			"for(int vertex=0;vertex<3;vertex++)" +
			"{" +
			"gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(0.0,1.0,0.0,0.0));" +
			"out_colorTri = out_color[vertex];" +
			"EmitVertex();" +
			"gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(-1.0,-1.0,0.0,0.0));" +
			"out_colorTri = out_color[vertex];" +
			"EmitVertex();" +
			"gl_Position = u_mvp_matrix * (gl_in[vertex].gl_Position + vec4(1.0,-1.0,0.0,0.0));" +
			"out_colorTri = out_color[vertex];" +
			"EmitVertex();" +
			"EndPrimitive();" +
			"}" +
		"}"
		
		);
		
		GLES32.glShaderSource(geometryShaderObject,geometryShaderSourceCode);
		GLES32.glCompileShader(geometryShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(geometryShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetShaderiv(geometryShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(geometryShaderObject);
					System.out.println("RTR: program compile status for Geometry  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}

		//Fragment shader code
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_colorTri;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_colorTri;" +
		"}"
		
		);
		
		GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES32.glCreateProgram();
		
		//Attache shaders to program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,geometryShaderObject);
		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

		
		//prelinking
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR,"vColor");
		
		//linking 
		GLES32.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status   " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		
		
		//Triangle vertices
		
		final float[] triangleVertices = new float[]
		{
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f
		};

		final float[] triangleColors = new float[]
		{
			1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,
			0.0f,0.0f,1.0f
		};
		
		//create vao
		GLES32.glGenVertexArrays(1,vao,0);
		
		GLES32.glBindVertexArray(vao[0]);
		
		//create vbo
		GLES32.glGenBuffers(1,vbo,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(triangleVertices);
		positionBuffer.position(0);
		
	
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,triangleVertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);


		//create vbo
		GLES32.glGenBuffers(1,vboColor,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);
		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(triangleColors.length * 4);
		byteBuffer1.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer1 = byteBuffer.asFloatBuffer();
		positionBuffer1.put(triangleColors);
		positionBuffer1.position(0);
		
	
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,triangleColors.length * 4, positionBuffer1, GLES32.GL_STATIC_DRAW);
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES32.GL_FLOAT,false,0,0);
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);
		
		//Depth Lines
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//GLES32.glDisable(GLES32.GL_CULL_FACE);
		
		GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

	
		 GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							width / height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
	
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		
		//declerations of matrix
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f,-5.0f);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES32.glBindVertexArray(vao[0]);

		//Bind texture if any

		//Draw function
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0, 3);

		//Unbind vao
		GLES32.glBindVertexArray(0);

		//Unused Program
		GLES32.glUseProgram(0);
		
		requestRender(); // like swapBuffers
	}
	
	
	void uninitialize()
	{
		if (vbo[0]!=0)
		{
			GLES32.glDeleteBuffers(1, vbo,0);
			vbo[0] = 0;
		}
		if (vao[0]!=0)
		{
			GLES32.glDeleteVertexArrays(1, vao,0);
			vao[0] = 0;
		}

		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES32.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
	}
}



package com.example.SmilyTexture;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

// packages for texture

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int[] texture_smiley = new int[1];
	private int[] vao_texture_smiley = new int[1];
	private int[] vbo_texture_textcoord = new int[1]; 
	private int[] vbo_tecture_postion = new int[1];
	private int sampleUniform; 
	
	private int mvpUniform;
	
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTextCoord;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec2 out_textcoord;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_textcoord = vTextCoord;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 out_textcoord;" +
		"uniform sampler2D u_sampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = texture(u_sampler , out_textcoord);" +
		"}"
		
		);
		
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_TEXTCOORD0,"vTextCoord");

		
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status   " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		sampleUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_sampler");

		
		//Triangle vertices
		
		final float[] smileyVertices = new float[]
		{
			1.0f,1.0f,0.0f,
				-1.0f,1.0f,0.0f,
				-1.0f,-1.0f,0.0f,
				1.0f,-1.0f,0.0f
		};
		
		final float[] smileyTextcoord = new float[]
		{
			1.0f,1.0f,0.0f,1.0f,0.0f,0.0f,1.0f,0.0f
		};
		
		//create vao
		GLES30.glGenVertexArrays(1,vao_texture_smiley,0);
		
		GLES30.glBindVertexArray(vao_texture_smiley[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_tecture_postion,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_tecture_postion[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(smileyVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer1.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer1.put(smileyVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer1.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,smileyVertices.length * 4, positionBuffer1, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		//###################################
		//texture coords for smiley
		GLES30.glGenBuffers(1,vbo_texture_textcoord,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_texture_textcoord[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(smileyTextcoord.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer2.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer textBuffer1 = byteBuffer2.asFloatBuffer();
		
		//Now fill the data of array
		textBuffer1.put(smileyTextcoord);
		
		//set the array to zero th postion (In case of interleaved )
		textBuffer1.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,smileyTextcoord.length * 4, textBuffer1, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXTCOORD0,2,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXTCOORD0);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		GLES30.glBindVertexArray(0);
		
		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		// call to texture
		texture_smiley[0] = loadTexture(R.raw.smiley);
		
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	private int loadTexture(int imageFileResourceID)
	{
		int[] texture = new int[1];
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inScaled = false;
		Bitmap bitmap =  BitmapFactory.decodeResource(context.getResources(),imageFileResourceID,options);
		GLES30.glPixelStorei(GLES30.GL_UNPACK_ALIGNMENT, 4);
		GLES30.glGenTextures(1, texture,0);
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, texture[0]);
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MAG_FILTER, GLES30.GL_LINEAR);
		GLES30.glTexParameteri(GLES30.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_LINEAR_MIPMAP_LINEAR);
		//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, bmp.bmWidth, bmp.bmHeight, GL_BGR_EXT, GL_UNSIGNED_BYTE, bmp.bmBits);
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bmp.bmWidth, bmp.bmHeight, 0, GL_BGR, GL_UNSIGNED_BYTE, bmp.bmBits);
		GLUtils.texImage2D(GLES30.GL_TEXTURE_2D,0,bitmap,0);
		
		GLES30.glGenerateMipmap(GLES30.GL_TEXTURE_2D);
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D , 0);
		return(texture[0]);
	}
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							width / height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);
		
		//declerations of matrix
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f,-5.0f);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		
		//Bind texture if any
		GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, texture_smiley[0]);
		GLES30.glUniform1f(sampleUniform, 0);
		//Bind with vao of rectangle
		GLES30.glBindVertexArray(vao_texture_smiley[0]);

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,0, 4);

		//Unbind vao
		GLES30.glBindVertexArray(0);

		//unbind texture
		GLES30.glBindTexture(GLES30.GL_TEXTURE_2D, 0);

		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
	}
	
	
	void uninitialize()
	{
		if (vbo_texture_textcoord[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_texture_textcoord,0);
			vbo_texture_textcoord[0] = 0;
		}
		if (vbo_tecture_postion[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_tecture_postion,0);
			vbo_tecture_postion[0] = 0;
		}
		if (vao_texture_smiley[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_texture_smiley,0);
			vao_texture_smiley[0] = 0;
		}

		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



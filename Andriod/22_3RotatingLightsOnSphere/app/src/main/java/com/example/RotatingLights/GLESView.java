package com.example.RotatingLights;


//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
 
//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.lang.Math;
//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	

	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

   	
	private float[] lightPositionRed = new float[]{0.0f,0.0f,0.0f,1.0f};
	private float[] lightPositionBlue = new float[]{0.0f,0.0f,0.0f,1.0f};
	private float[] lightPositionGreen = new float[]{0.0f,0.0f,0.0f,1.0f};


   	Sphere sphere=new Sphere();
    float sphere_vertices[]=new float[1146];
    float sphere_normals[]=new float[1146];
    float sphere_textures[]=new float[764];
    short sphere_elements[]=new short[2280];

  	
    int numVertices;
	int numElements;
  
	private int mUniform;
	private int vUniform;
	private int pUniform;
	
	private int laUniformRed;
	private int ldUniformRed;
	private int lsUniformRed;

	private int laUniformBlue;
	private int ldUniformBlue;
	private int lsUniformBlue;

	private int laUniformGreen;
	private int ldUniformGreen;
	private int lsUniformGreen;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShiUniform;
	

	private int lightPositionUniformRed;
	private int lightPositionUniformBlue;
	private int lightPositionUniformGreen;

	private int lKeyIsPressedUniform;
	
	boolean  gbLighting = false;


	//Animation varibales
	float lightAngleRed = 0.0f;
	float lightAngleBlue = 0.0f;
	float lightAngleGreen = 0.0f;
	
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if(gbLighting == true){
				gbLighting = false;
			}


		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		if (gbLighting == false)
			{
				gbLighting = true;
			}
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
		update();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"uniform vec4 u_lightPositionRed;\n" +
		"uniform vec4 u_lightPositionBlue;\n" +
		"uniform vec4 u_lightPositionGreen;\n" +
		"out vec3 tNorm;" +
		"out vec3 lightDirectionRed;" +
		"out vec3 lightDirectionBlue;" +
		"out vec3 lightDirectionGreen;" +
		"out vec3 viwerVector;" +
		"void main(void)\n" +
		"{\n" +
		"	if(u_lKeyIsPressed == 1)\n" +
		"	{\n" +
			"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
			"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" +
			"lightDirectionRed = vec3(u_lightPositionRed - eye_Coordinate);" +
			"lightDirectionBlue = vec3(u_lightPositionBlue - eye_Coordinate);" +
			"lightDirectionGreen = vec3(u_lightPositionGreen - eye_Coordinate);" +
			"viwerVector = -eye_Coordinate.xyz;" +
			
			"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;\n" +
		"in vec3 tNorm;" +
		"in vec3 lightDirectionRed;" +
		"in vec3 lightDirectionBlue;" +
		"in vec3 lightDirectionGreen;" +
		"in vec3 viwerVector;" +
		"uniform vec3 u_laRed;" +
		"uniform vec3 u_ldRed;" +
		"uniform vec3 u_lsRed;" +
		"uniform vec3 u_laBlue;" +
		"uniform vec3 u_ldBlue;" +
		"uniform vec3 u_lsBlue;" +
		"uniform vec3 u_laGreen;" +
		"uniform vec3 u_ldGreen;" +
		"uniform vec3 u_lsGreen;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform mediump float u_materialShine;" +
		"out vec4 FragColor;\n" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"void main(void)" +
		"{" +
		"	if(u_lKeyIsPressed == 1)" +
		"	{" +
				"vec3 normalizeTNorm = normalize(tNorm);" +
				"vec3 normalizeLightDirectionRed = normalize(lightDirectionRed);" +
				"vec3 normalizeLightDirectionBlue = normalize(lightDirectionBlue);" +
				"vec3 normalizeLightDirectionGreen = normalize(lightDirectionGreen);" +
				"vec3 normalizeViwerVector = normalize(viwerVector);" +
				"float tn_dot_ldRed = max(dot(normalizeLightDirectionRed,normalizeTNorm),0.0f);" +
				"float tn_dot_ldBlue = max(dot(normalizeLightDirectionBlue,normalizeTNorm),0.0f);" +
				"float tn_dot_ldGreen = max(dot(normalizeLightDirectionGreen,normalizeTNorm),0.0f);" +
				"vec3 reflectionVectorRed = reflect(-normalizeLightDirectionRed , normalizeTNorm);" +
				"vec3 reflectionVectorBlue = reflect(-normalizeLightDirectionBlue , normalizeTNorm);" +
				"vec3 reflectionVectorGreen = reflect(-normalizeLightDirectionGreen , normalizeTNorm);" +
				"vec3 ambiantRed = vec3(u_laRed * u_ka);" +
				"vec3 diffuseRed = vec3(u_ldRed * u_kd * tn_dot_ldRed);" +
				"vec3 specularRed = vec3(u_lsRed * u_ks * pow(max(dot(reflectionVectorRed , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 ambiantBlue = vec3(u_laBlue * u_ka);" +
				"vec3 diffuseBlue = vec3(u_ldBlue * u_kd * tn_dot_ldBlue);" +
				"vec3 specularBlue = vec3(u_lsBlue * u_ks * pow(max(dot(reflectionVectorBlue , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 ambiantGreen = vec3(u_laGreen * u_ka);" +
				"vec3 diffuseGreen = vec3(u_ldGreen * u_kd * tn_dot_ldGreen);" +
				"vec3 specularGreen = vec3(u_lsGreen * u_ks * pow(max(dot(reflectionVectorGreen , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 phong_ads_lightRed = ambiantRed + diffuseRed + specularRed;" +
				"vec3 phong_ads_lightBlue = ambiantBlue + diffuseBlue + specularBlue;" +
				"vec3 phong_ads_lightGreen = ambiantGreen + diffuseGreen + specularGreen;" +
				"FragColor = vec4(phong_ads_lightRed + phong_ads_lightBlue + phong_ads_lightGreen,1.0f);\n" +
		"	}" +
		"	else" +
		"	{" + 
		"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);\n" +
		"	}" +
		"}"
		
		);

		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		// ----position
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		// ---- color
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status error:  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_m_matrix");
		vUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_v_matrix");
		pUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		
		laUniformRed =GLES30.glGetUniformLocation(shaderProgramObject, "u_laRed");
		ldUniformRed = GLES30.glGetUniformLocation(shaderProgramObject, "u_ldRed");
		lsUniformRed =GLES30.glGetUniformLocation(shaderProgramObject, "u_lsRed");
		
		laUniformBlue =GLES30.glGetUniformLocation(shaderProgramObject, "u_laBlue");
		ldUniformBlue = GLES30.glGetUniformLocation(shaderProgramObject, "u_ldBlue");
		lsUniformBlue =GLES30.glGetUniformLocation(shaderProgramObject, "u_lsBlue");
		
		laUniformGreen =GLES30.glGetUniformLocation(shaderProgramObject, "u_laGreen");
		ldUniformGreen = GLES30.glGetUniformLocation(shaderProgramObject, "u_ldGreen");
		lsUniformGreen =GLES30.glGetUniformLocation(shaderProgramObject, "u_lsGreen");

		kaUniform =GLES30.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShiUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_materialShine");
		
		lightPositionUniformRed = GLES30.glGetUniformLocation(shaderProgramObject, "u_lightPositionRed");
		lightPositionUniformBlue = GLES30.glGetUniformLocation(shaderProgramObject, "u_lightPositionBlue");
		lightPositionUniformGreen = GLES30.glGetUniformLocation(shaderProgramObject, "u_lightPositionGreen");

		lKeyIsPressedUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
		
		
				
	
	
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    numVertices = sphere.getNumberOfSphereVertices();
    numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES30.glGenVertexArrays(1,vao_sphere,0);
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES30.glGenBuffers(1,vbo_sphere_position,0);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES30.GL_FLOAT,
                                     false,0,0);
        
        GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES30.glGenBuffers(1,vbo_sphere_normal,0);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES30.GL_FLOAT,
                                     false,0,0);
        
        GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES30.glGenBuffers(1,vbo_sphere_element,0);
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES30.glBindVertexArray(0);


		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							(float)width / (float)height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);

		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] translateMatrix = new float[16];

		float[] projectionMatrix = new float[16];
		//Rectangle
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-2.0f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
		
		if (gbLighting == true)
		{
			lightPositionRed[1] = (float)Math.cos(lightAngleRed) * 10.0f;
			lightPositionRed[2] = (float)-Math.sin(lightAngleRed) * 10.0f;
		
			lightPositionBlue[0] = (float)Math.cos(lightAngleBlue) * 10.0f;
			lightPositionBlue[2] = (float)-Math.sin(lightAngleBlue) * 10.0f;
		
			lightPositionGreen[0] = (float)Math.cos(lightAngleGreen) * 10.0f;
			lightPositionGreen[1] = (float)Math.sin(lightAngleGreen) * 10.0f;

			GLES30.glUniform1i(lKeyIsPressedUniform,1);
			
			GLES30.glUniform3f(laUniformRed, 0.0f,0.0f,0.0f);
			GLES30.glUniform3f(ldUniformRed, 1.0f,0.0f,0.0f);
			GLES30.glUniform3f(lsUniformRed, 1.0f,0.0f,0.0f);
			GLES30.glUniform4fv(lightPositionUniformRed,1,lightPositionRed,0);
			//GLES30.glUniform4f(lightPositionUniformRed,0.0f,0.0f,2.0f,1.0f);
			
			GLES30.glUniform3f(laUniformBlue, 0.0f,0.0f,0.0f);
			GLES30.glUniform3f(ldUniformBlue, 0.0f,0.0f,1.0f);
			GLES30.glUniform3f(lsUniformBlue, 0.0f,0.0f,1.0f);
			GLES30.glUniform4fv(lightPositionUniformBlue,1, lightPositionBlue,0);
			//GLES30.glUniform4f(lightPositionUniformBlue,-2.0f,0.0f,0.0f,1.0f);

			GLES30.glUniform3f(laUniformGreen, 0.0f,0.0f,0.0f);
			GLES30.glUniform3f(ldUniformGreen, 0.0f,1.0f,0.0f);
			GLES30.glUniform3f(lsUniformGreen, 0.0f,1.0f,0.0f);
			GLES30.glUniform4fv(lightPositionUniformGreen,1, lightPositionGreen,0);
			//GLES30.glUniform4f(lightPositionUniformGreen,2.0f,0.0f,0.0f,1.0f);

			GLES30.glUniform3f(kaUniform, 0.0f,0.0f,0.0f);
			GLES30.glUniform3f(kdUniform, 1.0f,1.0f,1.0f);
			GLES30.glUniform3f(ksUniform, 1.0f,1.0f,1.0f);
			GLES30.glUniform1f(materialShiUniform, 128.0f);
			
		
		}
		else {
			GLES30.glUniform1i(lKeyIsPressedUniform, 0);
		}
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);
		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
		
	}
	
	void update()
{

	lightAngleRed = lightAngleRed + 0.03f;
	if (lightAngleRed >= 360.0f)
	{
		lightAngleRed = 0.0f;
	}
	lightAngleBlue = lightAngleBlue + 0.03f;
	if (lightAngleBlue >= 360.0f)
	{
		lightAngleBlue = 0.0f;
	}
	lightAngleGreen = lightAngleGreen + 0.03f;
	if (lightAngleGreen >= 360.0f)
	{
		lightAngleGreen = 0.0f;
	}


}
	
	void uninitialize()
	{
		// destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES30.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



package com.example.Tessellation;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int tessControlShaderObject;
	private int tessEvaluationShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo = new int[1];
	
	private int numberOfSegments;
	private int numberOfStrips;
	private int lineColor;
	private int mvpUniform;

	int numberOfLineSegment;

	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		numberOfLineSegment--;
		if(numberOfLineSegment<=0)
		{
			numberOfLineSegment=1;
		}
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		numberOfLineSegment++;
		if(numberOfLineSegment>=50)
		{
			numberOfLineSegment=50;
		}
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
		
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES32.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES32.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"in vec2 vPosition;" +
		"void main(void)" +
		"{" +
		"gl_Position = vec4(vPosition,0.0,1.0);" +
		"}"
		);
		
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES32.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		tessControlShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);
		final String tessControlShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"layout(vertices=4)out;" +
		"uniform int numberOfSegments;" +
		"uniform int numberOfStrips;" +
		"void main(void)" +
		"{" +
		"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" +
		"gl_TessLevelOuter[0] = float(numberOfStrips);" +
		"gl_TessLevelOuter[1] = float(numberOfSegments);" +
		"}"
		);
		
		GLES32.glShaderSource(tessControlShaderObject,tessControlShaderSourceCode);
		GLES32.glCompileShader(tessControlShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(tessControlShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetShaderiv(tessControlShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(tessControlShaderObject);
					System.out.println("RTR: program compile status for tess control  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}


		tessEvaluationShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);
		final String tessEvaluationShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"layout(isolines)in;" +
		"uniform mat4 u_mvp_matrix;" +
		"void main(void)" +
		"{" +
		"float u = gl_TessCoord.x;" +
		"vec3 p0 = gl_in[0].gl_Position.xyz;" +
		"vec3 p1 = gl_in[1].gl_Position.xyz;" +
		"vec3 p2 = gl_in[2].gl_Position.xyz;" +
		"vec3 p3 = gl_in[3].gl_Position.xyz;" +
		"float u1 = (1.0-u);" +
		"float u2 = u * u;" +
		"float b3 =  u2 * u;" +
		"float b2 = 3.0 * u2 * u1;" +
		"float b1 = 3.0 * u * u1 * u1;" +
		"float b0 = u1 * u1 * u1;" +
		"vec3 p = p0*b0 + p1*b1 + p2*b2 + p3*b3;" +
		"gl_Position = u_mvp_matrix * vec4(p,1.0);" +
		"}"
		);
		
		GLES32.glShaderSource(tessEvaluationShaderObject,tessEvaluationShaderSourceCode);
		GLES32.glCompileShader(tessEvaluationShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(tessEvaluationShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetShaderiv(tessEvaluationShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(tessEvaluationShaderObject);
					System.out.println("RTR: program compile status for tess evaluation  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		//Fragment shader code
		
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 320 es" +
		"\n" +
		"precision highp float;" +
		"uniform vec4 lineColor;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = lineColor;" +
		"}"
		);
		
		GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES32.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES32.glCreateProgram();
		
		//Attache shaders to program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES32.glAttachShader(shaderProgramObject,tessControlShaderObject);
		GLES32.glAttachShader(shaderProgramObject,tessEvaluationShaderObject);
		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

		
		//prelinking
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
	
		//linking 
		GLES32.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES32.GL_FALSE)
		{
				GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status   " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		numberOfSegments = GLES32.glGetUniformLocation(shaderProgramObject,"numberOfSegments");
		numberOfStrips = GLES32.glGetUniformLocation(shaderProgramObject,"numberOfStrips");
		lineColor = GLES32.glGetUniformLocation(shaderProgramObject,"lineColor");
		
		
		//Triangle vertices
		
		final float[] vertices = new float[]
		{
			-1.0f,-1.0f,-0.5f,1.0f,0.5f,-1.0f,1.0f,1.0f
		};
		
		//create vao
		GLES32.glGenVertexArrays(1,vao,0);
		
		GLES32.glBindVertexArray(vao[0]);
		
		//create vbo
		GLES32.glGenBuffers(1,vbo,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);
		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();
		positionBuffer.put(vertices);
		positionBuffer.position(0);
		
	
		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,vertices.length * 4, positionBuffer, GLES32.GL_STATIC_DRAW);
		
		GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,2,GLES32.GL_FLOAT,false,0,0);
		
		GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);
		
		numberOfLineSegment=1;
		//Depth Lines
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//GLES32.glDisable(GLES32.GL_CULL_FACE);
		
		GLES32.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

	
		 GLES32.glViewport(0, 0, width, height);
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							width / height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
	
		GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
		GLES32.glUseProgram(shaderProgramObject);
		
		//declerations of matrix
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
	    float[] linecolor = {1.0f,1.0f,0.0f,1.0f};

		//Initialize above matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f,-5.0f);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		GLES32.glUniform1i(numberOfSegments,numberOfLineSegment);
		GLES32.glUniform1i(numberOfStrips,1);
		GLES32.glUniform4fv(lineColor,1,linecolor,0);


		GLES32.glPatchParameteri(GLES32.GL_PATCH_VERTICES, 4);
		//Bind with vao
		GLES32.glBindVertexArray(vao[0]);

		//Bind texture if any

		//Draw function
		GLES32.glDrawArrays(GLES32.GL_PATCHES,0, 4);

		//Unbind vao
		GLES32.glBindVertexArray(0);

		//Unused Program
		GLES32.glUseProgram(0);
		
		requestRender(); // like swapBuffers
	}
	
	
	void uninitialize()
	{
		if (vbo[0]!=0)
		{
			GLES32.glDeleteBuffers(1, vbo,0);
			vbo[0] = 0;
		}
		if (vao[0]!=0)
		{
			GLES32.glDeleteVertexArrays(1, vao,0);
			vao[0] = 0;
		}

		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES32.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES32.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES32.glUseProgram(0);
		}
	}
}



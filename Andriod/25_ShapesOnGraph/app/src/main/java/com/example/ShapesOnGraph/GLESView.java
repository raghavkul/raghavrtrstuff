package com.example.ShapesOnGraph;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.lang.Math;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int MESH_HEIGHT = 1;
	private int  MESH_WIDTH = 1;
	private int  MESH_ROW =  10;
	private int  MESH_COLUMN = 10;


	private float[] graphVertices = new float[MESH_ROW * MESH_COLUMN * 3];
	private float xDiff = (float)MESH_WIDTH / (float)MESH_COLUMN;
	private float yDiff = (float)MESH_HEIGHT / (float)MESH_ROW;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_graph_blue = new int[1];
	private int[] vao_graph_red = new int[1];

	private int[] vao_triangle = new int[1];
	private int[] vao_circle = new int[1];
	private int[] vao_rectangle = new int[1];

	private int[] vbo_position_triangle = new int[1];//vertex buffer object for tri
	private int[] vbo_position_circle = new int[1];//vertex buffer object for rect
	private int[] vbo_position_rectangle = new int[1] ;//vertex buffer object(color) for tri


	private int[] vbo_position_graph_blue = new int[1];//vertex buffer object for tri
	private int[] vbo_position_graph_red = new int[1];//vertex buffer object for rect
	private int[] vbo_color_graph_blue = new int[1] ;//vertex buffer object(color) for tri
	private int[] vbo_color_graph_red = new int[1];//vertex buffer object(color) for rect

	

	private int mvpUniform;
	
	//Animation varibales
	private float[] circleVertices = new float[1100];
	static double angale;
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_color;" +
		"}"
		
		);
		
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		// ----position
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		// ---- color
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status   " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		
		
		//Triangle vertices
		final float[] triangleVertices = new float[]
		{
			-1.1f,-1.2f,0.0f,
			0.0f,1.0f,0.0f,

			1.1f,-1.2f,0.0f,
			0.0f,1.0f,0.0f,

			-1.1f,-1.2f,0.0f,
			1.1f,-1.2f,0.0f
		};

		final float[] rectangleVertices = new float[]
		{
			1.0f,1.0f,0.0f,
			-1.0f,1.0f,0.0f,

			-1.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,

			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f,

			1.0f,-1.0f,0.0f,
			1.0f,1.0f,0.0f,
		};

	

		final float[] redLineVertices = new float[]
		{
			0.0f,2.0f,0.0f,
			0.0f,-2.0f,0.0f
		};
		//color
		final float[] redLineColor = new float[]
		{
			1.0f,0.0f,0.0f,
			1.0f,0.0f,0.0f
		};		
		
		// rectangle Vertices
		final float[] blueLineVertices = new float[]
		{
			-2.0f,0.0f,0.0f,
			6.0f,0.0f,0.0f
		};
		
		//color
		final float[] blueLineColor = new float[]
		{
			0.0f,0.0f,1.0f,
			0.0f,0.0f,1.0f
		};
		//Triangle
		//create vao
		GLES30.glGenVertexArrays(1,vao_graph_red,0);
		
		GLES30.glBindVertexArray(vao_graph_red[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_graph_red,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_graph_red[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(redLineVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer1.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer1.put(redLineVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer1.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,redLineVertices.length * 4, positionBuffer1, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		//---- triangle Color
		//create vbo
		GLES30.glGenBuffers(1,vbo_color_graph_red,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_graph_red[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(redLineColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer2.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer colorBuffer2 = byteBuffer2.asFloatBuffer();
		
		//Now fill the data of array
		colorBuffer2.put(redLineColor);
		
		//set the array to zero th postion (In case of interleaved )
		colorBuffer2.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,redLineColor.length * 4, colorBuffer2, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		GLES30.glBindVertexArray(0);
		
		
		//Rectangle
		//create vao
		GLES30.glGenVertexArrays(1,vao_graph_blue,0);
		
		GLES30.glBindVertexArray(vao_graph_blue[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_graph_blue,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_graph_blue[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(blueLineVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer3.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer3 = byteBuffer3.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer3.put(blueLineVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer3.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,blueLineVertices.length * 4, positionBuffer3, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		// ---- color rectangle
		//create vbo
		GLES30.glGenBuffers(1,vbo_color_graph_blue,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_graph_blue[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(blueLineColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer4.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer colorBuffer4 = byteBuffer4.asFloatBuffer();
		
		//Now fill the data of array
		colorBuffer4.put(blueLineColor);
		
		//set the array to zero th postion (In case of interleaved )
		colorBuffer4.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,blueLineColor.length * 4,colorBuffer4, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		GLES30.glBindVertexArray(0);
		
		//Rectangle
		//create vao
		GLES30.glGenVertexArrays(1,vao_rectangle,0);
		
		GLES30.glBindVertexArray(vao_rectangle[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_rectangle,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_rectangle[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer5 = ByteBuffer.allocateDirect(rectangleVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer5.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer5 = byteBuffer5.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer5.put(rectangleVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer5.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,rectangleVertices.length * 4, positionBuffer5, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		GLES30.glBindVertexArray(0);

		
		//Rectangle
		//create vao
		GLES30.glGenVertexArrays(1,vao_triangle,0);
		
		GLES30.glBindVertexArray(vao_triangle[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_triangle,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_triangle[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer6 = ByteBuffer.allocateDirect(triangleVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer6.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer6 = byteBuffer6.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer6.put(triangleVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer6.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,triangleVertices.length * 4, positionBuffer6, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		
		GLES30.glBindVertexArray(0);

		//circle

		//create vao
		GLES30.glGenVertexArrays(1,vao_circle,0);
		
		GLES30.glBindVertexArray(vao_circle[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_circle,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_circle[0]);
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,4 * 3 * 4, null, GLES30.GL_DYNAMIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);


		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							(float)width / (float)height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
		int graphI, graphJ;
		float redX = -4.0f , blueY = 2.0f;

		int  i = 0;
	    int j = 0;
		float x, y, z;

		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);

		//Triangle
		//declerations of matrix
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];

		//Initialize above matrix to identity
		
		for(graphI=0;graphI<=160;graphI++,redX = redX + 0.05f)
		{
			Matrix.setIdentityM(modelViewMatrix,0); 
			Matrix.setIdentityM(modelViewProjectionMatrix,0); 
			
			//Do neccessary transformation

			Matrix.translateM(modelViewMatrix,0,redX, 0.0f,-8.0f);
		
			//Do neccessary Matrix Multilication
			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
			
			GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

			//Bind with vao
			GLES30.glBindVertexArray(vao_graph_red[0]);

			//Bind texture if any

			//Draw function
			GLES30.glDrawArrays(GLES30.GL_LINES,0, 2);
		
			//Unbind vao
			GLES30.glBindVertexArray(0);
		}
		for(graphJ=0;graphJ<=80;graphJ++,blueY = blueY - 0.05f)
		{
			Matrix.setIdentityM(modelViewMatrix,0); 
			Matrix.setIdentityM(modelViewProjectionMatrix,0); 
			
			//Do neccessary transformation

			Matrix.translateM(modelViewMatrix,0,-2.0f, blueY,-8.0f);
		
			//Do neccessary Matrix Multilication
			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
			
			GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

			//Bind with vao
			GLES30.glBindVertexArray(vao_graph_blue[0]);

			//Bind texture if any

			//Draw function
			GLES30.glDrawArrays(GLES30.GL_LINES,0, 2);
		
			//Unbind vao
			GLES30.glBindVertexArray(0);
		}

		GLES30.glLineWidth(3.0f);
			// TRIANGLE
			Matrix.setIdentityM(modelViewMatrix,0); 
			Matrix.setIdentityM(modelViewProjectionMatrix,0); 
			
			// GLES30.glPointSize(3.2f);
			// GLES30.glLineWidth(3.2f);
			//Do neccessary transformation

			Matrix.translateM(modelViewMatrix,0,0.0f, 0.1f, -5.5f);
		
			//Do neccessary Matrix Multilication
			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
			
			GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

			//Bind with vao
			GLES30.glBindVertexArray(vao_triangle[0]);

			//Bind texture if any

			//Draw function
			GLES30.glDrawArrays(GLES30.GL_LINES,0, 2);
			GLES30.glDrawArrays(GLES30.GL_LINES,2, 2);
			GLES30.glDrawArrays(GLES30.GL_LINES,4, 2);
		
			//Unbind vao
			GLES30.glBindVertexArray(0);

			



			// RECTANGLE	

			GLES30.glLineWidth(3.0f);
			
			Matrix.setIdentityM(modelViewMatrix,0); 
			Matrix.setIdentityM(modelViewProjectionMatrix,0); 
			
			// GLES30.glPointSize(3.2f);
			// GLES30.glLineWidth(3.2f);
			//Do neccessary transformation

			Matrix.translateM(modelViewMatrix,0,0.0f, 0.0f, -5.0f);
		
			//Do neccessary Matrix Multilication
			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
			
			GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

			//Bind with vao
			GLES30.glBindVertexArray(vao_rectangle[0]);

			//Bind texture if any

			//Draw function
			GLES30.glDrawArrays(GLES30.GL_LINES,0, 2);
			GLES30.glDrawArrays(GLES30.GL_LINES,2, 2);
			GLES30.glDrawArrays(GLES30.GL_LINES,4, 2);
			GLES30.glDrawArrays(GLES30.GL_LINES,6, 2);
		
			//Unbind vao
			GLES30.glBindVertexArray(0);



		//CIRCLE

			GLES30.glLineWidth(3.0f);

			Matrix.setIdentityM(modelViewMatrix,0); 
			Matrix.setIdentityM(modelViewProjectionMatrix,0); 
			
			//Do neccessary transformation

			Matrix.translateM(modelViewMatrix,0,0.0f, -0.6f, -7.9f);
		
			//Do neccessary Matrix Multilication
			Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
			
			GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);			
		
			GLES30.glBindVertexArray(vao_circle[0]);
			for (i = 0; i < 360; i++)
		{
			angale = (2.0*3.14159265358979323846*i) / 360;
			x = (float)Math.cos(angale);
			y = (float)Math.sin(angale);
			z = 0.0f;

			circleVertices[j] = x;
			circleVertices[j + 1] = y;
			circleVertices[j + 2] = z;
			j = j + 3;
		}

		//Allocate buffer from native memory
		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(circleVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer1.order(ByteOrder.nativeOrder());

		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer posBuffer1 = byteBuffer1.asFloatBuffer();
		
		//Now fill the data of array
		posBuffer1.put(circleVertices);
		
		//set the array to zero th postion (In case of interleaved )
		posBuffer1.position(0);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_circle[0]);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,circleVertices.length * 4, posBuffer1, GLES30.GL_DYNAMIC_DRAW);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_POINTS,0, 360);

		//Unbind vao
		GLES30.glBindVertexArray(0);
		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
	}
	
	
	void uninitialize()
	{
		if (vbo_color_graph_blue[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_color_graph_blue,0);
			vbo_color_graph_blue[0] = 0;
		}
		if (vbo_color_graph_red[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vbo_color_graph_red,0);
			vbo_color_graph_red[0] = 0;
		}
		if (vbo_position_graph_blue[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_graph_blue,0);
			vbo_position_graph_blue[0] = 0;
		}
		if (vbo_position_graph_red[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vbo_position_graph_red,0);
			vbo_position_graph_red[0] = 0;
		}
		if (vbo_position_triangle[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vbo_position_triangle,0);
			vbo_position_triangle[0] = 0;
		}
		if (vbo_position_rectangle[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vbo_position_rectangle,0);
			vbo_position_rectangle[0] = 0;
		}
		if (vbo_position_circle[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vbo_position_circle,0);
			vbo_position_circle[0] = 0;
		}
		if (vao_graph_red[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_graph_red,0);
			vao_graph_red[0] = 0;
		}

		if (vao_graph_blue[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_graph_blue,0);
			vao_graph_blue[0] = 0;
		}
		if (vao_triangle[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_triangle,0);
			vao_triangle[0] = 0;
		}
		if (vao_rectangle[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_rectangle,0);
			vao_rectangle[0] = 0;
		}
		if (vao_circle[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_circle,0);
			vao_circle[0] = 0;
		}

		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



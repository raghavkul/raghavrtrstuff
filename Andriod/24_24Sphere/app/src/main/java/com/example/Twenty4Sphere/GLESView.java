package com.example.Twenty4Sphere;


//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
 
//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.lang.Math;
//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int singleTap = 0;

	private int iWinWidth,iWinHeight;

	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
	

   	Sphere sphere=new Sphere();
    float sphere_vertices[]=new float[1146];
    float sphere_normals[]=new float[1146];
    float sphere_textures[]=new float[764];
    short sphere_elements[]=new short[2280];

  	
    int numVertices;
	int numElements;
  
	private int mUniform;
	private int vUniform;
	private int pUniform;
	
	private int laUniform;
	private int ldUniform;
	private int lsUniform;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShiUniform;
	

	private int lightPositionUniform;

	private int lKeyIsPressedUniform;
	
	boolean  gbLighting = false;


	private float lightAmbient[] = new float[]{0.0f, 0.0f, 0.0f, 0.0f};
    private float lightDiffuse[] = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    private float lightSpecular[] = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    private float lightPosition[] = new float[]{0.0f, 0.0f, 0.0f, 1.0f};

	//Animation varibales
	float angleOfXRotation = 0.0f;
	float angleOfYRotation = 0.0f;
	float angleOfZRotation = 0.0f;
	
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if (gbLighting == false)
			{
				gbLighting = true;
			}else{
				gbLighting = false;
			}
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		singleTap++;
		if(singleTap>3)
		{
			singleTap=0;
		}
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		iWinWidth = width;
		iWinHeight = height;
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
		update();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"uniform vec4 u_lightPosition;\n" +
		"out vec3 tNorm;" +
		"out vec3 lightDirection;" +
		"out vec3 viwerVector;" +
		"void main(void)\n" +
		"{\n" +
		"	if(u_lKeyIsPressed == 1)\n" +
		"	{\n" +
			"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
			"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" +
			"lightDirection = vec3(u_lightPosition - eye_Coordinate);" +
			
			"viwerVector = vec3(-eye_Coordinate.xyz);" +
			
			"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;\n" +
		"in vec3 tNorm;" +
		"in vec3 lightDirection;" +
		"in vec3 viwerVector;" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform mediump float u_materialShine;" +
		"out vec4 FragColor;\n" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"void main(void)" +
		"{" +
		"	if(u_lKeyIsPressed == 1)" +
		"	{" +
				"vec3 normalizeTNorm = normalize(tNorm);" +
				"vec3 normalizeLightDirection = normalize(lightDirection);" +
				"vec3 normalizeViwerVector = normalize(viwerVector);" +
				"float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" +
				"vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" +
				"vec3 ambiant = vec3(u_la * u_ka);" +
				"vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" +
				"vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 phong_ads_light = ambiant + diffuse + specular;" +
		"		FragColor = vec4(phong_ads_light,1.0f);\n" +
		"	}" +
		"	else" +
		"	{" + 
		"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);\n" +
		"	}" +
		"}"
		
		);

		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		// ----position
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		// ---- color
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status error:  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_m_matrix");
		vUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_v_matrix");
		pUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		
		laUniform =GLES30.glGetUniformLocation(shaderProgramObject, "u_la");
		ldUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_ld");
		lsUniform =GLES30.glGetUniformLocation(shaderProgramObject, "u_ls");
		
		kaUniform =GLES30.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShiUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_materialShine");
		
		lightPositionUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lightPosition");
		lKeyIsPressedUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
		
		
				
	
	
		sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    	numVertices = sphere.getNumberOfSphereVertices();
    	numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES30.glGenVertexArrays(1,vao_sphere,0);
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES30.glGenBuffers(1,vbo_sphere_position,0);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES30.GL_FLOAT,
                                     false,0,0);
        
        GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES30.glGenBuffers(1,vbo_sphere_normal,0);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES30.GL_FLOAT,
                                     false,0,0);
        
        GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES30.glGenBuffers(1,vbo_sphere_element,0);
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES30.glBindVertexArray(0);


		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.3f,0.3f,0.3f,1.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							(float)width / (float)height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);

		
		
		if (gbLighting == true)
		{

			GLES30.glUniform1i(lKeyIsPressedUniform,1);
			
			GLES30.glUniform3fv(laUniform, 1, lightAmbient, 0);
            GLES30.glUniform3fv(ldUniform, 1, lightDiffuse, 0);
            GLES30.glUniform3fv(lsUniform, 1, lightSpecular, 0);
			
			if(singleTap == 1)
			{
				lightPosition[1] = (float)Math.cos(angleOfXRotation) * 10.0f;
				lightPosition[2] = (float)-Math.sin(angleOfXRotation) * 10.0f;
				GLES30.glUniform4fv(lightPositionUniform,1,lightPosition,0);
			}else if(singleTap == 2)
			{
				lightPosition[0] = (float)Math.cos(angleOfYRotation) * 10.0f;
				lightPosition[2] = (float)-Math.sin(angleOfYRotation) * 10.0f;
				GLES30.glUniform4fv(lightPositionUniform,1,lightPosition,0);
			}else if(singleTap == 3)
			{
				lightPosition[0] = (float)Math.cos(angleOfZRotation) * 10.0f;
				lightPosition[1] = (float)Math.sin(angleOfZRotation) * 10.0f;
				GLES30.glUniform4fv(lightPositionUniform,1,lightPosition,0);
			}			
		
		}
		else {
			GLES30.glUniform1i(lKeyIsPressedUniform, 0);
		}

			draw24Spheres();
			
		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
		
	}
	
void draw24Spheres()
{

		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] projectionMatrix = new float[16];
			
		float[] MaterialAmbiant = new float[4];
		float[] MaterialDiffuse = new float[4];
		float[] MaterialSpecular = new float[4];
		float[] MaterialShininess = new float[1];

		int currentViewportX;
		int currentViewportY;	

		 int windowCenterX = iWinWidth / 2;    //Calculate center to set initial viewport to
        int windowCenterY = iWinHeight / 2;
        int relocatedVPSizeX = iWinWidth / 6;     // Divide Width into equal 6 parts, 4 colums and 2 margins
        int relocatedVPSizeY = iWinHeight / 8;    //Divide Height into equal 8 parts, 6 rows and 2 margins
        //Now, if we are changing the viewing volume, we must chnage aspect ratio in perspective

		 Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (relocatedVPSizeX/relocatedVPSizeY), 0.1f, 100.0f);
        GLES30.glViewport(windowCenterX, windowCenterY, relocatedVPSizeX, relocatedVPSizeY);
    
        int xTransOffset = relocatedVPSizeX;
        int yTransOffset = relocatedVPSizeY;
      
        
        //1
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = windowCenterX - xTransOffset*3; //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.9); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        MaterialAmbiant[0] = 0.0215f;
		MaterialAmbiant[1] = 0.1745f;
		MaterialAmbiant[2] = 0.0215f;
		MaterialAmbiant[3] = 1.0f;

		MaterialDiffuse[0] = 0.07568f;
		MaterialDiffuse[1] = 0.61424f;
		MaterialDiffuse[2] = 0.0215f;
		MaterialDiffuse[3] = 1.0f;

		MaterialSpecular[0] = 0.633f;
		MaterialSpecular[1] = 0.727811f;
		MaterialSpecular[2] = 0.633f;
		MaterialSpecular[3] = 1.0f;

		MaterialShininess[0] = 0.6f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //2

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = windowCenterX - xTransOffset*3; //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.5); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.135f;
		MaterialAmbiant[1] = 0.2225f;
		MaterialAmbiant[2] = 0.1575f;
		MaterialAmbiant[3] = 1.0f;

		MaterialDiffuse[0] = 0.54f;
		MaterialDiffuse[1] = 0.89f;
		MaterialDiffuse[2] = 0.63f;
		MaterialDiffuse[3] = 1.0f;
		

		MaterialSpecular[0] = 0.316228f;
		MaterialSpecular[1] = 0.316228f;
		MaterialSpecular[2] = 0.316228f;
		MaterialSpecular[3] = 1.0f;
		

		MaterialShininess[0] = 0.1f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //3

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = windowCenterX - xTransOffset*3; //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.1); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.5375f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.06625f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.18275f;
	MaterialDiffuse[1] = 0.17f;
	MaterialDiffuse[2] = 0.22525f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.332741f;
	MaterialSpecular[1] = 0.328634f;
	MaterialSpecular[2] = 0.346435f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.3f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //4

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = windowCenterX - xTransOffset*3; //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.3)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.20725f;
	MaterialAmbiant[2] = 0.20725f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 1.0f;
	MaterialDiffuse[1] = 0.829f;
	MaterialDiffuse[2] = 0.829f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.296648f;
	MaterialSpecular[1] = 0.296648f;
	MaterialSpecular[2] = 0.296648f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.088f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //5

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = windowCenterX - xTransOffset*3; //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.7)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.1745f;
	MaterialAmbiant[1] = 0.01175f;
	MaterialAmbiant[2] = 0.01175f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.61424f;
	MaterialDiffuse[1] = 0.04136f;
	MaterialDiffuse[2] = 0.04136f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.727811f;
	MaterialSpecular[1] = 0.626959f;
	MaterialSpecular[2] = 0.626959f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.6f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //6

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = windowCenterX - xTransOffset*3; //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-1.1)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.1f;
	MaterialAmbiant[1] = 0.18725f;
	MaterialAmbiant[2] = 0.1745f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.396f;
	MaterialDiffuse[1] = 0.74151f;
	MaterialDiffuse[2] = 0.69102f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.297254f;
	MaterialSpecular[1] = 0.30829f;
	MaterialSpecular[2] = 0.306678f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.1f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);



         //7
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*1.5); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.9); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        MaterialAmbiant[0] = 0.329412f;
	MaterialAmbiant[1] = 0.223529f;
	MaterialAmbiant[2] = 0.27451f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.780392f;
	MaterialDiffuse[1] = 0.568627f;
	MaterialDiffuse[2] = 0.113725f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.992157f;
	MaterialSpecular[1] = 0.941176f;
	MaterialSpecular[2] = 0.807843f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.21794872f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //8

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*1.5); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.5); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.2125f;
	MaterialAmbiant[1] = 0.1275f;
	MaterialAmbiant[2] = 0.054f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.714f;
	MaterialDiffuse[1] = 0.4284f;
	MaterialDiffuse[2] = 0.18144f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.393548f;
	MaterialSpecular[1] = 0.271906f;
	MaterialSpecular[2] = 0.166721f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.2f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //9

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*1.5); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.1); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.25f;
	MaterialAmbiant[2] = 0.25f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.774597f;
	MaterialSpecular[1] = 0.774597f;
	MaterialSpecular[2] = 0.774597f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //10

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*1.5); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.3)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.19125f;
	MaterialAmbiant[1] = 0.0735f;
	MaterialAmbiant[2] = 0.02225f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.7038f;
	MaterialDiffuse[1] = 0.27048f;
	MaterialDiffuse[2] = 0.0828f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.256777f;
	MaterialSpecular[1] = 0.137622f;
	MaterialSpecular[2] = 0.086014f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.1f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //11

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*1.5); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.7)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.24725f;
	MaterialAmbiant[1] = 0.1995f;
	MaterialAmbiant[2] = 0.0745f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.75164f;
	MaterialDiffuse[1] = 0.60648f;
	MaterialDiffuse[2] = 0.22648f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.628281f;
	MaterialSpecular[1] = 0.555802f;
	MaterialSpecular[2] = 0.366065f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.4f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //12

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*1.5); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-1.1)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        
	MaterialAmbiant[0] = 0.19225f;
	MaterialAmbiant[1] = 0.19225f;
	MaterialAmbiant[2] = 0.19225f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.50754f;
	MaterialDiffuse[1] = 0.50754f;
	MaterialDiffuse[2] = 0.50754f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.508273f;
	MaterialSpecular[1] = 0.508273f;
	MaterialSpecular[2] = 0.508273f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.4f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


         //13
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-0.1)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.9); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.50f;
	MaterialSpecular[1] = 0.50f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //14

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-0.1)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.5); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.1f;
	MaterialAmbiant[2] = 0.06f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.0f;
	MaterialDiffuse[1] = 0.50980392f;
	MaterialDiffuse[2] = 0.50980392f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.50196078f;
	MaterialSpecular[1] = 0.50196078f;
	MaterialSpecular[2] = 0.50196078f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //15

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-0.1)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.1); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.1f;
	MaterialDiffuse[1] = 0.35f;
	MaterialDiffuse[2] = 0.1f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.45f;
	MaterialSpecular[1] = 0.55f;
	MaterialSpecular[2] = 0.45f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //16

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-0.1)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.3)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        
MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.0f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.6f;
	MaterialSpecular[2] = 0.6f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	MaterialShininess[0] = 0.088f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //17

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-0.1)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.7)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.55f;
	MaterialDiffuse[1] = 0.55f;
	MaterialDiffuse[2] = 0.55f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.70f;
	MaterialSpecular[1] = 0.70f;
	MaterialSpecular[2] = 0.70f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //18

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-0.1)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-1.1)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.60f;
	MaterialSpecular[1] = 0.60f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


         //19
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-2)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.9); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        MaterialAmbiant[0] = 0.02f;
	MaterialAmbiant[1] = 0.02f;
	MaterialAmbiant[2] = 0.02f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.4f;
	MaterialSpecular[1] = 0.4f;
	MaterialSpecular[2] = 0.4f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //20

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-2)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.5); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

		MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //21

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-2)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*0.1); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        
MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //22

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-2)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.3)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        
MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.04f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //23

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-2)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-0.7)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


        //24

        //Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-1.8f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
			
		 currentViewportX = (int)(windowCenterX - xTransOffset*(-2)); //get x to the left most side of X
         currentViewportY = (int)(windowCenterY + xTransOffset*(-1.1)); // get y to top most side of Y
        //set viewport there!.

		GLES30.glViewport(currentViewportX, currentViewportY, relocatedVPSizeX, relocatedVPSizeY);	

        

	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

		GLES30.glUniform3fv(kaUniform, 1,MaterialAmbiant,0);
		GLES30.glUniform3fv(kdUniform, 1,MaterialDiffuse,0);
		GLES30.glUniform3fv(ksUniform, 1,MaterialSpecular,0);
		GLES30.glUniform1fv(materialShiUniform,1,MaterialShininess,0);
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);


}

	void update()
{

	angleOfXRotation = angleOfXRotation + 0.03f;
	if (angleOfXRotation >= 360.0f)
	{
		angleOfXRotation = 0.0f;
	}
	angleOfYRotation = angleOfYRotation + 0.03f;
	if (angleOfYRotation >= 360.0f)
	{
		angleOfYRotation = 0.0f;
	}
	angleOfZRotation = angleOfZRotation + 0.03f;
	if (angleOfZRotation >= 360.0f)
	{
		angleOfZRotation = 0.0f;
	}


}
	
	void uninitialize()
	{
		// destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES30.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



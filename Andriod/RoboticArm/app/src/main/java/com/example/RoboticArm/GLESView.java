package com.example.RoboticArm;

import java.nio.ShortBuffer;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Window;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.View;
import android.view.WindowManager;
import android.opengl.GLSurfaceView;
import android.opengl.GLES32;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import  java.util.Stack;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	private float angleCube=0.0f;
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int[] vao_sphere=new int[1];
	private int[] vbo_sphere_position = new int[1];
	private int[] vbo_sphere_normal = new int[1];
	private int[] vbo_sphere_element = new int[1];
	int numVertices,numElements;
	Stack<float[]> st = new Stack<>();



	private static int lKeyIsPressedUniform;
	private static int ldUniform,kdUniform;
	private int mvUniform, pUniform, lightPositionUniform;

	private float shoulder = 0.0f;
	private float elbow = 0.0f;

	private float[] perspectiveProjectionMatrix = new float[16];
	boolean  gbLighting = false;

	public GLESView(Context drawingContext)
	{

		super(drawingContext);
		
		context=drawingContext;
		
		


		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(drawingContext,this,null,false);

		gestureDetector.setOnDoubleTapListener(this);

	}

	@Override
		public boolean onTouchEvent(MotionEvent event)
		{
				
			int eventaction=event.getAction();

			if(!gestureDetector.onTouchEvent(event))
			{
				super.onTouchEvent(event);
			}
		
				
			
			return (true);
		}

		@Override
			public boolean onDoubleTap(MotionEvent e)
		{
				//setText("Double Tap");
				
				shoulder = (shoulder + 3) % 360;
				shoulder = shoulder + 0.5f;
				return(true);
		}

		@Override
			public boolean onDoubleTapEvent(MotionEvent e)
		{
			
			return(true);
		}

		@Override
			public boolean onSingleTapConfirmed(MotionEvent e)
		{
			//setText("Single Tap");
			return (true);
		}

		@Override
			public boolean onDown(MotionEvent e)
		{
			return (true);
		}
		@Override
			public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)
		{
				return (true);
		}

		@Override
		public void onLongPress(MotionEvent e)
		{

				//setText("Long Press");
				if(gbLighting==false)
				{	
			
					gbLighting=true;
				}
				else
				{
			
					gbLighting=false;
				}

				
		}

		@Override
			public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
		{
			//setText("Scroll");
			uninitialize();
			System.exit(0);
			return (true);
		}

		@Override
			public void onShowPress(MotionEvent e)
		{

		}

		@Override
			public boolean onSingleTapUp(MotionEvent e)
		{
			
			return (true);
		}
		
		@Override
			public void onSurfaceCreated(GL10 gl,EGLConfig config)
		{
				String version = gl.glGetString(GL10.GL_VERSION);

				System.out.println("RTR:" + version);

				String sVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);

				System.out.println("RTR:" + sVersion);

				initialize();
		}

		@Override
			public void onSurfaceChanged(GL10 unused,int width,int height)
		{
			resize(width,height);
		}

		@Override
			public void onDrawFrame(GL10 unused)
		{
			display();
			//update();
		}



		private void initialize()
		{
			//GLES32.glClearColor(0.0f,0.0f,1.0f,1.0f);
			Sphere sphere = new Sphere();
			float sphere_vertices[]=new float[1146];
			float sphere_normals[]=new float[1146];
			float sphere_textures[]=new float[764];
			short sphere_elements[]=new short[2280];

			sphere.getSphereVertexData(sphere_vertices,sphere_normals,sphere_textures,sphere_elements);
			numVertices = sphere.getNumberOfSphereVertices();
			numElements = sphere.getNumberOfSphereElements();

			vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

			final String vertexShaderSourceCode = 
				String.format
				(
					" #version 320 es " +
					"\n" +
					"precision highp float;\n" +
					"precision highp int;\n" +

					"in vec4 vPosition;" +
					"in vec3 vNormal;" +
					"uniform mat4 u_mv_matrix;" +
					"uniform mat4 u_p_matrix;" +
					"uniform int u_lKeyIsPressed;" +
					"uniform vec3 u_ld;" +
					"uniform vec3 u_kd;" +
					"uniform vec4 u_lightPosition;" +
					"out vec3 diffuseColor;" +
					"void main(void)" +
					"{" +
						"if(u_lKeyIsPressed == 1)" +
						"{" +
							"vec4 eye_Coordinate = u_mv_matrix * vPosition;" +
							"mat3 normalMatrix = mat3(transpose(inverse(u_mv_matrix)));" +
							"vec3 tNorm = normalize(normalMatrix * vNormal);" +
							"vec3 s = vec3(u_lightPosition) -vec3(eye_Coordinate.xyz);" +
							"diffuseColor = u_ld * u_kd * dot(s,tNorm);" +
						"}" +
						"gl_Position = u_p_matrix * u_mv_matrix * vPosition;" +
					"}"

				);

			GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);


			GLES32.glCompileShader(vertexShaderObject);

			int[] iShaderCompileStatus = new int[1];
			int[] iInfoLogLen = new int[1];
			String szInfoLog = null;

			GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

			if(iShaderCompileStatus[0]==GLES32.GL_FALSE)
			{
				GLES32.glGetShaderiv(vertexShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLen,0);

				if(iInfoLogLen[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: Vertex Shader" + szInfoLog);
					uninitialize();
					System.exit(0);
				}
			}

			fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);


			final String fragmentShaderSourceCode = 
			String.format
			(
				"#version 320 es" +
				 "\n" +
				"precision highp float;\n" +
				"precision highp int;\n" +
				"in vec3 diffuseColor;" +
				"out vec4 FragColor;" +
				"uniform int u_lKeyIsPressed;" +
				"void main(void)" +
				"{" +
					"if(u_lKeyIsPressed == 1)" +
					"{" +
						"FragColor = vec4(diffuseColor,1.0f);" +
					"}" +
					"else" +
					"{" +
						"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" +
					"}" +
				"}"
			);

			GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
			GLES32.glCompileShader(fragmentShaderObject);

			iShaderCompileStatus[0] = 0;
			iInfoLogLen[0] = 0;
			szInfoLog = null;

			GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompileStatus,0);

			if(iShaderCompileStatus[0] ==GLES32.GL_FALSE)
			{
				GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLen,0);

				if(iInfoLogLen[0] > 0)
				{
					szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: Fragment Shader" + szInfoLog);
					uninitialize();
					System.exit(0);
				}
			}
			//Create , Attach,Bind ShaderProgram
			shaderProgramObject = GLES32.glCreateProgram();

			GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);

			GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);

			//Prelink
			GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
			GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");


			GLES32.glLinkProgram(shaderProgramObject);

				mvUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mv_matrix");
				pUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
				ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
				kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
				lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosition");
				lKeyIsPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");


	//write code based on compile error checking
			int[] iProgramLinkStatus = new int[1];
			iInfoLogLen[0] = 0;
			szInfoLog = null;

			GLES32.glGetShaderiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iProgramLinkStatus,0);
			if(iProgramLinkStatus[0] == GLES32.GL_FALSE)
			{
				GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH,iInfoLogLen,0);
				if(iInfoLogLen[0] > 0)
				{
					szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: Compilation" + szInfoLog);
					uninitialize();
					System.exit(0);
				}
			}



			GLES32.glGenVertexArrays(1,vao_sphere,0);

			GLES32.glBindVertexArray(vao_sphere[0]);

			GLES32.glGenBuffers(1,vbo_sphere_position,0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);

			ByteBuffer byteBuffer = ByteBuffer.allocateDirect(sphere_vertices.length * 4);
			
			//according to CPU reading (little endian or big endian)
			byteBuffer.order(ByteOrder.nativeOrder());


			//Create the float type buffer type & convert our buytbuffer into float buffer
			FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

			//Now put your array into the "cooked" buffer
			positionBuffer.put(sphere_vertices);

			//Set the array 0th position of the buffer
			positionBuffer.position(0);

			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,sphere_vertices.length * 4,positionBuffer,GLES32.GL_STATIC_DRAW);

			GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES32.GL_FLOAT,false,0,0);

			GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);


			//Rectangle Normals
			GLES32.glGenBuffers(1,vbo_sphere_normal,0);
			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);

			byteBuffer = ByteBuffer.allocateDirect(sphere_normals.length * 4 );
			
			//according to CPU reading (little endian or big endian)
			byteBuffer.order(ByteOrder.nativeOrder());


			//Create the float type buffer type & convert our buytbuffer into float buffer
			positionBuffer = byteBuffer.asFloatBuffer();

			//Now put your array into the "cooked" buffer
			positionBuffer.put(sphere_normals);

			//Set the array 0th position of the buffer
			positionBuffer.position(0);


			GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,sphere_normals.length *4,positionBuffer,GLES32.GL_STATIC_DRAW);

			GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,3,GLES32.GL_FLOAT,false,0,0);

			GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);

			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);


			//GLES32.glBindVertexArray(0);
			
			//Elements
			GLES32.glGenBuffers(1,vbo_sphere_element,0);
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);

			byteBuffer = ByteBuffer.allocateDirect(sphere_elements.length * 2 );
			
			//according to CPU reading (little endian or big endian)
			byteBuffer.order(ByteOrder.nativeOrder());


			//Create the float type buffer type & convert our buytbuffer into float buffer
			ShortBuffer elementsBuffer = byteBuffer.asShortBuffer();
			elementsBuffer.put(sphere_elements);
			elementsBuffer.position(0);


			GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,sphere_elements.length *2,elementsBuffer,GLES32.GL_STATIC_DRAW);

			GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);


			GLES32.glBindVertexArray(0);
		
			
			
			

			GLES32.glClearDepthf(1.0f);

			GLES32.glEnable(GLES32.GL_DEPTH_TEST);

			GLES32.glDepthFunc(GLES32.GL_LEQUAL);
			
			GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);



			Matrix.setIdentityM(perspectiveProjectionMatrix,0);
		}

		private void resize(int width,int height)
		{
			if(height == 0)
			{
				height = 1;
			}
			GLES32.glViewport(0,0,width,height);
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,60.0f,width / height,1.0f,20);
			System.out.println("RTR: In resize");			
		}



		private void display()
		{

			GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);
			GLES32.glUseProgram(shaderProgramObject);
			float[] translationMatrix = new float[16];
			float[] modelMatrix = new float[16];
			float[] modelViewMatrix = new float[16];

			float[] projectMatrix = new float[16];
			float[] rotationMatrixShoulder = new float[16];
			float[] rotationMatrixElbow = new float[16];
			float[] scaleMatrix = new float[16];
			float[] scaleMatrixElbow = new float[16];
			float[] translationMatrixElbow = new float[16];
			float[] translationMatrixShoulder = new float[16];

//------------------ Start of Shoulder Matrix-------------------------------------

			Matrix.setIdentityM(translationMatrix , 0);
			Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(modelViewMatrix,0);
			Matrix.setIdentityM(projectMatrix,0);
			Matrix.setIdentityM(rotationMatrixShoulder,0);
			Matrix.setIdentityM(scaleMatrix,0);
			Matrix.setIdentityM(translationMatrixShoulder,0);

			
			Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, -10.0f);
			Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translationMatrix,0);
			//st.push(modelMatrix);

			Matrix.rotateM(rotationMatrixShoulder,0,shoulder,0.0f,0.0f,1.0f);
			Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrixShoulder,0);
			//GLES32.glUniformMatrix4fv(mvUniform,1,false,modelMatrix,0);

			Matrix.translateM(translationMatrixShoulder, 0, 1.0f, 0.0f, 0.0f);
			Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translationMatrixShoulder,0);
			//GLES32.glUniformMatrix4fv(mvUniform,1,false,modelMatrix,0);
			/*st.push(modelMatrix);*/

			Matrix.scaleM(scaleMatrix,0,2.0f,0.4f,1.0f);

			Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,modelMatrix,0);
			Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);


			

			GLES32.glUniformMatrix4fv(mvUniform,1,false,modelViewMatrix,0);
			GLES32.glUniformMatrix4fv(pUniform,1,false,perspectiveProjectionMatrix,0);
			
			
			

			if(gbLighting == true)
			{
				
				GLES32.glUniform1i(lKeyIsPressedUniform,1);
				GLES32.glUniform3f(ldUniform, 0.5f, 0.35f, 0.05f);
				GLES32.glUniform3f(kdUniform, 0.5f, 0.35f, 0.05f);
				GLES32.glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);

			}
			else
			{
				GLES32.glUniform1i(lKeyIsPressedUniform,0);
			}

				
			GLES32.glBindVertexArray(vao_sphere[0]);

			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES,numElements,GLES32.GL_UNSIGNED_SHORT,0);
			GLES32.glBindVertexArray(0);



			
			//st.pop();
			
//--------------------------End of Shoulder Matrix--------------------------------------

/*float[] rotationMatrixElbow = new float[16];
			float[] translationMatrixElbow = new float[16];*/
// //--------------------------Elbow Matrix Start------------------------------------------
			Matrix.setIdentityM(translationMatrix , 0);
			Matrix.setIdentityM(scaleMatrixElbow,0);
			Matrix.setIdentityM(rotationMatrixElbow,0);
			Matrix.setIdentityM(translationMatrixElbow,0);
			//Matrix.setIdentityM(modelMatrix,0);
			Matrix.setIdentityM(modelViewMatrix,0);
			

			Matrix.translateM(translationMatrix, 0, 0.8f, 0.0f, 0.0f);

			// Matrix Multiplication
			Matrix.multiplyMM(modelMatrix, 0,modelMatrix, 0,translationMatrix, 0);
			
			//GLES32.glUniformMatrix4fv(mvUniform,1, false, modelMatrix, 0);
			
			// rotate
			Matrix.rotateM(rotationMatrixElbow, 0, (float)elbow, 0.0f, 0.0f, 1.0f);
			

			// Matrix Multiplication
			Matrix.multiplyMM(modelMatrix, 0,modelMatrix, 0,rotationMatrixElbow, 0);

			//GLES32.glUniformMatrix4fv(mvUniform,1, false,modelMatrix, 0);	
			
			Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, 0.0f);

			// Matrix Multiplication
			Matrix.multiplyMM(modelMatrix, 0,modelMatrix, 0,translationMatrix, 0);
			
			//GLES32.glUniformMatrix4fv(mvUniform,1, false,modelMatrix, 0);
			
			//st.push(modelMatrix);
			
			// scale
			Matrix.scaleM(scaleMatrixElbow, 0, 1.0f, 1.0f, 1.0f);

				Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,modelMatrix,0);
			Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);


			// Matrix Multiplication
			//Matrix.multiplyMM(modelMatrix, 0,modelMatrix, 0,scaleMatrixElbow, 0);
			
			GLES32.glUniformMatrix4fv(mvUniform,1, false, modelViewMatrix, 0);
					
			
			GLES32.glUniformMatrix4fv(pUniform,1,false, perspectiveProjectionMatrix, 0);
			
	
			
			// bind vao arm
			GLES32.glBindVertexArray(vao_sphere[0]);

			// bind with textures if any
			GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 0.5f, 0.35f, 0.05f);
			// draw scene
			GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
			GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

			// unbind vao arm
			GLES32.glBindVertexArray(0);
		

			
			//st.pop();

			
			


// //------------------------------Earth Matrix End----------------------------------------------------------


		
			GLES32.glUseProgram(0);
			

			elbow = (elbow + 3) % 360;
			elbow = elbow + 0.5f;
			requestRender();
		}

//yearEarth = ((yearEarth1)%360);
		void update()
		{
			
		}

		private void uninitialize()
		{

			if(vao_sphere[0]!=0)
			{
				GLES32.glDeleteVertexArrays(1,vao_sphere,0);
				vao_sphere[0]=0;
			}

			if(vbo_sphere_element[0] != 0)
			{
				GLES32.glDeleteBuffers(1,vbo_sphere_element,0);
				vbo_sphere_element[0]=0;
			}

			if(vbo_sphere_normal[0] != 0)
			{
				GLES32.glDeleteBuffers(1,vbo_sphere_normal,0);
				vbo_sphere_normal[0]=0;
			}
		
			if(vbo_sphere_position[0] != 0)
			{
				GLES32.glDeleteBuffers(1,vbo_sphere_position,0);
				vbo_sphere_position[0]=0;
			}


			if(shaderProgramObject !=0)
			{
				int[] shaderCount = new int[1];
				int shaderNumber ;
				int[] shaders = new int[1];

				GLES32.glUseProgram(shaderProgramObject);

				GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_ATTACHED_SHADERS,shaderCount,0);

				if (shaderCount[0] > 0)
				{
						GLES32.glGetAttachedShaders(shaderProgramObject,shaderCount[0],shaderCount,0,shaders,0);
						for(shaderNumber = 0;shaderNumber<shaderCount[0];shaderNumber++)
						{
							GLES32.glDetachShader(shaderProgramObject,shaders[shaderNumber]);
							GLES32.glDeleteShader(shaders[shaderNumber]);
							shaders[shaderNumber]=0;
						}
				}
				
				GLES32.glDeleteProgram(shaderProgramObject);
				shaderProgramObject=0;
				GLES32.glUseProgram(0);
			}
		}
}

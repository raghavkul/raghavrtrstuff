package com.example.PVLighting;


//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
 
//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	

	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

   	Sphere sphere=new Sphere();
    float sphere_vertices[]=new float[1146];
    float sphere_normals[]=new float[1146];
    float sphere_textures[]=new float[764];
    short sphere_elements[]=new short[2280];

  	//light values
	float[] lightAmbiant = new float[]{0.0f,0.0f,0.0f,0.0f};
	float[] lightDiffuse = new float[]{ 1.0f,1.0f,1.0f,1.0f };
	float[] lightSpecular = new float[]{ 1.0f,1.0f,1.0f,1.0f };
	float[] lightPosition = new float[]{100.0f,100.0f,100.0f,1.0f};
	//material values
	float[] materialAmbiant = new float[]{ 0.0f,0.0f,0.0f,0.0f };
	float[] materialDiffuse = new float[]{ 1.0f,1.0f,1.0f,1.0f };
	float[] materialSpecular = new float[]{ 1.0f,1.0f,1.0f,1.0f };
	float materialShinyness = 50.0f;

    int numVertices;
	int numElements;
  
	private int mUniform;
	private int vUniform;
	private int pUniform;
	private int laUniform;
	private int ldUniform;
	private int lsUniform;
	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShiUniform;
	private int lightPositionUniform;
	private int lKeyIsPressedUniform;
	
	boolean  gbLighting = false;
	
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if (gbLighting == true)
			{
				gbLighting = false;
			}
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		if (gbLighting == false)
			{
				gbLighting = true;
			}
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform mediump float u_materialShine;" +
		"uniform vec4 u_lightPosition;\n" +
		"out vec3 phong_ads_light;\n" +
		"void main(void)\n" +
		"{\n" +
		"	if(u_lKeyIsPressed == 1)\n" +
		"	{\n" +
			"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
			"vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix ) * vNormal);" +
			"vec3 lightDirection = normalize(vec3(u_lightPosition - eye_Coordinate));" +
			"float tn_dot_ld = max(dot(lightDirection,tNorm),0.0f);" +
			"vec3 reflectionVector = reflect(-lightDirection , tNorm);" +
			"vec3 viwerVector = normalize(-eye_Coordinate.xyz);" +
			"vec3 ambiant = u_la * u_ka;" +
			"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" +
			"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector , viwerVector),0.0f),u_materialShine);" +
			"phong_ads_light = ambiant + diffuse + specular;" +
		"	}\n" +
		"	else" +
			"{" +
			"phong_ads_light = vec3(1.0f,1.0f,1.0f);" +
			"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;\n" +
		"in vec3 phong_ads_light;\n" +
		"out vec4 FragColor;\n" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"void main(void)" +
		"{" +
		"	if(u_lKeyIsPressed == 1)" +
		"	{" +
		"		FragColor = vec4(phong_ads_light,1.0f);\n" +
		"	}" +
		"	else" +
		"	{" +
		"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);\n" +
		"	}" +
		"}"
		
		);
		
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		// ----position
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		// ---- color
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status error:  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_m_matrix");
		vUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_v_matrix");
		pUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		laUniform =GLES30.glGetUniformLocation(shaderProgramObject, "u_la");
		ldUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_ld");
		lsUniform =GLES30.glGetUniformLocation(shaderProgramObject, "u_ls");
		kaUniform =GLES30.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShiUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_materialShine");
		lightPositionUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lightPosition");
		lKeyIsPressedUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
		
		
				
	
	
	sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    numVertices = sphere.getNumberOfSphereVertices();
    numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES30.glGenVertexArrays(1,vao_sphere,0);
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES30.glGenBuffers(1,vbo_sphere_position,0);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES30.GL_FLOAT,
                                     false,0,0);
        
        GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES30.glGenBuffers(1,vbo_sphere_normal,0);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES30.GL_FLOAT,
                                     false,0,0);
        
        GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES30.glGenBuffers(1,vbo_sphere_element,0);
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES30.glBufferData(GLES30.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES30.GL_STATIC_DRAW);
        
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES30.glBindVertexArray(0);


		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							(float)width / (float)height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);

		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] translateMatrix = new float[16];

		float[] projectionMatrix = new float[16];
		//Rectangle
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-2.0f);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		//Do neccessary Matrix Multilication

		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);
		
		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
		
		if (gbLighting == true)
		{
			GLES30.glUniform1i(lKeyIsPressedUniform,1);
			GLES30.glUniform3f(laUniform, 0.0f,0.0f,0.0f);
			GLES30.glUniform3f(ldUniform, 1.0f,1.0f,1.0f);
			GLES30.glUniform3f(lsUniform, 1.0f,1.0f,1.0f);
			GLES30.glUniform3f(kaUniform, 0.0f,0.0f,0.0f);
			GLES30.glUniform3f(kdUniform, 1.0f,1.0f,1.0f);
			GLES30.glUniform3f(ksUniform, 1.0f,1.0f,1.0f);
			GLES30.glUniform1f(materialShiUniform, 50.0f);
			GLES30.glUniform4f(lightPositionUniform, 100.0f,100.0f,100.0f,1.0f);
		
		}
		else {
			GLES30.glUniform1i(lKeyIsPressedUniform, 0);
		}
	 // bind vao
        GLES30.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, numElements, GLES30.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES30.glBindVertexArray(0);
		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
		
		
	}
	
	
	void uninitialize()
	{
		// destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES30.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES30.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }
		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



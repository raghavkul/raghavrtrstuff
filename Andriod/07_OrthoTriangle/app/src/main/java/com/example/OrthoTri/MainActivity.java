package com.example.OrthoTri;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

//added manually
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.View;

public class MainActivity extends AppCompatActivity {

	private GLESView glesView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_main);
	   
	   this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
	 
	 this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	   WindowManager.LayoutParams.FLAG_FULLSCREEN);
	   
	   this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	   
	   this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);
	   
	   this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
	   
	   glesView = new GLESView(this);
	   
	   setContentView(glesView);
    }
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		HideNavigationBar();
		
	}
	public void HideNavigationBar()

	{

		View decorView = getWindow().getDecorView();

		decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE

		| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION

		| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

		| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

		| View.SYSTEM_UI_FLAG_FULLSCREEN

		| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

	}
}

package com.example.lightsOnPyramid;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao_pyramid = new int[1];//vertex array object for rect
	private int[] vbo_position_pyramid = new int[1];//vertex buffer object for rect
	private int[] vbo_normal_pyramid = new int[1]; // 

	private int mUniform;
	private int vUniform;
	private int pUniform;
	
	private int laUniformRed;
	private int ldUniformRed;
	private int lsUniformRed;

	private int laUniformBlue;
	private int ldUniformBlue;
	private int lsUniformBlue;

	private int kaUniform;
	private int kdUniform;
	private int ksUniform;
	private int materialShinynessUnifrom;

	private int lightPositionRedUniform;
	private int lightPositionBlueUniform;

	private int lKeyIsPressedUniform;
	
	

	private float[] materialAmbiant = new float[]{ 0.0f,0.0f,0.0f,0.0f };
	private float[] materialDiffuse = new float[]{ 1.0f,1.0f,1.0f,1.0f };
	private float[] materialSpecular = new float[]{ 1.0f,1.0f,1.0f,1.0f };
	private float[] materialShinyness = new float[]{50.0f};



	//light values
	
	private float[] lightAmbiantRed = new float[]{0.0f,0.0f,0.0f,0.0f};
	private float[] lightAmbiantBlue = new float[]{0.0f,0.0f,0.0f,0.0f};

	private float[] lightDiffuseRed = new float[]{1.0f,0.0f,0.0f,0.0f};
	private float[] lightDiffuseBlue = new float[]{0.0f,0.0f,1.0f,0.0f};

	private float[] lightSpecularRed = new float[]{1.0f,0.0f,0.0f,0.0f};
	private float[] lightSpecularBlue = new float[]{0.0f,0.0f,1.0f,0.0f};

	private float[] lightPositionRed = new float[]{-2.0f,0.0f,0.0f,0.0f};
	private float[] lightPositionBlue = new float[]{2.0f,0.0f,0.0f,0.0f};

	boolean  gbLighting = false;
	
	//Animation varibales
	private float angleRect = 0.0f;
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if (gbLighting == true)
			{
				gbLighting = false;
			}
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		if (gbLighting == false)
			{
				gbLighting = true;
			}
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		



		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +

		"uniform mediump int u_lKeyIsPressed;" +

		"uniform vec3 u_la_red;" +
		"uniform vec3 u_ld_red;" +
		"uniform vec3 u_ls_red;" +
		"uniform vec3 u_la_blue;" +
		"uniform vec3 u_ld_blue;" +
		"uniform vec3 u_ls_blue;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShine;" +
		"uniform vec4 u_lightPosition_red;" +
		"uniform vec4 u_lightPosition_blue;" +
		"out vec3 phong_ads_light1;" +
		"out vec3 phong_ads_light2;" +
		"void main(void)" +
		"{" +
		"if(u_lKeyIsPressed == 1)" +
		"{" +
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
		"vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix ) * vNormal);" +
		"vec3 lightDirection1 = normalize(vec3(u_lightPosition_red - eye_Coordinate));" +
		"vec3 lightDirection2 = normalize(vec3(u_lightPosition_blue - eye_Coordinate));" +
		"float tn_dot_ld1 = max(dot(lightDirection1,tNorm),0.0f);" +
		"float tn_dot_ld2 = max(dot(lightDirection2,tNorm),0.0f);" +
		"vec3 reflectionVector1 = reflect(-lightDirection1 , tNorm);" +
		"vec3 reflectionVector2 = reflect(-lightDirection2 , tNorm);" +
		"vec3 viwerVector = normalize(-eye_Coordinate.xyz);" +
		"vec3 ambiant1 = u_la_red * u_ka;" +
		"vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld1;" +
		"vec3 specular1 = u_ls_red * u_ks * pow(max(dot(reflectionVector1 , viwerVector),0.0f),u_materialShine);" +
		"vec3 ambiant2 = u_la_blue * u_ka;" +
		"vec3 diffuse2 = u_ld_blue * u_kd * tn_dot_ld2;" +
		"vec3 specular2 = u_ls_blue * u_ks * pow(max(dot(reflectionVector2 , viwerVector),0.0f),u_materialShine);" +
		"phong_ads_light1 = ambiant1 + diffuse1 + specular1;" +
		"phong_ads_light2 = ambiant2 + diffuse2 + specular2;" +
		"}" +
		"else" +
		"{" +
		"phong_ads_light1 = vec3(1.0f,1.0f,1.0f);" +
		"phong_ads_light2 = vec3(1.0f,1.0f,1.0f);" +
		"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;\n" +
		"in vec3 phong_ads_light1;" +
		"in vec3 phong_ads_light2;" +
		"out vec4 FragColor;\n" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"void main(void)" +
		"{" +
		"	if(u_lKeyIsPressed == 1)" +
		"	{" +
		"		FragColor = vec4(phong_ads_light1 + phong_ads_light2,1.0f);\n" +
		"	}" +
		"	else" +
		"	{" +
		"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);\n" +
		"	}" +
		"}"
		
		);
		
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment error: " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		// ----position
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		// ---- color
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status error:  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_m_matrix");
		vUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_v_matrix");
		pUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_p_matrix");
		
		laUniformRed = GLES30.glGetUniformLocation(shaderProgramObject, "u_la_red");
		ldUniformRed = GLES30.glGetUniformLocation(shaderProgramObject, "u_ld_red");
		lsUniformRed = GLES30.glGetUniformLocation(shaderProgramObject, "u_ls_red");

		laUniformBlue = GLES30.glGetUniformLocation(shaderProgramObject, "u_la_blue");
		ldUniformBlue = GLES30.glGetUniformLocation(shaderProgramObject, "u_ld_blue");
		lsUniformBlue = GLES30.glGetUniformLocation(shaderProgramObject, "u_ls_blue");

		kaUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_ka");
		kdUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_kd");
		ksUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_ks");
		materialShinynessUnifrom= GLES30.glGetUniformLocation(shaderProgramObject, "u_materialShine");		

		lightPositionRedUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lightPosition_red");
		lightPositionBlueUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lightPosition_blue");

		lKeyIsPressedUniform = GLES30.glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
		
			
		// rectangle Vertices
		final float[] pyramidVertices = new float[]
		{
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, 1.0f,
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f,
			1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			0.0f, 1.0f, 0.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f, 1.0f
		};
		
		//color
		final float[] pyramidNormal = new float[]
		{
		0.0f, 0.447214f, 0.89442f,
		0.0f, 0.447214f, 0.89442f,
		0.0f, 0.447214f, 0.89442f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		0.0f, 0.447214f, -0.89442f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f
		};
		
		//Rectangle
		//create vao
		GLES30.glGenVertexArrays(1,vao_pyramid,0);
		
		GLES30.glBindVertexArray(vao_pyramid[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_pyramid,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_pyramid[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer2.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer2 = byteBuffer2.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer2.put(pyramidVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer2.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidVertices.length * 4, positionBuffer2, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		// ---- color rectangle
		//create vbo
		GLES30.glGenBuffers(1,vbo_normal_pyramid,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_normal_pyramid[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(pyramidNormal.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer4.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer colorBuffer2 = byteBuffer4.asFloatBuffer();
		
		//Now fill the data of array
		colorBuffer2.put(pyramidNormal);
		
		//set the array to zero th postion (In case of interleaved )
		colorBuffer2.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidNormal.length * 4,colorBuffer2, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		GLES30.glBindVertexArray(0);
		
		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							(float)width / (float)height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);

		float[] rotationMatrix = new float[16];
		float[] modelMatrix = new float[16];
		float[] viewMatrix = new float[16];
		float[] translateMatrix = new float[16];
		float[] projectionMatrix = new float[16];
	//	float[] modelViewProjectionMatrix = new float[16];
		//Rectangle
		//Initialize above matrix to identity
		
		Matrix.setIdentityM(rotationMatrix, 0);
		Matrix.setIdentityM(modelMatrix,0); 
		Matrix.setIdentityM(viewMatrix,0);
		Matrix.setIdentityM(translateMatrix ,0);
	
		Matrix.setIdentityM(projectionMatrix,0); 
	//	Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(translateMatrix,0,0.0f, 0.0f,-5.0f);
		Matrix.rotateM(rotationMatrix,0,angleRect,0.0f,1.0f,0.0f);
		
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,translateMatrix,0);
		Matrix.multiplyMM(modelMatrix,0,modelMatrix,0,rotationMatrix,0);

	//	Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		Matrix.multiplyMM(projectionMatrix,0,perspectiveProjectionMatrix,0,projectionMatrix,0);

		GLES30.glUniformMatrix4fv(mUniform,1,false,modelMatrix,0);
		GLES30.glUniformMatrix4fv(vUniform,1,false,viewMatrix,0);
		GLES30.glUniformMatrix4fv(pUniform,1,false,projectionMatrix,0);
		
		
		if (gbLighting == true)
		{
			GLES30.glUniform1i(lKeyIsPressedUniform,1);
			

			GLES30.glUniform3fv(laUniformRed,1,lightAmbiantRed,0);
			GLES30.glUniform3fv(ldUniformRed,1,lightDiffuseRed,0);
			GLES30.glUniform3fv(lsUniformRed,1,lightSpecularRed,0);
			GLES30.glUniform3fv(laUniformBlue,1,lightAmbiantBlue,0);
			GLES30.glUniform3fv(ldUniformBlue,1,lightDiffuseBlue,0);
			GLES30.glUniform3fv(lsUniformBlue,1,lightSpecularBlue,0);
			
			GLES30.glUniform3fv(kaUniform, 1,materialAmbiant,0);
			GLES30.glUniform3fv(kdUniform, 1,materialDiffuse,0);
			GLES30.glUniform3fv(ksUniform, 1,materialSpecular,0);
			GLES30.glUniform1fv(materialShinynessUnifrom,1,materialShinyness,0);


			GLES30.glUniform4fv(lightPositionRedUniform,1,lightPositionRed,0);
			GLES30.glUniform4fv(lightPositionBlueUniform,1,lightPositionBlue,0);
		}
		else {
			GLES30.glUniform1i(lKeyIsPressedUniform, 0);
		}
	
		//Bind with vao
		GLES30.glBindVertexArray(vao_pyramid[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,0, 3);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,3, 3);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,6, 3);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,9, 3);
	
		//Unbind vao
		GLES30.glBindVertexArray(0);
		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
		
		angleRect = angleRect + 0.3f;
		if (angleRect >= 360)
		{
			angleRect = 0.0f;
		}
	}
	
	
	void uninitialize()
	{
		if (vbo_normal_pyramid[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_normal_pyramid,0);
			vbo_normal_pyramid[0] = 0;
		}
		if (vbo_position_pyramid[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_pyramid,0);
			vbo_position_pyramid[0] = 0;
		}
		if (vao_pyramid[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_pyramid,0);
			vao_pyramid[0] = 0;
		}
		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



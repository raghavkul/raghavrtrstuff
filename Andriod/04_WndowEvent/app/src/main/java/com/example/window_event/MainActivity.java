package com.example.window_event;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

//added manually
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.view.View;

public class MainActivity extends AppCompatActivity {

	private windowEvent windowevent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_main);
	   
	   this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
	 
	 this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	   WindowManager.LayoutParams.FLAG_FULLSCREEN);
	   
	   this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	   
	   this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);
	   
	   this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
	   
	   windowevent = new windowEvent(this);
	   
	   setContentView(windowevent);
    }
	
	@Override
	protected void onPause()
	{
		super.onPause();
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
	}
}

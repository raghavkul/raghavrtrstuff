package com.example.FBO;

//added manually

import android.content.Context;
import  android.view.Gravity;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;

//packages for pp

import android.opengl.GLSurfaceView;
import android.opengl.GLES30;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

//Packages for ortho

//For OpenGL Buffers
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

//for Matrix Math
import android.opengl.Matrix; 

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnGestureListener , OnDoubleTapListener
{
	private GestureDetector gestureDetector;
	private final Context context;
	
	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	
	private int[] vao = new int[1];
	private int[] vbo = new int[1];
	
	private int[] vao_pyramid = new int[1];//vertex array object for tri
	private int[] vao_cube = new int[1];//vertex array object for rect
	private int[] vbo_position_pyramid = new int[1];//vertex buffer object for tri
	private int[] vbo_position_cube = new int[1];//vertex buffer object for rect
	private int[] vbo_color_pyramid = new int[1] ;//vertex buffer object(color) for tri
	private int[] vbo_color_cube = new int[1];//vertex buffer object(color) for rect

	
	private int mvpUniform;
	
	//Animation varibales
	private float angleTri = 0.0f;
	 private float angleRect = 0.0f;
	//4*4 Matrix
	private float[] perspectiveProjectionMatrix = new float[16]; 
	//Constructor
	public GLESView(Context drawingContext)
	{
		super(drawingContext);
		context = drawingContext;
		
		//functions of glSurfaceView 
		setEGLContextClientVersion(3);
		setRenderer(this);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		gestureDetector = new GestureDetector(drawingContext,this,null,false);
		gestureDetector.setOnDoubleTapListener(this);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if(!gestureDetector.onTouchEvent(event))
		{
			super.onTouchEvent(event);
		}
		return(true);
	}
	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}
	@Override
	public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX, float velocityY)
	{
		return(true);
	}
	@Override
	public void onLongPress(MotionEvent e)
	{
	}
	@Override
	public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX, float distanceY)
	{
		uninitialize();
		return(true);
	}
	
	@Override
	public void onShowPress(MotionEvent e)
	{
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}
	
	//Implements glSurfaceView.Renderer methods
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
		String version = gl.glGetString(GL10.GL_VERSION);
		System.out.println("RTR: Version of openGL ES " +version);
		
		String ver1 = gl.glGetString(GLES30.GL_SHADING_LANGUAGE_VERSION);
		System.out.println("RTR: Version of shading language  " +ver1);
		
		
		// String ver2 = gl.glGetString(GLES30.GL_VENDER);
		// System.out.println("RTR: Version of Vender  " +ver2);
		
		
		// String ver3 = gl.glGetString(GLES30.GL_RENDERER);
		// System.out.println("RTR: Version of Renderer  " +ver3);
		
		initialize();
	}
	@Override
	public void onSurfaceChanged(GL10 unused, int width , int height)
	{
		resize(width,height);
	}
	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}
	
	
	//our functions
	
	
	private void initialize()
	{
		
		
		//vertex shader
		vertexShaderObject = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
		final String vertexShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}"
		
		);
		
		GLES30.glShaderSource(vertexShaderObject,vertexShaderSourceCode);
		GLES30.glCompileShader(vertexShaderObject);
		
		//Error checking
		
		int[] iShaderCompileStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog = null;
		int[] iShaderLinkStatus = new int[1];
		
		GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(vertexShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(vertexShaderObject);
					System.out.println("RTR: program compile status for vertex  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Fragment shader code
		
		fragmentShaderObject = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
		final String fragmentShaderSourceCode = String.format
		(
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_color;" +
		"}"
		
		);
		
		GLES30.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);
		GLES30.glCompileShader(fragmentShaderObject);
		
		//Error checking
		
		iShaderCompileStatus[0] = 0;
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		
		GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_COMPILE_STATUS,iShaderCompileStatus,0);
		
		if(iShaderCompileStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetShaderiv(fragmentShaderObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetShaderInfoLog(fragmentShaderObject);
					System.out.println("RTR: program compile status for fragment  " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//create program 
		shaderProgramObject = GLES30.glCreateProgram();
		
		//Attache shaders to program
		GLES30.glAttachShader(shaderProgramObject,vertexShaderObject);
		GLES30.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//prelinking
		// ----position
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_POSITION,"vPosition");
		// ---- color
		GLES30.glBindAttribLocation(shaderProgramObject,GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");
		//linking 
		GLES30.glLinkProgram(shaderProgramObject);
		
		//Error Checking
		iInfoLogLength[0] =0;
		szInfoLog = null;
		
		GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_LINK_STATUS,iShaderLinkStatus,0);
		
		if(iShaderLinkStatus[0] == GLES30.GL_FALSE)
		{
				GLES30.glGetProgramiv(shaderProgramObject,GLES30.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
				
				if(iInfoLogLength[0] > 0)
				{
					szInfoLog = GLES30.glGetProgramInfoLog(shaderProgramObject);
					System.out.println("RTR: program Link status   " +szInfoLog);
					uninitialize();
					System.exit(0);
				}
		
		}
		
		//Get uniform location
		mvpUniform = GLES30.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		
		
		//Triangle vertices
		
		final float[] pyramidVertices = new float[]
		{
			0.0f,1.0f,0.0f,-1.0f,-1.0f,1.0f,1.0f,-1.0f,1.0f,
			0.0f,1.0f,0.0f,1.0f,-1.0f,1.0f,1.0f,-1.0f,-1.0f,
			0.0f,1.0f,0.0f,1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,
			0.0f,1.0f,0.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,1.0f
		};
		//color
		final float[] pyramidColor = new float[]
		{
			1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,1.0f,
			1.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f,1.0f,0.0f,
			1.0f,0.0f,0.0f,0.0f,1.0f,0.0f,0.0f,0.0f,1.0f,
			1.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0,0.0f,1.0f,0.0f
		};		
		
		// rectangle Vertices
		final float[] cubeVertices = new float[]
		{
			1.0f,1.0f,-1.0f,-1.0f,1.0f,-1.0f,-1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,
			1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,1.0f,1.0f,-1.0f,1.0f,
			1.0f,1.0f,1.0f,-1.0f,1.0f,1.0f,-1.0f,-1.0f,1.0f,1.0f,-1.0f,1.0f,
			1.0f,1.0f,-1.0f,-1.0f,1.0f,-1.0f,-1.0f,-1.0f,-1.0f,1.0f,-1.0f,-1.0f,
			1.0f,1.0f,-1.0f,1.0f,1.0f,1.0f,1.0f,-1.0f,1.0f,1.0f,-1.0f,-1.0f,
			-1.0f,1.0f,1.0f,-1.0f,1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,1.0f
		};
		
		//color
		final float[] cubeColor = new float[]
		{
			1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,
			0.0f,1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,1.0f,0.0f,
			0.0f,0.0f,1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,1.0f,0.0f,0.0f,1.0f,
			0.0f,1.0f,1.0f,0.0f,1.0f,1.0f,0.0f,1.0f,1.0f,0.0f,1.0f,1.0f,
			1.0f,0.0f,1.0f,1.0f,0.0f,1.0f,1.0f,0.0f,1.0f,1.0f,0.0f,1.0f,
			1.0f,1.0f,0.0f,1.0f,1.0f,0.0f,1.0f,1.0f,0.0f,1.0f,1.0f,0.0f
		};
		//Triangle
		//create vao
		GLES30.glGenVertexArrays(1,vao_pyramid,0);
		
		GLES30.glBindVertexArray(vao_pyramid[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_pyramid,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_pyramid[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer1 = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer1.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer1 = byteBuffer1.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer1.put(pyramidVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer1.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidVertices.length * 4, positionBuffer1, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		//---- triangle Color
		//create vbo
		GLES30.glGenBuffers(1,vbo_color_pyramid,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_pyramid[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer3 = ByteBuffer.allocateDirect(pyramidColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer3.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer colorBuffer1 = byteBuffer3.asFloatBuffer();
		
		//Now fill the data of array
		colorBuffer1.put(pyramidColor);
		
		//set the array to zero th postion (In case of interleaved )
		colorBuffer1.position(0);
		
		
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,pyramidColor.length * 4, colorBuffer1, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		GLES30.glBindVertexArray(0);
		
		
		//Rectangle
		//create vao
		GLES30.glGenVertexArrays(1,vao_cube,0);
		
		GLES30.glBindVertexArray(vao_cube[0]);
		
		//create vbo
		GLES30.glGenBuffers(1,vbo_position_cube,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_position_cube[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer2 = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer2.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer positionBuffer2 = byteBuffer2.asFloatBuffer();
		
		//Now fill the data of array
		positionBuffer2.put(cubeVertices);
		
		//set the array to zero th postion (In case of interleaved )
		positionBuffer2.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,cubeVertices.length * 4, positionBuffer2, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		
		// ---- color rectangle
		//create vbo
		GLES30.glGenBuffers(1,vbo_color_cube,0);
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vbo_color_cube[0]);
		
		
		//Creating Buffer
		//Allocate buffer from native memory
		ByteBuffer byteBuffer4 = ByteBuffer.allocateDirect(cubeColor.length * 4);
		
		//Arrange the buffer in native byte order
		byteBuffer4.order(ByteOrder.nativeOrder());
		
		//create float type buffer and convert your byte buffer into float type buffer
		FloatBuffer colorBuffer2 = byteBuffer4.asFloatBuffer();
		
		//Now fill the data of array
		colorBuffer2.put(cubeColor);
		
		//set the array to zero th postion (In case of interleaved )
		colorBuffer2.position(0);
		
		GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,cubeColor.length * 4,colorBuffer2, GLES30.GL_STATIC_DRAW);
		
		GLES30.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,3,GLES30.GL_FLOAT,false,0,0);
		
		GLES30.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
		
		GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER,0);
		GLES30.glBindVertexArray(0);
		
		//Depth Lines
		GLES30.glEnable(GLES30.GL_DEPTH_TEST);
		GLES30.glDepthFunc(GLES30.GL_LEQUAL);
		//GLES30.glDisable(GLES30.GL_CULL_FACE);
		
		GLES30.glClearColor(0.0f,0.0f,0.0f,0.0f);
		
		//Give identity toorthographicProjectMatrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);

	}
	
	
	private void resize(int width , int height)
	{
		if (height == 0)
		{
			height = 1;
		}

		GLES30.glViewport(0,0,width,height);
		
		Matrix.perspectiveM(perspectiveProjectionMatrix,0,
							45.0f,
							width / height,
							0.1f,
							100.0f);

		
	}
	
	
	private void display()
	{
		GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT | GLES30.GL_DEPTH_BUFFER_BIT);
		GLES30.glUseProgram(shaderProgramObject);

		//Triangle
		//declerations of matrix
		float[] modelViewMatrix = new float[16];
		float[] modelViewProjectionMatrix = new float[16];
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,-1.5f, 0.0f,-7.0f);
		Matrix.rotateM(modelViewMatrix,0,angleTri,0.0f,1.0f,0.0f);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao_pyramid[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,0, 3);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,3, 3);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,6, 3);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLES,9, 3);
		

		//Unbind vao
		GLES30.glBindVertexArray(0);
		
		//Rectangle
		//Initialize above matrix to identity
		Matrix.setIdentityM(modelViewMatrix,0); 
		Matrix.setIdentityM(modelViewProjectionMatrix,0); 
		
		//Do neccessary transformation

		Matrix.translateM(modelViewMatrix,0,1.5f, 0.0f,-7.0f);
		Matrix.rotateM(modelViewMatrix,0,angleRect,1.0f,0.0f,0.0f);
		Matrix.scaleM(modelViewMatrix,0,0.75f,0.75f,0.75f);
		//Do neccessary Matrix Multilication
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);
		
		GLES30.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		//Bind with vao
		GLES30.glBindVertexArray(vao_cube[0]);

		//Bind texture if any

		//Draw function
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,0, 4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,4, 4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,8, 4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,12, 4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,16, 4);
		GLES30.glDrawArrays(GLES30.GL_TRIANGLE_FAN,20, 4);

		//Unbind vao
		GLES30.glBindVertexArray(0);
		//Unused Program
		GLES30.glUseProgram(0);
		
		requestRender(); // like swapBuffers
		
		angleTri = angleTri + 0.3f;
		if (angleTri >= 360)
		{
			angleTri = 0.0f;
		}
		angleRect = angleRect + 0.3f;
		if (angleRect >= 360)
		{
			angleRect = 0.0f;
		}
	}
	
	
	void uninitialize()
	{
		if (vbo_color_cube[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_color_cube,0);
			vbo_color_cube[0] = 0;
		}
		if (vbo_color_pyramid[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vbo_color_pyramid,0);
			vbo_color_pyramid[0] = 0;
		}
		if (vbo_position_cube[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_cube,0);
			vbo_position_cube[0] = 0;
		}
		if (vao_cube[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_cube,0);
			vao_cube[0] = 0;
		}
		if (vbo_position_pyramid[0]!=0)
		{
			GLES30.glDeleteBuffers(1, vbo_position_pyramid,0);
			vbo_position_pyramid[0] = 0;
		}
		if (vao_pyramid[0]!=0)
		{
			GLES30.glDeleteVertexArrays(1, vao_pyramid,0);
			vao_pyramid[0] = 0;
		}

		if (shaderProgramObject!=0)
		{
			int[] shaderCount = new int[1];
			int shaderNumber;
			
			GLES30.glUseProgram(shaderProgramObject);

			//Ask shader how many shaders are attached to you
			GLES30.glGetProgramiv(shaderProgramObject, GLES30.GL_ATTACHED_SHADERS, shaderCount,0);

			int[] shaders = new int[shaderCount[0]];

			if (shaders[0]!=0)
			{
				GLES30.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount,0, shaders,0);
				for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
				{
					//Dettach shaders
					GLES30.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

					//Delete Shaders
					GLES30.glDeleteShader(shaders[shaderNumber]);
					shaders[shaderNumber] = 0;
				}
			}
			GLES30.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
			GLES30.glUseProgram(0);
		}
	}
}



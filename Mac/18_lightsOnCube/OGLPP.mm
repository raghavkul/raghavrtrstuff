//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0,
};

// C style global function decleration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const
	CVTimeStamp *, CVOptionFlags, CVOptionFlags * , void *);

//global varibales
FILE *gpFile = NULL;
bool gbLighting = false;

//interface decleration
@interface APPDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry-point function
 
 int main(int argc, const char * argv[])
 {
 	//code

 	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

 	NSApp = [NSApplication sharedApplication];

 	[NSApp setDelegate:[[APPDelegate alloc]init]];

 	[NSApp run];

 	[pPool release];

 	return(0);
 } 


 //interface implemention

@implementation APPDelegate
{
@private
	NSWindow *window;
	GLView *glView;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

	//code log File

	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("can not create log file: \n");
		[self release];
		[NSApp terminate:self];
	}

	fprintf(gpFile, "Program started successfull\n" );


	//code
	//window

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window

	window=[[NSWindow alloc] initWithContentRect:win_rect 
							 styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
							 backing:NSBackingStoreBuffered
							 defer:NO];
	[window setTitle:@"macOs Window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSApplication *)notification{
	//code
	fprintf(gpFile, "program is terminate successfully\n");

	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSApplication *)notification{
	//code
	[NSApp terminate:self];
} 

- (void)dealloc{
	//code
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;

	//variables for PP
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao_cube;//vertex array object for rect
    GLuint vbo_position_cube;//vertex buffer object(position) for rect
    GLuint vbo_normal_cube;//vertex buffer object(color) for rect


    GLuint mvUniform; // model view matrix
    GLuint pUniform; //projection matrix
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint lightPositionUniform;
    GLuint lKeyIsPressedUniform;

	vmath::mat4 perspectiveProjectionmatrix;


}

-(id)initWithFrame:(NSRect)frame
{
	//code
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			//Must specify the 4.1 core profile to use openGL 4.1
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,	

			//Specify the display ID to associated the GL Context With 
			//(main display for now)

			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask
			(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};
		
		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]
			initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "No valid openGL pixel format is available \n");

			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]
			initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext]; // it automatically release the
		//older context, if present and set newer one
	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	//code
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);

}

-(void)prepareOpenGL
{
	//code
	//OpenGL info

	[super prepareOpenGL];

	fprintf(gpFile, "OpenGL Version %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt =1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


		//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_mv_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_kd;" \
    "uniform vec4 u_lightPosition;" \
    "out vec3 diffuseColor;" \
    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed == 1)" \
    "{" \
    "vec4 eye_Coordinate = u_mv_matrix * vPosition;" \
    "mat3 normalMatrix = mat3(transpose(inverse(u_mv_matrix)));" \
    "vec3 tNorm = normalize(normalMatrix * vNormal);" \
    "vec3 s = vec3(u_lightPosition) - vec3(eye_Coordinate.xyz);" \
    "diffuseColor = u_ld * u_kd * dot(s,tNorm);" \
    "}" \
    "gl_Position = u_p_matrix * u_mv_matrix * vPosition;" \
    "}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}





	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{	"#version 400 core" \
    "\n" \
    "in vec3 diffuseColor;" \
    "out vec4 FragColor;" \
    "uniform int u_lKeyIsPressed;" \
    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed == 1)" \
    "{" \
    "FragColor = vec4(diffuseColor,1.0f);" \
    "}" \
    "else" \
    "{" \
    "FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
    "}" \
    "}"
	};

	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}



	//Create shader program object 
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding of vertex shader
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");

	// ---- texture
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking For Program Object

	iInfoLogLength = 0;

	GLint iProgramLinkStatus = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


	//Postlinking Retriving Uniform locations
    //Postlinking Retriving Uniform locations
    mvUniform = glGetUniformLocation(gShaderProgramObject, "u_mv_matrix");
    pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
    ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
    kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
    lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
    lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	//Here we give vertex,color,texcoord,normals in arrays 
	//for triangle position

	//for rectangle position
	const GLfloat cubeVertices[] = {
			1.0f,1.0f,-1.0f,-1.0f,1.0f,-1.0f,-1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,
			1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,1.0f,1.0f,-1.0f,1.0f,
			1.0f,1.0f,1.0f,-1.0f,1.0f,1.0f,-1.0f,-1.0f,1.0f,1.0f,-1.0f,1.0f,
			1.0f,1.0f,-1.0f,-1.0f,1.0f,-1.0f,-1.0f,-1.0f,-1.0f,1.0f,-1.0f,-1.0f,
			1.0f,1.0f,-1.0f,1.0f,1.0f,1.0f,1.0f,-1.0f,1.0f,1.0f,-1.0f,-1.0f,
			-1.0f,1.0f,1.0f,-1.0f,1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,1.0f
	};
	//for rectangle color
	const GLfloat cubeNormals[] = {
		0.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,

        0.0f,-1.0f,0.0f,
        0.0f,-1.0f,0.0f,
        0.0f,-1.0f,0.0f,
        0.0f,-1.0f,0.0f,

        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,

        0.0f,0.0f,-1.0f,
        0.0f,0.0f,-1.0f,
        0.0f,0.0f,-1.0f,
        0.0f,0.0f,-1.0f,

        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,

        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f,
        -1.0f,0.0f,0.0f
	};

	//****************************** CUBE ****************************
    //Create vao for rect
    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);

    //################### POSITION ###################3
    //Generating Buffer for rect
    glGenBuffers(1, &vbo_position_cube);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and arra for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //################### NORMAL ###################3
    //Generating Buffer for rect
    glGenBuffers(1, &vbo_normal_cube);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_cube);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    //Unbinding buffer for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Unbinding Array for rectangle
    glBindVertexArray(0);


	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//set bk color

	glClearColor(0.0f,0.0f,0.0f,0.0f);//blue


	perspectiveProjectionmatrix = vmath::mat4::identity();


	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat); 
	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{
	[super reshape];
	//code
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width; 
	GLfloat height = rect.size.height;


	if(height ==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionmatrix = vmath::perspective(45.0f,
												width / height,
												0.1f,
												100.0f);
	

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	}




- (void)drawRect:(NSRect)dirtyRect
{
	//code
	[self drawView];
}


- (void)drawView
{
	//Animation varibales
	static float angleRect = 0.0f;

	//code
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	//Using program object 
	glUseProgram(gShaderProgramObject);
	
	//declerations of matrix
	vmath::mat4 modelViewMatrix;
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	vmath::mat4 projectionMatrix;
    
	//************************** CUBE ************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    
    
	//Do neccessary transformation
	translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = vmath::rotate(angleRect, angleRect, angleRect);
	//Do neccessary Matrix Multilication
	modelViewMatrix = translationMatrix * rotationMatrix;
	projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

	//Send neccessary matrices to shader in respective to uniforms
	glUniformMatrix4fv(mvUniform, 1, GL_FALSE, modelViewMatrix);
    glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
    
    
    if (gbLighting == true)
    {
        glUniform1i(lKeyIsPressedUniform,1);
        glUniform3f(ldUniform, 0.3f, 0.3f, 0.3f);
        glUniform3f(kdUniform, 0.5f, 0.5f, 0.5f);
        glUniform4f(lightPositionUniform, 0.0f, 2.0f, 2.0f, 1.0f);
    }
    else {
        glUniform1i(lKeyIsPressedUniform, 0);
    }
	//Bind with vao of rectangle
	glBindVertexArray(vao_cube);

	//Bind texture if any

	//Draw function
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

	//Unbind vao of rectangle
	glBindVertexArray(0);

	//Unused Program
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);	


	angleRect = angleRect+1.0f;
}

-(BOOL)acceptsFirstResponder{
	//code
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27: // ESC Key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
		
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
            
        case 'L':
        case 'l':
            if (gbLighting == false)
            {
                gbLighting = true;
            }
            else {
                gbLighting = false;
            }		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent{
	//code
	
	[self setNeedsDisplay:YES]; //repainting
}

-(void)mouseDragged:(NSEvent *)theEvent{
	//code
}
-(void)RightMouseDown:(NSEvent *)theEvent{
	//code

	[self setNeedsDisplay:YES]; //repainting
}

- (void) dealloc{
	//code

	if (vbo_normal_cube)
	{
		glDeleteBuffers(1, &vbo_normal_cube);
		vbo_normal_cube = 0;
	}
	if (vbo_position_cube)
	{
		glDeleteBuffers(1, &vbo_position_cube);
		vbo_position_cube = 0;
	}
	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);
		vao_cube = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
	}
		
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
								const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
								CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"
#include "Sphere.h"


enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0,
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp*,const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void*);

FILE *gpFile=NULL;
//vmath::mat4 error = {0.0f,0.0f,0.0f,0.0f};
int MAXSIZE = 8;


@interface AppDelegate : NSObject <NSApplicationDelegate , NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int args,const char * argv[])
{
        NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
        NSApp = [NSApplication sharedApplication];
        
        [NSApp setDelegate:[[AppDelegate alloc] init]];
        [NSApp run];
        [pPool release];
        return (0);
}

@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    
    const char *pszLogNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogNameWithPath,"w");
    
    if(gpFile==NULL)
    {
        printf("Can not Create Log file.\n Exitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile,"Program is Started Successfully\n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    window=[[NSWindow alloc] initWithContentRect:win_rect             styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable |             NSWindowStyleMaskMiniaturizable |             NSWindowStyleMaskResizable backing:NSBackingStoreBuffered  defer:NO];
        [window setTitle:@"macOS OpenGL Window"];
        [window center];

        glView = [[GLView alloc] initWithFrame:win_rect];
        [window setContentView:glView];
        [window setDelegate:self];
        [window makeKeyAndOrderFront:self];
        fprintf(gpFile,"Program is Started Successfully\n");
}

- (void) applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile,"Program Is Terminated Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}
- (void) windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    
    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    int angleMoon;
    int top;
    float sphere_vertices[1146];
    float  sphere_normals[1146];
    float sphere_texture[764];
    unsigned short sphere_elements[2280];
    
    GLuint vao_sphere;//vertex array object for rect
    GLuint vbo_position_sphere;//vertex buffer object(position) for rect
    GLuint vbo_normal_sphere;//vertex buffer object(color) for rect
    GLuint vbo_elements_sphere;
    bool gbLighting;
    int gNumVertices;
    int gNumElements;
    
    int year;
    int day;
    
    
 
    vmath::mat4 stack[20];
    
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint mvUniform;
    GLuint pUniform;
    GLuint ldUniform;
    GLuint kdUniform;
    GLuint lightPositionUniform;
    GLuint lKeyIsPressedUniform;
    
    vmath::mat4 perspetiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    gbLighting=false;
    self = [super initWithFrame:frame];
    if(self)
    {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,0};
            
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
            
            if(pixelFormat ==nil)
            {
                fprintf(gpFile,"No Valid OpenGL Pixel Format Is available Exitting...\n");
                [self release];
                [NSApp terminate:self];
            }
            
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
            
            [self setPixelFormat:pixelFormat];
            
            [self setOpenGLContext:glContext];
            fprintf(gpFile,"Context created successfully\n");
        
    }
    return(self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    top=-1;
    fprintf(gpFile,"OpenGL Version : %s\n\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    fprintf(gpFile,"\n In prepareOpenGL:%d\n",swapInt);
    
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_mv_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_kd;" \
    "uniform vec4 u_lightPosition;" \
    "out vec3 diffuseColor;" \
    "void main(void)" \
    "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
            "vec4 eye_Coordinate = u_mv_matrix * vPosition;" \
            "mat3 normalMatrix = mat3(transpose(inverse(u_mv_matrix)));" \
            "vec3 tNorm = normalize(normalMatrix * vNormal);" \
            "vec3 s = vec3(u_lightPosition) - vec3(eye_Coordinate.xyz);" \
            "diffuseColor = u_ld * u_kd * dot(s,tNorm);" \
        "}" \
    "gl_Position = u_p_matrix * u_mv_matrix * vPosition;" \
    "}\n";
    
    glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    
    glCompileShader(vertexShaderObject);
    
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength > 0)
        {
            szInfoLog = (char *) malloc(iINfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Vertex Shader Compilation Error %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    iINfoLogLength =0;
    iShaderCompiledStatus =0;
    szInfoLog=NULL;
    
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec3 diffuseColor;" \
    "out vec4 FragColor;" \
    "uniform int u_lKeyIsPressed;" \
    "void main(void)" \
    "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
            "FragColor = vec4(diffuseColor,1.0f);" \
        "}" \
        "else" \
        "{" \
            "FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
        "}" \
    "}";
    
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*) malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Fragment Shader Compilation Log %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    shaderProgramObject = glCreateProgram();
    
    glAttachShader(shaderProgramObject,vertexShaderObject);
    glAttachShader(shaderProgramObject,fragmentShaderObject);
    
    
    
    glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");
    
    glLinkProgram(shaderProgramObject);
    GLint iShaderProgramLinkStatus=0;
    glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    
    if(iShaderProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject,iINfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Shader Program Link Log : %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    mvUniform = glGetUniformLocation(shaderProgramObject,"u_mv_matrix");
    pUniform=glGetUniformLocation(shaderProgramObject,"u_p_matrix");
    ldUniform=glGetUniformLocation(shaderProgramObject,"u_ld");
    kdUniform=glGetUniformLocation(shaderProgramObject,"u_kd");
    lightPositionUniform=glGetUniformLocation(shaderProgramObject,"u_lightPosition");
    lKeyIsPressedUniform=glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
    
    getSphereVertexData(sphere_vertices,sphere_normals,sphere_texture,sphere_elements);
    
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();
    
    
   
    glGenVertexArrays(1,&vao_sphere);
    glBindVertexArray(vao_sphere);
    
    glGenBuffers(1,&vbo_position_sphere);
    glBindBuffer(GL_ARRAY_BUFFER,vbo_position_sphere);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and arra for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    
   glGenBuffers(1, &vbo_normal_sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    //Unbinding buffer for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Unbinding Array for rectangle
    glBindVertexArray(0);
    
    
   glGenBuffers(1, &vbo_elements_sphere);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Unbinding Array for rectangle
    glBindVertexArray(0);
    
    glClearDepth(1.0f);
    
    glEnable(GL_DEPTH_TEST);
    
    glDepthFunc(GL_LEQUAL);
    
   
    
    perspetiveProjectionMatrix = vmath::mat4::identity();
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}

-(void)reshape
{
    CGLLockContext ((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect = [self bounds];
    
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspetiveProjectionMatrix= vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    fprintf(gpFile,"In reshape\n");
    
   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}


- (void) drawRect:(NSRect)dirtyRect
{
    [self drawView];
}
- (int) isEmpty
{
    if(top == -1)
        return 1;
    else
        return 0;
}
- (vmath::mat4) pop
{
    vmath::mat4 data;
    
    //error={0.0f,0.0f,0.0f,0.0f};
    if(![self isEmpty]    )
    {
        data = stack[top];
        top = top -1;
        return data;
    }
    else
    {
        
        fprintf(gpFile,"\nCould not retrieve data, Stack is empty.\n");
        return (vmath::mat4(0.0f,0.0f,0.0f,0.0f));
    }
}
-(int) isFull
{
    if (top == MAXSIZE)
        return 1;
    else
        return 0;
}
-(void) push:(vmath::mat4)data
{
    
    if(![self isFull])
    {
        top = top +1;
        stack[top] = data;
        fprintf(gpFile,"Data Inserted Successfully.\n");
    }
    else
    {
        fprintf(gpFile,"Could not insert data, Stack is full.\n");
    }
}
-(void) drawView
{
    
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    vmath::mat4 projectionMatrix = vmath::mat4::identity();
    
    vmath::mat4 rotationMatrix;
    
    modelViewMatrix= vmath::translate(0.0f, 0.0f, -4.0f);
    [self push:modelViewMatrix];
    projectionMatrix = perspetiveProjectionMatrix * projectionMatrix;
    glUniformMatrix4fv(mvUniform,1,GL_FALSE,modelViewMatrix);
    
    glUniformMatrix4fv(pUniform,1,GL_FALSE,projectionMatrix);
    
    if(gbLighting == true)
    {
        glUniform1i(lKeyIsPressedUniform,1);
        glUniform3f(ldUniform,1.0f,1.0f,0.0f);
        glUniform3f(kdUniform,0.1f,0.1f,0.1f);
        glUniform4f(lightPositionUniform,0.0f,0.0f,2.0f,1.0f);
    }
    else
    {
        glUniform1i(lKeyIsPressedUniform,0);
    }
    
    glBindVertexArray(vao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_elements_sphere);
    glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
    glBindVertexArray(0);
    vmath::mat4 sunMatrix = [self pop];
    
    
    //Earth
    vmath::mat4 modelViewMatrix1 = vmath::mat4::identity();

    projectionMatrix = vmath::mat4::identity();

    
     vmath::mat4 translationMatrix= vmath::translate(1.5f, 0.0f, 0.0f);
    vmath::mat4 scaleMatrix = vmath::scale(0.30f,0.30f,0.30f);
    modelViewMatrix1 = sunMatrix * vmath::rotate((GLfloat)year,0.0f,1.0f,0.0f) * translationMatrix * vmath::rotate((GLfloat)day,0.0f,1.0f,0.0f) * scaleMatrix;
    
    [self push : modelViewMatrix1];
    projectionMatrix = perspetiveProjectionMatrix * projectionMatrix;
    glUniformMatrix4fv(mvUniform,1,GL_FALSE,modelViewMatrix1);
       
    glUniformMatrix4fv(pUniform,1,GL_FALSE,projectionMatrix);
       
    if (gbLighting == true)
    {
        glUniform1i(lKeyIsPressedUniform, 1);
        glUniform3f(ldUniform, 0.4f, 0.9f, 1.0f);
        glUniform3f(kdUniform, 0.1f, 0.1f, 0.1f);
        glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
    }
    else
    {
        glUniform1i(lKeyIsPressedUniform, 0);
    }

       
       glBindVertexArray(vao_sphere);
       glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_elements_sphere);
       glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
       glBindVertexArray(0);
       vmath::mat4 earthMatrix = [self pop];

    
    
    //Moon
    vmath::mat4 modelViewMatrix2 = vmath::mat4::identity();

       projectionMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    vmath::mat4 scaleMatrix1 = vmath::mat4::identity();
        translationMatrix= vmath::translate(1.f, 0.0f, 0.0f);
       scaleMatrix1 = vmath::scale(0.25f,0.25f,0.25f);
       modelViewMatrix2 = earthMatrix * vmath::rotate((GLfloat)angleMoon,0.0f,1.0f,0.0f) * translationMatrix  * scaleMatrix1;
       
       [self push : modelViewMatrix2];
    projectionMatrix = perspetiveProjectionMatrix * projectionMatrix;
       glUniformMatrix4fv(mvUniform,1,GL_FALSE,modelViewMatrix2);
          
       glUniformMatrix4fv(pUniform,1,GL_FALSE,projectionMatrix);
          
       if (gbLighting == true)
       {
           glUniform1i(lKeyIsPressedUniform, 1);
           glUniform3f(ldUniform, 1.0f, 1.0f, 1.0f);
           glUniform3f(kdUniform, 0.1f, 0.1f, 0.1f);
           glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
       }
       else
       {
           glUniform1i(lKeyIsPressedUniform, 0);
       }

          
          glBindVertexArray(vao_sphere);
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_elements_sphere);
          glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
          glBindVertexArray(0);
          
    
    vmath::mat4 moonMatrix = [self pop];
    glUseProgram(0);
    
    
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
  
}

-(BOOL) acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case 'y':
            year = (year + 3) % 360;
            angleMoon = (angleMoon + 3) % 360;
            break;
            
        case 'Y':
            year = (year - 3) % 360;
            angleMoon = (angleMoon - 3) % 360;
            break;
            
        case 'D':
            day = (day + 6) % 360;
            break;
            
        case 'd':
            day = (day - 6) % 360;
            break;
            
        case 'm':
            angleMoon = (angleMoon + 3) % 360;
            break;
            
        case 'M':
            angleMoon = (angleMoon - 3) % 360;
            break;
        case 'l':
        case 'L':
            if(gbLighting==true)
                gbLighting=false;
            else
                gbLighting=true;
            break;
            
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    
}

-(void) mouseFragged:(NSEvent *)theEvent
{
    
}
-(void)rightMouseDown:(NSEvent *)theEvent
{
    
 
}
-(void) dealloc
{
    if(vao_sphere)
    {
        glDeleteVertexArrays(1,&vao_sphere);
        vao_sphere=0;
    }
    if(vbo_elements_sphere)
    {
        glDeleteBuffers(1,&vbo_elements_sphere);
        vbo_elements_sphere=0;
    }
    if(vbo_normal_sphere)
    {
        glDeleteBuffers(1,&vbo_normal_sphere);
        vbo_normal_sphere=0;
    }
    
    
    
    glDetachShader(shaderProgramObject,vertexShaderObject);
    glDetachShader(shaderProgramObject,fragmentShaderObject);
    glDeleteShader(vertexShaderObject);
    glDeleteShader(fragmentShaderObject);
    
    vertexShaderObject=0;
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}
@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}


//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"


enum
{
    AMC_ATTRIBUTE_POSITION = 0,
    AMC_ATTRIBUTE_COLOR,
    AMC_ATTRIBUTE_NORMAL,
    AMC_ATTRIBUTE_TEXTURE0,
};

// C style global function decleration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const
    CVTimeStamp *, CVOptionFlags, CVOptionFlags * , void *);

//global varibales
FILE *gpFile = NULL;
 BOOL ifLI = FALSE;
 BOOL ifA = FALSE;
 BOOL ifN = FALSE;
 BOOL ifRI = FALSE;
 BOOL ifD = FALSE;

//interface decleration
@interface APPDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry-point function
 
 int main(int argc, const char * argv[])
 {
     //code

     NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

     NSApp = [NSApplication sharedApplication];

     [NSApp setDelegate:[[APPDelegate alloc]init]];

     [NSApp run];

     [pPool release];

     return(0);
 }


 //interface implemention

@implementation APPDelegate
{
@private
    NSWindow *window;
    GLView *view;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

    //code log File

    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *appDirName = [mainBundle bundlePath];
    NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

    gpFile = fopen(pszLogFileNameWithPath,"w");
    if(gpFile == NULL)
    {
        printf("can not create log file: \n");
        [self release];
        [NSApp terminate:self];
    }

    fprintf(gpFile, "Program started successfull\n" );


    //code
    //window

    NSRect win_rect;
    win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

    //create simple window

    window=[[NSWindow alloc] initWithContentRect:win_rect
                             styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
                             backing:NSBackingStoreBuffered
                             defer:NO];
    [window setTitle:@"macOs Window"];
    [window center];

    view = [[GLView alloc]initWithFrame:win_rect];

    [window setContentView:view];
    [window setDelegate:self];
    [window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSApplication *)notification{
    //code
    fprintf(gpFile, "program is terminate successfully\n");

    if(gpFile)
    {
        fclose(gpFile);
        gpFile = NULL;
    }
}

- (void)windowWillClose:(NSApplication *)notification{
    //code
    [NSApp terminate:self];
}

- (void)dealloc{
    //code
    [view release];

    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
@private
    CVDisplayLinkRef displayLink;
    
    
    //variables for PP

    
   GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

	   GLuint vao_LeftI_raghav;
       GLuint vao_N_raghav;
       GLuint vao_D_raghav;
       GLuint vao_RightI_raghav;
       GLuint vao_A_raghav;

       GLuint vbo_position_LeftI_raghav;//vertex buffer object for tri
       GLuint vbo_color_LeftI_raghav;

       GLuint vbo_position_N_raghav;//vertex buffer object for tri
       GLuint vbo_color_N_raghav;

       GLuint vbo_position_D_raghav;//vertex buffer object for tri
       GLuint vbo_color_D_raghav;

       GLuint vbo_position_RightI_raghav;//vertex buffer object for tri
       GLuint vbo_color_RightI_raghav;

       GLuint vbo_position_A_raghav;//vertex buffer object for tri
       GLuint vbo_color_A_raghav;
    
    
    GLuint mvpUniform;

    vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
    //code
    self = [super initWithFrame:frame];

    if(self)
    {
        [[self window]setContentView:self];

        NSOpenGLPixelFormatAttribute attrs[] =
        {
            //Must specify the 4.1 core profile to use openGL 4.1
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,

            //Specify the display ID to associated the GL Context With
            //(main display for now)

            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask
            (kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]
            initWithAttributes:attrs] autorelease];

        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No valid openGL pixel format is available \n");

            [self release];
            [NSApp terminate:self];
        }

        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]
            initWithFormat:pixelFormat shareContext:nil] autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext]; // it automatically release the
        //older context, if present and set newer one
    }
    return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    //code
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

    [self drawView];

    [pool release];

    return(kCVReturnSuccess);

}

-(void)prepareOpenGL
{
    //code
    //OpenGL info

    [super prepareOpenGL];
    
    fprintf(gpFile, "OpenGL Version %s\n", glGetString(GL_VERSION));
    fprintf(gpFile, "GLSL version %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];

    GLint swapInt =1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


        //Define vertex shader object

    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

    //write vertex shader code
    const GLchar *vertexShaderSourceCode =
    { "#version 410 " \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
        "}"
    };

    //specify source code to vertex shader
    glShaderSource(gVertexShaderObject, 1,
        (const GLchar **)& vertexShaderSourceCode,
        NULL);

    //compile the vertex shader
    glCompileShader(gVertexShaderObject);

    GLint iShaderCompileStatus = 0;
    GLint iInfoLogLength = 0;
    GLchar *szInfoLog = NULL;
      GLint iProgramLinkStatus = 0;

    //Error Checking For Vertex Shader Object

    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }




        //Define fragment shader object

     iShaderCompileStatus = 0;
     iInfoLogLength = 0;
     szInfoLog = NULL;
  
    
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

    //write fragment shader code

    const GLchar *fragmentShaderSourceCode =
    {    "#version 410" \
        "\n" \
        "in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
        "}"
    };

    //specify source code to fragment shader object

    glShaderSource(gFragmentShaderObject, 1,
        (const GLchar**)&fragmentShaderSourceCode,
        NULL);

    //compile fragment shader

    glCompileShader(gFragmentShaderObject);

    //Error checking For Fragment Shader Object

    iShaderCompileStatus = 0;
    iInfoLogLength = 0;
    //*szInfoLog = NULL;

    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment shader comiplation error: ");
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }



    //Create shader program object
    gShaderProgramObject = glCreateProgram();

    //Attach Vertex Shader to Program Object
    glAttachShader(gShaderProgramObject, gVertexShaderObject);


    //Attach Fragment Shader to Program Object
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    //Prelinking binding of vertex shader

    //----- Position
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    // ---- color
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //Link Shader Program
    glLinkProgram(gShaderProgramObject);

    //Error Checking For Program Object

    iInfoLogLength = 0;
    //*szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    //Postlinking Retriving Uniform locations
    mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
const GLfloat leftIVertices[] = {
            -1.30f,0.70f,0.0f,
            -1.30f,-0.70f,0.0f
        };
        const GLfloat leftIColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat nVertices[] = {
            -1.10f,0.70f,0.0f,
            -1.10f,-0.70f,0.0f,
            -1.10f,0.70f,0.0f,
            -0.60f,-0.70f,0.0f,
            -0.60f,0.70f,0.0f,
            -0.60f,-0.70f,0.0f
        };
        const GLfloat nColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat dVertices[] = {
            -0.4f,0.70f,0.0f,
            -0.4f,-0.70f,0.0f,
            -0.50f,0.70f,0.0f,
            0.1f,0.70f,0.0f,
            0.1f,0.70f,0.0f,
            0.1f,-0.70f,0.0f,
            -0.50f,-0.70f,0.0f,
            0.1f,-0.70f,0.0f
        };
        const GLfloat dColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            1.0f, 0.6f, 0.2f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            0.07f, 0.533f, 0.027f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat rightIVertices[] = {
            0.3f,0.70f,0.0f,
            0.3f,-0.70f,0.0f
        };
        const GLfloat rightIColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat aVertices[] = {
            0.8f,0.70f,0.0f,
            0.50f,-0.70f,0.0f,
            0.80f,0.70f,0.0f,
            1.1f,-0.70f,0.0f,
            0.66f,0.02f,0.0f,
            0.94f,0.02f,0.0f,
            0.66f,0.0f,0.0f,
            0.94f,0.0f,0.0f,
            0.66f,-0.02f,0.0f,
            0.94f,-0.02f,0.0f
        };
        const GLfloat aColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            1.0f, 0.6f, 0.2f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            0.07f, 0.533f, 0.027f,
            0.07f, 0.533f, 0.027f
        };

         //leftI
        glGenVertexArrays(1, &vao_LeftI_raghav);
        glBindVertexArray(vao_LeftI_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_LeftI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_LeftI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(leftIVertices), leftIVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_LeftI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_LeftI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(leftIColor), leftIColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);


        //N
        //Create vao for triangle
        glGenVertexArrays(1, &vao_N_raghav);
        glBindVertexArray(vao_N_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_N_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_N_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(nVertices), nVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_N_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_N_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);

        //D
        //Create vao for triangle
        glGenVertexArrays(1, &vao_D_raghav);
        glBindVertexArray(vao_D_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_D_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_D_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(dVertices), dVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_D_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);

        //RightI
        //Create vao for triangle
        glGenVertexArrays(1, &vao_RightI_raghav);
        glBindVertexArray(vao_RightI_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_RightI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_RightI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(rightIVertices), rightIVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_RightI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_RightI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(rightIColor), rightIColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);

        //A
        //Create vao for triangle
        glGenVertexArrays(1, &vao_A_raghav);
        glBindVertexArray(vao_A_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_A_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_A_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(aVertices), aVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_A_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_A_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);


    
    //Depth Lines
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    //set bk color

    glClearColor(0.0f,0.0f,0.0f,0.0f);//blue


    perspectiveProjectionMatrix = vmath::mat4::identity();


    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    CVDisplayLinkStart(displayLink);

}

-(void)reshape
{
    [super reshape];
    //code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    NSRect rect = [self bounds];

    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;


    if(height ==0)
    {
        height=1;
    }

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                                     (GLsizei)width / (GLsizei)height,
                                                0.1f,
                                                100.0f);
    

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}



- (void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}


- (void)drawView
{
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
   //LI
    static GLfloat Litx = -3.0f, Lity = 0.0f, Litz = -5.0f;
    
    //A
//    static GLfloat x = 0.0f, y = 0.0f, z = 0.0f, a = 0.0f, b = 0.0f, c = 0.0f, d = 0.0f, e = 0.0f, f = 0.0f; //For  middle color of A
    static GLfloat atx = 4.0f, aty = 0.0f, atz = -5.0f;//, aty1 = 0.01f;
    //RI
    static GLfloat Ritx = 0.0f, Rity = -6.0f, Ritz = -5.0f;

    //D
    static GLfloat dtx = 0.0f, dty = 0.0f, dtz = -5.0f;
    static GLfloat i = 0.0f, j = 0.0f, k = 0.0f, l = 0.0f, m = 0.0f, n = 0.0f;
    
    //N
    static GLfloat ntx = 0.0f, nty = 4.0f, ntz = -5.0f;

    const GLfloat dColor[] = {
        i, j, k,
        l, m, n,
        i, j, k,
        i, j, k,
        i, j, k,
        l, m, n,
        l, m, n,
        l, m, n
    };

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Using program object
    glUseProgram(gShaderProgramObject);

    glLineWidth(5.0f);
    //declerations of matrix
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    vmath::mat4 translateMatrix;

    

    if (ifLI == false)
    {
        //leftI
    //Initialize above matrix to identity
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        translateMatrix = vmath::mat4::identity();

        //Do neccessary transformation

        translateMatrix = vmath::translate(Litx, Lity, Litz);


        //Do neccessary Matrix Multilication
        modelViewMatrix = translateMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
        glBindVertexArray(vao_LeftI_raghav);


        //Draw function
        glDrawArrays(GL_LINES, 0, 2);

        //Unbind vao of triangle
        glBindVertexArray(0);

        if ((Litx < 0.0f) || (Litx == 0.0f))
        {
            Litx = Litx + 0.09f;
        }
        if (Litx >= 0.0f)
        {
            ifA = true;
            //A();
        }
    }
    if (ifA == true)
    {
        //A
    //Initialize above matrix to identity
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix =vmath::mat4::identity();
        translateMatrix = vmath::mat4::identity();

        //Do neccessary transformation

        translateMatrix = vmath::translate(atx, aty, atz);


        //Do neccessary Matrix Multilication
        modelViewMatrix = translateMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
        glBindVertexArray(vao_A_raghav);


        //Draw function
        glDrawArrays(GL_LINES, 0, 10);

        //Unbind vao of triangle
        glBindVertexArray(0);
        if ((atx > 0.0) || (atx == 0.0f))
        {
            atx = atx - 0.09f;

        }
        if (atx <= 0.0f)
        {
            //N();
            ifN = true;
        }

    }
    if (ifN == true)
    {
        //N
    //Initialize above matrix to identity
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        translateMatrix = vmath::mat4::identity();

        //Do neccessary transformation

        translateMatrix = vmath::translate(ntx, nty, ntz);


        //Do neccessary Matrix Multilication
        modelViewMatrix = translateMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
        glBindVertexArray(vao_N_raghav);


        //Draw function
        glDrawArrays(GL_LINES, 0, 6);

        //Unbind vao of triangle
        glBindVertexArray(0);

        if ((nty >= 0.0f) || (nty == 0.0f))
        {
            nty = nty - 0.02f;

        }
        if (nty <= 0.0f)
        {
            //rightI();
            ifRI = true;
        }
    }
    if (ifRI == true)
    {
        //rightI
    //Initialize above matrix to identity
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        translateMatrix = vmath::mat4::identity();

        //Do neccessary transformation

        translateMatrix = vmath::translate(Ritx, Rity, Ritz);


        //Do neccessary Matrix Multilication
        modelViewMatrix = translateMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
        glBindVertexArray(vao_RightI_raghav);


        //Draw function
        glDrawArrays(GL_LINES, 0, 2);

        //Unbind vao of triangle
        glBindVertexArray(0);

        if ((Rity <= 0.0f) || (Rity == 0.0f))
        {
            Rity = Rity + 0.02f;

        }
        if (Rity >= 0.0f)
        {
            //D();
            ifD = true;
        }
    }
    if (ifD == true)
    {
        //D
    //Initialize above matrix to identity
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();
        translateMatrix = vmath::mat4::identity();

        //Do neccessary transformation

        translateMatrix = vmath::translate(dtx, dty, dtz);


        //Do neccessary Matrix Multilication
        modelViewMatrix = translateMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
        glBindVertexArray(vao_D_raghav);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D_raghav);
        glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        //Draw function
        glDrawArrays(GL_LINES, 0, 8);

        //Unbind vao of triangle
        glBindVertexArray(0);

        if (i <= 1.0f && j < 0.6f && k < 0.2f && l < 0.07f && m < 0.533f && n < 0.027f)
        {
            i = i + 0.001f;
            j = j + 0.0006f;
            k = k + 0.0002f;

            l = l + 0.00007f;
            m = m + 0.000533f;
            n = n + 0.000027f;
        
        }
    }
    //Unused Program
    glUseProgram(0);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder{
    //code
    [[self window]makeFirstResponder:self];
    return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
    //code
    int key = (int)[[theEvent characters]characterAtIndex:0];

    switch(key)
    {
        case 27: // ESC Key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self]; // repainting occurs automatically
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent{
    //code
    [self setNeedsDisplay:YES]; //repainting
}

-(void)mouseDragged:(NSEvent *)theEvent{
    //code
}
-(void)RightMouseDown:(NSEvent *)theEvent{
    //code
    [self setNeedsDisplay:YES]; //repainting
}

- (void) dealloc{
    //code
    if (gShaderProgramObject)
    {
        if (vbo_color_A_raghav)
        {
            glDeleteBuffers(1, &vbo_color_A_raghav);
            vbo_color_A_raghav = 0;
        }
        if (vbo_color_D_raghav)
        {
            glDeleteBuffers(1, &vbo_color_D_raghav);
            vbo_color_D_raghav= 0;
        }
        if (vbo_color_LeftI_raghav)
        {
            glDeleteBuffers(1, &vbo_color_LeftI_raghav);
            vbo_color_LeftI_raghav = 0;
        }
        if (vbo_color_N_raghav)
        {
            glDeleteBuffers(1, &vbo_color_N_raghav);
            vbo_color_N_raghav = 0;
        }
        if (vbo_color_RightI_raghav)
        {
            glDeleteBuffers(1, &vbo_color_RightI_raghav);
            vbo_color_RightI_raghav = 0;
        }

        if (vbo_position_A_raghav)
        {
            glDeleteBuffers(1, &vbo_position_A_raghav);
            vbo_position_A_raghav = 0;
        }
        if (vbo_position_D_raghav)
        {
            glDeleteBuffers(1, &vbo_position_D_raghav);
            vbo_position_D_raghav = 0;
        }
        if (vbo_position_LeftI_raghav)
        {
            glDeleteBuffers(1, &vbo_position_LeftI_raghav);
            vbo_position_LeftI_raghav = 0;
        }
        if (vbo_position_N_raghav)
        {
            glDeleteBuffers(1, &vbo_position_N_raghav);
            vbo_position_N_raghav = 0;
        }
        if (vbo_position_RightI_raghav)
        {
            glDeleteBuffers(1, &vbo_position_RightI_raghav);
            vbo_position_RightI_raghav = 0;
        }

        if (vao_A_raghav)
        {
            glDeleteVertexArrays(1, &vao_A_raghav);
            vao_A_raghav = 0;
        }
        if (vao_D_raghav)
        {
            glDeleteVertexArrays(1, &vao_D_raghav);
            vao_D_raghav = 0;
        }
        if (vao_LeftI_raghav)
        {
            glDeleteVertexArrays(1, &vao_LeftI_raghav);
            vao_LeftI_raghav = 0;
        }
        if (vao_RightI_raghav)
        {
            glDeleteVertexArrays(1, &vao_RightI_raghav);
            vao_RightI_raghav = 0;
        }
        if (vao_N_raghav)
        {
            glDeleteVertexArrays(1, &vao_N_raghav);
            vao_N_raghav = 0;
        }        GLsizei shaderCount;
        GLsizei shaderNumber;
        glUseProgram(gShaderProgramObject);

        //Ask shader how many shaders are attached to you
        glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

        GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

        if (pShaders)
        {
            glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
            for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
            {
                //Dettach shaders
                glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

                //Delete Shaders
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        glDeleteProgram(gShaderProgramObject);
        gShaderProgramObject = 0;

        
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    [super dealloc];
    }
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
                                const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
                                CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"


enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0,
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp*,const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void*);

FILE *gpFile=NULL;

unsigned int gNumberOfLineSegments;


@interface AppDelegate : NSObject <NSApplicationDelegate , NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int args,const char * argv[])
{
        NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
        NSApp = [NSApplication sharedApplication];
        
        [NSApp setDelegate:[[AppDelegate alloc] init]];
        [NSApp run];
        [pPool release];
        return (0);
}

@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    
    const char *pszLogNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogNameWithPath,"w");
    
    if(gpFile==NULL)
    {
        printf("Can not Create Log file.\n Exitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile,"Program is Started Successfully\n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    window=[[NSWindow alloc] initWithContentRect:win_rect             styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable |             NSWindowStyleMaskMiniaturizable |             NSWindowStyleMaskResizable backing:NSBackingStoreBuffered  defer:NO];
        [window setTitle:@"Tessellation Shader in macOS"];
        [window center];

        glView = [[GLView alloc] initWithFrame:win_rect];
        [window setContentView:glView];
        [window setDelegate:self];
        [window makeKeyAndOrderFront:self];
        fprintf(gpFile,"Program is Started Successfully\n");
}

- (void) applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile,"Program Is Terminated Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}
- (void) windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    
    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;

    GLuint shaderProgramObject;
    
    GLuint vao;
    GLuint vbo,vbo_color;
    GLuint gTessellationShaderObject;
    GLuint gTessellationEvaluationShaderObject;
    GLuint gNumberOfSegmentsUniform, gNumberOfStripsUniform;
    GLuint gLineColorUniform;
    
    GLuint mvpUniform;
    
    
    
    vmath::mat4 perspetiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,0};
            
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
            
            if(pixelFormat ==nil)
            {
                fprintf(gpFile,"No Valid OpenGL Pixel Format Is available Exitting...\n");
                [self release];
                [NSApp terminate:self];
            }
            
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
            
            [self setPixelFormat:pixelFormat];
            
            [self setOpenGLContext:glContext];
            fprintf(gpFile,"Context created successfully\n");
        
    }
    return(self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile,"OpenGL Version : %s\n\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    fprintf(gpFile,"\n In prepareOpenGL:%d\n",swapInt);
    
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode=
    "#version 410" \
    "\n" \
   "in vec2 vPosition;" \
    "void main(void)" \
    "{" \
        "gl_Position = vec4(vPosition,0.0,1.0);" \
    "}";
    
    glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    
    glCompileShader(vertexShaderObject);
    
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength > 0)
        {
            szInfoLog = (char *) malloc(iINfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Vertex Shader Compilation Error %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
   
    
    //Tessellation Control Shader Start-----------------------
   gTessellationShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);
    const GLchar* tesselationControlShaderSourceCode = "#version 410 core" \
        "\n" \
        "layout(vertices=4)out;" \
        "uniform int numberOfSegments;" \
        "uniform int numberOfStrips;" \

        "void main(void)" \
        "{" \
                "gl_out[gl_InvocationID].gl_Position=gl_in[gl_InvocationID].gl_Position;" \
                "gl_TessLevelOuter[0] = float(numberOfStrips);" \
                "gl_TessLevelOuter[1] = float(numberOfSegments);" \
        
       "}";
       
       glShaderSource(gTessellationShaderObject,1,(const GLchar **)&tesselationControlShaderSourceCode,NULL);
       
       glCompileShader(gTessellationShaderObject);
       
       iINfoLogLength = 0;
       iShaderCompiledStatus =0;
       szInfoLog = NULL;
       
       glGetShaderiv(gTessellationShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
       if(iShaderCompiledStatus==GL_FALSE)
       {
           glGetShaderiv(gTessellationShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
           if(iINfoLogLength > 0)
           {
               szInfoLog = (char *) malloc(iINfoLogLength);
               if(szInfoLog != NULL)
               {
                   GLsizei written;
                   glGetShaderInfoLog(gTessellationShaderObject,iINfoLogLength,&written,szInfoLog);
                   
                   fprintf(gpFile,"Tessellation Shader Compilation Error %s\n",szInfoLog);
                   free(szInfoLog);
                   [self release];
                   [NSApp terminate:self];
               }
           }
       }
    
    //Tessellation Shader End ------------------------
    
    //Tessellation Evaluation Shader Start------------------------
    
    gTessellationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);
    const GLchar* tesselationEvaluationShaderSourceCode = "#version 410 core" \
        "\n" \
        "layout(isolines)in;" \
        "uniform mat4 u_mvp_matrix;" \

        "void main(void)" \
        "{" \
        "float u = gl_TessCoord.x;" \
        "vec3 p0 = gl_in[0].gl_Position.xyz;" \
        "vec3 p1 = gl_in[1].gl_Position.xyz;" \
        "vec3 p2 = gl_in[2].gl_Position.xyz;" \
        "vec3 p3 = gl_in[3].gl_Position.xyz;" \
        "float u1 = (1.0 - u);" \
        "float u2 = u *u;" \
        "float b3 = u2 * u;" \
        "float b2 = 3.0 * u2 * u1;" \
        "float b1 = 3.0 * u * u1 * u1;" \
        "float b0 = u1 * u1 * u1;" \
        "vec3 p =p0 *b0 + p1 *b1 + p2*b2+p3*b3;" \
        "gl_Position = u_mvp_matrix * vec4(p,1.0);" \
        "}";
       
       glShaderSource(gTessellationEvaluationShaderObject,1,(const GLchar **)&tesselationEvaluationShaderSourceCode,NULL);
       
       glCompileShader(gTessellationEvaluationShaderObject);
       
       iINfoLogLength = 0;
       iShaderCompiledStatus =0;
       szInfoLog = NULL;
       
       glGetShaderiv(gTessellationShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
       if(iShaderCompiledStatus==GL_FALSE)
       {
           glGetShaderiv(gTessellationEvaluationShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
           if(iINfoLogLength > 0)
           {
               szInfoLog = (char *) malloc(iINfoLogLength);
               if(szInfoLog != NULL)
               {
                   GLsizei written;
                   glGetShaderInfoLog(gTessellationEvaluationShaderObject,iINfoLogLength,&written,szInfoLog);
                   
                   fprintf(gpFile,"Tessellation Evaluation Shader Compilation Error %s\n",szInfoLog);
                   free(szInfoLog);
                   [self release];
                   [NSApp terminate:self];
               }
           }
       }
    
    //Tessellation Evaluation Shader End-----------------------------
    
    
    
     fprintf(gpFile,"After Geometry Shader \n");
        iINfoLogLength =0;
       iShaderCompiledStatus =0;
       szInfoLog=NULL;
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode=
    "#version 410" \
    "\n" \
    "uniform vec4 lineColor;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
        "FragColor = lineColor;" \
    "}";
    
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*) malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Fragment Shader Compilation Log %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    shaderProgramObject = glCreateProgram();
    
    glAttachShader(shaderProgramObject,vertexShaderObject);
    glAttachShader(shaderProgramObject,gTessellationShaderObject);
    glAttachShader(shaderProgramObject,gTessellationEvaluationShaderObject);
    glAttachShader(shaderProgramObject,fragmentShaderObject);
    
    
    
    glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
    
    
    glLinkProgram(shaderProgramObject);
    GLint iShaderProgramLinkStatus=0;
    glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
    
    if(iShaderProgramLinkStatus==GL_FALSE)
    {
        glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*)malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(shaderProgramObject,iINfoLogLength,&written,szInfoLog);
                fprintf(gpFile,"Shader Program Link Log : %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
    gNumberOfSegmentsUniform = glGetUniformLocation(shaderProgramObject, "numberOfSegments");
    gNumberOfStripsUniform = glGetUniformLocation(shaderProgramObject, "numberOfStrips");
    gLineColorUniform = glGetUniformLocation(shaderProgramObject, "lineColor");
    
    float vertices[] = {
    -1.0f,-1.0f,
    -0.5f,1.0f,
    0.5f,-1.0f,
    1.0f,1.0f
    };
    
    glGenVertexArrays(1,&vao);
    glBindVertexArray(vao);
    
    glGenBuffers(1,&vbo);
    glBindBuffer(GL_ARRAY_BUFFER,vbo);
    glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,2,GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);
    
   

    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glLineWidth(3.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    gNumberOfLineSegments=1;
    
    perspetiveProjectionMatrix = vmath::mat4::identity();
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}

-(void)reshape
{
    CGLLockContext ((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect = [self bounds];
    
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspetiveProjectionMatrix= vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    fprintf(gpFile,"In reshape\n");
    
   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}


- (void) drawRect:(NSRect)dirtyRect
{
    [self drawView];
}

-(void) drawView
{
    
    
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    modelViewMatrix= vmath::translate(0.0f, 0.0f, -6.0f);
    modelViewProjectionMatrix = perspetiveProjectionMatrix * modelViewMatrix;
    
    glUniformMatrix4fv(mvpUniform,1,GL_FALSE,modelViewProjectionMatrix);
    glUniform1i(gNumberOfSegmentsUniform, gNumberOfLineSegments);
    glUniform1i(gNumberOfStripsUniform, 1);
    glUniform4fv(gLineColorUniform, 1, vmath::vec4(1.0f, 1.0f, 0.0f, 1.0f));

    glPatchParameteri(GL_PATCH_VERTICES,4);
    glBindVertexArray(vao);
    
    glDrawArrays(GL_PATCHES,0,4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL) acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];
            break;
        case 'u': //Up key
        case 'U':
            gNumberOfLineSegments++;
            if (gNumberOfLineSegments >= 50)
                gNumberOfLineSegments = 50;
            break;
        case 'd': //Down key
        case 'D':
            gNumberOfLineSegments--;
            if (gNumberOfLineSegments <= 0)
                gNumberOfLineSegments = 1;
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    
}

-(void) mouseFragged:(NSEvent *)theEvent
{
    
}
-(void)rightMouseDown:(NSEvent *)theEvent
{
    
 
}
-(void) dealloc
{
    if(vao)
    {
        glDeleteVertexArrays(1,&vao);
        vao=0;
    }
    if(vbo)
    {
        glDeleteBuffers(1,&vbo);
        vbo=0;
    }
    glDetachShader(shaderProgramObject,vertexShaderObject);
    glDetachShader(shaderProgramObject,fragmentShaderObject);
    glDeleteShader(vertexShaderObject);
    glDeleteShader(fragmentShaderObject);
    
    vertexShaderObject=0;
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}
@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}


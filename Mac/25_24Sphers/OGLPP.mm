//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "sphere.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0,
};

// C style global function decleration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const
	CVTimeStamp *, CVOptionFlags, CVOptionFlags * , void *);

//global varibales
FILE *gpFile = NULL;
bool gbLighting = false;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];
GLfloat width =0.0f;
GLfloat height=0.0f;
//Animation varibales
static float angleOfXRotation = 0.0f;
static float angleOfYRotation = 0.0f;
static float angleOfZRotation = 0.0f;

int keyPress = 0;
//light values
GLfloat lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };

//interface decleration
@interface APPDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry-point function
 
 int main(int argc, const char * argv[])
 {
 	//code

 	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

 	NSApp = [NSApplication sharedApplication];

 	[NSApp setDelegate:[[APPDelegate alloc]init]];

 	[NSApp run];

 	[pPool release];

 	return(0);
 } 


 //interface implemention

@implementation APPDelegate
{
@private
	NSWindow *window;
	GLView *glView;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

	//code log File

	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("can not create log file: \n");
		[self release];
		[NSApp terminate:self];
	}

	fprintf(gpFile, "Program started successfull\n" );


	//code
	//window

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window

	window=[[NSWindow alloc] initWithContentRect:win_rect 
							 styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
							 backing:NSBackingStoreBuffered
							 defer:NO];
	[window setTitle:@"macOs Window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSApplication *)notification{
	//code
	fprintf(gpFile, "program is terminate successfully\n");

	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSApplication *)notification{
	//code
	[NSApp terminate:self];
} 

- (void)dealloc{
	//code
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;

	//variables for PP
    GLuint gVertexShaderObject_raghav;
    GLuint gFragmentShaderObject_raghav;
    GLuint gShaderProgramObject_raghav;

    GLuint vao_sphere_raghav;//vertex array object for rect
    GLuint vbo_position_sphere_raghav;//vertex buffer object(position) for rect
    GLuint vbo_normal_sphere_raghav;//vertex buffer object(color) for rect
    GLuint vbo_elements_sphere_raghav;
    
    GLuint mUniform_raghav; // model view matrix
    GLuint vUniform_raghav;
    GLuint pUniform_raghav; //projection matrix
    
    GLuint laUniform_raghav;
    GLuint ldUniform_raghav;
    GLuint lsUniform_raghav;
    
    GLuint kaUniform_raghav;
    GLuint kdUniform_raghav;
    GLuint ksUniform_raghav;
    GLuint materialShinynessUniform_raghav;
    
    GLuint lightPositionUniform_raghav;
    GLuint lKeyIsPressedUniform_raghav;

    unsigned int gNumVertices_raghav;
    unsigned int gNumElements_raghav;
    
	vmath::mat4 perspectiveProjectionmatrix;


}

-(id)initWithFrame:(NSRect)frame
{
	//code
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			//Must specify the 4.1 core profile to use openGL 4.1
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,	

			//Specify the display ID to associated the GL Context With 
			//(main display for now)

			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask
			(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};
		
		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]
			initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "No valid openGL pixel format is available \n");

			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]
			initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext]; // it automatically release the
		//older context, if present and set newer one
	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	//code
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);

}

-(void)prepareOpenGL
{
	//code
	//OpenGL info

	[super prepareOpenGL];

	fprintf(gpFile, "OpenGL Version %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt =1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


		//Define vertex shader object

	gVertexShaderObject_raghav = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec4 u_lightPosition;" \
    "out vec3 tNorm;" \
    "out vec3 lightDirection;" \
    "out vec3 viwerVector;" \
    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed == 1)" \
    "{" \
    "vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
    "tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" \
    "lightDirection = vec3(u_lightPosition - eye_Coordinate);" \
    "viwerVector = -eye_Coordinate.xyz;" \
    "}" \
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
    "}"	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject_raghav, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject_raghav);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject_raghav, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_raghav, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_raghav, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}





	gFragmentShaderObject_raghav = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{	"#version 410 core" \
     "\n" \
     "in vec3 tNorm;" \
     "in vec3 lightDirection;" \
     "in vec3 viwerVector;" \
     "uniform vec3 u_la;" \
     "uniform vec3 u_ld;" \
     "uniform vec3 u_ls;" \
     "uniform vec3 u_ka;" \
     "uniform vec3 u_kd;" \
     "uniform vec3 u_ks;" \
     "uniform float u_materialShine;" \
     "uniform vec4 u_lightPosition;" \
     "uniform int u_lKeyIsPressed;" \
     "out vec4 FragColor;" \
     "void main(void)" \
     "{" \
     "if(u_lKeyIsPressed == 1)" \
     "{" \
     "vec3 normalizeTNorm = normalize(tNorm);" \
     "vec3 normalizeLightDirection = normalize(lightDirection);" \
     "vec3 normalizeViwerVector = normalize(viwerVector);" \
     "float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" \
     "vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" \
     "vec3 ambiant = vec3(u_la * u_ka);" \
     "vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" \
     "vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" \
     "vec3 phong_ads_light = ambiant + diffuse + specular;" \
     "FragColor = vec4(phong_ads_light,1.0f);" \
     "}" \
     "else" \
     "{" \
     "FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
     "}" \
     "}"
	};

	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject_raghav, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject_raghav);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject_raghav, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_raghav, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_raghav, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}



	//Create shader program object 
	gShaderProgramObject_raghav = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject_raghav, gVertexShaderObject_raghav);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject_raghav, gFragmentShaderObject_raghav);

	//Prelinking binding of vertex shader
	glBindAttribLocation(gShaderProgramObject_raghav, AMC_ATTRIBUTE_POSITION, "vPosition");

	// ---- texture
    glBindAttribLocation(gShaderProgramObject_raghav, AMC_ATTRIBUTE_NORMAL, "vNormal");	//Link Shader Program
	glLinkProgram(gShaderProgramObject_raghav);

	//Error Checking For Program Object

	iInfoLogLength = 0;

	GLint iProgramLinkStatus = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_raghav, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_raghav, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_raghav, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


    //Postlinking Retriving Uniform locations
    mUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_m_matrix");
    vUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_v_matrix");
    pUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_p_matrix");
    laUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_la");
    ldUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ld");
    lsUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ls");
    kaUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ka");
    kdUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_kd");
    ksUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ks");
    materialShinynessUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_materialShine");
    lightPositionUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_lightPosition");
    lKeyIsPressedUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_lKeyIsPressed");
    
    getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
    gNumVertices_raghav = getNumberOfSphereVertices();
    gNumElements_raghav = getNumberOfSphereElements();

    //****************************** CUBE ****************************
    //Create vao for rect
    glGenVertexArrays(1, &vao_sphere_raghav);
    glBindVertexArray(vao_sphere_raghav);

    //################### POSITION ###################3
    //Generating Buffer for rect
    glGenBuffers(1, &vbo_position_sphere_raghav);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere_raghav);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and arra for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //################### NORMAL ###################3
    //Generating Buffer for rect
    glGenBuffers(1, &vbo_normal_sphere_raghav);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere_raghav);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    //Unbinding buffer for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Unbinding Array for rectangle
    glBindVertexArray(0);


    //########################## Elements ###############
    //Generating Buffer for rect
    glGenBuffers(1, &vbo_elements_sphere_raghav);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    
    ////how many slots my array is break
    //glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    ////Enabling the position
    //glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    //Unbinding buffer for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
//
    //Unbinding Array for rectangle
    glBindVertexArray(0);//	//for rectangle position

	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//set bk color

	glClearColor(0.5f,0.5f,0.5f,0.0f);//blue


	perspectiveProjectionmatrix = vmath::mat4::identity();


	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat); 
	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{
	[super reshape];
	//code
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	 width = rect.size.width;
	 height = rect.size.height;

    int distorationX = width/6;
    int distorationY = height/8;

	if(height ==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionmatrix = vmath::perspective(45.0f,
												(GLfloat)distorationX / (GLfloat)distorationY,
												0.1f,
												100.0f);
	

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	}




- (void)drawRect:(NSRect)dirtyRect
{
	//code
	[self drawView];
}


- (void)drawView
{
	//Animation varibales
	//static float angleRect = 0.0f;
    

	//code
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	//Using program object 
	glUseProgram(gShaderProgramObject_raghav);
	
        if (gbLighting == true)
    {
        glUniform1i(lKeyIsPressedUniform_raghav, 1);
        glUniform3fv(laUniform_raghav, 1, lightAmbiant);
        glUniform3fv(ldUniform_raghav, 1, lightDiffuse);
        glUniform3fv(lsUniform_raghav, 1, lightSpecular);
        if (keyPress == 1)
        {
            lightPosition[1] = cos(angleOfXRotation) * 100.0f;
            lightPosition[2] = -sin(angleOfXRotation) * 100.0f;
            
            glUniform4fv(lightPositionUniform_raghav, 1, lightPosition);
        }else if(keyPress == 2)
        {
            lightPosition[0] = cos(angleOfYRotation) * 100.0f;
            lightPosition[2] = -sin(angleOfYRotation) * 100.0f;
        
            glUniform4fv(lightPositionUniform_raghav, 1, lightPosition);
        }else if(keyPress==3) {

            lightPosition[0] = cos(angleOfZRotation) * 100.0f;
            lightPosition[1] = sin(angleOfZRotation) * 100.0f;
            
            glUniform4fv(lightPositionUniform_raghav, 1, lightPosition);
        }
        
        
    }
    else {
        glUniform1i(lKeyIsPressedUniform_raghav, 0);
    }
    
    
	
    
    [self draw24Sphere];


	//Unused Program
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    angleOfXRotation =angleOfXRotation+0.03f;
    angleOfYRotation=angleOfYRotation+0.03f;
    angleOfZRotation =angleOfZRotation+0.03f;

}
-(void)draw24Sphere
{
    int xWindowCenter = width / 2;
    int yWindowCenter = height / 2;


    int distorasionX = width / 6;
    int distorasionY = height / 8;

    int xTransOffset = distorasionX;
    int yTransOffset = distorasionY;

    int currentViewportX;
    int currentViewportY;
    
    //declerations of matrix
        vmath::mat4 modelMatrix;
        vmath::mat4 viewMatrix;
        vmath::mat4 translationMatrix;
        vmath::mat4 rotationMatrix;
        vmath::mat4 projectionMatrix;
    
    
        GLfloat MaterialAmbiant[4];
        GLfloat MaterialDiffuse[4];
        GLfloat MaterialSpecular[4];
        GLfloat MaterialShininess[1];
    
        translationMatrix = vmath::mat4::identity();
        rotationMatrix = vmath::mat4::identity();
        modelMatrix = vmath::mat4::identity();
        viewMatrix = vmath::mat4::identity();
        projectionMatrix = vmath::mat4::identity();
        
        
        //Do neccessary transformation
        translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //    rotationMatrix = vmath::rotate(angleRect, angleRect, angleRect);
        //Do neccessary Matrix Multilication
        modelMatrix = translationMatrix;// * rotationMatrix;
        projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    
    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;
    
    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);    //Bind with vao of rectangle
    
    MaterialAmbiant[0] = 0.0215f;
    MaterialAmbiant[1] = 0.1745f;
    MaterialAmbiant[2] = 0.0215f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 0.07568f;
    MaterialDiffuse[1] = 0.61424f;
    MaterialDiffuse[2] = 0.0215f;
    MaterialDiffuse[3] = 1.0f;

    MaterialSpecular[0] = 0.633f;
    MaterialSpecular[1] = 0.727811f;
    MaterialSpecular[2] = 0.633f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.6f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);        glBindVertexArray(vao_sphere_raghav);

        //Draw function
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT,0);    //Bind texture if any
    
        //Unbind vao of rectangle
    glBindVertexArray(0);
    
    
    //*********************************** SPHERE 2 *****************************
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);

    
    MaterialAmbiant[0] = 0.135f;
    MaterialAmbiant[1] = 0.2225f;
    MaterialAmbiant[2] = 0.1575f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 0.54f;
    MaterialDiffuse[1] = 0.89f;
    MaterialDiffuse[2] = 0.63f;
    MaterialDiffuse[3] = 1.0f;
    

    MaterialSpecular[0] = 0.316228f;
    MaterialSpecular[1] = 0.316228f;
    MaterialSpecular[2] = 0.316228f;
    MaterialSpecular[3] = 1.0f;
    

    MaterialShininess[0] = 0.1f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 3 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


        MaterialAmbiant[0] = 0.5375f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.06625f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.18275f;
    MaterialDiffuse[1] = 0.17f;
    MaterialDiffuse[2] = 0.22525f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.332741f;
    MaterialSpecular[1] = 0.328634f;
    MaterialSpecular[2] = 0.346435f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.3f * 128.0f;
    
    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);


    //*********************************** SPHERE 4 *****************************
  translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.25f;
    MaterialAmbiant[1] = 0.20725f;
    MaterialAmbiant[2] = 0.20725f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 1.0f;
    MaterialDiffuse[1] = 0.829f;
    MaterialDiffuse[2] = 0.829f;
    MaterialDiffuse[3] = 1.0f;

    MaterialSpecular[0] = 0.296648f;
    MaterialSpecular[1] = 0.296648f;
    MaterialSpecular[2] = 0.296648f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.088f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 5 *****************************
  translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.1745f;
    MaterialAmbiant[1] = 0.01175f;
    MaterialAmbiant[2] = 0.01175f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.61424f;
    MaterialDiffuse[1] = 0.04136f;
    MaterialDiffuse[2] = 0.04136f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.727811f;
    MaterialSpecular[1] = 0.626959f;
    MaterialSpecular[2] = 0.626959f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.6f * 128.0f;
    

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 6 *****************************
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.1f;
    MaterialAmbiant[1] = 0.18725f;
    MaterialAmbiant[2] = 0.1745f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.396f;
    MaterialDiffuse[1] = 0.74151f;
    MaterialDiffuse[2] = 0.69102f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.297254f;
    MaterialSpecular[1] = 0.30829f;
    MaterialSpecular[2] = 0.306678f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.1f * 128.0f;
    

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 7 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.329412f;
    MaterialAmbiant[1] = 0.223529f;
    MaterialAmbiant[2] = 0.27451f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.780392f;
    MaterialDiffuse[1] = 0.568627f;
    MaterialDiffuse[2] = 0.113725f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.992157f;
    MaterialSpecular[1] = 0.941176f;
    MaterialSpecular[2] = 0.807843f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.21794872f * 128.0f;
    

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 8 *****************************
 translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.2125f;
    MaterialAmbiant[1] = 0.1275f;
    MaterialAmbiant[2] = 0.054f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.714f;
    MaterialDiffuse[1] = 0.4284f;
    MaterialDiffuse[2] = 0.18144f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.393548f;
    MaterialSpecular[1] = 0.271906f;
    MaterialSpecular[2] = 0.166721f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.2f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 9 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.25f;
    MaterialAmbiant[1] = 0.25f;
    MaterialAmbiant[2] = 0.25f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 0.4f;
    MaterialDiffuse[1] = 0.4f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;

    MaterialSpecular[0] = 0.774597f;
    MaterialSpecular[1] = 0.774597f;
    MaterialSpecular[2] = 0.774597f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.6f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 10 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.19125f;
    MaterialAmbiant[1] = 0.0735f;
    MaterialAmbiant[2] = 0.02225f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.7038f;
    MaterialDiffuse[1] = 0.27048f;
    MaterialDiffuse[2] = 0.0828f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.256777f;
    MaterialSpecular[1] = 0.137622f;
    MaterialSpecular[2] = 0.086014f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.1f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 11 *****************************
    //Initialize above matrix to identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.24725f;
    MaterialAmbiant[1] = 0.1995f;
    MaterialAmbiant[2] = 0.0745f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.75164f;
    MaterialDiffuse[1] = 0.60648f;
    MaterialDiffuse[2] = 0.22648f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.628281f;
    MaterialSpecular[1] = 0.555802f;
    MaterialSpecular[2] = 0.366065f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.4f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 12 *****************************
    //Initialize above matrix to identity
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.19225f;
    MaterialAmbiant[1] = 0.19225f;
    MaterialAmbiant[2] = 0.19225f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.50754f;
    MaterialDiffuse[1] = 0.50754f;
    MaterialDiffuse[2] = 0.50754f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.508273f;
    MaterialSpecular[1] = 0.508273f;
    MaterialSpecular[2] = 0.508273f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.4f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 13 *****************************
    //Initialize above matrix to identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.01f;
    MaterialDiffuse[1] = 0.01f;
    MaterialDiffuse[2] = 0.01f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.50f;
    MaterialSpecular[1] = 0.50f;
    MaterialSpecular[2] = 0.50f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 14 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.1f;
    MaterialAmbiant[2] = 0.06f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.0f;
    MaterialDiffuse[1] = 0.50980392f;
    MaterialDiffuse[2] = 0.50980392f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.50196078f;
    MaterialSpecular[1] = 0.50196078f;
    MaterialSpecular[2] = 0.50196078f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 15 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.1f;
    MaterialDiffuse[1] = 0.35f;
    MaterialDiffuse[2] = 0.1f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.45f;
    MaterialSpecular[1] = 0.55f;
    MaterialSpecular[2] = 0.45f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 16 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.0f;
    MaterialDiffuse[2] = 0.0f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.6f;
    MaterialSpecular[2] = 0.6f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;
    
    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 17 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.55f;
    MaterialDiffuse[1] = 0.55f;
    MaterialDiffuse[2] = 0.55f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.70f;
    MaterialSpecular[1] = 0.70f;
    MaterialSpecular[2] = 0.70f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 18 *****************************
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.0f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.60f;
    MaterialSpecular[1] = 0.60f;
    MaterialSpecular[2] = 0.50f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 19 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.02f;
    MaterialAmbiant[1] = 0.02f;
    MaterialAmbiant[2] = 0.02f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.01f;
    MaterialDiffuse[1] = 0.01f;
    MaterialDiffuse[2] = 0.01f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.4f;
    MaterialSpecular[1] = 0.4f;
    MaterialSpecular[2] = 0.4f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.078125f * 128.0f;
    
    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 20 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.05f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.4f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.5f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.04f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.7f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 21 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.4f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.04f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.04f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 22 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.05f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.4f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.04f;
    MaterialSpecular[2] = 0.04f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 23 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.05f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.05f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.5f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.7f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 24 *****************************
 translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.05f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.04f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);}
-(BOOL)acceptsFirstResponder{
	//code
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27: // ESC Key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
		
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
            
        case 'L':
        case 'l':
            if (gbLighting == false)
            {
                gbLighting = true;
            }
            else {
                gbLighting = false;
            }		default:
			break;
                    case'x':
            case'X':
                keyPress = 1;
                angleOfXRotation = 0.0f;
                break;
            case'y':
            case'Y':
                keyPress = 2;
                angleOfXRotation = 0.0f;
                break;
            case'z':
            case'Z':
                keyPress = 3;
                angleOfXRotation = 0.0f;
                break;

	}
}

-(void)mouseDown:(NSEvent *)theEvent{
	//code
	
	[self setNeedsDisplay:YES]; //repainting
}

-(void)mouseDragged:(NSEvent *)theEvent{
	//code
}
-(void)RightMouseDown:(NSEvent *)theEvent{
	//code

	[self setNeedsDisplay:YES]; //repainting
}

- (void) dealloc{
	//code

	if (vbo_normal_sphere_raghav)
	{
		glDeleteBuffers(1, &vbo_normal_sphere_raghav);
		vbo_normal_sphere_raghav = 0;
	}
	if (vbo_position_sphere_raghav)
	{
		glDeleteBuffers(1, &vbo_position_sphere_raghav);
		vbo_position_sphere_raghav = 0;
	}
	if (vao_sphere_raghav)
	{
		glDeleteVertexArrays(1, &vao_sphere_raghav);
		vao_sphere_raghav = 0;
	}

	if (gShaderProgramObject_raghav)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject_raghav);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject_raghav, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_raghav, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject_raghav, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_raghav);
		gShaderProgramObject_raghav = 0;
	}
		
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
								const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
								CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}

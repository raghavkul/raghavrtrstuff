#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import<QuartzCore/CVDisplayLink.h>
#import<OpenGL/gl3.h>
#import<OpenGL/gl3ext.h>
#import "vmath.h"
#import "Sphere.h"

enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXTURE0,
};

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp*,const CVTimeStamp *,CVOptionFlags,CVOptionFlags*,void*);

FILE *gpFile=NULL;

bool gbLighting = false;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];
int keyPress = 2;

GLuint u_model,u_View, u_Projection,  lightPositionUniform, lKeyPressed;
GLuint vbo_sphere_position, vbo_sphere_normal, vbo_sphere_elements;
GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat LIghtDiifuse[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialShineness = 128.0f;
GLuint u_la, u_ld, u_ls, u_ka, u_kd, u_ks,u_materialShineness;

unsigned int gNumVertices, gNumElements;

@interface AppDelegate : NSObject <NSApplicationDelegate , NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

int main(int args,const char * argv[])
{
        NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc] init];
        NSApp = [NSApplication sharedApplication];
        
        [NSApp setDelegate:[[AppDelegate alloc] init]];
        [NSApp run];
        [pPool release];
        return (0);
}

@implementation AppDelegate
{
    @private
        NSWindow *window;
        GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSBundle *mainBundle=[NSBundle mainBundle];
    NSString *appDirName=[mainBundle bundlePath];
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
    
    const char *pszLogNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    gpFile = fopen(pszLogNameWithPath,"w");
    
    if(gpFile==NULL)
    {
        printf("Can not Create Log file.\n Exitting...\n");
        [self release];
        [NSApp terminate:self];
    }
    fprintf(gpFile,"Program is Started Successfully\n");
    
    NSRect win_rect;
    win_rect=NSMakeRect(0.0,0.0,800.0,600.0);
    window=[[NSWindow alloc] initWithContentRect:win_rect             styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable |             NSWindowStyleMaskMiniaturizable |             NSWindowStyleMaskResizable backing:NSBackingStoreBuffered  defer:NO];
        [window setTitle:@"macOS OpenGL Window"];
        [window center];

        glView = [[GLView alloc] initWithFrame:win_rect];
        [window setContentView:glView];
        [window setDelegate:self];
        [window makeKeyAndOrderFront:self];
        fprintf(gpFile,"Program is Started Successfully\n");
}

- (void) applicationWillTerminate:(NSNotification *)notification
{
    fprintf(gpFile,"Program Is Terminated Successfully\n");
    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}
- (void) windowWillClose:(NSNotification *)notification
{
    [NSApp terminate:self];
}

- (void) dealloc
{
    [glView release];
    
    [window release];

    [super dealloc];
}
@end

@implementation GLView
{
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint vertexShaderObject;
    GLuint fragmentShaderObject;
    GLuint shaderProgramObject;
    
    GLuint vao_sphere;
    GLuint vbo_sphere_position, vbo_sphere_normal, vbo_sphere_elements;
    
    
    vmath::mat4 perspetiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [[self window] setContentView:self];
        NSOpenGLPixelFormatAttribute attrs[]=
        {
            NSOpenGLPFAOpenGLProfile,
            NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,0};
            
            NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
            
            if(pixelFormat ==nil)
            {
                fprintf(gpFile,"No Valid OpenGL Pixel Format Is available Exitting...\n");
                [self release];
                [NSApp terminate:self];
            }
            
            NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];
            
            [self setPixelFormat:pixelFormat];
            
            [self setOpenGLContext:glContext];
            fprintf(gpFile,"Context created successfully\n");
        
    }
    return(self);
}

- (CVReturn) getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
    [self drawView];
    [pool release];
    return (kCVReturnSuccess);
}

-(void)prepareOpenGL
{
    fprintf(gpFile,"OpenGL Version : %s\n\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version : %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    [[self openGLContext]makeCurrentContext];
    
    GLint swapInt=1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    fprintf(gpFile,"\n In prepareOpenGL:%d\n",swapInt);
    
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \

    "in vec3 vNormal;" \
    "uniform mat4 u_model;" \
    "uniform mat4 u_view;" \
    "uniform mat4 u_Projection;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec4 u_light_position;" \
    "out vec3 transformedNormed;" \
    "out vec3 lightdirection;" \
    "out vec3 viewervector;" \
    "out vec4 eye_coordinates;" \
    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed==1)" \
    "{" \
            "eye_coordinates = u_view * u_model * vPosition;" \
            "transformedNormed = mat3(u_view * u_model )* vNormal;" \
            "lightdirection = vec3(u_light_position) - vec3(eye_coordinates);" \
            "viewervector = vec3(-eye_coordinates.xyz);" \
    "}" \
        "gl_Position = u_Projection * u_view * u_model * vPosition;" \
    "}\n";
    
    glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    
    glCompileShader(vertexShaderObject);
    
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength > 0)
        {
            szInfoLog = (char *) malloc(iINfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Vertex Shader Compilation Error %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    
        getSphereVertexData(sphere_vertices,sphere_normals,sphere_texture,sphere_elements);
    gNumVertices=getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();
    
   glGenVertexArrays(1, &vao_sphere);
   glBindVertexArray(vao_sphere);

   glGenBuffers(1, &vbo_sphere_position);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);

   glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
   glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
   glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

   glBindBuffer(GL_ARRAY_BUFFER, 0);

   glGenBuffers(1, &vbo_sphere_normal);
   glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);

   glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
   glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
   glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

   glBindBuffer(GL_ARRAY_BUFFER, 0);


   glGenBuffers(1, &vbo_sphere_elements);
   glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);


   glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

   glBindBuffer(GL_ARRAY_BUFFER, 0);

   glBindVertexArray(0);
    
    
    glClearDepth(1.0f);
    
    glEnable(GL_DEPTH_TEST);
    
    glDepthFunc(GL_LEQUAL);
    
   
    
    perspetiveProjectionMatrix = vmath::mat4::identity();
    glClearColor(0.0f,0.0f,0.0f,0.0f);
    
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);
    
    CVDisplayLinkStart(displayLink);
    [super prepareOpenGL];
}
-(void) VertexShader_PerFragment
{
    
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    
    "in vec4 vPosition;" \

    "in vec3 vNormal;" \
    "uniform mat4 u_model;" \
    "uniform mat4 u_view;" \
    "uniform mat4 u_Projection;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec4 u_light_position;" \
    "out vec3 transformedNormed;" \
    "out vec3 lightdirection;" \
    "out vec3 viewervector;" \

    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed==1)" \
    "{" \
            "vec4 eye_coordinates = u_view * u_model * vPosition;" \
            "transformedNormed = mat3(u_view * u_model )* vNormal;" \
            "lightdirection = vec3(u_light_position) - vec3(eye_coordinates);" \
            "viewervector = vec3(-eye_coordinates.xyz);" \
    "}" \
    "gl_Position = u_Projection * u_view * u_model * vPosition;" \
    "}\n";
    
    glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    
    glCompileShader(vertexShaderObject);
    
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength > 0)
        {
            szInfoLog = (char *) malloc(iINfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Vertex Shader Per Vertex Compilation Error %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

}
-(void) FragmentShader_PerFragment
{
     GLint iINfoLogLength = 0;
     GLint iShaderCompiledStatus =0;
     char *szInfoLog = NULL;
    
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode=
    "#version 410" \
    "\n" \
   "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_matrialShineness;" \
    "in vec3 transformedNormed;" \
    "in vec3 lightdirection;" \
    "vec3 ambient;" \
    "in vec3 viewervector;" \
    "vec3 phong_ADS_lighting;" \
    "uniform int u_lKeyIsPressed;" \
    "out vec4 fragColor;" \
    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed==1)" \
    "{" \
                "vec3 transformedNormalized = normalize(transformedNormed);" \
                "vec3 normalizedlightdirection = normalize(lightdirection);" \
                "float tn_dot_ld = max(dot(normalizedlightdirection,transformedNormalized),0.0);" \
                "vec3 reflectionVector = reflect(-normalizedlightdirection,transformedNormalized);" \
                "vec3 normalizedViewerVector = normalize(viewervector);" \
                "ambient = u_la * u_ka;" \
                "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
                "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector,normalizedViewerVector),0.0),u_matrialShineness);" \
                "phong_ADS_lighting = ambient + diffuse + specular;" \
                "fragColor = vec4(phong_ADS_lighting,1.0);" \
    "}" \
    "else" \
    "{" \
                "phong_ADS_lighting = vec3(1.0,1.0,1.0);" \
                "fragColor = vec4(phong_ADS_lighting,1.0);" \
    "}" \
    "}";
    
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*) malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Fragment Shader Compilation Log %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
}
-(void) vertexShader_PerVertex
{
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode=
    "#version 410" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model;" \
    "uniform mat4 u_view;" \
    "uniform mat4 u_Projection;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_matrialShineness;" \
    "uniform vec4 u_light_position;" \
    "out vec3 phong_ADS_lighting;" \
    "void main(void)" \
    "{" \
            "if(u_lKeyIsPressed==1)" \
            "{" \
                    "vec4 eye_coordinates = u_view * u_model * vPosition;" \
                    "vec3 transformedNormed = normalize(mat3(u_view * u_model )* vNormal);" \
                    "vec3 lightdirection = normalize(vec3(u_light_position) - vec3(eye_coordinates));" \
                    "float tn_dot_ld = max(dot(lightdirection,transformedNormed),0.0);" \
                    "vec3 reflectionVector = reflect(-lightdirection,transformedNormed);" \
                    "vec3 viewervector = normalize(vec3(-eye_coordinates.xyz));" \
                    "vec3 ambient = u_la * u_ka;" \
                    "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
                    "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector,viewervector),0.0),u_matrialShineness);" \
                    "phong_ADS_lighting = ambient + diffuse + specular;" \
            "}" \
            "else" \
            "{" \
                    "phong_ADS_lighting = vec3(1.0,1.0,1.0);" \
            "}" \
                "gl_Position = u_Projection * u_view * u_model * vPosition;" \
    "}\n";
    
    glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    
    glCompileShader(vertexShaderObject);
    
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength > 0)
        {
            szInfoLog = (char *) malloc(iINfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Vertex Shader Per Vertex Compilation Error %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

}
-(void) fragmentShader_PerVertex
{
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode=
    "#version 410" \
    "\n" \
   "in vec3 phong_ADS_lighting;" \
    "out vec4 fragColor;" \
    "void main(void)" \
    "{" \
            "fragColor = vec4(phong_ADS_lighting,1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*) malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iINfoLogLength,&written,szInfoLog);
                
                fprintf(gpFile,"Fragment Shader Per Vertex Compilation Log %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                [NSApp terminate:self];
            }
        }
    }

}
-(void) ShaderProgramLinking
{
    GLint iINfoLogLength = 0;
       GLint iShaderCompiledStatus =0;
       char *szInfoLog = NULL;
        shaderProgramObject = glCreateProgram();
      
      glAttachShader(shaderProgramObject,vertexShaderObject);
      glAttachShader(shaderProgramObject,fragmentShaderObject);
      
      
      
      glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
      glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");
      
      glLinkProgram(shaderProgramObject);
      GLint iShaderProgramLinkStatus=0;
      glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
      
      if(iShaderProgramLinkStatus==GL_FALSE)
      {
          glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
          if(iINfoLogLength>0)
          {
              szInfoLog=(char*)malloc(iINfoLogLength);
              if(szInfoLog!=NULL)
              {
                  GLsizei written;
                  glGetProgramInfoLog(shaderProgramObject,iINfoLogLength,&written,szInfoLog);
                  fprintf(gpFile,"Shader Program Link Log : %s\n",szInfoLog);
                  free(szInfoLog);
                  [self release];
                  [NSApp terminate:self];
              }
          }
      }
      
      u_model= glGetUniformLocation(shaderProgramObject, "u_model");
      u_View = glGetUniformLocation(shaderProgramObject, "u_view");
      u_Projection = glGetUniformLocation(shaderProgramObject, "u_Projection");
      lKeyPressed = glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
      u_la= glGetUniformLocation(shaderProgramObject, "u_la");
      u_ld = glGetUniformLocation(shaderProgramObject, "u_ld");
      u_ls = glGetUniformLocation(shaderProgramObject, "u_ls");
      u_ka = glGetUniformLocation(shaderProgramObject, "u_ka");
      u_kd = glGetUniformLocation(shaderProgramObject, "u_kd");
      u_ks = glGetUniformLocation(shaderProgramObject, "u_ks");
      u_materialShineness = glGetUniformLocation(shaderProgramObject, "u_matrialShineness");
      lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");

    

}
-(void)reshape
{
    CGLLockContext ((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    NSRect rect = [self bounds];
    
    GLfloat width = rect.size.width;
    GLfloat height = rect.size.height;
    
    if(height==0)
    {
        height=1;
    }
    glViewport(0,0,(GLsizei)width,(GLsizei)height);
    perspetiveProjectionMatrix= vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    fprintf(gpFile,"In reshape\n");
    
   
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    [super reshape];
}


- (void) drawRect:(NSRect)dirtyRect
{
    [self drawView];
}

-(void) drawView
{
    
    [[self openGLContext]makeCurrentContext];
    
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shaderProgramObject);
    
    vmath::mat4 modelMatrix = vmath::mat4::identity();
    vmath::mat4 viewMatrix = vmath::mat4::identity();
    vmath::mat4 projectMatrix = vmath::mat4::identity();

    
    
    modelMatrix= vmath::translate(0.0f, 0.0f, -2.0f);
 
    
    
      
      projectMatrix = perspetiveProjectionMatrix * projectMatrix * modelMatrix;

      //send necessary matrices to shader in respective uniform
      glUniformMatrix4fv(u_model, 1, GL_FALSE, modelMatrix);
      glUniformMatrix4fv(u_View, 1, GL_FALSE, viewMatrix);
      glUniformMatrix4fv(u_Projection, 1, GL_FALSE, projectMatrix);

        if (gbLighting == true)
        {
            glUniform1i(lKeyPressed, 1);
            glUniform3fv(u_la, 1, LightAmbient);
            glUniform3fv(u_ld, 1, LIghtDiifuse);
            glUniform3fv(u_ls, 1, LightSpecular);
            glUniform3fv(lightPositionUniform, 1, LightPosition);
            glUniform3fv(u_ka, 1, MaterialAmbient);
            glUniform3fv(u_kd, 1, MaterialDiffuse);
            glUniform3fv(u_ks, 1, MaterialSpecular);
            glUniform1f(u_materialShineness,  MaterialShineness);

        }

        else
        {
            glUniform1i(lKeyPressed, 0);
        }




    //Bind with vao
    //(This will avoid repetative steps)

    glBindVertexArray(vao_sphere);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);


   //draw neseaary scene

    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
    
    

    glBindVertexArray(0);
    
    glUseProgram(0);
    if(keyPress==1)
    {
        [self VertexShader_PerFragment];
        [self FragmentShader_PerFragment];
        [self ShaderProgramLinking];
    }
    else if(keyPress==2)
    {
        [self vertexShader_PerVertex];
        [self fragmentShader_PerVertex];
        [self ShaderProgramLinking];
    }
    
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
  
}

-(BOOL) acceptsFirstResponder
{
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void) keyDown:(NSEvent *)theEvent
{
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
            break;
        case 't':
        case 'T':
            [[self window]toggleFullScreen:self];
            break;
        case 'l':
        case 'L':
            if(gbLighting==false)
                gbLighting=true;
            else
                gbLighting=false;
            break;
        case 'f':
        case 'F':
            keyPress=1;
            break;
        case 'v':
        case 'V':
            keyPress=2;
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
    
}

-(void) mouseFragged:(NSEvent *)theEvent
{
    
}
-(void)rightMouseDown:(NSEvent *)theEvent
{
    
 
}
-(void) dealloc
{
    if (vbo_sphere_elements)
    {
        glDeleteBuffers(1, &vbo_sphere_elements);
        vbo_sphere_elements = 0;
    }
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);
        vao_sphere = 0;
    }

    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);
        vbo_sphere_normal = 0;
    }

    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);
        vbo_sphere_position = 0;
    }
    glDetachShader(shaderProgramObject,vertexShaderObject);
    glDetachShader(shaderProgramObject,fragmentShaderObject);
    glDeleteShader(vertexShaderObject);
    glDeleteShader(fragmentShaderObject);
    
    vertexShaderObject=0;
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    [super dealloc];
}
@end


CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,const CVTimeStamp *pOutputTime,CVOptionFlags flagIn,CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
    CVReturn result = [(GLView *) pDisplayLinkContext getFrameForTime:pOutputTime];
    return(result);
}


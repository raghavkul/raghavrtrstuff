//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"


enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0,
};

// C style global function decleration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const
	CVTimeStamp *, CVOptionFlags, CVOptionFlags * , void *);

//global varibales
FILE *gpFile = NULL;
bool gbLighting = false;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];

struct Light
{
    GLfloat ambiant[4];
    GLfloat diffuse[4];
    GLfloat specular[4];
    GLfloat position[4];
};
struct Light lights[2];

//material values
float materialAmbiant[4] = { 0.0f,0.0f,0.0f,0.0f };
float materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialShinyness = 50.0f;

//interface decleration
@interface APPDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry-point function
 
 int main(int argc, const char * argv[])
 {
 	//code

 	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

 	NSApp = [NSApplication sharedApplication];

 	[NSApp setDelegate:[[APPDelegate alloc]init]];

 	[NSApp run];

 	[pPool release];

 	return(0);
 } 


 //interface implemention

@implementation APPDelegate
{
@private
	NSWindow *window;
	GLView *glView;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

	//code log File

	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("can not create log file: \n");
		[self release];
		[NSApp terminate:self];
	}

	fprintf(gpFile, "Program started successfull\n" );


	//code
	//window

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window

	window=[[NSWindow alloc] initWithContentRect:win_rect 
							 styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
							 backing:NSBackingStoreBuffered
							 defer:NO];
	[window setTitle:@"macOs Window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSApplication *)notification{
	//code
	fprintf(gpFile, "program is terminate successfully\n");

	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSApplication *)notification{
	//code
	[NSApp terminate:self];
} 

- (void)dealloc{
	//code
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;

	//variables for PP
    GLuint gVertexShaderObject_raghav;
    GLuint gFragmentShaderObject_raghav;
    GLuint gShaderProgramObject_raghav;

    GLuint vao_pyramid_raghav;//vertex array object for tri
    GLuint vbo_position_pyramid_raghav;//vertex buffer object(position) for tri
    GLuint vbo_normal_pyramid_raghav;//vertex buffer object(color) for tri
    
    GLuint mUniform_raghav; // model view matrix
    GLuint vUniform_raghav;
    GLuint pUniform_raghav; //projection matrix
    
  //lights
    GLuint laUniformRed_raghav;
    GLuint ldUniformRed_raghav;
    GLuint lsUniformRed_raghav;


    GLuint laUniformBlue_raghav;
    GLuint ldUniformBlue_raghav;
    GLuint lsUniformBlue_raghav;
    
    GLuint kaUniform_raghav;
    GLuint kdUniform_raghav;
    GLuint ksUniform_raghav;
    GLuint materialShinynessUniform_raghav;
    
    GLuint lightPositionUniformRed_raghav;
    GLuint lightPositionUniformBlue_raghav;
    
    GLuint lKeyIsPressedUniform_raghav;

    unsigned int gNumVertices_raghav;
    unsigned int gNumElements_raghav;
    
	vmath::mat4 perspectiveProjectionmatrix;


}

-(id)initWithFrame:(NSRect)frame
{
	//code
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			//Must specify the 4.1 core profile to use openGL 4.1
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,	

			//Specify the display ID to associated the GL Context With 
			//(main display for now)

			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask
			(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};
		
		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]
			initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "No valid openGL pixel format is available \n");

			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]
			initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext]; // it automatically release the
		//older context, if present and set newer one
	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	//code
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);

}

-(void)prepareOpenGL
{
	//code
	//OpenGL info

	[super prepareOpenGL];

	fprintf(gpFile, "OpenGL Version %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt =1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    
    
    //peace meal array initialization
    //for light 0
    lights[0].ambiant[0] =  0.0f ;
    lights[0].ambiant[1] =  0.0f ;
    lights[0].ambiant[2] =  0.0f ;
    lights[0].ambiant[3] =  0.0f ;

    lights[0].diffuse[0] =  1.0f ;
    lights[0].diffuse[1] =  0.0f ;
    lights[0].diffuse[2] =  0.0f ;
    lights[0].diffuse[3] =  0.0f ;

    lights[0].specular[0] =  1.0f ;
    lights[0].specular[1] =  0.0f ;
    lights[0].specular[2] =  0.0f ;
    lights[0].specular[3] =  0.0f ;

    lights[0].position[0] =  -2.0f;
    lights[0].position[1] =  0.0f ;
    lights[0].position[2] =  0.0f ;
    lights[0].position[3] =  0.0f ;

    //for light 1
    lights[1].ambiant[0] =  0.0f ;
    lights[1].ambiant[1] =  0.0f ;
    lights[1].ambiant[2] =  0.0f ;
    lights[1].ambiant[3] =  0.0f ;

    lights[1].diffuse[0] =  0.0f ;
    lights[1].diffuse[1] =  0.0f ;
    lights[1].diffuse[2] =  1.0f ;
    lights[1].diffuse[3] =  0.0f ;

    lights[1].specular[0] =  0.0f ;
    lights[1].specular[1] =  0.0f ;
    lights[1].specular[2] =  1.0f ;
    lights[1].specular[3] =  0.0f ;

    lights[1].position[0] =  2.0f ;
    lights[1].position[1] =  0.0f ;
    lights[1].position[2] =  0.0f ;
    lights[1].position[3] =  0.0f ;		//Define vertex shader object

	gVertexShaderObject_raghav = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_m_matrix;" \
    "uniform mat4 u_v_matrix;" \
    "uniform mat4 u_p_matrix;" \

    "uniform int u_lKeyIsPressed;" \

    "uniform vec3 u_la_red;" \
    "uniform vec3 u_ld_red;" \
    "uniform vec3 u_ls_red;" \
    "uniform vec3 u_la_blue;" \
    "uniform vec3 u_ld_blue;" \
    "uniform vec3 u_ls_blue;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_materialShine;" \
    "uniform vec4 u_lightPosition_red;" \
    "uniform vec4 u_lightPosition_blue;" \
    "out vec3 phong_ads_light1;" \
    "out vec3 phong_ads_light2;" \
    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed == 1)" \
    "{" \
    "vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
    "vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix ) * vNormal);" \
    "vec3 lightDirection1 = normalize(vec3(u_lightPosition_red - eye_Coordinate));" \
    "vec3 lightDirection2 = normalize(vec3(u_lightPosition_blue - eye_Coordinate));" \
    "float tn_dot_ld1 = max(dot(lightDirection1,tNorm),0.0f);" \
    "float tn_dot_ld2 = max(dot(lightDirection2,tNorm),0.0f);" \
    "vec3 reflectionVector1 = reflect(-lightDirection1 , tNorm);" \
    "vec3 reflectionVector2 = reflect(-lightDirection2 , tNorm);" \
    "vec3 viwerVector = normalize(-eye_Coordinate.xyz);" \
    "vec3 ambiant1 = u_la_red * u_ka;" \
    "vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld1;" \
    "vec3 specular1 = u_ls_red * u_ks * pow(max(dot(reflectionVector1 , viwerVector),0.0f),u_materialShine);" \
    "vec3 ambiant2 = u_la_blue * u_ka;" \
    "vec3 diffuse2 = u_ld_blue * u_kd * tn_dot_ld2;" \
    "vec3 specular2 = u_ls_blue * u_ks * pow(max(dot(reflectionVector2 , viwerVector),0.0f),u_materialShine);" \
    "phong_ads_light1 = ambiant1 + diffuse1 + specular1;" \
    "phong_ads_light2 = ambiant2 + diffuse2 + specular2;" \
    "}" \
    "else" \
    "{" \
    "phong_ads_light1 = vec3(1.0f,1.0f,1.0f);" \
    "phong_ads_light2 = vec3(1.0f,1.0f,1.0f);" \
    "}" \
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
    "}"	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject_raghav, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject_raghav);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject_raghav, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_raghav, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject_raghav, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}





	gFragmentShaderObject_raghav = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{	"#version 410 core" \
     "\n" \
     "in vec3 phong_ads_light1;" \
     "in vec3 phong_ads_light2;" \
     "out vec4 FragColor;" \
     "uniform int u_lKeyIsPressed;" \
     "void main(void)" \
     "{" \
     "if(u_lKeyIsPressed == 1)" \
     "{" \
     "FragColor = vec4(phong_ads_light1 + phong_ads_light2,1.0f);" \
     
     "}" \
     "else" \
     "{" \
     "FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
     "}" \
     "}"
	};

	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject_raghav, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject_raghav);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject_raghav, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_raghav, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject_raghav, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}



	//Create shader program object 
	gShaderProgramObject_raghav = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject_raghav, gVertexShaderObject_raghav);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject_raghav, gFragmentShaderObject_raghav);

	//Prelinking binding of vertex shader
	glBindAttribLocation(gShaderProgramObject_raghav, AMC_ATTRIBUTE_POSITION, "vPosition");

	// ---- texture
    glBindAttribLocation(gShaderProgramObject_raghav, AMC_ATTRIBUTE_NORMAL, "vNormal");	//Link Shader Program
	glLinkProgram(gShaderProgramObject_raghav);

	//Error Checking For Program Object

	iInfoLogLength = 0;

	GLint iProgramLinkStatus = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject_raghav, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_raghav, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject_raghav, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}


   //Postlinking Retriving Uniform locations
    mUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_m_matrix");
    vUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_v_matrix");
    pUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_p_matrix");
    laUniformRed_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_la_red");
    ldUniformRed_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ld_red");
    lsUniformRed_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ls_red");
    lightPositionUniformRed_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_lightPosition_red");
    laUniformBlue_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_la_blue");
    ldUniformBlue_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ld_blue");
    lsUniformBlue_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ls_blue");
    lightPositionUniformBlue_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_lightPosition_blue");
    kaUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ka");
    kdUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_kd");
    ksUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_ks");
    materialShinynessUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_materialShine");
    lKeyIsPressedUniform_raghav = glGetUniformLocation(gShaderProgramObject_raghav, "u_lKeyIsPressed");
    
    const GLfloat pyramidVertices[] = {
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            0.0f, 1.0f, 0.0f,
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            0.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f
    };
    //for triangle color
    const GLfloat pyramidNormal[] = {
        0.0f, 0.447214f, 0.89442f,
        0.0f, 0.447214f, 0.89442f,
        0.0f, 0.447214f, 0.89442f,
        0.894427f, 0.447214f, 0.0f,
        0.894427f, 0.447214f, 0.0f,
        0.894427f, 0.447214f, 0.0f,
        0.0f, 0.447214f, -0.89442f,
        0.0f, 0.447214f, -0.89442f,
        0.0f, 0.447214f, -0.89442f,
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f

    };





    //****************************** PYRAMID ****************************
    //Create vao for triangle
    glGenVertexArrays(1, &vao_pyramid_raghav);
    glBindVertexArray(vao_pyramid_raghav);

    //########### POSITION ##############
    //Generating Buffer for triangle
    glGenBuffers(1, &vbo_position_pyramid_raghav);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid_raghav);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidVertices), pyramidVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    //########### COLOR ##############
    //Generating Buffer for triangle
    glGenBuffers(1, &vbo_normal_pyramid_raghav);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid_raghav);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(pyramidNormal), pyramidNormal, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Unbinding Array for triangle

    glBindVertexArray(0);
	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//set bk color

	glClearColor(0.0f,0.0f,0.0f,0.0f);//blue


	perspectiveProjectionmatrix = vmath::mat4::identity();


	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat); 
	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{
	[super reshape];
	//code
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width; 
	GLfloat height = rect.size.height;


	if(height ==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionmatrix = vmath::perspective(45.0f,
												width / height,
												0.1f,
												100.0f);
	

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
	}




- (void)drawRect:(NSRect)dirtyRect
{
	//code
	[self drawView];
}


- (void)drawView
{
	//Animation varibales
	static float angle_pyr = 0.0f;

	//code
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	//Using program object 
	glUseProgram(gShaderProgramObject_raghav);
	
	//declerations of matrix
	vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	vmath::mat4 projectionMatrix;
    
	//************************** CUBE ************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    
    
	//Do neccessary transformation
	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	rotationMatrix = vmath::rotate(0.0f, angle_pyr, 0.0f);
	//Do neccessary Matrix Multilication
    modelMatrix = translationMatrix * rotationMatrix;
	projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

	//Send neccessary matrices to shader in respective to uniforms
	glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    
    
  if (gbLighting == true)
  {
      glUniform1i(lKeyIsPressedUniform_raghav, 1);

      glUniform3fv(laUniformRed_raghav, 1, lights[0].ambiant);
      glUniform3fv(ldUniformRed_raghav, 1, lights[0].diffuse);
      glUniform3fv(lsUniformRed_raghav, 1, lights[0].specular);
      glUniform4fv(lightPositionUniformRed_raghav, 1, lights[0].position);

      glUniform3fv(laUniformBlue_raghav, 1, lights[1].ambiant);
      glUniform3fv(ldUniformBlue_raghav, 1, lights[1].diffuse);
      glUniform3fv(lsUniformBlue_raghav, 1, lights[1].specular);
      glUniform4fv(lightPositionUniformBlue_raghav, 1, lights[1].position);
      
      
      glUniform3fv(kaUniform_raghav, 1, materialAmbiant);
      glUniform3fv(kdUniform_raghav, 1, materialDiffuse);
      glUniform3fv(ksUniform_raghav, 1, materialSpecular);
      glUniform1f(materialShinynessUniform_raghav, materialShinyness);

      
  }
  else {
      glUniform1i(lKeyIsPressedUniform_raghav, 0);
  }
	//Bind with vao of rectangle
	glBindVertexArray(vao_pyramid_raghav);

    //Draw function
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glDrawArrays(GL_TRIANGLES, 3, 3);
    glDrawArrays(GL_TRIANGLES, 6, 3);
    glDrawArrays(GL_TRIANGLES, 9, 3);

	//Unbind vao of rectangle
	glBindVertexArray(0);

	//Unused Program
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);	

    angle_pyr = angle_pyr + 0.4f;
}

-(BOOL)acceptsFirstResponder{
	//code
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27: // ESC Key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
		
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
            
        case 'L':
        case 'l':
            if (gbLighting == false)
            {
                gbLighting = true;
            }
            else {
                gbLighting = false;
            }		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent{
	//code
	
	[self setNeedsDisplay:YES]; //repainting
}

-(void)mouseDragged:(NSEvent *)theEvent{
	//code
}
-(void)RightMouseDown:(NSEvent *)theEvent{
	//code

	[self setNeedsDisplay:YES]; //repainting
}

- (void) dealloc{
	//code

	if (vbo_normal_pyramid_raghav)
	{
		glDeleteBuffers(1, &vbo_normal_pyramid_raghav);
		vbo_normal_pyramid_raghav = 0;
	}
	if (vbo_position_pyramid_raghav)
	{
		glDeleteBuffers(1, &(vbo_position_pyramid_raghav));
		(vbo_position_pyramid_raghav) = 0;
	}
	if (vao_pyramid_raghav)
	{
		glDeleteVertexArrays(1, &vao_pyramid_raghav);
		vao_pyramid_raghav = 0;
	}

	if (gShaderProgramObject_raghav)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject_raghav);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject_raghav, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject_raghav, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject_raghav, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject_raghav);
		gShaderProgramObject_raghav = 0;
	}
		
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
								const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
								CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}

//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

#define MESH_HEIGHT 1
#define MESH_WIDTH 1
#define MESH_ROW 10
#define MESH_COLUMN 10

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0,
};

// C style global function decleration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const
	CVTimeStamp *, CVOptionFlags, CVOptionFlags * , void *);

//global varibales
FILE *gpFile = NULL;
GLfloat graphVertices[MESH_ROW * MESH_COLUMN * 3];
GLfloat xDiff = (GLfloat)MESH_WIDTH / (GLfloat)MESH_COLUMN;
GLfloat yDiff = (GLfloat)MESH_HEIGHT / (GLfloat)MESH_ROW;
GLfloat circleVertices[1100];
static GLfloat angale;

//interface decleration
@interface APPDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry-point function
 
 int main(int argc, const char * argv[])
 {
 	//code

 	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

 	NSApp = [NSApplication sharedApplication];

 	[NSApp setDelegate:[[APPDelegate alloc]init]];

 	[NSApp run];

 	[pPool release];

 	return(0);
 } 


 //interface implemention

@implementation APPDelegate
{
@private
	NSWindow *window;
	GLView *view;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

	//code log File

	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("can not create log file: \n");
		[self release];
		[NSApp terminate:self];
	}

	fprintf(gpFile, "Program started successfull\n" );


	//code
	//window

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window

	window=[[NSWindow alloc] initWithContentRect:win_rect 
							 styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
							 backing:NSBackingStoreBuffered
							 defer:NO];
	[window setTitle:@"macOs Window"];
	[window center];

	view = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:view];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSApplication *)notification{
	//code
	fprintf(gpFile, "program is terminate successfully\n");

	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSApplication *)notification{
	//code
	[NSApp terminate:self];
} 

- (void)dealloc{
	//code
	[view release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;

	//variables for PP
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint vaoGraphBlue;//vertex array object
    GLuint vaoGraphRed;
    GLuint vaoTriangle, vboTrianglePos, vboTriangleCol;
    GLuint vaoCircle, vboCirclePos, vboCircleCol;
    GLuint vaoRect, vboRectPos, vboRectCol;
    GLuint vboPositionGraphBlue;//vertex buffer object(position)
    GLuint vboPositionGraphRed;
    GLuint vboColorGraphBlue;//vertex buffer object(color)
    GLuint vboColorGraphRed;
    
    GLuint mvpUniform;

	vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
	//code
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			//Must specify the 4.1 core profile to use openGL 4.1
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,	

			//Specify the display ID to associated the GL Context With 
			//(main display for now)

			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask
			(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};
		
		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]
			initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "No valid openGL pixel format is available \n");

			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]
			initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext]; // it automatically release the
		//older context, if present and set newer one
	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	//code
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);

}

-(void)prepareOpenGL
{
    
    GLfloat fx1, fx2, fx3, fy1, fy2, fy3;
    GLfloat fArea, fRad, fPer;
    GLfloat fdAB, fdBC, fdAC;
    GLfloat fxCord, fyCord;
    
	//code
	//OpenGL info

    [super prepareOpenGL];
    
	fprintf(gpFile, "OpenGL Version %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt =1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


		//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 410 " \
		"\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
        "}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	  GLint iProgramLinkStatus = 0;

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}




		//Define fragment shader object

	 iShaderCompileStatus = 0;
	 iInfoLogLength = 0;
	 szInfoLog = NULL;
  
    
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{	"#version 410" \
		"\n" \
        "in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
        "}"
	};

	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}



	//Create shader program object
    gShaderProgramObject = glCreateProgram();

    //Attach Vertex Shader to Program Object
    glAttachShader(gShaderProgramObject, gVertexShaderObject);


    //Attach Fragment Shader to Program Object
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    //Prelinking binding of vertex shader

    //----- Position
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    // ---- color
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //Link Shader Program
    glLinkProgram(gShaderProgramObject);

    //Error Checking For Program Object

    iInfoLogLength = 0;
    //*szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

	//Postlinking Retriving Uniform locations
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    

    const GLfloat redLineVertices[] = {
        0.0f,2.0f,0.0f,
        0.0f,-2.0f,0.0f
    };

    const GLfloat redLineColor[] = {
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f
    };
    const GLfloat blueLineVertices[] = {
        -2.0f,0.0f,0.0f,
        6.0f,0.0f,0.0f
    };
    const GLfloat blueLineColor[] = {
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f
    };
    
    //Create vao
    glGenVertexArrays(1, &vaoGraphBlue);
    glBindVertexArray(vaoGraphBlue);

    //########### POSITION ##############
    //Generating Buffer
    glGenBuffers(1, &vboPositionGraphBlue);
    glBindBuffer(GL_ARRAY_BUFFER, vboPositionGraphBlue);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(blueLineVertices), blueLineVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    //########### COLOR ##############
    //Generating Buffer
    glGenBuffers(1, &vboColorGraphBlue);
    glBindBuffer(GL_ARRAY_BUFFER, vboColorGraphBlue);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(blueLineColor), blueLineColor, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


   
    //Create vao
    glGenVertexArrays(1, &vaoGraphRed);
    glBindVertexArray(vaoGraphRed);

    //########### POSITION ##############
    //Generating Buffer
    glGenBuffers(1, &vboPositionGraphRed);
    glBindBuffer(GL_ARRAY_BUFFER, vboPositionGraphRed);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(redLineVertices), redLineVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    //########### COLOR ##############
    //Generating Buffer
    glGenBuffers(1, &vboColorGraphRed);
    glBindBuffer(GL_ARRAY_BUFFER, vboColorGraphRed);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(redLineColor), blueLineColor, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //****************************** TRIANGLE ****************************
    
    fx1 = 0.0;
       fy1 = 1.0;
       fx2 = -1.0;
       fy2 = -1.0;
       fx3 = 1.0;
       fy3 = -1.0;
    
    const GLfloat triangleVertices[] = {
          fx1, fy1, 0.0,
            fx2, fy2, 0.0,
            fx3, fy3, 0.0,
            fx1, fy1, 0.0
    };

    const GLfloat triangleColor[] = {
            1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0  };
    
    
    //Create vao for triangle
    glGenVertexArrays(1, &vaoTriangle);
    glBindVertexArray(vaoTriangle);

    //Generating Buffer for triangle
    glGenBuffers(1, &vboTrianglePos);
    glBindBuffer(GL_ARRAY_BUFFER, vboTrianglePos);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and array for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    //Generating Buffer for triangle
    glGenBuffers(1, &vboTriangleCol);
    glBindBuffer(GL_ARRAY_BUFFER, vboTriangleCol);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleColor), triangleColor, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and array for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //****************************** RECTANGLE ****************************
    
    
    const GLfloat rectangleVertices[] = {
            fx3, fy1, 0.0, fx2, fy1, 0.0, fx2, fy2, 0.0, fx3, fy3, 0.0, fx3, fy1, 0.0    };
    
    const GLfloat rectangleColors[] = {
              1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0 };
    //Create vao for rect
    glGenVertexArrays(1, &vaoRect);
    glBindVertexArray(vaoRect);

    //Generating Buffer for rect
    glGenBuffers(1, &vboRectPos);
    glBindBuffer(GL_ARRAY_BUFFER, vboRectPos);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleVertices), rectangleVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and arra for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //Generating Buffer for triangle
    glGenBuffers(1, &vboRectCol);
    glBindBuffer(GL_ARRAY_BUFFER, vboRectCol);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(rectangleColors), rectangleColors, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and array for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    
    ///// /  CIRCLE
    
    fdAB = sqrt(((fx2 - fx1)*(fx2 - fx1)) + ((fy2 - fy1)*(fy2 - fy1)));
    fdBC = sqrt(((fx3 - fx2)*(fx3 - fx2)) + ((fy3 - fy2)*(fy3 - fy2)));
    fdAC = sqrt(((fx3 - fx1)*(fx3 - fx1)) + ((fy3 - fy1)*(fy3 - fy1)));
    
    fPer = ((fdAB + fdAB + fdBC) / 2);

    fArea = sqrt(fPer*(fPer - fdAB)*(fPer - fdBC)*(fPer - fdAC));

    fRad = (fArea / fPer);

    fxCord = (((fdBC*fx1) + (fx2*fdAC) + (fx3*fdAB)) / (fPer * 2));
    fyCord = (((fdBC*fy1) + (fy2*fdAC) + (fy3*fdAB)) / (fPer * 2));
	
    GLfloat circleVert[500000];
    GLfloat circleCol[500000];
    GLint i, j;
    float circleSteps = 0.0;
    for(i = 0; i < 6290; i++) {
        for(j = 0; j < 2; j++) {
            if(j==0)
                circleVert[ (i*2) + j] =  fxCord + cos(circleSteps)*fRad;
            else
                circleVert[ (i*2) + j] =  fyCord + sin(circleSteps)*fRad;
        }
        circleSteps += 0.01;
        circleCol[(i*2) + 0] = 1.0;
        circleCol[(i*2) + 1] = 1.0;
        circleCol[(i*2) + 2] = 0.0;
    }
    
    //Create vao for rect
    glGenVertexArrays(1, &vaoCircle);
    glBindVertexArray(vaoCircle);

    //Generating Buffer for rect
    glGenBuffers(1, &vboCirclePos);
    glBindBuffer(GL_ARRAY_BUFFER, vboCirclePos);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(circleVert), circleVert, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and arra for rectangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //Generating Buffer for triangle
    glGenBuffers(1, &vboCircleCol);
    glBindBuffer(GL_ARRAY_BUFFER, vboCircleCol);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(circleCol), circleCol, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 2, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer and array for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//set bk color

	glClearColor(0.0f,0.0f,0.0f,0.0f);//blue


	perspectiveProjectionMatrix = vmath::mat4::identity();


	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat); 
	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{
    [super reshape];
	//code
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width; 
	GLfloat height = rect.size.height;


	if(height ==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                                     (GLsizei)width / (GLsizei)height,
												0.1f,
												100.0f);
	

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}



- (void)drawRect:(NSRect)dirtyRect
{
	//code
	[self drawView];
}


- (void)drawView
{
    int graphI, graphJ;
    GLfloat redX = -4.0f, blueY = 2.0f;

    int  i = 0;
    long int j = 0;
    GLfloat x, y, z;
	//code
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelViewMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 modelViewProjectionMatrix;

    
	//Using program object 
	glUseProgram(gShaderProgramObject);

        

        //Do neccessary Matrix Multilication
        modelViewMatrix = translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

       for (graphI = 0; graphI <= 160; graphI++, redX = redX + 0.05f)
       {
           translationMatrix = vmath::mat4::identity();
           modelViewMatrix = vmath::mat4::identity();
           modelViewProjectionMatrix = vmath::mat4::identity();
           translationMatrix = vmath::translate(redX, 0.0f, -8.0f);
           //Do neccessary Matrix Multilication
           modelViewMatrix = translationMatrix;
           modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

           //Send neccessary matrices to shader in respective to uniforms
           glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

    //Bind with vao of triangle
        glBindVertexArray(vaoGraphRed);

        glDrawArrays(GL_LINES, 0, 2);

        glBindVertexArray(0);
       }
    
        for (graphJ = 0; graphJ <= 80; graphJ++, blueY = blueY - 0.05f)
        {
            translationMatrix = vmath::mat4::identity();
            modelViewMatrix = vmath::mat4::identity();
            modelViewProjectionMatrix = vmath::mat4::identity();
            
            translationMatrix = vmath::translate(-2.0f, blueY, -8.0f);
            //Do neccessary Matrix Multilication
            modelViewMatrix = translationMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

            //Send neccessary matrices to shader in respective to uniforms
            glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
            //Bind with vao of triangle
           glBindVertexArray(vaoGraphBlue);

           glDrawArrays(GL_LINES, 0, 2);

           glBindVertexArray(0);
          }
    
    //Initialize above matrix to identity
    translationMatrix = vmath::mat4::identity();
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();

    //Do neccessary transformation
    translationMatrix = vmath::translate(0.0f, 0.0f, -5.0f);
    

        //Do neccessary Matrix Multilication
        modelViewMatrix = translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
       
    
    

    //Bind with vao of triangle
    glBindVertexArray(vaoTriangle);

    //Bind texture if any

    //Draw function
    //glDrawArrays(GL_TRIANGLES, 0, 3);
    glDrawArrays(GL_LINES, 0, 2);
    glDrawArrays(GL_LINES, 1, 2);
    glDrawArrays(GL_LINES, 2, 2);
    //glDrawArrays(GL_LINE_STRIP, 9, 3);

    //glDrawArrays(GL_LINES, 0, 3);

    //Unbind vao of triangle
    glBindVertexArray(0);

    //*********************************** Rectangle*****************************


    //Bind with vao of rectangle
    glBindVertexArray(vaoRect);

    //Bind texture if any

    //Draw function
    glDrawArrays(GL_LINES, 0, 2);
    glDrawArrays(GL_LINES, 1, 2);
    glDrawArrays(GL_LINES, 2, 2);
    glDrawArrays(GL_LINES, 3, 2);

    //Unbind vao of rectangle
    glBindVertexArray(0);

    //************************** CIRCLE ************************

        glBindVertexArray(vaoCircle);
    glDrawArrays(GL_POINTS, 0, 6280);
    glBindVertexArray(0);
    
	//Unused Program
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);	
}

-(BOOL)acceptsFirstResponder{
	//code
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27: // ESC Key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent{
	//code
	[self setNeedsDisplay:YES]; //repainting
}

-(void)mouseDragged:(NSEvent *)theEvent{
	//code
}
-(void)RightMouseDown:(NSEvent *)theEvent{
	//code
	[self setNeedsDisplay:YES]; //repainting
}

- (void) dealloc{
	//code
	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
    }
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
								const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
								CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}

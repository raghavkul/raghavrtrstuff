//headers
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

#define MESH_HEIGHT 1
#define MESH_WIDTH 1
#define MESH_ROW 10
#define MESH_COLUMN 10

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXTURE0,
};

// C style global function decleration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef,const CVTimeStamp *,const
	CVTimeStamp *, CVOptionFlags, CVOptionFlags * , void *);

//global varibales
FILE *gpFile = NULL;
GLfloat graphVertices[MESH_ROW * MESH_COLUMN * 3];
GLfloat xDiff = (GLfloat)MESH_WIDTH / (GLfloat)MESH_COLUMN;
GLfloat yDiff = (GLfloat)MESH_HEIGHT / (GLfloat)MESH_ROW;

//interface decleration
@interface APPDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

//Entry-point function
 
 int main(int argc, const char * argv[])
 {
 	//code

 	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];

 	NSApp = [NSApplication sharedApplication];

 	[NSApp setDelegate:[[APPDelegate alloc]init]];

 	[NSApp run];

 	[pPool release];

 	return(0);
 } 


 //interface implemention

@implementation APPDelegate
{
@private
	NSWindow *window;
	GLView *view;

}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

	//code log File

	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];

	gpFile = fopen(pszLogFileNameWithPath,"w");
	if(gpFile == NULL)
	{
		printf("can not create log file: \n");
		[self release];
		[NSApp terminate:self];
	}

	fprintf(gpFile, "Program started successfull\n" );


	//code
	//window

	NSRect win_rect;
	win_rect = NSMakeRect(0.0,0.0,800.0,600.0);

	//create simple window

	window=[[NSWindow alloc] initWithContentRect:win_rect 
							 styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable
							 backing:NSBackingStoreBuffered
							 defer:NO];
	[window setTitle:@"macOs Window"];
	[window center];

	view = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:view];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSApplication *)notification{
	//code
	fprintf(gpFile, "program is terminate successfully\n");

	if(gpFile)
	{
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSApplication *)notification{
	//code
	[NSApp terminate:self];
} 

- (void)dealloc{
	//code
	[view release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displayLink;

	GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    GLuint vao_graph_blue;//vertex array object
    GLuint vao_graph_red;
    GLuint vbo_position_graph_blue;//vertex buffer object(position)
    GLuint vbo_position_graph_red;
    GLuint vbo_color_graph_blue;//vertex buffer object(color)
    GLuint vbo_color_graph_red;
	GLuint mvpUniform;

	vmath::mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
	//code
	self = [super initWithFrame:frame];

	if(self)
	{
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			//Must specify the 4.1 core profile to use openGL 4.1
			NSOpenGLPFAOpenGLProfile,
			NSOpenGLProfileVersion4_1Core,	

			//Specify the display ID to associated the GL Context With 
			//(main display for now)

			NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask
			(kCGDirectMainDisplay),
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAAccelerated,
			NSOpenGLPFAColorSize,24,
			NSOpenGLPFADepthSize,24,
			NSOpenGLPFAAlphaSize,8,
			NSOpenGLPFADoubleBuffer,
			0
		};
		
		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]
			initWithAttributes:attrs] autorelease];

		if(pixelFormat==nil)
		{
			fprintf(gpFile, "No valid openGL pixel format is available \n");

			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]
			initWithFormat:pixelFormat shareContext:nil] autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext]; // it automatically release the
		//older context, if present and set newer one
	}
	return(self);
}


-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	//code
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];

	return(kCVReturnSuccess);

}

-(void)prepareOpenGL
{
	//code
	//OpenGL info

    [super prepareOpenGL];
    
	fprintf(gpFile, "OpenGL Version %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version %s \n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt =1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];


		//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 410 " \
		"\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
        "}"
	};

	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	  GLint iProgramLinkStatus = 0;

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}




		//Define fragment shader object

	 iShaderCompileStatus = 0;
	 iInfoLogLength = 0;
	 szInfoLog = NULL;
  
    
    
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{	"#version 410" \
		"\n" \
        "in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
        "}"
	};

	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment shader comiplation error: ");
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
			}
		}
	}



	//Create shader program object
    gShaderProgramObject = glCreateProgram();

    //Attach Vertex Shader to Program Object
    glAttachShader(gShaderProgramObject, gVertexShaderObject);


    //Attach Fragment Shader to Program Object
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    //Prelinking binding of vertex shader

    //----- Position
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    // ---- color
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

    //Link Shader Program
    glLinkProgram(gShaderProgramObject);

    //Error Checking For Program Object

    iInfoLogLength = 0;
    //*szInfoLog = NULL;

    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

        if (iInfoLogLength > 0)
        {
            szInfoLog = (GLchar*)malloc(iInfoLogLength);

            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
                fprintf(gpFile, "Program Object Linking  error: %s", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

	//Postlinking Retriving Uniform locations
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat redLineVertices[] = {
        0.0f,2.0f,0.0f,
        0.0f,-2.0f,0.0f
    };

    const GLfloat redLineColor[] = {
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f
    };
    const GLfloat blueLineVertices[] = {
        -2.0f,0.0f,0.0f,
        6.0f,0.0f,0.0f
    };
    const GLfloat blueLineColor[] = {
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f
    };
    /*
     float graphVertices[40];
    static float diff = 0.05f;

    for (int i = 0; i < 10; i++)
    {
        diff += 0.05f;
        for (int j = 0; j < 4; j++)
        {
            if(j==0)
            {
                graphVertices[(i * 4) + j] = 1.0f;
                fprintf(gpFile, "graph vertices j==0 :%d\n", graphVertices);

            }
            else if (j == 2)
            {
                graphVertices[(i * 4) + j] = -1.0f;
                fprintf(gpFile, "graph vertices j==2:%d\n", graphVertices);

            }
            else {
                graphVertices[(i * 4) + j] = -diff;
                fprintf(gpFile, "graph vertices  #########:%d\n", graphVertices);

            }
            
        }
        fprintf(gpFile, "graph vertices :%d\n", graphVertices);
        
    }*/

    //GLfloat x, y, z;
    //int k = 0;

    ////for (int i = 0; i < MESH_ROW; i++)
    //for (int i = 0; i < 1; i++)
    //{
    ////    for (int j = 0; j < MESH_COLUMN; j++)
    //    for (int j = 0; j < 1; j++)
    //    {
    //        /*x = j * xDiff;
    //        y = i * yDiff;*/

    //        x = i  * 0.05f;
    //        y = j  * 0.05f;
    //        z = 0.0f;

    //        graphVertices[k] = x;
    //        graphVertices[k + 1] = y;
    //        graphVertices[k + 2] = z;
    //        k += 3;
    //    }
    //}
    //
    ////for triangle color
    //const GLfloat graphColor[] = {
    //    1.0f, 0.0f, 0.0f
    //};
    //

    //Create vao
    glGenVertexArrays(1, &vao_graph_red);
    glBindVertexArray(vao_graph_red);

    //########### POSITION ##############
    //Generating Buffer
    glGenBuffers(1, &vbo_position_graph_red);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_graph_red);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(redLineVertices), redLineVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    //########### COLOR ##############
    //Generating Buffer
    glGenBuffers(1, &vbo_color_graph_red);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_graph_red);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(redLineColor), redLineColor, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Unbinding Array

    glBindVertexArray(0);


    //Create vao
    glGenVertexArrays(1, &vao_graph_blue);
    glBindVertexArray(vao_graph_blue);

    //########### POSITION ##############
    //Generating Buffer
    glGenBuffers(1, &vbo_position_graph_blue);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_graph_blue);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(blueLineVertices), blueLineVertices, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);


    //########### COLOR ##############
    //Generating Buffer
    glGenBuffers(1, &vbo_color_graph_blue);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_graph_blue);
    //push data into buffers immediate
    glBufferData(GL_ARRAY_BUFFER, sizeof(blueLineColor), blueLineColor, GL_STATIC_DRAW);
    //how many slots my array is break
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    //Enabling the position
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

    //Unbinding buffer for triangle
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Unbinding Array

    glBindVertexArray(0);

	
	//Depth Lines
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//set bk color

	glClearColor(0.0f,0.0f,0.0f,0.0f);//blue


	perspectiveProjectionMatrix = vmath::mat4::identity();


	CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
	CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat); 
	CVDisplayLinkStart(displayLink);

}

-(void)reshape
{
    [super reshape];
	//code
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width; 
	GLfloat height = rect.size.height;


	if(height ==0)
	{
		height=1;
	}

	glViewport(0,0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
                                                     (GLsizei)width / (GLsizei)height,
												0.1f,
												100.0f);
	

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}



- (void)drawRect:(NSRect)dirtyRect
{
	//code
	[self drawView];
}


- (void)drawView
{
    int i, j;
    GLfloat redX = -4.0f , blueY = 2.0f;
	//code
	[[self openGLContext]makeCurrentContext];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vmath::mat4 modelViewMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 modelViewProjectionMatrix;

    
	//Using program object 
	glUseProgram(gShaderProgramObject);
    
    for (i = 0; i <= 160; i++,redX=redX+0.05f)
    {
        //Initialize above matrix to identity
        translationMatrix = vmath::mat4::identity();
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();

        //Do neccessary transformation
        translationMatrix = vmath::translate(redX, 0.0f, -4.0f);

        //Do neccessary Matrix Multilication
        modelViewMatrix = translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
        glBindVertexArray(vao_graph_red);

        glDrawArrays(GL_LINES, 0, 2);
    
        glBindVertexArray(0);
    }

    
    for (j = 0; j <= 80; j++, blueY = blueY - 0.05f)
    {
        //Initialize above matrix to identity
        translationMatrix = vmath::mat4::identity();
        modelViewMatrix = vmath::mat4::identity();
        modelViewProjectionMatrix = vmath::mat4::identity();

        //Do neccessary transformation
        translationMatrix = vmath::translate(-2.0f, blueY, -4.0f);

        //Do neccessary Matrix Multilication
        modelViewMatrix = translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

        //Bind with vao of triangle
        glBindVertexArray(vao_graph_blue);

        glDrawArrays(GL_LINES, 0, 2);

        glBindVertexArray(0);
    }
	//Unused Program
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);	
}

-(BOOL)acceptsFirstResponder{
	//code
	[[self window]makeFirstResponder:self];
	return(YES);
}


-(void)keyDown:(NSEvent *)theEvent
{
	//code
	int key = (int)[[theEvent characters]characterAtIndex:0];

	switch(key)
	{
		case 27: // ESC Key
			[self release];
			[NSApp terminate:self];
			break;

		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occurs automatically
			break;
		default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent{
	//code
	[self setNeedsDisplay:YES]; //repainting
}

-(void)mouseDragged:(NSEvent *)theEvent{
	//code
}
-(void)RightMouseDown:(NSEvent *)theEvent{
	//code
	[self setNeedsDisplay:YES]; //repainting
}

- (void) dealloc{
	//code
	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;

		
	CVDisplayLinkStop(displayLink);
	CVDisplayLinkRelease(displayLink);
	[super dealloc];
    }
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink,const CVTimeStamp *pNow,
								const CVTimeStamp *pOutputTime,CVOptionFlags flagsIn,
								CVOptionFlags *pFlagsOut,void *pDisplayLinkContext)
{
	CVReturn result = [(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];
	return(result);
}

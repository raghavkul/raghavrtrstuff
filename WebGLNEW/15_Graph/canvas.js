// //global variable

// var canvas = null;
// var gl = null; //webgl context
// var bFullScreen = false;
// var canvas_original_width;
// var canvas_original_height;


// const WebGLMacros = {

// 	AMC_ATTRIBUTE_VERTEX:0,
// 	AMC_ATTRIBUTE_COLOR:1,
// 	AMC_ATTRIBUTE_NORMAL:2,
// 	AMC_ATTRIBUTE_TEXTURE0:3
// };

// var MESH_HEIGHT = 1;
// var  MESH_WIDTH = 1;
// var  MESH_ROW =  10;
// var  MESH_COLUMN = 10;

// var graphVertices = [MESH_ROW * MESH_COLUMN * 3];
// var xDiff = MESH_WIDTH / MESH_COLUMN;
// var yDiff = MESH_HEIGHT / MESH_ROW;

// var vertexShaderObject_raghav;
// var fragmentShaderObject_raghav;
// var shaderProgramObject_raghav;

// var vao_graph_blue_raghav;//vertex array object 
// var vao_graph_red_raghav;
// var vbo_position_graph_blue_raghav;//vertex buffer object(position)
// var vbo_position_graph_red_raghav;
// var vbo_color_graph_blue_raghav;//vertex buffer object(color)
// var vbo_color_graph_red_raghav;

// var mvpUniform_raghav;

// var perspectiveProjectionMatrix;

// var requestAnimationFrame = 
// 	window.requestAnimationFrame ||
// 	window.webkitRequestAnimationFrame ||
// 	window.mozRequestAnimationFrame ||
// 	window.oRequestAnimationFrame ||
// 	window.msRequestAnimationFrame ||
// 	null;


// var cancleAnimationFrame = 
// 	window.cancleAnimationFrame ||
// 	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
// 	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
// 	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
// 	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
// 	null;


// //onload function

// function main()
// {
// 	//get canvas element
// 	canvas = document.getElementById("AMC");
// 	if(!canvas)
// 		console.log("Obtaining canvas failed!" );
// 	else	
// 		console.log("Obtaining canvas succeeded!" );
	
// 	//print canvas width and height on console
// 	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
// 	canvas_original_width = canvas.width;
// 	canvas_original_height = canvas.height;


// 	//register keyborad and mouse event handler
	
// 	window.addEventListener("keydown" , keyDown , false);
// 	window.addEventListener("click" , mouseDown , false);
// 	window.addEventListener("resize" , resize , false);

// 	init();

// 	resize();
// 	draw();
// }



// function toggleFullScreen()
// {
// 	//code
// 	var fullscreen_element = document.fullscreenElement || 
// 							 document.webkitFullscreenElement ||
// 							 document.mozFullScreenElement ||
// 							 document.msFullscreenElement ||
// 							 null;
							 
// 	if(fullscreen_element == null)
// 	{
// 		if(canvas.requestFullscreen)
// 			canvas.requestFullscreen();
// 		else if(canvas.mozRequestFullScreen)
// 			canvas.mozRequestFullScreen();
// 		else if(canvas.webkitRequestFullscreen)
// 			canvas.webkitRequestFullscreen();
// 		else if(canvas.msRequestFullscreen)
// 			canvas.msRequestFullscreen();
// 		bFullScreen = true;
// 	}
// 	else{
// 		if(document.exitFullscreen)
// 			document.exitFullscreen();
// 		else if(document.mozCancelFullScreen)
// 			document.mozCancelFullScreen();
// 		else if(document.webkitExitFullscreen)
// 			document.webkitExitFullscreen();
// 		else if(document.msExitFullscreen)
// 			document.msExitFullscreen();
// 		bFullScreen = false;
// 	}
// }


// function init()
// {
// 	gl = canvas.getContext("webgl2");

// 	if(gl == null)
// 		console.log("Obtaining  context failed!" );
// 	else	
// 		console.log("Obtaining context succeeded!" );

// 	gl.viewportWidth = canvas.width;
// 	gl.viewportHeight = canvas.height;

// 	var vertexShaderSourceObject =
// 	"#version 300 es" +
// 		"\n" +
// 		"in vec4 vPosition;" +
// 		"in vec4 vColor;" +
// 		"uniform mat4 u_mvp_matrix;" +
// 		"out vec4 out_color;" +
// 		"void main(void)" +
// 		"{" +
// 		"gl_Position = u_mvp_matrix * vPosition;" +
// 		"out_color = vColor;" +
// 		"}" ;

// 	vertexShaderObject_raghav = gl.createShader(gl.VERTEX_SHADER);
// 	gl.shaderSource(vertexShaderObject_raghav,vertexShaderSourceObject);
// 	gl.compileShader(vertexShaderObject_raghav);

// 	if(gl.getShaderParameter(vertexShaderObject_raghav,gl.COMPILE_STATUS)==false)
// 	{
// 		var error = gl.getShaderInfoLog(vertexShaderObject_raghav);
// 		if(error.length > 0)
// 		{
// 			alert(error);
// //			uninitialize();
// 		}
// 	}

// 	var fragmentShaderSourceObject =
// 	"#version 300 es" +
// 		"\n" +
// 		"precision highp float;" +
// 		"in vec4 out_color;" +
// 		"out vec4 FragColor;" +
// 		"void main(void)" +
// 		"{" +
// 		"FragColor = out_color;" +
// 		"}";

// 	fragmentShaderObject_raghav = gl.createShader(gl.FRAGMENT_SHADER);
// 	gl.shaderSource(fragmentShaderObject_raghav,fragmentShaderSourceObject);
// 	gl.compileShader(fragmentShaderObject_raghav);

// 	if(gl.getShaderParameter(fragmentShaderObject_raghav,gl.COMPILE_STATUS)==false)
// 	{
// 		var error = gl.getShaderInfoLog(fragmentShaderObject_raghav);
// 		if(error.length > 0)
// 		{
// 			alert(error);
// //			uninitialize();
// 		}
// 	}

// 	shaderProgramObject_raghav = gl.createProgram();
// 	gl.attachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
// 	gl.attachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);

// 	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");
// 	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");

// 	gl.linkProgram(shaderProgramObject_raghav);

// 	if(!gl.getProgramParameter(shaderProgramObject_raghav,gl.LINK_STATUS))
// 	{
// 		var error = gl.getProgramInfoLog(shaderProgramObject_raghav);
// 		if(error.length > 0)
// 		{
// 			alert(error);
// 			uninitialize();
// 		}
// 	}

// 	mvpUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_mvp_matrix");

// 	var  redLineVertices =  new Float32Array([
// 		0.0,2.0,0.0,
// 			0.0,-2.0,0.0
// 	]);

// 	var  redLineColor = new Float32Array([
// 		1.0,0.0,0.0,
// 		1.0,0.0,0.0
// 	]);
// 	var blueLineVertices = new Float32Array([
// 		-4.0,0.0,0.0,
// 		6.0,0.0,0.0
// 	]);
// 	var  blueLineColor = new Float32Array([
// 		0.0,0.0,1.0,
// 		0.0,0.0,1.0
// 	]);


// 	vao_graph_red = gl.createVertexArray();
// 	gl.bindVertexArray(vao_graph_red);


// 	vbo_position_graph_red = gl.createBuffer();
// 	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_graph_red);
// 	gl.bufferData(gl.ARRAY_BUFFER,redLineVertices,gl.STATIC_DRAW);
// 	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
// 	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
// 	gl.bindBuffer(gl.ARRAY_BUFFER,null);

// 	vbo_color_graph_red = gl.createBuffer();
// 	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_graph_red);
// 	gl.bufferData(gl.ARRAY_BUFFER,redLineColor,gl.STATIC_DRAW);
// 	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
// 	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
// 	gl.bindBuffer(gl.ARRAY_BUFFER,null);


// 	gl.bindVertexArray(null);


// 	vao_graph_blue = gl.createVertexArray();
// 	gl.bindVertexArray(vao_graph_blue);


// 	vbo_position_graph_blue = gl.createBuffer();
// 	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_graph_blue);
// 	gl.bufferData(gl.ARRAY_BUFFER,blueLineVertices,gl.STATIC_DRAW);
// 	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
// 	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
// 	gl.bindBuffer(gl.ARRAY_BUFFER,null);

// 	vbo_color_graph_blue = gl.createBuffer();
// 	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_graph_blue);
// 	gl.bufferData(gl.ARRAY_BUFFER,blueLineColor,gl.STATIC_DRAW);
// 	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
// 	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
// 	gl.bindBuffer(gl.ARRAY_BUFFER,null);


// 	gl.bindVertexArray(null);



// 	gl.clearColor(0.0,0.0,0.0,1.0);

// 	perspectiveProjectionMatrix = mat4.create();

// }


// function resize()
// {
// 	if(bFullScreen == true)
// 	{
// 		canvas.width = window.innerWidth;
// 		canvas.height = window.innerHeight;
// 	}
// 	else
// 	{
// 		canvas.width = canvas_original_width;
// 		canvas.height = canvas_original_height;
// 	}

// 	gl.viewport(0,0,canvas.width,canvas.height);


// 	mat4.perspective(perspectiveProjectionMatrix,45.0,
// 		parseFloat(canvas.width)/parseFloat(canvas.height),
// 		0.1,
// 		100.0);
// 	console.log("perspective projection succeefull");
// }


// function draw()
// {
// 	var redX = -4.0 , blueY = 2.0;

// 	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

// 	gl.useProgram(shaderProgramObject_raghav);

// 	var modelViewMatrix = mat4.create();
// 	var modelViewProjectionMatrix = mat4.create();

	
// 	for(var i=0;i<160;i++,redX=redX+0.08)
// 	{
// 		mat4.identity(modelViewMatrix);
// 		mat4.identity(modelViewProjectionMatrix);

// 		mat4.translate(modelViewMatrix,modelViewMatrix,[redX,0.0,-3.0]);

// 		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
// 		gl.uniformMatrix4fv(mvpUniform_raghav,false,modelViewProjectionMatrix);

// 		gl.bindVertexArray(vao_graph_red);
// 		gl.drawArrays(gl.LINES,0,2);

// 		gl.bindVertexArray(null);

// 	}
	
	
// 	for(var j=0;j<80;j++,blueY=blueY-0.08)
// 	{
// 		mat4.identity(modelViewMatrix);
// 		mat4.identity(modelViewProjectionMatrix);

// 		mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,blueY,-3.0]);

// 		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
// 		gl.uniformMatrix4fv(mvpUniform_raghav,false,modelViewProjectionMatrix);

// 		gl.bindVertexArray(vao_graph_blue);
// 		gl.drawArrays(gl.LINES,0,2);

// 		gl.bindVertexArray(null);

// 	}

	
// 	gl.useProgram(null);

// 	requestAnimationFrame(draw,canvas);

// }

// function uninitialize()
// {
// 	if(vao_graph_blue)
// 	{
// 		gl.deleteVertexArray(vao_graph_blue);
// 		vao_graph_blue = null;
// 	}

// 	if(vao_graph_red)
// 	{
// 		gl.deleteVertexArray(vao_graph_red);
// 		vao_graph_red = null;
// 	}


// 	if(vbo_position_graph_blue)
// 	{
// 		gl.deleteBuffer(vbo_position_graph_blue);
// 		vbo_position_graph_blue = null;	
// 	}

// 	if(vbo_position_graph_red)
// 	{
// 		gl.deleteBuffer(vbo_position_graph_red);
// 		vbo_position_graph_red = null;	
// 	}

// 	if(vbo_color_graph_blue)
// 	{
// 		gl.deleteBuffer(vbo_color_graph_blue);
// 		vbo_color_graph_blue = null;	
// 	}

// 	if(vbo_color_graph_red)
// 	{
// 		gl.deleteBuffer(vbo_color_graph_red);
// 		vbo_color_graph_red = null;	
// 	}
// 	if(shaderProgramObject_raghav)
// 	{
// 		if(fragmentShaderObject_raghav)
// 		{
// 			gl.detachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);
// 			gl.deleteShader(fragmentShaderObject_raghav);
// 			fragmentShaderObject_raghav = null;
// 		}
// 		if(vertexShaderObject_raghav)
// 		{
// 			gl.detachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
// 			gl.deleteShader(vertexShaderObject_raghav);
// 			vertexShaderObject_raghav = null;
// 		}

// 		gl.deleteProgram(shaderProgramObject_raghav);
// 		shaderProgramObject_raghav = null;
// 	}
// }

// function keyDown(event)
// {
// 	//code
// 	switch(event.keyCode)
// 	{
// 		case 27:
// 		//	uninitialize();
// 			window.close();
// 			break;
// 		case 70:
// 		 toggleFullScreen();
// 		 break;
// 	}
// }

// function mouseDown()
// {
// 	//code
// }



//Global Vars!.
var canvas = null;
var gl = null;
var bFullScreen = null;
var canvasOriginalWidth = null;
var canvasOriginalHeight =  null;

const WebGLMacros = {
    AMC_ATTRIBUTE_POSITION:0,
    AMC_ATTRIBUTE_COLOR:1,
    AMC_ATTRIBUTE_NORMAL:2,
    AMC_ATTRIBUTE_TEXCOORD0:3
}

var vertexShaderObj;
var fragmentShaderObj;
var shaderProgObj;

var vaoTriangle, vboTrianglePos, vboTriangleCol;
var vaoCircle, vboCirclePos, vboCircleCol;

var vaoAxes, vboAxesPos, vboAxesCol;
var vaoHorUp, vboHorUpPos, vaoHorDwn, vboHorDwnPos;
var vaoRt, vboRtPos, vaoLft, vboLftPos;
var vaoRect, vboRectPos, vboRectCol;
var vaoOutCir, vboOutCirPos, vboOutCirCol;

var mvpUniform;
var perspectiveProjMat;

//Request Animation Frame!
var requestAnimationFrame = 
    window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;

const main = function() {
    //Acquire canvas!
    canvas = document.getElementById("AMC");
    if(!canvas){
        console.log("Failed To Acquire Canvas Element!..");
    }
    else {
        console.log("Canvas Element Acquired Successfully!..");
    }

    canvasOriginalWidth = canvas.width;
    canvasOriginalHeight = canvas.height;

    //Add Event Listener!.
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseEvent, false);
    window.addEventListener("resize", resize, false);

    //Init.
    initialize();

    resize();
    display();
}

const toggleFullScreen = function(){
    //code/
    var fullScreenEelemet = document.fullscreenElement || document.webkitFullscrenElement ||
                            document.mozFullScreenElement || document.msFullscreenElement || null;

    if(fullScreenEelemet == null) {
        if(canvas.requestFullscreen){
            canvas.requestFullscreen();
        }
        else if(canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        }
        else if(canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        }
        else if(canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }

        bFullScreen = true;
    }
    else {
        if(document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if(document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        else if(document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullScreen = false;
    }
}

const initialize = function() {

    var fx1, fx2, fx3, fy1, fy2, fy3;
    var fArea, fRad, fPer;
    var fdAB, fdBC, fdAC;
    var fxCord, fyCord;

    //Code!.
    gl = canvas.getContext("webgl2");
    if(gl == null){
        console.log("Failed to get WebGL2 Context!");
    }
    else {
        console.log("Successfully Acquired the WebGL2 Context!!");
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //Vertex Shader; 
    var vertexShaderSource = 
            "#version 300 es" +     //OpenGL ES 300 vaprun 2.0 WbeGL SO!
            "\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "out vec4 out_vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void) {" +
            "   gl_Position = u_mvp_matrix * vPosition;" +
            "   gl_PointSize = 1.0;" +
            "   out_vColor = vColor;" +
            "}";
    
    vertexShaderObj = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObj, vertexShaderSource);
    gl.compileShader(vertexShaderObj);
    if(gl.getShaderParameter(vertexShaderObj, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObj);
        if(error.lenght > 0){
            alert(error);
            uninitialize();
        }
    }
    else {
        console.log("Vertex Shader Compiled Successfully!");
    }

    var fragmentShaderSource = 
            "#version 300 es" +
            "\n" +
            "precision highp float;" +
            "out vec4 FragColor;" +
            "in vec4 out_vColor;" +
            "void main(void) {" +
            "   FragColor = out_vColor;" +
            "}";

    fragmentShaderObj = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObj, fragmentShaderSource);
    gl.compileShader(fragmentShaderObj);
    if(gl.getShaderParameter(fragmentShaderObj, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObj);
        if(error.lenght > 0){
            alert(error);
            uninitialize();
        }
    }
    else {
        console.log("Fragment Shader Compiled Successfully!");
    }

    //Shader Prog!.
    shaderProgObj = gl.createProgram();
    gl.attachShader(shaderProgObj, vertexShaderObj);
    gl.attachShader(shaderProgObj, fragmentShaderObj);

    //Pre-Link!
    gl.bindAttribLocation(shaderProgObj, WebGLMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
    gl.bindAttribLocation(shaderProgObj, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");

    gl.linkProgram(shaderProgObj);
    if(gl.getProgramParameter(shaderProgObj, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgObj)
        if(error.lenght > 0){
            alert(error);
            uninitialize();
        }
    }
    else {
        console.log("Shader Program Linked Successfully!");
    }

    mvpUniform = gl.getUniformLocation(shaderProgObj, "u_mvp_matrix");

    //VAOs & VBOs Posting
    var axesVert = new Float32Array([5.0, 0.0, 0.0, -5.0, 0.0, 0.0, 0.0, 3.0, 0.0, 0.0, -3.0, 0.0]);
    var axesCol = new Float32Array([0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0]);
        var horLinesVert = [];
        var fSteps = 0.1;
        for(var i = 0; i< 20; i++) {
            for(var j = 0; j < 4; j++) {
                if(j == 0) {
                    horLinesVert[(i*4) + j] = 5.0;
                }
                else if(j == 2) {
                    horLinesVert[(i * 4) + j] = -5.0;
                }
                else {
                    horLinesVert[(i * 4) + j] = fSteps;
                }
            }
            fSteps += 0.1;
        }
        var horLinesVertDwn = [];
        fSteps = 0.1;
        for(var i = 0; i< 20; i++) {
            for(var j = 0; j < 4; j++) {
                if(j == 0) {
                    horLinesVertDwn[(i*4) + j] = 5.0;
                }
                else if(j == 2) {
                    horLinesVertDwn[(i * 4) + j] = -5.0;
                }
                else {
                    horLinesVertDwn[(i * 4) + j] = -fSteps;
                }
            }
            fSteps += 0.1;
        }
        var vertLinesVert = [80];
        fSteps = 0.1;
        for(var i = 0; i< 40; i++) {
            for(var j = 0; j < 4; j++) {
                if(j == 1) {
                    vertLinesVert[(i*4) + j] = 3.0;
                }
                else if(j == 3) {
                    vertLinesVert[(i * 4) + j] = -3.0;
                }
                else {
                    vertLinesVert[(i * 4) + j] = fSteps;
                }
            }
            fSteps += 0.1;
        }
        var vertLinesVertDwn = [80];
        fSteps = 0.1;
        for(var i = 0; i< 40; i++) {
            for(var j = 0; j < 4; j++) {
                if(j == 1) {
                    vertLinesVertDwn[(i*4) + j] = 3.0;
                }
                else if(j == 3) {
                    vertLinesVertDwn[(i * 4) + j] = -3.0;
                }
                else {
                    vertLinesVertDwn[(i * 4) + j] = -fSteps;
                }
            }
            fSteps += 0.1;
        }
            console.log(horLinesVert);
            console.log(horLinesVertDwn);
            console.log(vertLinesVert);
            console.log(vertLinesVertDwn);
            
        //Axes
        vaoAxes = gl.createVertexArray();
        gl.bindVertexArray(vaoAxes);
        vboAxesPos = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vboAxesPos);
        gl.bufferData(gl.ARRAY_BUFFER, axesVert, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        vboAxesCol = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vboAxesCol);
        gl.bufferData(gl.ARRAY_BUFFER, axesCol, gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
        //Hor UP
        vaoHorUp = gl.createVertexArray();
        gl.bindVertexArray(vaoHorUp);
        vboHorUpPos = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vboHorUpPos);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(horLinesVert), gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);
        gl.bindVertexArray(null);
        //Hor Down
        vaoHorDwn = gl.createVertexArray();
        gl.bindVertexArray(vaoHorDwn);
        vboHorDwnPos = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vboHorDwnPos);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(horLinesVertDwn), gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);
        gl.bindVertexArray(null);
        //Left 
        vaoLft = gl.createVertexArray();
        gl.bindVertexArray(vaoLft);
        vboLftPos = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vboLftPos);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertLinesVert), gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);
        gl.bindVertexArray(null);
        //Right
        vaoRt = gl.createVertexArray();
        gl.bindVertexArray(vaoRt);
        vboRtPos = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vboRtPos);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertLinesVertDwn), gl.STATIC_DRAW);
        gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 0.0, 0.0, 1.0);
        gl.bindVertexArray(null);
  
    //Set Color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjMat = mat4.create();

}

const resize = function() {
    if(bFullScreen == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvasOriginalWidth;
        canvas.height = canvasOriginalHeight;
    }

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjMat, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

const display = function() {
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.useProgram(shaderProgObj);

    var modelViewMat = mat4.create();
    var modelViewProjMat = mat4.create();

    mat4.translate(modelViewMat, modelViewMat, [0.0, 0.0, -3.5]);
    mat4.multiply(modelViewProjMat, perspectiveProjMat, modelViewMat);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjMat);

    gl.bindVertexArray(vaoAxes);
    gl.drawArrays(gl.LINES, 0, 2);
    gl.drawArrays(gl.LINES, 2, 2);
    gl.bindVertexArray(null);

    gl.bindVertexArray(vaoHorUp);
    for(i = 0; i < 40; i += 2) {
        gl.drawArrays(gl.LINES, i, 2);
    }
    gl.bindVertexArray(null);

    gl.bindVertexArray(vaoHorDwn);
    for(i = 0; i < 40; i += 2) {
        gl.drawArrays(gl.LINES, i, 2);
    }
    gl.bindVertexArray(null);

    gl.bindVertexArray(vaoLft);
    for(i = 0; i < 80; i += 2) {
        gl.drawArrays(gl.LINES, i, 2);
    }
    gl.bindVertexArray(null);

    gl.bindVertexArray(vaoRt);
    for(i = 0; i < 80; i += 2) {
        gl.drawArrays(gl.LINES, i, 2);
    }
    gl.bindVertexArray(null);

    gl.useProgram(null); 

    requestAnimationFrame(display, canvas);
}


const uninitialize = function() {
    //Code!.

    if(vboHorUpPos){
        gl.deleteBuffer(vboHorUpPos);
        vboHorUpPos = null;
    }
    if(vboHorDwnPos){
        gl.deleteBuffer(vboHorDwnPos);
        vboHorDwnPos = null;
    }
    if(vboLftPos){
        gl.deleteBuffer(vboLftPos);
        vboLftPos = null;
    }
    if(vboRtPos){
        gl.deleteBuffer(vboRtPos);
        vboRtPos = null;
    }
    if(vboAxesPos){
        gl.deleteBuffer(vboAxesPos);
        vboAxesPos = null;
    }
    if(vaoAxes) {
        gl.deleteVertexArray(vaoAxes);
        vaoAxes = null;
    }
    if(vaoHorUp) {
        gl.deleteVertexArray(vaoHorUp);
        vaoHorUp = null;
    }
    if(vaoHorDwn) {
        gl.deleteVertexArray(vaoHorDwn);
        vaoHorDwn = null;
    }
    if(vaoLft) {
        gl.deleteVertexArray(vaoLft);
        vaoLft = null;
    }
    if(vaoRt) {
        gl.deleteVertexArray(vaoRt);
        vaoRt = null;
    }

    if(shaderProgObj) {
        if(fragmentShaderObj) {
            gl.detachShader(shaderProgObj, fragmentShaderObj);
            gl.deleteShader(fragmentShaderObj);
            fragmentShaderObj = null;
        }

        if(vertexShaderObj) {
            gl.detachShader(shaderProgObj, vertexShaderObj);
            gl.deleteShader(vertexShaderObj);
            vertexShaderObj = null;
        }

        gl.deleteProgram(shaderProgObj);
        shaderProgObj = null;
    }
}

const keyDown = function(event) {
    
    switch(event.keyCode) {
        case 27:
            uninitialize();
            window.close();
            break;

        case 70:
            toggleFullScreen();
            break;
    }
}

const mouseEvent = function() {
    //Code!
}


//onload function

function main()
{
	//get canvas element
	var canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	//get 2d context
	var context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2d context failed!" );
	else	
		console.log("Obtaining 2d context succeeded!" );
	
	//fill canvas with black color
	context.fillStyle = "black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	//center the text
	context.textAlign = "center";
	context.textBaseline="middle";
	
	//text
	var str = "Hello World !!!";
	
	//text font
	context.font = "48px sans-serif";
	
	//text color
	context.fillStyle = "white"; 
	
	//display the text in center
	context.fillText(str,canvas.width/2,canvas.height/2);
}
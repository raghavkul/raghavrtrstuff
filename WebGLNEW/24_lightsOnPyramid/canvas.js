//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,

	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var light_ambient_Red=[0.0,0.0,0.0];
var light_diffuse_Red=[1.0,0.0,0.0];
var light_specular_Red=[1.0,0.0,0.0];
var light_position_Red=[-2.0,0.0,0.0,1.0];



var light_ambient_Blue=[0.0,0.0,0.0];
var light_diffuse_Blue=[0.0,0.0,1.0];
var light_specular_Blue=[0.0,0.0,1.0];
var light_position_Blue=[2.0,0.0,0.0,1.0];


var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var material_shininess =50.0;

var sphere=null;

var vertexShaderObject_raghav;
var fragmentShaderObject_raghav;
var shaderProgramObject_raghav;

var vao_pyramid_raghav;//vertex array object for tri
var vbo_position_pyramid_raghav;//vertex buffer object(position) for tri
var vbo_normal_pyramid_raghav;//vertex buffer object(color) for tri



var modelMatrixUniform,viewMatrixUniform,projectionMatrixUniform;


var laUniformRed_raghav,ldUniformRed_raghav,lsUniformRed_raghav,lightpositionuniformRed_raghav;
var laUniformBlue_raghav,ldUniformBlue_raghav,lsUniformBlue_raghav,lightpositionuniformBlue_raghav;
var kaUniform_raghav,kdUniform_raghav,ksUniform_raghav,matrialShininessuniform_raghav;
var LkeyPressedUnifrom_raghav;

var bLKeyPressed=false;

var perspectiveProjectionMatrix;


var angle_sphere =0.0;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	


	// var vertexShaderSourceObject =
	// 	"#version 300 es" +
	// 	"+n" +
	// 	"in vec4 vPosition;" +
	// 	"in vec2 vTex_coord;" +
	// 	"uniform mat4 u_mvp_matrix;" +
	// 	"out vec2 out_texture_coord;" +
	// 	"void main(void)" +
	// 	"{" +
	// 	"gl_Position = u_mvp_matrix * vPosition;" +
	// 	"out_texture_coord = vTex_coord;" +
	// 	"}" ;

	var vertexShaderSourceObject =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"uniform mediump int u_lKeyIsPressed;" +
		"uniform vec3 u_la_red;" +
		"uniform vec3 u_ld_red;" +
		"uniform vec3 u_ls_red;" +
		"uniform vec3 u_la_blue;" +
		"uniform vec3 u_ld_blue;" +
		"uniform vec3 u_ls_blue;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShine;" +
		"uniform vec4 u_lightPosition_red;" +
		"uniform vec4 u_lightPosition_blue;" +
		"out vec3 phong_ads_light1;" +
		"out vec3 phong_ads_light2;" +
		"void main(void)" +
		"{" +
		"if(u_lKeyIsPressed == 1)" +
		"{" +
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
		"vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix ) * vNormal);" +
		"vec3 lightDirection1 = normalize(vec3(u_lightPosition_red - eye_Coordinate));" +
		"vec3 lightDirection2 = normalize(vec3(u_lightPosition_blue - eye_Coordinate));" +
		"float tn_dot_ld1 = max(dot(lightDirection1,tNorm),0.0f);" +
		"float tn_dot_ld2 = max(dot(lightDirection2,tNorm),0.0f);" +
		"vec3 reflectionVector1 = reflect(-lightDirection1 , tNorm);" +
		"vec3 reflectionVector2 = reflect(-lightDirection2 , tNorm);" +
		"vec3 viwerVector = normalize(-eye_Coordinate.xyz);" +
		"vec3 ambiant1 = u_la_red * u_ka;" +
		"vec3 diffuse1 = u_ld_red * u_kd * tn_dot_ld1;" +
		"vec3 specular1 = u_ls_red * u_ks * pow(max(dot(reflectionVector1 , viwerVector),0.0f),u_materialShine);" +
		"vec3 ambiant2 = u_la_blue * u_ka;" +
		"vec3 diffuse2 = u_ld_blue * u_kd * tn_dot_ld2;" +
		"vec3 specular2 = u_ls_blue * u_ks * pow(max(dot(reflectionVector2 , viwerVector),0.0f),u_materialShine);" +
		"phong_ads_light1 = ambiant1 + diffuse1 + specular1;" +
		"phong_ads_light2 = ambiant2 + diffuse2 + specular2;" +
		"}" +
		"else" +
		"{" +
		"phong_ads_light1 = vec3(1.0f,1.0f,1.0f);" +
		"phong_ads_light2 = vec3(1.0f,1.0f,1.0f);" +
		"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}" ;


	// var vertexShaderSourceObject =
	// 	"#version 300 es" +
	// 	"\n" +
	// 		"in vec4 vPosition;" +
	// 		"in vec3 vNormal;" +
	// 		"in vec4 vTexCoord0;" +

	// 		"out float Diffuse;" +
	// 		"out vec3 Specular;" +
	// 		"out vec2 TexCoord;" +
			
	// 		"uniform vec3 LightPosition;" +

	// 		"uniform mat4 mvMatrix;" +
	// 		"uniform mat4 mvpMatrix;" +
	// 		"mat3 NormalMatrix;" +
	// 		"void main(void)" +
	// 		"{" +
	// 				"vec3 ecPosition = vec3(mvMatrix)  * vec3(vPosition);" +
	// 				"NormalMatrix = mat3(transpose(inverse(mvMatrix)));" +
	// 				"vec3 tnorm = normalize(NormalMatrix * vNormal);" +
	// 				"vec3 lightVec = normalize(LightPosition - ecPosition);" +
	// 				"vec3 reflectVec = reflect(-lightVec,tnorm);" +
	// 				"vec3 viewVec = normalize(-ecPosition);" +
	// 				"float spec = clamp(dot(reflectVec,viewVec),0.0,1.0);" +
	// 				"spec = pow(spec,8.0);" +
	// 				"Specular = spec * vec3(1.0,0.941,0.898) *0.3;" +
	// 				"Diffuse = max(dot(lightVec,tnorm),0.0);" +
	// 				"TexCoord = vTexCoord0.st;" +
	// 				"gl_Position = mvpMatrix * vPosition;" +
	// 		"}";

	vertexShaderObject_raghav = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_raghav,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject_raghav);

	if(gl.getShaderParameter(vertexShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_raghav);
		if(error.length > 0)
		{
			alert("vertexShader compile error : " + error);

		}
	}

//"vec4 tex = texture(EarthCloud,out_texture_coord);" +
	
	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;\n" +
		"in vec3 phong_ads_light1;" +
		"in vec3 phong_ads_light2;" +
		"out vec4 FragColor;\n" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"void main(void)" +
		"{" +
		"	if(u_lKeyIsPressed == 1)" +
		"	{" +
		"		FragColor = vec4(phong_ads_light1 + phong_ads_light2,1.0f);\n" +
		"	}" +
		"	else" +
		"	{" +
		"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);\n" +
		"	}" +
		"}";

	// var fragmentShaderSourceObject =
	// "#version 300 es" +
	// 	"\n" +
	// 	"precision highp float;" +
	// 	"uniform highp sampler2D EarthNight;" +
	// 	"uniform highp sampler2D EarthDay;" +
	// 	"uniform highp sampler2D EarthCloud;" +

	// 	"in float Diffuse;" +
	// 	"in vec3 Specular;" +
	// 	"in vec2 TexCoord;" +

	// 	"out vec4 FragColor;" +

	// 	"void main()" +
	// 	"{" +
	// 				"vec2 clouds = texture(EarthCloud,TexCoord).rg;" +
	// 				"vec3 daytime = (texture(EarthDay,TexCoord).rgb * Diffuse + Specular * clouds.g) * (1.0 - clouds.r) + clouds.r * Diffuse;" +
	// 				"vec3 nighttime = texture(EarthNight , TexCoord).rgb * (1.0 - clouds.r) * 2.0;" +
	// 				"vec3 color = daytime;" +
	// 				"if(Diffuse < 0.1)" +
	// 				"{" +
	// 					"color = mix(nighttime,daytime,(Diffuse + 0.1) * 5.0);" +
	// 				"}" +
	// 				"FragColor = vec4(color,1.0);" +
	// 			//	"FragColor = texture(EarthNight,TexCoord);" +
	// 	"}";


	fragmentShaderObject_raghav = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_raghav,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject_raghav);

	if(gl.getShaderParameter(fragmentShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_raghav);
		if(error.length > 0)
		{
			alert("fragmentShader compile error : " + error);

		}
	}

	shaderProgramObject_raghav = gl.createProgram();
	gl.attachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
	gl.attachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);

	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

//	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTex_coord");

//.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord0");

	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(shaderProgramObject_raghav);

	if(!gl.getProgramParameter(shaderProgramObject_raghav,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_raghav);
		if(error.length > 0)
		{
			alert(" link error : " + error);
			
		}
	}


	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject_raghav,"u_m_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject_raghav,"u_v_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject_raghav,"u_p_matrix");
	LkeyPressedUnifrom_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lKeyIsPressed");
	

	laUniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_la_red");
	ldUniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ld_red");
	lsUniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ls_red");
	lightpositionuniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lightPosition_red");


	laUniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_la_blue");
	ldUniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ld_blue");
	lsUniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ls_blue");
	lightpositionuniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lightPosition_blue");

	//lightPosition = gl.getUniformLocation(shaderProgramObject_raghav,"LightPosition");
	kaUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ka");
	kdUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_kd");
	ksUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ks");
	matrialShininessuniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_materialShine");


	var pyramidVertices = new Float32Array([
			0.0,1.0,0.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,

			0.0,1.0,0.0,
			1.0,-1.0,1.0,
			1.0,-1.0,-1.0,
			
			0.0,1.0,0.0,
			1.0,-1.0,-1.0,
			-1.0,-1.0,-1.0,

			0.0,1.0,0.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0
	]);
	var pyramidNormal = new Float32Array([
			0.0, 0.447214, 0.89442,
		0.0, 0.447214, 0.89442,
		0.0, 0.447214, 0.89442,
		0.894427, 0.447214, 0.0,
		0.894427, 0.447214, 0.0,
		0.894427, 0.447214, 0.0,
		0.0, 0.447214, -0.89442,
		0.0, 0.447214, -0.89442,
		0.0, 0.447214, -0.89442,
		-0.894427, 0.447214, 0.0,
		-0.894427, 0.447214, 0.0,
		-0.894427, 0.447214, 0.0
	]);

	// //load texture


	vao_pyramid_raghav=gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid_raghav);


	vbo_position_pyramid_raghav = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_pyramid_raghav);
	gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);


	vbo_normal_pyramid_raghav = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_normal_pyramid_raghav);
	gl.bufferData(gl.ARRAY_BUFFER,pyramidNormal,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	//gl.enable(gl.TEXTURE_2D);

	gl.clearColor(0.0,0.0,0.0,1.0);


	perspectiveProjectionMatrix = mat4.create();

	
}


function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject_raghav);

	if(bLKeyPressed == true)
	{
		gl.uniform1i(LkeyPressedUnifrom_raghav,1);

		gl.uniform3fv(laUniformRed_raghav,light_ambient_Red);
		gl.uniform3fv(ldUniformRed_raghav,light_diffuse_Red);
		gl.uniform3fv(lsUniformRed_raghav,light_specular_Red);
		gl.uniform4fv(lightpositionuniformRed_raghav,light_position_Red);

		gl.uniform3fv(laUniformBlue_raghav,light_ambient_Blue);
		gl.uniform3fv(ldUniformBlue_raghav,light_diffuse_Blue);
		gl.uniform3fv(lsUniformBlue_raghav,light_specular_Blue);
		gl.uniform4fv(lightpositionuniformBlue_raghav,light_position_Blue);


		gl.uniform3fv(kaUniform_raghav,material_ambient);
		gl.uniform3fv(kdUniform_raghav,material_diffuse);
		gl.uniform3fv(ksUniform_raghav,material_specular);
		gl.uniform1f(matrialShininessuniform_raghav,material_shininess);		
	}
	else{
		gl.uniform1i(LkeyPressedUnifrom_raghav,0);
	}

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var projectionmatrix = mat4.create();

	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-4.0]);
	
	  mat4.rotateY(modelMatrix,modelMatrix,degToRad(angle_sphere));
	
	mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);	 		
	gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform,false,projectionmatrix);

	gl.bindVertexArray(vao_pyramid_raghav);
	
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.drawArrays(gl.TRIANGLES,3,3);
	gl.drawArrays(gl.TRIANGLES,6,3);
	gl.drawArrays(gl.TRIANGLES,9,3);


	gl.bindVertexArray(null);

	//update();
	requestAnimationFrame(draw,canvas);
	angle_sphere = angle_sphere + 0.4;

}

function update()
{
	

	// angle_cube = angle_cube + 0.2;
	// 	if(angle_cube>=360.0)
	// 	{
	// 		angle_cube = angle_cube -360.0;
	// 	}
}

function uninitialize()
{
	
	
	// if(cube_texture)
	// {
	// 	gl.deleteTexture(cube_texture);
	// 	cube_texture =0;
	// }
	
	// if(vao_cube)
	// {
	// 	gl.deleteBuffer(vao_cube);
	// 	vao_cube = null;	
	// }
	// if(vbo_position_cube)
	// {
	// 	gl.deleteBuffer(vbo_position_cube);
	// 	vbo_position_cube = null;	
	// }
	// if(vbo_texture_cube)
	// {
	// 	gl.deleteBuffer(vbo_texture_cube);
	// 	vbo_texture_cube = null;	
	// }

	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}

	if(shaderProgramObject_raghav)
	{
		if(fragmentShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);
			gl.deleteShader(fragmentShaderObject_raghav);
			fragmentShaderObject_raghav = null;
		}
		if(vertexShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
			gl.deleteShader(vertexShaderObject_raghav);
			vertexShaderObject_raghav = null;
		}

		gl.deleteProgram(shaderProgramObject_raghav);
		shaderProgramObject_raghav = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
			 break;
		case 76:
		if(bLKeyPressed == false)
		{
			bLKeyPressed =true;
		}else{
			bLKeyPressed =false;
		}

	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
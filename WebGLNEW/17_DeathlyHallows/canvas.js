//Global Vars!.
var canvas = null;
var gl = null;
var bFullScreen = null;
var canvasOriginalWidth = null;
var canvasOriginalHeight =  null;

const WebGLMacros = {
    AMC_ATTRIBUTE_POSITION:0,
    AMC_ATTRIBUTE_COLOR:1,
    AMC_ATTRIBUTE_NORMAL:2,
    AMC_ATTRIBUTE_TEXCOORD0:3
}

var vertexShaderObj;
var fragmentShaderObj;
var shaderProgObj;

var vaoTriangle_raghav, vboTrianglePos_raghav, vboTriangleCol_raghav;
var vaoCircle_raghav, vboCirclePos_raghav, vboCircleCol_raghav;
var vaoline_raghav,vbolinePos_raghav,vbolineColor_raghav;

var triTrans = -4.0;
var cirTrans = 4.0;
var lineTrans = 2.0;
var triAng = 0.0,cirAng = 0.0;
var angale=0.0;

var circleVertices = [1100];
var mvpUniform;
var perspectiveProjMat;

//Request Animation Frame!
var requestAnimationFrame = 
    window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;

const main = function() {
    //Acquire canvas!
    canvas = document.getElementById("AMC");
    if(!canvas){
        console.log("Failed To Acquire Canvas Element!..");
    }
    else {
        console.log("Canvas Element Acquired Successfully!..");
    }

    canvasOriginalWidth = canvas.width;
    canvasOriginalHeight = canvas.height;

    //Add Event Listener!.
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseEvent, false);
    window.addEventListener("resize", resize, false);

    //Init.
    initialize();

    resize();
    display();
}

const toggleFullScreen = function(){
    //code/
    var fullScreenEelemet = document.fullscreenElement || document.webkitFullscrenElement ||
                            document.mozFullScreenElement || document.msFullscreenElement || null;

    if(fullScreenEelemet == null) {
        if(canvas.requestFullscreen){
            canvas.requestFullscreen();
        }
        else if(canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        }
        else if(canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        }
        else if(canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }

        bFullScreen = true;
    }
    else {
        if(document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if(document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if(document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
        else if(document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullScreen = false;
    }
}

const initialize = function() {

    var fx1, fx2, fx3, fy1, fy2, fy3;
    var fArea, fRad, fPer;
    var fdAB, fdBC, fdAC;
    var fxCord, fyCord;

    //Code!.
    gl = canvas.getContext("webgl2");
    if(gl == null){
        console.log("Failed to get WebGL2 Context!");
    }
    else {
        console.log("Successfully Acquired the WebGL2 Context!!");
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //Vertex Shader; 
    var vertexShaderSource = 
     "#version 300 es" +     //OpenGL ES 300 vaprun 2.0 WbeGL SO!
            "\n" +
            "in vec4 vPosition;" +
            "in vec4 vColor;" +
            "out vec4 out_vColor;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void) {" +
            "   gl_Position = u_mvp_matrix * vPosition;" +
            "   gl_PointSize = 1.0;" +
            "   out_vColor = vColor;" +
            "}";
    
    vertexShaderObj = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObj, vertexShaderSource);
    gl.compileShader(vertexShaderObj);
    if(gl.getShaderParameter(vertexShaderObj, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObj);
        if(error.lenght > 0){
            alert(error);
            uninitialize();
        }
    }
    else {
        console.log("Vertex Shader Compiled Successfully!");
    }

    var fragmentShaderSource = 
          "#version 300 es" +
            "\n" +
            "precision highp float;" +
            "out vec4 FragColor;" +
            "in vec4 out_vColor;" +
            "void main(void) {" +
            "   FragColor = out_vColor;" +
            "}";

    fragmentShaderObj = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObj, fragmentShaderSource);
    gl.compileShader(fragmentShaderObj);
    if(gl.getShaderParameter(fragmentShaderObj, gl.COMPILE_STATUS) == false){
        var error = gl.getShaderInfoLog(fragmentShaderObj);
        if(error.lenght > 0){
            alert(error);
            uninitialize();
        }
    }
    else {
        console.log("Fragment Shader Compiled Successfully!");
    }

    //Shader Prog!.
    shaderProgObj = gl.createProgram();
    gl.attachShader(shaderProgObj, vertexShaderObj);
    gl.attachShader(shaderProgObj, fragmentShaderObj);

    //Pre-Link!
    gl.bindAttribLocation(shaderProgObj, WebGLMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
    gl.bindAttribLocation(shaderProgObj, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");

    gl.linkProgram(shaderProgObj);
    if(gl.getProgramParameter(shaderProgObj, gl.LINK_STATUS) == false) {
        var error = gl.getProgramInfoLog(shaderProgObj)
        if(error.lenght > 0){
            alert(error);
            uninitialize();
        }
    }
    else {
        console.log("Shader Program Linked Successfully!");
    }

    mvpUniform = gl.getUniformLocation(shaderProgObj, "u_mvp_matrix");

      var lineVertices  = new Float32Array([
		0.0,1.0,0.0,
			0.0,-1.0,0.0
        ]);
       
       var lineColor  = new Float32Array([
		1.0,1.0,0.0,
			1.0,1.0,0.0
        ]);


    vaoline_raghav = gl.createVertexArray();
    gl.bindVertexArray(vaoline_raghav);
    vbolinePos_raghav = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbolinePos_raghav);
    gl.bufferData(gl.ARRAY_BUFFER, lineVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    vbolineColor_raghav = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbolineColor_raghav);
    gl.bufferData(gl.ARRAY_BUFFER, lineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
 
    gl.bindVertexArray(null);
    

     fx1 = 0.0;
    fy1 = 1.0;
    fx2 = -1.0;
    fy2 = -1.0;
    fx3 = 1.0;
    fy3 = -1.0;

    var triangleVertices = new Float32Array([
		fx1, fy1, 0.0,
		fx2, fy2, 0.0,
        fx3, fy3, 0.0,
        fx1, fy1, 0.0
        ]);

    var triangleCol = new Float32Array(
    	[1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0
    	]);
        

    vaoTriangle_raghav = gl.createVertexArray();
    gl.bindVertexArray(vaoTriangle_raghav);
    vboTrianglePos_raghav = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTrianglePos_raghav);
    gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    vboTriangleCol = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboTriangleCol);
    gl.bufferData(gl.ARRAY_BUFFER, triangleCol, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //InCircle cha Rada!.
        //diatances of each side of triangle by distance formula
    fdAB = Math.sqrt(((fx2 - fx1)*(fx2 - fx1)) + ((fy2 - fy1)*(fy2 - fy1)));
    fdBC = Math.sqrt(((fx3 - fx2)*(fx3 - fx2)) + ((fy3 - fy2)*(fy3 - fy2)));
    fdAC = Math.sqrt(((fx3 - fx1)*(fx3 - fx1)) + ((fy3 - fy1)*(fy3 - fy1)));
            
    //perimeter of triangle >> A+B+C and we need half of it for area 

    fPer = ((fdAB + fdAB + fdBC) / 2);

    //are of T = sqrt(P(P-A)(P-B)(P-C))
    fArea = Math.sqrt(fPer*(fPer - fdAB)*(fPer - fdBC)*(fPer - fdAC));

    //Radius of inCircle = AreaOf T/Perimete Of T
    fRad = (fArea / fPer);

    fxCord = (((fdBC*fx1) + (fx2*fdAC) + (fx3*fdAB)) / (fPer * 2));
    fyCord = (((fdBC*fy1) + (fy2*fdAC) + (fy3*fdAB)) / (fPer * 2));

    console.log(fxCord, fyCord);

    var circleVert = [];
    var circleCol = [];
    var i, j;
    var circleSteps = 0.0;
    for(i = 0; i < 6290; i++) {
        for(j = 0; j < 2; j++) {
            if(j==0)
                circleVert[ (i*2) + j] =  fxCord + Math.cos(circleSteps)*fRad;
            else
                circleVert[ (i*2) + j] =  fyCord + Math.sin(circleSteps)*fRad;
        }
        circleSteps += 0.01;
        circleCol[(i*2) + 0] = 1.0;
        circleCol[(i*2) + 1] = 1.0;
        circleCol[(i*2) + 2] = 0.0;
    }
    vaoCircle_raghav = gl.createVertexArray();
    gl.bindVertexArray(vaoCircle_raghav);
    vboCirclePos_raghav = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboCirclePos_raghav);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(circleVert), gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    vboCircleCol = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vboCircleCol);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(circleCol), gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);
    //Set Color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    perspectiveProjMat = mat4.create();

}

const resize = function() {
    if(bFullScreen == true){
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvasOriginalWidth;
        canvas.height = canvasOriginalHeight;
    }

    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjMat, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);
}

const display = function() {

	var  i = 0, j = 0;
	var x=0.0, y=0.0, z=0.0;

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgObj);

    var modelViewMat = mat4.create();
    var modelViewProjMat = mat4.create();

    mat4.translate(modelViewMat, modelViewMat, [triTrans, 0.0, -5.5]);
    mat4.rotateY(modelViewMat,modelViewMat,degToRad(triAng));

    mat4.multiply(modelViewProjMat, perspectiveProjMat, modelViewMat);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjMat);


    gl.bindVertexArray(vaoTriangle_raghav);
    gl.drawArrays(gl.LINES, 0, 2);
    gl.drawArrays(gl.LINES, 1, 2);
    gl.drawArrays(gl.LINES, 2, 2);
    gl.bindVertexArray(null);

    mat4.identity(modelViewMat);
	mat4.identity(modelViewProjMat);

	mat4.translate(modelViewMat, modelViewMat, [0.0, lineTrans, -5.5]);
    mat4.multiply(modelViewProjMat, perspectiveProjMat, modelViewMat);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjMat);


    gl.bindVertexArray(vaoline_raghav);
    gl.drawArrays(gl.LINES, 0, 2);
    gl.bindVertexArray(null);

     

     mat4.identity(modelViewMat);
	mat4.identity(modelViewProjMat);

	mat4.translate(modelViewMat, modelViewMat, [cirTrans, 0.0, -5.5]);
	mat4.rotateY(modelViewMat,modelViewMat,degToRad(cirAng));

    mat4.multiply(modelViewProjMat, perspectiveProjMat, modelViewMat);
    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjMat);

    gl.bindVertexArray(vaoCircle_raghav);
 //    for (var i = 0; i < 360.0; i++)
	// {
	// 	angale = (2.0*3.14159265358979323846*i) / 360;
	// 	x = Math.cos(angale);
	// 	y = Math.sin(angale);
	// 	z = 0.0;

	// 	circleVertices[j] = x;
	// 	circleVertices[j + 1] = y;
	// 	circleVertices[j + 2] = z;
	// 	j = j + 3;


	// }

	// vboCirclePos_raghav = gl.createBuffer();
 //    gl.bindBuffer(gl.ARRAY_BUFFER, vboCirclePos_raghav);
 //    gl.bufferData(gl.ARRAY_BUFFER,circleVertices, gl.DYNAMIC_DRAW);
 //    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.drawArrays(gl.POINTS, 0, 6280);
    gl.bindVertexArray(null);


    gl.useProgram(null); 

    requestAnimationFrame(display, canvas);



	triTrans = triTrans + 0.009;
	if (triTrans >= 0.0)
	{
		triTrans = 0.0;
	}
	cirTrans = cirTrans - 0.009;
	if (cirTrans <= 0.0)
	{
		cirTrans = 0.0;
	}
	lineTrans = lineTrans - 0.0045;
	if (lineTrans <= 0.0)
	{
		lineTrans = 0.0;
	}
	triAng = triAng + 0.2;
	if (triAng >= 360.0)
	{
		triAng = 0.0;
	}
	cirAng = cirAng + 0.2;
	if (cirAng >= 360.0)
	{
		cirAng = 0.0;
	}

}
function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}

const uninitialize = function() {
    //Code!.

    if(vboTrianglePos_raghav){
        gl.deleteBuffer(vboTrianglePos_raghav);
        vboTrianglePos_raghav = null;
    }
    if(vaoTriangle_raghav) {
        gl.deleteVertexArray(vaoTriangle_raghav);
        vaoTriangle_raghav = null;
    }
    if(vboCirclePos_raghav){
        gl.deleteBuffer(vboCirclePos_raghav);
        vboCirclePos_raghav = null;
    }
    if(vaoCircle_raghav) {
        gl.deleteVertexArray(vaoCircle_raghav);
        vaoCircle_raghav = null;
    }
  
    if(shaderProgObj) {
        if(fragmentShaderObj) {
            gl.detachShader(shaderProgObj, fragmentShaderObj);
            gl.deleteShader(fragmentShaderObj);
            fragmentShaderObj = null;
        }

        if(vertexShaderObj) {
            gl.detachShader(shaderProgObj, vertexShaderObj);
            gl.deleteShader(vertexShaderObj);
            vertexShaderObj = null;
        }

        gl.deleteProgram(shaderProgObj);
        shaderProgObj = null;
    }
}

const keyDown = function(event) {
    
    switch(event.keyCode) {
        case 27:
            uninitialize();
            window.close();
            break;

        case 70:
            toggleFullScreen();
            break;
    }
}

const mouseEvent = function() {
    //Code!
}


//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,

	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[10.0,10.0,10.0,1.0];


var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var material_shininess =128.0;

var sphere=null;

var gVertexShaderObjectForPerVertex_raghav;
var gFragmentShaderObjectForPerVertex_raghav;
var gShaderProgramObjectForPerVertex_raghav;

var gVertexShaderObjectForPerFragment_raghav;
var gFragmentShaderObjectForPerFragment_raghav;
var gShaderProgramObjectForPerFragment_raghav;


var modelMatrixUniformPerVertex_raghav,viewMatrixUniformPerVertex_raghav,projectionMatrixUniformPerVertex_raghav;
var modelMatrixUniformPerFragment_raghav,viewMatrixUniformPerFragment_raghav,projectionMatrixUniformPerFragment_raghav;
var laUniformPerVertex_raghav,ldUniformPerVertex_raghav,lsUniformPerVertex_raghav;
var laUniformPerFragment_raghav,ldUniformPerFragment_raghav,lsUniformPerFragment_raghav;
var kaUniformPerVertex_raghav,kdUniformPerVertex_raghav,ksUniformPerVertex_raghav,matrialShininessuniformPerVertex_raghav;
var kaUniformPerFragment_raghav,kdUniformPerFragment_raghav,ksUniformPerFragment_raghav,matrialShininessuniformPerFragment_raghav;
var LkeyPressedUnifromPerVertex_raghav;
var lightpositionuniformPerVertex_raghav;
var LkeyPressedUnifromPerFragment_raghav;
var lightpositionuniformPerFragment_raghav;

var bLKeyPressed=false;
var isPerVertex = false;
var isPerFragment = false; 

var perspectiveProjectionMatrix;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	
	var vertexShaderSourceObjectPerVertex_raghav =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +

		"uniform mediump int u_lKeyIsPressed;" +

		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShine;" +
		"uniform vec4 u_lightPosition;" +
		"out vec3 phong_ads_light;" +
		"void main(void)" +
		"{" +
		"if(u_lKeyIsPressed == 1)" +
		"{" +
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
		"vec3 tNorm = normalize(mat3(u_v_matrix * u_m_matrix ) * vNormal);" +
		"vec3 lightDirection = normalize(vec3(u_lightPosition - eye_Coordinate));" +
		"float tn_dot_ld = max(dot(lightDirection,tNorm),0.0f);" +
		"vec3 reflectionVector = reflect(-lightDirection , tNorm);" +
		"vec3 viwerVector = normalize(-eye_Coordinate.xyz);" +
		"vec3 ambiant = u_la * u_ka;" +
		"vec3 diffuse = u_ld * u_kd * tn_dot_ld;" +
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector , viwerVector),0.0f),u_materialShine);" +
		"phong_ads_light = ambiant + diffuse + specular;" +
		"}" +
		"else" +
		"{" +
		"phong_ads_light = vec3(1.0f,1.0f,1.0f);" +
		"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}" ;

	gVertexShaderObjectForPerVertex_raghav = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObjectForPerVertex_raghav,vertexShaderSourceObjectPerVertex_raghav);
	gl.compileShader(gVertexShaderObjectForPerVertex_raghav);

	if(gl.getShaderParameter(gVertexShaderObjectForPerVertex_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectForPerVertex_raghav);
		if(error.length > 0)
		{
			alert("vertexShader compile error : " + error);

		}
	}

	
	var fragmentShaderSourceObjectPerVertex_raghav =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec3 phong_ads_light;" +
		"out vec4 FragColor;" +
		"uniform mediump int u_lKeyIsPressed;" +
		"void main(void)" +
		"{" +
		"if(u_lKeyIsPressed == 1)" +
		"{" +
		"FragColor = vec4(phong_ads_light,1.0f);" +
		"}" +
		"else" +
		"{" +
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" +
		"}" +
		"}";


	gFragmentShaderObjectForPerVertex_raghav = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObjectForPerVertex_raghav,fragmentShaderSourceObjectPerVertex_raghav);
	gl.compileShader(gFragmentShaderObjectForPerVertex_raghav);

	if(gl.getShaderParameter(gFragmentShaderObjectForPerVertex_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectForPerVertex_raghav);
		if(error.length > 0)
		{
			alert("fragmentShader compile error : " + error);

		}
	}






var vertexShaderSourceObjectPerFragment_raghav =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"uniform mediump int u_lKeyIsPressed;" +
		"uniform vec4 u_lightPosition;" +
		"out vec3 tNorm;" +
		"out vec3 lightDirection;" +
		"out vec3 viwerVector;" +
		"void main(void)" +
		"{" +
		"if(u_lKeyIsPressed == 1)" +
		"{" +
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
		"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" +
		"lightDirection = vec3(u_lightPosition - eye_Coordinate);" +
		"viwerVector = -eye_Coordinate.xyz;" +
		"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}" ;

	gVertexShaderObjectForPerFragment_raghav = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(gVertexShaderObjectForPerFragment_raghav,vertexShaderSourceObjectPerFragment_raghav);
	gl.compileShader(gVertexShaderObjectForPerFragment_raghav);

	if(gl.getShaderParameter(gVertexShaderObjectForPerFragment_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(gVertexShaderObjectForPerFragment_raghav);
		if(error.length > 0)
		{
			alert("vertexShader compile error : " + error);

		}
	}





	var fragmentShaderSourceObjectPerFragment_raghav =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec3 tNorm;" +
		"in vec3 lightDirection;" +
		"in vec3 viwerVector;" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShine;" +
		"uniform vec4 u_lightPosition;" +
		"uniform mediump int u_lKeyIsPressed;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"if(u_lKeyIsPressed == 1)" +
		"{" +
		"vec3 normalizeTNorm = normalize(tNorm);" +
		"vec3 normalizeLightDirection = normalize(lightDirection);" +
		"vec3 normalizeViwerVector = normalize(viwerVector);" +
		"float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" +
		"vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" +
		"vec3 ambiant = vec3(u_la * u_ka);" +
		"vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" +
		"vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" +
		"vec3 phong_ads_light = ambiant + diffuse + specular;" +
		"FragColor = vec4(phong_ads_light,1.0f);" +
		"}" +
		"else" +
		"{" +
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" +
		"}" +
		"}";


	gFragmentShaderObjectForPerFragment_raghav = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(gFragmentShaderObjectForPerFragment_raghav,fragmentShaderSourceObjectPerFragment_raghav);
	gl.compileShader(gFragmentShaderObjectForPerFragment_raghav);

	if(gl.getShaderParameter(gFragmentShaderObjectForPerFragment_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(gFragmentShaderObjectForPerFragment_raghav);
		if(error.length > 0)
		{
			alert("fragmentShader compile error : " + error);

		}
	}



	createProgramPerVertex();

	createProgramPerFragment();

	modelMatrixUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_m_matrix");
	viewMatrixUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_v_matrix");
	projectionMatrixUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_p_matrix");
	LkeyPressedUnifromPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_lKeyIsPressed");
	

	laUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_la");
	ldUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_ld");
	lsUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_ls");

	lightpositionuniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_lightPosition");

	kaUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_ka");
	kdUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_kd");
	ksUniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_ks");
	matrialShininessuniformPerVertex_raghav = gl.getUniformLocation(gShaderProgramObjectForPerVertex_raghav,"u_materialShine");



	modelMatrixUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_m_matrix");
	viewMatrixUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_v_matrix");
	projectionMatrixUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_p_matrix");
	LkeyPressedUnifromPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_lKeyIsPressed");
	

	laUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_la");
	ldUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_ld");
	lsUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_ls");

	lightpositionuniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_lightPosition");

	kaUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_ka");
	kdUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_kd");
	ksUniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_ks");
	matrialShininessuniformPerFragment_raghav = gl.getUniformLocation(gShaderProgramObjectForPerFragment_raghav,"u_materialShine");




	//sphere
	sphere=new Mesh();
	makeSphere(sphere,2.0,30,30);

	
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);

	gl.clearColor(0.0,0.0,0.0,1.0);


	perspectiveProjectionMatrix = mat4.create();

	
}

function createProgramPerVertex()
{
	gShaderProgramObjectForPerVertex_raghav = gl.createProgram();
	gl.attachShader(gShaderProgramObjectForPerVertex_raghav,gVertexShaderObjectForPerVertex_raghav);
	gl.attachShader(gShaderProgramObjectForPerVertex_raghav,gFragmentShaderObjectForPerVertex_raghav);

	gl.bindAttribLocation(gShaderProgramObjectForPerVertex_raghav,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(gShaderProgramObjectForPerVertex_raghav,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(gShaderProgramObjectForPerVertex_raghav);

	if(!gl.getProgramParameter(gShaderProgramObjectForPerVertex_raghav,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectForPerVertex_raghav);
		if(error.length > 0)
		{
			alert(" link error : " + error);
			
		}
	}
}


function createProgramPerFragment()
{
	gShaderProgramObjectForPerFragment_raghav = gl.createProgram();
	gl.attachShader(gShaderProgramObjectForPerFragment_raghav,gVertexShaderObjectForPerFragment_raghav);
	gl.attachShader(gShaderProgramObjectForPerFragment_raghav,gFragmentShaderObjectForPerFragment_raghav);

	gl.bindAttribLocation(gShaderProgramObjectForPerFragment_raghav,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(gShaderProgramObjectForPerFragment_raghav,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(gShaderProgramObjectForPerFragment_raghav);

	if(!gl.getProgramParameter(gShaderProgramObjectForPerFragment_raghav,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(gShaderProgramObjectForPerFragment_raghav);
		if(error.length > 0)
		{
			alert(" link error : " + error);
			
		}
	}

}
function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


	if(isPerVertex == true)
	{
			gl.useProgram(gShaderProgramObjectForPerVertex_raghav);

			var modelMatrix = mat4.create();
			var viewMatrix = mat4.create();
			var projectionmatrix = mat4.create();
			mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);

			gl.uniformMatrix4fv(modelMatrixUniformPerVertex_raghav,false,modelMatrix);
	 		gl.uniformMatrix4fv(viewMatrixUniformPerVertex_raghav,false,viewMatrix);
			

			mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);	 		
	 		gl.uniformMatrix4fv(projectionMatrixUniformPerVertex_raghav,false,projectionmatrix);

	 			if(bLKeyPressed == true)
				{
					gl.uniform1i(LkeyPressedUnifromPerVertex_raghav,1);

					gl.uniform3fv(laUniformPerVertex_raghav,light_ambient);
					gl.uniform3fv(ldUniformPerVertex_raghav,light_diffuse);
					gl.uniform3fv(lsUniformPerVertex_raghav,light_specular);
					gl.uniform4fv(lightpositionuniformPerVertex_raghav,light_position);

					gl.uniform3fv(kaUniformPerVertex_raghav,material_ambient);
					gl.uniform3fv(kdUniformPerVertex_raghav,material_diffuse);
					gl.uniform3fv(ksUniformPerVertex_raghav,material_specular);
					gl.uniform1f(matrialShininessuniformPerVertex_raghav,material_shininess);		
				
				}
				else{
					gl.uniform1i(LkeyPressedUnifromPerVertex_raghav,0);
				}

				sphere.draw();
				gl.useProgram(null);
	}


	if(isPerFragment == true)
	{
			gl.useProgram(gShaderProgramObjectForPerFragment_raghav);

			var modelMatrix = mat4.create();
			var viewMatrix = mat4.create();
			var projectionmatrix = mat4.create();
			mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);

			gl.uniformMatrix4fv(modelMatrixUniformPerFragment_raghav,false,modelMatrix);
	 		gl.uniformMatrix4fv(viewMatrixUniformPerFragment_raghav,false,viewMatrix);
			

			mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);	 		
	 		gl.uniformMatrix4fv(projectionMatrixUniformPerFragment_raghav,false,projectionmatrix);

	 			if(bLKeyPressed == true)
				{
					gl.uniform1i(LkeyPressedUnifromPerFragment_raghav,1);

					gl.uniform3fv(laUniformPerFragment_raghav,light_ambient);
					gl.uniform3fv(ldUniformPerFragment_raghav,light_diffuse);
					gl.uniform3fv(lsUniformPerFragment_raghav,light_specular);
					gl.uniform4fv(lightpositionuniformPerFragment_raghav,light_position);

					gl.uniform3fv(kaUniformPerFragment_raghav,material_ambient);
					gl.uniform3fv(kdUniformPerFragment_raghav,material_diffuse);
					gl.uniform3fv(ksUniformPerFragment_raghav,material_specular);
					gl.uniform1f(matrialShininessuniformPerFragment_raghav,material_shininess);		
				}
				else{
					gl.uniform1i(LkeyPressedUnifromPerFragment_raghav,0);
				}

				sphere.draw();
				gl.useProgram(null);

	}
	requestAnimationFrame(draw,canvas);


}

function update()
{
	
	//code
}

function uninitialize()
{
	
	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}

	if(gShaderProgramObjectForPerVertex_raghav)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(gShaderProgramObjectForPerVertex_raghav,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(gShaderProgramObjectForPerVertex_raghav,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(gShaderProgramObjectForPerVertex_raghav);
		gShaderProgramObjectForPerVertex_raghav = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 84:
		 toggleFullScreen();
			 break;
		case 76:
		if(bLKeyPressed == false)
		{
			bLKeyPressed =true;
		}else{
			bLKeyPressed =false;
		}
		break;
	   case 70:
	   console.log("F is pressed:");
	   	if (isPerFragment == false)
			{
				isPerFragment = true;
			}
			else {
				isPerFragment = false;
			}
			break;
		case 86:
		console.log("V is pressed:");
		if (isPerVertex == false)
			{
				isPerVertex = true;
			}
			else {
				isPerVertex = false;
			}
			break;

	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,

	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var vao_cube; 
var vbo_position_cube;
var vbo_normal_cube;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var modelMatrixUniform,viewMatrixUniform,projectionMatrixUniform;
var laUniform,ldUniform,lsUniform,lightpositionuniform;
var kaUniform,kdUniform,ksUniform,matrialShininessuniform;
var LkeyPressedUnifrom;

var bLKeyPressed=false;

var angle_cube=0.0;

var perspectiveProjectionMatrix;



var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	

	var vertexShaderSourceObject =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"uniform mat4 u_model_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int u_LKeyPressed;" +
		"uniform vec3 u_Ld;" +
		"uniform vec3 u_kd;" +
		"uniform vec4 u_light_position;" +
		"out vec3 diffuse_light;" +
		"void main(void)" +
		"{" +
		"if(u_LKeyPressed == 1)" +
		"{" +
			"vec4 eye_coordinates=u_model_view_matrix*vPosition;" +
			"vec3 tnorm=normalize(mat3(u_model_view_matrix)*vNormal);" +
			"vec3 s = normalize(vec3(u_light_position - eye_coordinates));" +
			"diffuse_light = u_Ld*u_kd*max(dot(s,tnorm),0.0);" +
		"}" +
		"gl_Position = u_projection_matrix * u_model_view_matrix* vPosition;" +
		"}" ;


	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert("vertexShader compile error : " + error);

		}
	}

	
	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec3 diffuse_light;" +
		"out vec4 FragColor;" +
		"uniform mediump int u_LKeyPressed;" +
		"void main(void)" +
		"{" +
			"vec4 color;" +
			"if(u_LKeyPressed==1)" +
			"{" +
				"color=vec4(diffuse_light,1.0);" +
			"}" +
			"else" +
			"{" +
				"color=vec4(1.0,1.0,1.0,1.0);" +
			"}" +
			"FragColor = color;" +
		"}";


	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragmentShader compile error : " + error);

		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(" link error : " + error);
			
		}
	}


	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_model_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LkeyPressedUnifrom = gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
	

	ldUniform = gl.getUniformLocation(shaderProgramObject,"u_Ld");
	

	lightpositionuniform = gl.getUniformLocation(shaderProgramObject,"u_light_position");

	kdUniform = gl.getUniformLocation(shaderProgramObject,"u_kd");
	

	var cubeVertices = new Float32Array([
			1.0,1.0,-1.0,
			-1.0,1.0,-1.0,
			-1.0,1.0,1.0,
			1.0,1.0,1.0,

			1.0,-1.0,1.0,
			-1.0,-1.0,1.0,
			-1.0,-1.0,-1.0,
			1.0,-1.0,-1.0,

			1.0,1.0,1.0,
			-1.0,1.0,1.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,

			1.0,-1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,1.0,-1.0,
			1.0,1.0,-1.0,

			-1.0,1.0,1.0,
			-1.0,1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0,

			1.0,1.0,-1.0,
			1.0,1.0,1.0,
			1.0,-1.0,1.0,
			1.0,-1.0,-1.0,
	]);

	var cubeNormals = new Float32Array([
		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0,

		0.0,-1.0,0.0,
		0.0,-1.0,0.0,
		0.0,-1.0,0.0,
		0.0,-1.0,0.0,

		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,

		0.0,0.0,-1.0,
		0.0,0.0,-1.0,
		0.0,0.0,-1.0,
		0.0,0.0,-1.0,

		-1.0,0.0,0.0,
		-1.0,0.0,0.0,
		-1.0,0.0,0.0,
		-1.0,0.0,0.0,

		1.0,0.0,0.0,
		1.0,0.0,0.0,
		1.0,0.0,0.0,
		1.0,0.0,0.0
		]);


	//cube
	vao_cube=gl.createVertexArray();
	gl.bindVertexArray(vao_cube);


	vbo_position_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_cube);
	gl.bufferData(gl.ARRAY_BUFFER,cubeVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);


	vbo_normal_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_normal_cube);
	gl.bufferData(gl.ARRAY_BUFFER,cubeNormals,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);



	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	//gl.enable(gl.TEXTURE_2D);

	gl.clearColor(0.0,0.0,0.0,1.0);


	perspectiveProjectionMatrix = mat4.create();

	
}


function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	if(bLKeyPressed == true)
	{
		gl.uniform1i(LkeyPressedUnifrom,1);

		gl.uniform3f(ldUniform,1.0,1.0,1.0);
		gl.uniform3f(kdUniform,0.5,0.5,0.5);

		var lightPosition = [0.0,0.0,2.0,1.0];
		gl.uniform4fv(lightpositionuniform,lightPosition);
				
	}
	else{
		gl.uniform1i(LkeyPressedUnifrom,0);
	}

	var modelMatrix = mat4.create();



	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);
	
	 mat4.rotateX(modelMatrix,modelMatrix,degToRad(angle_cube));
	 mat4.rotateY(modelMatrix,modelMatrix,degToRad(angle_cube));
	mat4.rotateZ(modelMatrix,modelMatrix,degToRad(angle_cube));

	// mat4.multiply(modelViewMatrix,modelViewMatrix,modelMatrix);
	// mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
	 //gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);

	 gl.bindVertexArray(vao_cube);

	 gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	gl.drawArrays(gl.TRIANGLE_FAN,20,4);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();
	requestAnimationFrame(draw,canvas);
}

function update()
{
	angle_cube =angle_cube +0.2;
	if(angle_cube>360.0)
	{
		angle_cube = 360.0;
	}
	//code
}

function uninitialize()
{


	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
			 break;
		case 76:
		if(bLKeyPressed == false)
		{
			bLKeyPressed =true;
		}else{
			bLKeyPressed =false;
		}

	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
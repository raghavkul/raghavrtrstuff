//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3
};


var vertexShaderObject_raghav;
var fragmentShaderObject_raghav;
var shaderProgramObject_raghav;

var vao_cube_raghav;

var vbo_position_cube_raghav;//vertex buffer object for tri
var vbo_texture_cube_raghav;//vertex buffer object for tri
var mvpUniform;
var u_texture_sampler;

var cube_texture_raghav=0;
var angle_cube =0.0;

var keyPresed=0;

var perspectiveProjectionMatrix;

var CheckImageWidth=64;
var CheckImageHeight=64;
var CheckImage =  [CheckImageWidth *CheckImageHeight * 4];

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();

	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	var vertexShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTex_coord;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec2 out_texture_coord;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_texture_coord = vTex_coord;" +
		"}" ;

	vertexShaderObject_raghav = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_raghav,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject_raghav);

	if(gl.getShaderParameter(vertexShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_raghav);
		if(error.length > 0)
		{
			alert(error);
//			uninitialize();
		}
	}

	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 out_texture_coord;" +
		"uniform highp sampler2D u_texture_sampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = texture(u_texture_sampler,out_texture_coord);" +
		"}";

	fragmentShaderObject_raghav = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_raghav,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject_raghav);

	if(gl.getShaderParameter(fragmentShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_raghav);
		if(error.length > 0)
		{
			alert(error);
//			uninitialize();
		}
	}

	shaderProgramObject_raghav = gl.createProgram();
	gl.attachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
	gl.attachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);

	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTex_coord");

	gl.linkProgram(shaderProgramObject_raghav);

	if(!gl.getProgramParameter(shaderProgramObject_raghav,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_raghav);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}
		makeCheckImage();


		cube_texture_raghav = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D,cube_texture_raghav);
		gl.pixelStorei(gl.UNPACK_ALIGNMENT,1);
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,CheckImageWidth,CheckImageHeight,0,gl.RGBA,gl.UNSIGNED_BYTE,new Uint8Array(CheckImage));
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.REPEAT);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.REPEAT);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D,null);
	


	
	mvpUniform = gl.getUniformLocation(shaderProgramObject_raghav,"u_mvp_matrix");
	u_texture_sampler = gl.getUniformLocation(shaderProgramObject_raghav,"u_texture_sampler");

	var cubetexCoords = new Float32Array([
		0.0,0.0,
		0.0,1.0,
		1.0,1.0,
		1.0,0.0
	]);

	//cube
	vao_cube_raghav=gl.createVertexArray();
	gl.bindVertexArray(vao_cube_raghav);


	vbo_position_cube_raghav = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_cube_raghav);
	gl.bufferData(gl.ARRAY_BUFFER,4*3*4,gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);


	vbo_texture_cube_raghav = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture_cube_raghav);
	gl.bufferData(gl.ARRAY_BUFFER,cubetexCoords,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);

	gl.clearColor(0.0,0.0,0.0,1.0);

	gl.enable(gl.DEPTH_TEST);
	//gl.enable(gl.CULL_FACE);

	perspectiveProjectionMatrix = mat4.create();

}

function makeCheckImage()
{
	var i, j, k;
	for (i = 0; i < CheckImageHeight; i++)
	{
		for (j = 0; j < CheckImageWidth; j++)
		{
			k = ((i & 8) ^ (j & 8)) * 255;

			CheckImage[(i * 64 + j) * 4 + 0] = k;

			CheckImage[(i * 64 + j) * 4 + 1] = k;

			CheckImage[(i * 64 + j) * 4 + 2] = k;

			CheckImage[(i * 64 + j) * 4 + 3] = 0xff;
		}
	}
}
function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{
	var rectanglePosition1 = new Float32Array([
		-2.0,-1.0,0.0,
		-2.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,-1.0,0.0
	]);

	var rectanglePosition2 = new Float32Array([
		1.0, -1.0, 0.0,
		1.0, 1.0, 0.0,
		2.41421, 1.0, -1.41421,
		2.41421, -1.0, -1.41421
	]);

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject_raghav);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	
	mat4.translate(modelViewMatrix,modelViewMatrix,[0.0,0.0,-5.0]);
	
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

	
	//bind texture
	
	gl.bindVertexArray(vao_cube_raghav);



	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_cube_raghav);
	gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(rectanglePosition1),gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	
	gl.bindTexture(gl.TEXTURE_2D,cube_texture_raghav);
	gl.uniform1i(u_texture_sampler,0);

	gl.drawArrays(gl.TRIANGLE_FAN,0,4);

	gl.bindVertexArray(null);



	gl.bindVertexArray(vao_cube_raghav);

	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_cube_raghav);
	gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(rectanglePosition2),gl.DYNAMIC_DRAW);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	gl.bindTexture(gl.TEXTURE_2D,cube_texture_raghav);
	gl.uniform1i(u_texture_sampler,0);

	gl.drawArrays(gl.TRIANGLE_FAN,0,4);

	
	gl.bindVertexArray(null);
	gl.useProgram(null);

	//update();
	requestAnimationFrame(draw,canvas);

}

function update()
{


	// angle_cube = angle_cube + 0.2;
	// 	if(angle_cube>=360.0)
	// 	{
	// 		angle_cube = angle_cube -360.0;
	// 	}
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}


function uninitialize()
{
	if(cube_texture_raghav)
	{
		gl.deleteTexture(cube_texture_raghav);
		cube_texture_raghav =0;
	}
	if(vao_cube_raghav)
	{
		gl.deleteBuffer(vao_cube_raghav);
		vao_cube_raghav = null;	
	}
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;	
	}
	if(vbo_texture)
	{
		gl.deleteBuffer(vbo_texture);
		vbo_texture = null;	
	}

	if(shaderProgramObject_raghav)
	{
		if(fragmentShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);
			gl.deleteShader(fragmentShaderObject_raghav);
			fragmentShaderObject_raghav = null;
		}
		if(vertexShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
			gl.deleteShader(vertexShaderObject_raghav);
			vertexShaderObject_raghav = null;
		}

		gl.deleteProgram(shaderProgramObject_raghav);
		shaderProgramObject_raghav = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
		 break;
	}
}

function mouseDown()
{
	//code
}
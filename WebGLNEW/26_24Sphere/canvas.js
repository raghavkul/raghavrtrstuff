//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,

	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};


var lightAmbiant = [ 0.0,0.0,0.0];
var lightDiffuse = [ 1.0,1.0,1.0];
var lightSpecular = [ 1.0,1.0,1.0];
var lightPosition = [ 0.0,0.0,0.0,1.0];


var sphere=null;

var vertexShaderObject_raghav_raghav;
var fragmentShaderObject_raghav;
var shaderProgramObject_raghav;



var modelMatrixUniform_raghav,viewMatrixUniform_raghav,projectionMatrixUniform_raghav;

var laUniform_raghav,ldUniform_raghav,lsUniform_raghav,lightPositionUniform_raghav;

var kaUniform_raghav,kdUniform_raghav,ksUniform_raghav,matrialShininessuniform_raghav;
var LkeyPressedUnifrom_raghav;

var bLKeyPressed=false;
var keyPresed=0;

var perspectiveProjectionMatrix;

	
var lightAngleX=0.0;
var lightAngleY=0.0;
var lightAngleZ=0.0;
	

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	


	var vertexShaderSourceObject =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"uniform vec4 u_lightPosition;\n" +
		"out vec3 tNorm;" +
		"out vec3 lightDirection;" +
		"out vec3 viwerVector;" +
		"void main(void)\n" +
		"{\n" +
		"	if(u_lKeyIsPressed == 1)\n" +
		"	{\n" +
			"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
			"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" +
			"lightDirection = vec3(u_lightPosition - eye_Coordinate);" +
			
			"viwerVector = vec3(-eye_Coordinate.xyz);" +
			
			"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}";


	
	vertexShaderObject_raghav = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_raghav,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject_raghav);

	if(gl.getShaderParameter(vertexShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_raghav);
		if(error.length > 0)
		{
			alert("vertexShader compile error : " + error);

		}
	}


	
	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;\n" +
		"in vec3 tNorm;" +
		"in vec3 lightDirection;" +
		"in vec3 viwerVector;" +
		"uniform vec3 u_la;" +
		"uniform vec3 u_ld;" +
		"uniform vec3 u_ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform mediump float u_materialShine;" +
		"out vec4 FragColor;\n" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"void main(void)" +
		"{" +
		"	if(u_lKeyIsPressed == 1)" +
		"	{" +
				"vec3 normalizeTNorm = normalize(tNorm);" +
				"vec3 normalizeLightDirection = normalize(lightDirection);" +
				"vec3 normalizeViwerVector = normalize(viwerVector);" +
				"float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" +
				"vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" +
				"vec3 ambiant = vec3(u_la * u_ka);" +
				"vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" +
				"vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 phong_ads_light = ambiant + diffuse + specular;" +
		"		FragColor = vec4(phong_ads_light,1.0f);\n" +
		"	}" +
		"	else" +
		"	{" + 
		"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);\n" +
		"	}" +
		"}";

	fragmentShaderObject_raghav = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_raghav,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject_raghav);

	if(gl.getShaderParameter(fragmentShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_raghav);
		if(error.length > 0)
		{
			alert("fragmentShader compile error : " + error);

		}
	}

	shaderProgramObject_raghav = gl.createProgram();
	gl.attachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
	gl.attachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);

	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");
	
	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	
	gl.linkProgram(shaderProgramObject_raghav);

	if(!gl.getProgramParameter(shaderProgramObject_raghav,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_raghav);
		if(error.length > 0)
		{
			alert(" link error : " + error);
			
		}
	}


	modelMatrixUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_m_matrix");
	viewMatrixUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_v_matrix");
	projectionMatrixUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_p_matrix");
	LkeyPressedUnifrom_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lKeyIsPressed");
	

	laUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_la");
	ldUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ld");
	lsUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ls");

	lightPositionUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lightPosition");

	kaUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ka");
	kdUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_kd");
	ksUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ks");
	matrialShininessuniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_materialShine");


	//sphere
	sphere=new Mesh();
	makeSphere(sphere,0.5,30,30);

	
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL)
	//gl.enable(gl.CULL_FACE);
	//gl.enable(gl.TEXTURE_2D);

	gl.clearColor(0.3,0.3,0.3,1.0);


	perspectiveProjectionMatrix = mat4.create();

	
}


function resize()
{
	i
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	var xWindowCenter = canvas.width/2;
	var yWindowCenter = canvas.height/2;
	var distorasionX = canvas.width/6;
	var distorasionY = canvas.height/8;

	//gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(distorasionX)/parseFloat(distorasionY),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject_raghav);

	
	

	if(bLKeyPressed == true)
	{

		gl.uniform1i(LkeyPressedUnifrom_raghav,1);

		gl.uniform3fv(laUniform_raghav,lightAmbiant);
		gl.uniform3fv(ldUniform_raghav,lightDiffuse);
		gl.uniform3fv(lsUniform_raghav,lightSpecular);
	
		

		if(keyPresed==1)
		{
			lightPosition[1] = Math.cos(lightAngleX) * 100.0;
			lightPosition[2] = Math.sin(lightAngleX) * 100.0;
			gl.uniform4fv(lightPositionUniform_raghav,lightPosition);
		}else if(keyPresed==2)
		{
			lightPosition[0] = Math.cos(lightAngleY)* 100.0;
			lightPosition[2] = Math.sin(lightAngleY) * 100.0;
			gl.uniform4fv(lightPositionUniform_raghav,lightPosition);
		}else if(keyPresed==3)
		{
			lightPosition[0] = Math.cos(lightAngleZ)* 100.0;
			lightPosition[1] = Math.sin(lightAngleZ) * 100.0;
			gl.uniform4fv(lightPositionUniform_raghav,lightPosition);
		}
		
	}
	else{

		gl.uniform1i(LkeyPressedUnifrom_raghav,0);
	}


	draw24Spheres();
  //  sphere.draw();


	update();
	requestAnimationFrame(draw,canvas);
	

}

function draw24Spheres()
{
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var projectionmatrix = mat4.create();

	
	var currentViewportX;
	var currentViewportY;	

	var xWindowCenter = canvas.width/2;
	var yWindowCenter = canvas.height/2;
	var distorasionX = canvas.width/6;
	var distorasionY = canvas.height/8;

	var xTransOffset = distorasionX;
	var yTransOffset = distorasionY;

	var MaterialAmbiant=[0.0215,0.1745,0.0215];
	var MaterialDiffuse=[0.07568,0.61424,0.0215];
	var MaterialSpecular=[0.633,0.727811,0.633];
	var MaterialShininess=[0.6 * 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();


	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.135,0.2225,0.1575];
	var MaterialDiffuse=[0.54,0.89,0.63];
	var MaterialSpecular=[0.316228,0.316228,0.316228];
	var MaterialShininess=[0.1* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.5375,0.05,0.06625];
	var MaterialDiffuse=[0.18275,0.17,0.22525];
	var MaterialSpecular=[0.332741,0.328634,0.346435];
	var MaterialShininess=[0.3* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.25,0.20725,0.20725];
	var MaterialDiffuse=[1.0,0.829, 0.829];
	var MaterialSpecular=[0.296648,0.296648,0.296648];
	var MaterialShininess=[0.088* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.1745,0.011755,0.01175];
	var MaterialDiffuse=[0.61424,0.04136,0.04136];
	var MaterialSpecular=[0.727811,0.626959,0.626959];
	var MaterialShininess=[0.6* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset *-1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.1,0.18725,0.1745];
	var MaterialDiffuse=[0.396,0.74151,0.69102];
	var MaterialSpecular=[0.297254,0.30829,0.306678];
	var MaterialShininess=[0.1* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	/////////////////////////////////////////////////////////////////////////////////


	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.329412,0.223529,0.27451];
	var MaterialDiffuse=[0.780392,0.568627,0.113725];
	var MaterialSpecular=[0.992157,0.941176,0.807843];
	var MaterialShininess=[0.21794872* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.2125,0.1275, 0.054];
	var MaterialDiffuse=[0.714,0.4284,0.18144];
	var MaterialSpecular=[0.393548,0.271906,0.166721];
	var MaterialShininess=[0.2* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.25,0.25,0.25];
	var MaterialDiffuse=[0.4,0.4,0.4];
	var MaterialSpecular=[0.774597,0.774597,0.774597];
	var MaterialShininess=[0.6* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.19125,0.0735,0.02225];
	var MaterialDiffuse=[0.7038,0.27048,0.0828];
	var MaterialSpecular=[0.256777,0.137622,0.086014];
	var MaterialShininess=[0.1* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.24725,0.1995,0.1745];
	var MaterialDiffuse=[0.75164,0.60648, 0.22648];
	var MaterialSpecular=[0.628281,0.555802,0.366065];
	var MaterialShininess=[0.4* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.19225,0.19225,0.19225];
	var MaterialDiffuse=[0.50754,0.50754,0.50754];
	var MaterialSpecular=[0.508273,0.508273,0.508273];
	var MaterialShininess=[0.4* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();


	///////////////////////////////////////////////////////////////////////////////

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.0,0.0];
	var MaterialDiffuse=[0.01,0.01,0.01];
	var MaterialSpecular=[0.50,0.50,0.50];
	var MaterialShininess=[0.25* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.1,0.06];
	var MaterialDiffuse=[0.0,0.50980392,0.50980392];
	var MaterialSpecular=[0.50196078,0.50196078,0.50196078];
	var MaterialShininess=[0.25* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.0,0.0];
	var MaterialDiffuse=[0.1,0.35,0.1];
	var MaterialSpecular=[0.45,0.55,0.45];
	var MaterialShininess=[0.25* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.0,0.0];
	var MaterialDiffuse=[0.5,0.0,0.0];
	var MaterialSpecular=[0.7,0.6,0.6];
	var MaterialShininess=[0.25* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.0,0.0];
	var MaterialDiffuse=[0.55,0.55,0.55];
	var MaterialSpecular=[0.70,0.70,0.70];
	var MaterialShininess=[0.25* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.0,0.0];
	var MaterialDiffuse=[0.5,0.5,0.5];
	var MaterialSpecular=[0.60,0.60,0.60];
	var MaterialShininess=[0.25* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();


	/////////////////////////////////////////////////////////////////////////////////////

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.02,0.02,0.02];
	var MaterialDiffuse=[0.01,0.01,0.01];
	var MaterialSpecular=[0.4,0.4,0.4];
	var MaterialShininess=[0.078125* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.05,0.05];
	var MaterialDiffuse=[0.4,0.5,0.5];
	var MaterialSpecular=[0.04,0.7,0.7];
	var MaterialShininess=[0.078125* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.0,0.05,0.0];
	var MaterialDiffuse=[0.4,0.5,0.4];
	var MaterialSpecular=[0.04,0.7,0.04];
	var MaterialShininess=[0.078125* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.05,0.0,0.0];
	var MaterialDiffuse=[0.5,0.4,0.4];
	var MaterialSpecular=[0.7,0.04,0.04];
	var MaterialShininess=[0.078125* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.05,0.05,0.05];
	var MaterialDiffuse=[0.5,0.5,0.5];
	var MaterialSpecular=[0.7,0.7,0.7];
	var MaterialShininess=[0.078125* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();

	mat4.identity(modelMatrix);
	mat4.identity(viewMatrix);
	mat4.identity(projectionmatrix);

	var MaterialAmbiant=[0.05,0.05,0.05];
	var MaterialDiffuse=[0.5,0.5,0.4];
	var MaterialSpecular=[0.7,0.7,0.04];
	var MaterialShininess=[0.078125* 128.0];


	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-1.8]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);


	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;

	gl.viewport(currentViewportX,currentViewportY,distorasionX,distorasionY);

	gl.uniform3fv(kaUniform_raghav,MaterialAmbiant);
	gl.uniform3fv(kdUniform_raghav,MaterialDiffuse);
	gl.uniform3fv(ksUniform_raghav,MaterialSpecular);
	gl.uniform1fv(matrialShininessuniform_raghav,MaterialShininess);		

	sphere.draw();
}
function update()
{
	lightAngleX = lightAngleX + 0.01;
	if (lightAngleX >= 360.0)
	{
		lightAngleX = 0.0;
	}
	lightAngleY = lightAngleY+0.01;
	if (lightAngleY >= 360.0)
	{
		lightAngleY = 0.0;
	}
	lightAngleZ = lightAngleZ+0.01;
	if (lightAngleZ >= 360.0)
	{
		lightAngleZ = 0.0;
	}

//code
}

function uninitialize()
{
	
	
	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}

	if(shaderProgramObject_raghav)
	{
		if(fragmentShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);
			gl.deleteShader(fragmentShaderObject_raghav);
			fragmentShaderObject_raghav = null;
		}
		if(vertexShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
			gl.deleteShader(vertexShaderObject_raghav);
			vertexShaderObject_raghav = null;
		}

		gl.deleteProgram(shaderProgramObject_raghav);
		shaderProgramObject_raghav = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
			 break;
		case 76:
		if(bLKeyPressed == false)
		{
			bLKeyPressed =true;
		}else{
			bLKeyPressed =false;
		}
		break;

		case 88:
			keyPresed = 1;
			break;
		case 89:
			keyPresed = 2;

			break;
		case 90:
			keyPresed = 3;

			break;

	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
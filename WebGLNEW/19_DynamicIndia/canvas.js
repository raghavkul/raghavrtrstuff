//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;
var ifLI = false;
var ifA = false;
var ifN = false;
var ifRI = false;
var ifD = false;

const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3
};


var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_LeftI;
var vao_N;
var vao_D;
var vao_RightI;
var vao_A;

var vbo_position_LeftI;//vertex buffer object for tri
var vbo_color_LeftI;

var vbo_position_N;//vertex buffer object for tri
var vbo_color_N;

var vbo_position_D;//vertex buffer object for tri
var vbo_color_D;

var vbo_position_RightI;//vertex buffer object for tri
var vbo_color_RightI;

var vbo_position_A;//vertex buffer object for tri
var vbo_color_A;


var mvpUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();

	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	var vertexShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
//			uninitialize();
		}
	}

	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_color;" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
//			uninitialize();
		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

	var leftIVertices =  new Float32Array([
		
			-1.30,0.70,0.0,
		-1.30,-0.70,0.0
		]);

	var  leftIColor = new Float32Array([
		
			1.0, 0.6, 0.2,
		0.07, 0.533, 0.027
		]);

		var nVertices =  new Float32Array([
		
			-1.10,0.70,0.0,
		-1.10,-0.70,0.0,
		-1.10,0.70,0.0,
		-0.60,-0.70,0.0,
		-0.60,0.70,0.0,
		-0.60,-0.70,0.0
		]);

		var  nColor = new Float32Array([
		
			1.0, 0.6, 0.2,
		0.07, 0.533, 0.027,
		1.0, 0.6, 0.2,
		0.07, 0.533, 0.027,
		1.0, 0.6, 0.2,
		0.07, 0.533, 0.027
		]);

		var  dVertices =  new Float32Array([
		
			-0.4,0.70,0.0,
		-0.4,-0.70,0.0,
		-0.50,0.70,0.0,
		0.1,0.70,0.0,
		0.1,0.70,0.0,
		0.1,-0.70,0.0,
		-0.50,-0.70,0.0,
		0.1,-0.70,0.0
		]);


		var  rightIVertices =  new Float32Array([
		
			0.3,0.70,0.0,
		0.3,-0.70,0.0
		]);

		var  rightIColor =  new Float32Array([
		
			1.0, 0.6, 0.2,
		0.07, 0.533, 0.027
		]);

		var aVertices =  new Float32Array([		
			0.8,0.70,0.0,
		0.50,-0.70,0.0,
		0.80,0.70,0.0,
		1.1,-0.70,0.0,
		0.66,0.02,0.0,
		0.94,0.02,0.0,
		0.66,0.0,0.0,
		0.94,0.0,0.0,
		0.66,-0.02,0.0,
		0.94,-0.02,0.0
		]);

		var  aColor =  new Float32Array([
		
			1.0, 0.6, 0.2,
		0.07, 0.533, 0.027,
		1.0, 0.6, 0.2,
		0.07, 0.533, 0.027,
		1.0, 0.6, 0.2,
		1.0, 0.6, 0.2,
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		0.07, 0.533, 0.027,
		0.07, 0.533, 0.027
		]);

	vao_LeftI = gl.createVertexArray();
	gl.bindVertexArray(vao_LeftI);


	vbo_position_LeftI = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_LeftI);
	gl.bufferData(gl.ARRAY_BUFFER,leftIVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_LeftI = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_LeftI);
	gl.bufferData(gl.ARRAY_BUFFER,leftIColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);


	vao_N = gl.createVertexArray();
	gl.bindVertexArray(vao_N);


	vbo_position_N = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_N);
	gl.bufferData(gl.ARRAY_BUFFER,nVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_N = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_N);
	gl.bufferData(gl.ARRAY_BUFFER,nColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);



	vao_D = gl.createVertexArray();
	gl.bindVertexArray(vao_D);


	vbo_position_D = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_D);
	gl.bufferData(gl.ARRAY_BUFFER,dVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_D = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_D);
	gl.bufferData(gl.ARRAY_BUFFER,4*3*4,gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);



	vao_RightI = gl.createVertexArray();
	gl.bindVertexArray(vao_RightI);


	vbo_position_RightI = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_RightI);
	gl.bufferData(gl.ARRAY_BUFFER,rightIVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_RightI = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_RightI);
	gl.bufferData(gl.ARRAY_BUFFER,rightIColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);



	vao_A = gl.createVertexArray();
	gl.bindVertexArray(vao_A);


	vbo_position_A = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_A);
	gl.bufferData(gl.ARRAY_BUFFER,aVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);

	vbo_color_A = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_A);
	gl.bufferData(gl.ARRAY_BUFFER,aColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	gl.bindVertexArray(null);

	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);


	gl.clearColor(0.0,0.0,0.0,1.0);

	perspectiveProjectionMatrix = mat4.create();

}


function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}

//LI
	 var Litx = -6.0, Lity = 0.0, Litz = -5.0;
	
	//A
	//var x = 0.0, y = 0.0, z = 0.0, a = 0.0, b = 0.0, c = 0.0, d = 0.0, e = 0.0, f = 0.0; //For  middle color of A
	var atx = 4.0, aty = 0.0, atz = -5.0, aty1 = 0.01;
	//RI
	var Ritx = 0.0, Rity = -6.0, Ritz = -5.0;

	//D
	var dtx = 0.0, dty = 0.0, dtz = -5.0;
	var i = 0.0, j = 0.0, k = 0.0, l = 0.0, m = 0.0, n = 0.0;
	
	//N
	var ntx = 0.0, nty = 4.0, ntz = -5.0;


function draw()
{
	

	var  dColor = new Float32Array([
		
		i, j, k,
		l, m, n,
		i, j, k,
		i, j, k,
		i, j, k,
		l, m, n,
		l, m, n,
		l, m, n
	]);


	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	if(ifLI == false)
	{

		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.translate(modelViewMatrix,modelViewMatrix,[Litx, Lity, Litz]);
		
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

		gl.bindVertexArray(vao_LeftI);
		gl.drawArrays(gl.LINES,0,2);

		gl.bindVertexArray(null);

		if ((Litx < 0.0) || (Litx == 0.0))
		{
			Litx = Litx + 0.009;
		}
		if (Litx >= 0.0)
		{
			ifA = true;

		}

	}


	if(ifA == true)
	{
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.translate(modelViewMatrix,modelViewMatrix,[atx, aty, atz]);
		
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

		gl.bindVertexArray(vao_A);
		gl.drawArrays(gl.LINES,0,10);

		gl.bindVertexArray(null);

		if ((atx > 0.0) || (atx == 0.0))
		{
			atx = atx - 0.009;

		}
		if (atx <= 0.0)
		{
			//N();
			ifN = true;
		}
	}

	if(ifN==true)
	{
			mat4.identity(modelViewMatrix);
			mat4.identity(modelViewProjectionMatrix);
			mat4.translate(modelViewMatrix,modelViewMatrix,[ntx, nty, ntz]);
			
			mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
			gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

			gl.bindVertexArray(vao_N);
			gl.drawArrays(gl.LINES,0,6);

			gl.bindVertexArray(null);


			if ((nty >= 0.0) || (nty == 0.0))
		{
			nty = nty - 0.02;

		}
		if (nty <= 0.0)
		{
			//rightI();
			ifRI = true;
		}
	}

	if(ifRI == true)
	{
		

		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.translate(modelViewMatrix,modelViewMatrix,[Ritx, Rity, Ritz]);
		
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

		gl.bindVertexArray(vao_RightI);
		gl.drawArrays(gl.LINES,0,2);

		gl.bindVertexArray(null);


		if ((Rity <= 0.0) || (Rity == 0.0))
		{
			Rity = Rity + 0.02;

		}
		if (Rity >= 0.0)
		{
			//D();
			ifD = true;
		}
	}
	
	if(ifD==true)
	{
		mat4.identity(modelViewMatrix);
		mat4.identity(modelViewProjectionMatrix);
		mat4.translate(modelViewMatrix,modelViewMatrix,[dtx, dty, dtz]);
		
		mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
		gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

		gl.bindVertexArray(vao_D);
		gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_D)
		gl.bufferData(gl.ARRAY_BUFFER,dColor,gl.DYNAMIC_DRAW)
		gl.bindBuffer(gl.ARRAY_BUFFER,null);
		gl.drawArrays(gl.LINES,0,8);

		gl.bindVertexArray(null);

		if (i <= 1.0 && j < 0.6 && k < 0.2 && l < 0.07 && m < 0.533 && n < 0.027)
		{
			i = i + 0.001;
			j = j + 0.0006;
			k = k + 0.0002;

			l = l + 0.00007;
			m = m + 0.000533;
			n = n + 0.000027;
		}

		// if (n >= 0.027)
		// {
		// 	movingPlane = true;
		// 	movingtopPlane = true;
		// 	movingbottomPlane = true;
		// }
	}



	

	gl.useProgram(null);

	requestAnimationFrame(draw,canvas);

}

function uninitialize()
{
	if(vao_A)
	{
		gl.deleteVertexArray(vao_A);
		vao_A = null;
	}
	if(vao_D)
	{
		gl.deleteVertexArray(vao_D);
		vao_D = null;
	}
	if(vao_LeftI)
	{
		gl.deleteVertexArray(vao_LeftI);
		vao_LeftI = null;
	}
	if(vao_RightI)
	{
		gl.deleteVertexArray(vao_RightI);
		vao_RightI = null;
	}
	if(vao_N)
	{
		gl.deleteVertexArray(vao_N);
		vao_N = null;
	}

	if(vbo_color_A)
	{
		gl.deleteBuffer(vbo_color_A);
		vbo_color_A = null;	
	}
		if(vbo_color_D)
	{
		gl.deleteBuffer(vbo_color_D);
		vbo_color_D = null;	
	}
		if(vbo_color_LeftI)
	{
		gl.deleteBuffer(vbo_color_LeftI);
		vbo_color_LeftI = null;	
	}
		if(vbo_color_N)
	{
		gl.deleteBuffer(vbo_color_N);
		vbo_color_N = null;	
	}
		if(vbo_color_RightI)
	{
		gl.deleteBuffer(vbo_color_RightI);
		vbo_color_RightI = null;	
	}

		if(vbo_position_A)
	{
		gl.deleteBuffer(vbo_position_A);
		vbo_position_A = null;	
	}
		if(vbo_position_D)
	{
		gl.deleteBuffer(vbo_position_D);
		vbo_position_D = null;	
	}
		if(vbo_position_LeftI)
	{
		gl.deleteBuffer(vbo_position_LeftI);
		vbo_position_LeftI = null;	
	}
		if(vbo_position_N)
	{
		gl.deleteBuffer(vbo_position_N);
		vbo_position_N = null;	
	}
		if(vbo_position_RightI)
	{
		gl.deleteBuffer(vbo_position_RightI);
		vbo_position_RightI = null;	
	}


	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
		 break;
	}
}

function mouseDown()
{
	//code
}
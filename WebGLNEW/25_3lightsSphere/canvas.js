//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,

	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var lightPositionRed=[0.0,0.0,0.0,1.0];
var lightPositionBlue=[0.0,0.0,0.0,1.0];
var lightPositionGreen=[0.0,0.0,0.0,1.0];


var sphere=null;

var vertexShaderObject_raghav_raghav;
var fragmentShaderObject_raghav;
var shaderProgramObject_raghav;



var modelMatrixUniform_raghav,viewMatrixUniform_raghav,projectionMatrixUniform_raghav;

var laUniformRed_raghav,ldUniformRed_raghav,lsUniformRed_raghav,lightPositionUniformRed_raghav;
var laUniformBlue_raghav,ldUniformBlue_raghav,lsUniformBlue_raghav,lightPositionUniformBlue_raghav;
var laUniformGreen_raghav,ldUniformGreen_raghav,lsUniformGreen_raghav,lightPositionUniformGreen_raghav;

var kaUniform_raghav,kdUniform_raghav,ksUniform_raghav,matrialShininessuniform_raghav;
var LkeyPressedUnifrom_raghav;

var bLKeyPressed=false;


var perspectiveProjectionMatrix;

	
var lightAngleRed=0.0;
var lightAngleBlue=0.0;
var lightAngleGreen=0.0;
	

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	


	// var vertexShaderSourceObject =
	// 	"#version 300 es" +
	// 	"+n" +
	// 	"in vec4 vPosition;" +
	// 	"in vec2 vTex_coord;" +
	// 	"uniform mat4 u_mvp_matrix;" +
	// 	"out vec2 out_texture_coord;" +
	// 	"void main(void)" +
	// 	"{" +
	// 	"gl_Position = u_mvp_matrix * vPosition;" +
	// 	"out_texture_coord = vTex_coord;" +
	// 	"}" ;

	var vertexShaderSourceObject =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;\n" +
		"in vec3 vNormal;\n" +
		"uniform mat4 u_m_matrix;" +
		"uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"uniform vec4 u_lightPositionRed;\n" +
		"uniform vec4 u_lightPositionBlue;\n" +
		"uniform vec4 u_lightPositionGreen;\n" +
		"out vec3 tNorm;" +
		"out vec3 lightDirectionRed;" +
		"out vec3 lightDirectionBlue;" +
		"out vec3 lightDirectionGreen;" +
		"out vec3 viwerVector;" +
		"void main(void)\n" +
		"{\n" +
		"	if(u_lKeyIsPressed == 1)\n" +
		"	{\n" +
			"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" +
			"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" +
			"lightDirectionRed = vec3(u_lightPositionRed - eye_Coordinate);" +
			"lightDirectionBlue = vec3(u_lightPositionBlue - eye_Coordinate);" +
			"lightDirectionGreen = vec3(u_lightPositionGreen - eye_Coordinate);" +
			"viwerVector = -eye_Coordinate.xyz;" +
			"}" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
		"}";


	
	vertexShaderObject_raghav = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject_raghav,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject_raghav);

	if(gl.getShaderParameter(vertexShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject_raghav);
		if(error.length > 0)
		{
			alert("vertexShader compile error : " + error);

		}
	}

//"vec4 tex = texture(EarthCloud,out_texture_coord);" +
	
	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;\n" +
		"in vec3 tNorm;" +
		"in vec3 lightDirectionRed;" +
		"in vec3 lightDirectionBlue;" +
		"in vec3 lightDirectionGreen;" +
		"in vec3 viwerVector;" +
		"uniform vec3 u_laRed;" +
		"uniform vec3 u_ldRed;" +
		"uniform vec3 u_lsRed;" +
		"uniform vec3 u_laBlue;" +
		"uniform vec3 u_ldBlue;" +
		"uniform vec3 u_lsBlue;" +
		"uniform vec3 u_laGreen;" +
		"uniform vec3 u_ldGreen;" +
		"uniform vec3 u_lsGreen;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_materialShine;" +
		"out vec4 FragColor;\n" +
		"uniform mediump int u_lKeyIsPressed;\n" +
		"void main(void)" +
		"{" +
		"	if(u_lKeyIsPressed == 1)" +
		"	{" +
				"vec3 normalizeTNorm = normalize(tNorm);" +
				"vec3 normalizeLightDirectionRed = normalize(lightDirectionRed);" +
				"vec3 normalizeLightDirectionBlue = normalize(lightDirectionBlue);" +
				"vec3 normalizeLightDirectionGreen = normalize(lightDirectionGreen);" +
				"vec3 normalizeViwerVector = normalize(viwerVector);" +
				"float tn_dot_ldRed = max(dot(normalizeLightDirectionRed,normalizeTNorm),0.0f);" +
				"float tn_dot_ldBlue = max(dot(normalizeLightDirectionBlue,normalizeTNorm),0.0f);" +
				"float tn_dot_ldGreen = max(dot(normalizeLightDirectionGreen,normalizeTNorm),0.0f);" +
				"vec3 reflectionVectorRed = reflect(-normalizeLightDirectionRed , normalizeTNorm);" +
				"vec3 reflectionVectorBlue = reflect(-normalizeLightDirectionBlue , normalizeTNorm);" +
				"vec3 reflectionVectorGreen = reflect(-normalizeLightDirectionGreen , normalizeTNorm);" +
				"vec3 ambiantRed = vec3(u_laRed * u_ka);" +
				"vec3 diffuseRed = vec3(u_ldRed * u_kd * tn_dot_ldRed);" +
				"vec3 specularRed = vec3(u_lsRed * u_ks * pow(max(dot(reflectionVectorRed , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 ambiantBlue = vec3(u_laBlue * u_ka);" +
				"vec3 diffuseBlue = vec3(u_ldBlue * u_kd * tn_dot_ldBlue);" +
				"vec3 specularBlue = vec3(u_lsBlue * u_ks * pow(max(dot(reflectionVectorBlue , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 ambiantGreen = vec3(u_laGreen * u_ka);" +
				"vec3 diffuseGreen = vec3(u_ldGreen * u_kd * tn_dot_ldGreen);" +
				"vec3 specularGreen = vec3(u_lsGreen * u_ks * pow(max(dot(reflectionVectorGreen , normalizeViwerVector),0.0f),u_materialShine));" +
				"vec3 phong_ads_lightRed = ambiantRed + diffuseRed + specularRed;" +
				"vec3 phong_ads_lightBlue = ambiantBlue + diffuseBlue + specularBlue;" +
				"vec3 phong_ads_lightGreen = ambiantGreen + diffuseGreen + specularGreen;" +
				"FragColor = vec4(phong_ads_lightRed + phong_ads_lightBlue + phong_ads_lightGreen,1.0f);\n" +
		"	}" +
		"	else" +
		"	{" + 
		"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);\n" +
		"	}" +
		"}";

	fragmentShaderObject_raghav = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject_raghav,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject_raghav);

	if(gl.getShaderParameter(fragmentShaderObject_raghav,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject_raghav);
		if(error.length > 0)
		{
			alert("fragmentShader compile error : " + error);

		}
	}

	shaderProgramObject_raghav = gl.createProgram();
	gl.attachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
	gl.attachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);

	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");
	
	gl.bindAttribLocation(shaderProgramObject_raghav,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	
	gl.linkProgram(shaderProgramObject_raghav);

	if(!gl.getProgramParameter(shaderProgramObject_raghav,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject_raghav);
		if(error.length > 0)
		{
			alert(" link error : " + error);
			
		}
	}


	modelMatrixUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_m_matrix");
	viewMatrixUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_v_matrix");
	projectionMatrixUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_p_matrix");
	LkeyPressedUnifrom_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lKeyIsPressed");
	

	laUniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_laRed");
	ldUniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ldRed");
	lsUniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lsRed");

	laUniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_laBlue");
	ldUniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ldBlue");
	lsUniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lsBlue");

	laUniformGreen_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_laGreen");
	ldUniformGreen_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ldGreen");
	lsUniformGreen_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lsGreen");


	lightPositionUniformRed_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lightPositionRed");
	lightPositionUniformBlue_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lightPositionBlue");
	lightPositionUniformGreen_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_lightPositionGreen");

	kaUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ka");
	kdUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_kd");
	ksUniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_ks");
	matrialShininessuniform_raghav = gl.getUniformLocation(shaderProgramObject_raghav,"u_materialShine");


	//sphere
	sphere=new Mesh();
	makeSphere(sphere,2.0,30,30);

	
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL)
	//gl.enable(gl.CULL_FACE);
	//gl.enable(gl.TEXTURE_2D);

	gl.clearColor(0.0,0.0,0.0,1.0);


	perspectiveProjectionMatrix = mat4.create();

	
}


function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject_raghav);

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	var projectionmatrix = mat4.create();

	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);
	
	 mat4.multiply(projectionmatrix,perspectiveProjectionMatrix,projectionmatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform_raghav,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform_raghav,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform_raghav,false,projectionmatrix);
	

	if(bLKeyPressed == true)
	{

		lightPositionRed[1] = Math.cos(lightAngleRed) * 100.0;
		lightPositionRed[2] = Math.sin(lightAngleRed) * 100.0;

		lightPositionBlue[0] = Math.cos(lightAngleBlue)* 100.0;
		lightPositionBlue[2] = Math.sin(lightAngleBlue) * 100.0;
		
		lightPositionGreen[0] = Math.cos(lightAngleGreen)* 100.0;
		lightPositionGreen[1] = Math.sin(lightAngleGreen) * 100.0;


		gl.uniform1i(LkeyPressedUnifrom_raghav,1);

		gl.uniform3f(laUniformRed_raghav,0.0,0.0,0.0);
		gl.uniform3f(ldUniformRed_raghav,1.0,0.0,0.0);
		gl.uniform3f(lsUniformRed_raghav,1.0,0.0,0.0);
		gl.uniform4fv(lightPositionUniformRed_raghav,lightPositionRed);
		//gl.uniform4f(lightPositionUniformRed_raghav,-2.0,0.0,0.0,1.0);



		gl.uniform3f(laUniformBlue_raghav,0.0,0.0,0.0);
		gl.uniform3f(ldUniformBlue_raghav,0.0,0.0,1.0);
		gl.uniform3f(lsUniformBlue_raghav,0.0,0.0,1.0);
		gl.uniform4fv(lightPositionUniformBlue_raghav,lightPositionBlue);
		//gl.uniform4f(lightPositionUniformBlue_raghav,2.0,0.0,0.0,1.0);

		gl.uniform3f(laUniformGreen_raghav,0.0,0.0,0.0);
		gl.uniform3f(ldUniformGreen_raghav,0.0,1.0,0.0);
		gl.uniform3f(lsUniformGreen_raghav,0.0,1.0,0.0);
		gl.uniform4fv(lightPositionUniformGreen_raghav,lightPositionGreen);
		//gl.uniform4f(lightPositionUniformGreen_raghav,0.0,0.0,2.0,1.0);



		gl.uniform3f(kaUniform_raghav,0.0,0.0,0.0);
		gl.uniform3f(kdUniform_raghav,1.0,1.0,1.0);
		gl.uniform3f(ksUniform_raghav,1.0,1.0,1.0);
		gl.uniform1f(matrialShininessuniform_raghav,128.0);		

	}
	else{

		gl.uniform1i(LkeyPressedUnifrom_raghav,0);
	}



    sphere.draw();


	update();
	requestAnimationFrame(draw,canvas);
	

}

function update()
{
	lightAngleRed = lightAngleRed + 0.03;
	if (lightAngleRed >= 360.0)
	{
		lightAngleRed = 0.0;
	}
	lightAngleBlue = lightAngleBlue+0.03;
	if (lightAngleBlue >= 360.0)
	{
		lightAngleBlue = 0.0;
	}
	lightAngleGreen = lightAngleGreen+0.03;
	if (lightAngleGreen >= 360.0)
	{
		lightAngleGreen = 0.0;
	}

//code
}

function uninitialize()
{
	
	
	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}

	if(shaderProgramObject_raghav)
	{
		if(fragmentShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,fragmentShaderObject_raghav);
			gl.deleteShader(fragmentShaderObject_raghav);
			fragmentShaderObject_raghav = null;
		}
		if(vertexShaderObject_raghav)
		{
			gl.detachShader(shaderProgramObject_raghav,vertexShaderObject_raghav);
			gl.deleteShader(vertexShaderObject_raghav);
			vertexShaderObject_raghav = null;
		}

		gl.deleteProgram(shaderProgramObject_raghav);
		shaderProgramObject_raghav = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
			 break;
		case 76:
		if(bLKeyPressed == false)
		{
			bLKeyPressed =true;
		}else{
			bLKeyPressed =false;
		}

	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
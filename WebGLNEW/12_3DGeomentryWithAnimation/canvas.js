//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3
};



var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var vao_triangle;//vertex array object for tri
var vao_rectangle;

var vbo_position_triangle;//vertex buffer object for tri
var vbo_color_triangle;//vertex buffer object for tri

var vbo_position_rectangle;//vertex buffer object for tri
var vbo_color_rectangle;//vertex buffer object for tri

var angletri = 0.0;
var anglerect =0.0;
var mvpUniform;

var perspectiveProjectionMatrix;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	var vertexShaderSourceObject =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_color = vColor;" +
		"}" ;

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);
//			uninitialize();
		}
	}

	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec4 out_color;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = out_color;" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);
//			uninitialize();
		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_COLOR,"vColor");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			uninitialize();
		}
	}

	mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");

	var triangleVertices = new Float32Array([
			0.0,1.0,0.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,

			0.0,1.0,0.0,
			1.0,-1.0,1.0,
			1.0,-1.0,-1.0,
			
			0.0,1.0,0.0,
			1.0,-1.0,-1.0,
			-1.0,-1.0,-1.0,

			0.0,1.0,0.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0
	]);

	var triangleColor = new Float32Array([
		1.0,0.0,0.0,
		0.0,1.0,0.0,
		0.0,0.0,1.0,

		1.0,0.0,0.0,
		0.0,1.0,0.0,
		0.0,0.0,1.0,

		1.0,0.0,0.0,
		0.0,1.0,0.0,
		0.0,0.0,1.0,

		1.0,0.0,0.0,
		0.0,1.0,0.0,
		0.0,0.0,1.0
	]);

	var rectangleVertices = new Float32Array([
		1.0,1.0,-1.0,
			-1.0,1.0,-1.0,
			-1.0,1.0,1.0,
			1.0,1.0,1.0,

			1.0,-1.0,1.0,
			-1.0,-1.0,1.0,
			-1.0,-1.0,-1.0,
			1.0,-1.0,-1.0,

			1.0,1.0,1.0,
			-1.0,1.0,1.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,

			1.0,-1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,1.0,-1.0,
			1.0,1.0,-1.0,

			-1.0,1.0,1.0,
			-1.0,1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0,

			1.0,1.0,-1.0,
			1.0,1.0,1.0,
			1.0,-1.0,1.0,
			1.0,-1.0,-1.0
	]);

	var rectangleColor = new Float32Array([
		1.0,0.0,0.0,
		1.0,0.0,0.0,
		1.0,0.0,0.0,
		1.0,0.0,0.0,

		1.0,1.0,0.0,
		1.0,1.0,0.0,
		1.0,1.0,0.0,
		1.0,1.0,0.0,

		1.0,0.0,1.0,
		1.0,0.0,1.0,
		1.0,0.0,1.0,
		1.0,0.0,1.0,

		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,
		0.0,0.0,1.0,

		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0,
		0.0,1.0,0.0,

		0.0,1.0,1.0,
		0.0,1.0,1.0,
		0.0,1.0,1.0,
		0.0,1.0,1.0
	]);
	
	for(var i =0;i<72;i++)
	{
		if(rectangleVertices[i]<0.0)
		{
			rectangleVertices[i] = rectangleVertices[i]+0.25;
		}else if(rectangleVertices[i]>0.0)
		{
			rectangleVertices[i] = rectangleVertices[i]-0.25;
		}else{
			rectangleVertices[i] = rectangleVertices[i];
		}
	}

	vao_triangle=gl.createVertexArray();
	gl.bindVertexArray(vao_triangle);


	vbo_position_triangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_triangle);
	gl.bufferData(gl.ARRAY_BUFFER,triangleVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);


	vbo_color_triangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_triangle);
	gl.bufferData(gl.ARRAY_BUFFER,triangleColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	vao_rectangle=gl.createVertexArray();
	gl.bindVertexArray(vao_rectangle);


	vbo_position_rectangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_rectangle);
	gl.bufferData(gl.ARRAY_BUFFER,rectangleVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);


	vbo_color_rectangle = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_color_rectangle);
	gl.bufferData(gl.ARRAY_BUFFER,rectangleColor,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	gl.enable(gl.DEPTH_TEST);
	//gl.enable(gl.CULL_FACE);

	gl.clearColor(0.0,0.0,0.0,1.0);

	perspectiveProjectionMatrix = mat4.create();

}


function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();



	mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,0.0,-4.0]);
	
	mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(angletri));
	//mat4.multiply(modelViewMatrix,modelViewMatrix,modelMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

	gl.bindVertexArray(vao_triangle);
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.drawArrays(gl.TRIANGLES,3,3);
	gl.drawArrays(gl.TRIANGLES,6,3);
	gl.drawArrays(gl.TRIANGLES,9,3);


	gl.bindVertexArray(null);


	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);


	mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-4.0]);
	mat4.rotateX(modelViewMatrix,modelViewMatrix,degToRad(anglerect));
	mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(anglerect));
	mat4.rotateZ(modelViewMatrix,modelViewMatrix,degToRad(anglerect));

	
	//mat4.multiply(modelViewMatrix,modelViewMatrix,modelMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

	gl.bindVertexArray(vao_rectangle);
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	gl.drawArrays(gl.TRIANGLE_FAN,20,4);


	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();
	requestAnimationFrame(draw,canvas);

}

function update()
{
		
	angletri = angletri +1.0;
	if(angletri >=360)
	{
		angletri = 0.0;
	}

	anglerect = anglerect +1.0;
	if(anglerect >=360)
	{
		anglerect = 0.0;
	}
	
}

function uninitialize()
{
	

	if(vao_triangle)
	{
		gl.deleteBuffer(vao_triangle);
		vao_triangle = null;	
	}
	
	if(vbo_position_triangle)
	{
		gl.deleteBuffer(vbo_position_triangle);
		vbo_position_triangle = null;	
	}
	if(vbo_color_triangle)
	{
		gl.deleteBuffer(vbo_color_triangle);
		vbo_color_triangle = null;	
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
			 break;

		
	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
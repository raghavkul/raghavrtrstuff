//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3,

	VDG_ATTRIBUTE_VERTEX:0,
	VDG_ATTRIBUTE_COLOR:1,
	VDG_ATTRIBUTE_NORMAL:2,
	VDG_ATTRIBUTE_TEXTURE0:3
};

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[10.0,10.0,10.0,1.0];


var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];
var material_specular=[1.0,1.0,1.0];
var material_shininess =50.0;

var sphere=null;

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


// var vao_cube;
// var vbo_position_cube;//vertex buffer object for tri
// var vbo_texture_cube;//vertex buffer object for tri

// var vao_Rendercube;
// var vbo_position_Rendercube;//vertex buffer object for tri
// var vbo_texture_Rendercube;//vertex buffer object for tri


var modelMatrixUniform,viewMatrixUniform,projectionMatrixUniform;
var laUniform,ldUniform,lsUniform,lightpositionuniform;
var kaUniform,kdUniform,ksUniform,matrialShininessuniform;
var LkeyPressedUnifrom;

var bLKeyPressed=false;
// var defaultFrameBuffer;
// var colorRenderBuffer;
// var depthRenderBuffer;

// var fb;

// var lightPosition,mvMatrix,mvpMatrix;

// // var mvpUniform;
// var EarthCloud;
// var EarthNight;
// var EarthDay;

var perspectiveProjectionMatrix;



var earth_cloud_texture=0;
var earth_day_texture =0;
var earth_night_texture =0;

var angle_sphere =0.0;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;
	


	// var vertexShaderSourceObject =
	// 	"#version 300 es" +
	// 	"+n" +
	// 	"in vec4 vPosition;" +
	// 	"in vec2 vTex_coord;" +
	// 	"uniform mat4 u_mvp_matrix;" +
	// 	"out vec2 out_texture_coord;" +
	// 	"void main(void)" +
	// 	"{" +
	// 	"gl_Position = u_mvp_matrix * vPosition;" +
	// 	"out_texture_coord = vTex_coord;" +
	// 	"}" ;

	var vertexShaderSourceObject =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec3 vNormal;" +
		"in vec2 vTex_coord;" +
		"uniform mat4 u_model_matrix;" +
		"uniform mat4 u_view_matrix;" +
		"uniform mat4 u_projection_matrix;" +
		"uniform mediump int u_LKeyPressed;" +
		"uniform vec4 u_light_position;" +
		"out vec3 tranformed_normals;" +
		"out vec3 light_direction;" +
		"out vec3 viwer_vector;" +
		"void main(void)" +
		"{" +
		"if(u_LKeyPressed == 1)" +
		"{" +
			"vec4 eye_coordinates=u_view_matrix*u_model_matrix*vPosition;" +
			"tranformed_normals=mat3(u_view_matrix*u_model_matrix)*vNormal;" +
			"light_direction = vec3(u_light_position)-eye_coordinates.xyz;" +
			"viwer_vector = -eye_coordinates.xyz;" +


		"}" +
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix* vPosition;" +
		"}" ;


	// var vertexShaderSourceObject =
	// 	"#version 300 es" +
	// 	"\n" +
	// 		"in vec4 vPosition;" +
	// 		"in vec3 vNormal;" +
	// 		"in vec4 vTexCoord0;" +

	// 		"out float Diffuse;" +
	// 		"out vec3 Specular;" +
	// 		"out vec2 TexCoord;" +
			
	// 		"uniform vec3 LightPosition;" +

	// 		"uniform mat4 mvMatrix;" +
	// 		"uniform mat4 mvpMatrix;" +
	// 		"mat3 NormalMatrix;" +
	// 		"void main(void)" +
	// 		"{" +
	// 				"vec3 ecPosition = vec3(mvMatrix)  * vec3(vPosition);" +
	// 				"NormalMatrix = mat3(transpose(inverse(mvMatrix)));" +
	// 				"vec3 tnorm = normalize(NormalMatrix * vNormal);" +
	// 				"vec3 lightVec = normalize(LightPosition - ecPosition);" +
	// 				"vec3 reflectVec = reflect(-lightVec,tnorm);" +
	// 				"vec3 viewVec = normalize(-ecPosition);" +
	// 				"float spec = clamp(dot(reflectVec,viewVec),0.0,1.0);" +
	// 				"spec = pow(spec,8.0);" +
	// 				"Specular = spec * vec3(1.0,0.941,0.898) *0.3;" +
	// 				"Diffuse = max(dot(lightVec,tnorm),0.0);" +
	// 				"TexCoord = vTexCoord0.st;" +
	// 				"gl_Position = mvpMatrix * vPosition;" +
	// 		"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert("vertexShader compile error : " + error);

		}
	}

//"vec4 tex = texture(EarthCloud,out_texture_coord);" +
	
	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec3 tranformed_normals;" +
		"in vec3 light_direction;" +
		"in vec3 viwer_vector;" +
		"uniform vec3 u_La;" +
		"uniform vec3 u_Ld;" +
		"uniform vec3 u_Ls;" +
		"uniform vec3 u_ka;" +
		"uniform vec3 u_kd;" +
		"uniform vec3 u_ks;" +
		"uniform float u_material_shininess;" +
		"uniform int u_LKeyPressed;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"vec3 phong_ads_color;" +
		"if(u_LKeyPressed == 1)" +
		"{" +
			"vec3 normalized_transformed_normals = normalize(tranformed_normals);" +
			"vec3 normalized_light_direction=normalize(light_direction);" +
			"vec3 normalized_viewer_direction=normalize(viwer_vector);" +
			"vec3 ambient = u_La*u_ka;" +
			"float tn_dot_ld = max(dot(normalized_transformed_normals,normalized_light_direction),0.0);" +
			"vec3 diffuse = u_Ld*u_kd * tn_dot_ld;" +
			"vec3 reflction_vector = reflect(-normalized_light_direction,normalized_transformed_normals);" +
			"vec3 specular = u_Ls*u_ks *pow(max(dot(reflction_vector,normalized_viewer_direction),0.0),u_material_shininess);" +
			"phong_ads_color = ambient+diffuse+specular;" +

		"}" +
		"else" +
		"{" +
			"phong_ads_color = vec3(1.0,1.0,1.0);" +
		"}"+
			"FragColor = vec4(phong_ads_color,1.0);" +
		"}";

	// var fragmentShaderSourceObject =
	// "#version 300 es" +
	// 	"\n" +
	// 	"precision highp float;" +
	// 	"uniform highp sampler2D EarthNight;" +
	// 	"uniform highp sampler2D EarthDay;" +
	// 	"uniform highp sampler2D EarthCloud;" +

	// 	"in float Diffuse;" +
	// 	"in vec3 Specular;" +
	// 	"in vec2 TexCoord;" +

	// 	"out vec4 FragColor;" +

	// 	"void main()" +
	// 	"{" +
	// 				"vec2 clouds = texture(EarthCloud,TexCoord).rg;" +
	// 				"vec3 daytime = (texture(EarthDay,TexCoord).rgb * Diffuse + Specular * clouds.g) * (1.0 - clouds.r) + clouds.r * Diffuse;" +
	// 				"vec3 nighttime = texture(EarthNight , TexCoord).rgb * (1.0 - clouds.r) * 2.0;" +
	// 				"vec3 color = daytime;" +
	// 				"if(Diffuse < 0.1)" +
	// 				"{" +
	// 					"color = mix(nighttime,daytime,(Diffuse + 0.1) * 5.0);" +
	// 				"}" +
	// 				"FragColor = vec4(color,1.0);" +
	// 			//	"FragColor = texture(EarthNight,TexCoord);" +
	// 	"}";


	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert("fragmentShader compile error : " + error);

		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

//	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTex_coord");

gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTexCoord0");

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_NORMAL,"vNormal");
	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(" link error : " + error);
			
		}
	}


	modelMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_model_matrix");
	viewMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_view_matrix");
	projectionMatrixUniform = gl.getUniformLocation(shaderProgramObject,"u_projection_matrix");
	LkeyPressedUnifrom = gl.getUniformLocation(shaderProgramObject,"u_LKeyPressed");
	

	laUniform = gl.getUniformLocation(shaderProgramObject,"u_La");
	ldUniform = gl.getUniformLocation(shaderProgramObject,"u_Ld");
	lsUniform = gl.getUniformLocation(shaderProgramObject,"u_Ls");


	// mvMatrix = gl.getUniformLocation(shaderProgramObject,"mvMatrix");
	// mvpMatrix = gl.getUniformLocation(shaderProgramObject,"mvpMatrix");
	lightpositionuniform = gl.getUniformLocation(shaderProgramObject,"u_light_position");

	//lightPosition = gl.getUniformLocation(shaderProgramObject,"LightPosition");
	kaUniform = gl.getUniformLocation(shaderProgramObject,"u_ka");
	kdUniform = gl.getUniformLocation(shaderProgramObject,"u_kd");
	ksUniform = gl.getUniformLocation(shaderProgramObject,"u_ks");
	matrialShininessuniform = gl.getUniformLocation(shaderProgramObject,"u_material_shininess");


	//sphere
	sphere=new Mesh();
	makeSphere(sphere,2.0,30,30);


	// //load texture


	// earth_cloud_texture = gl.createTexture();
	// earth_cloud_texture.image = new Image();
	// earth_cloud_texture.image.src="earth.png";
	// earth_cloud_texture.image.onload = function()
	// {
	// 	gl.bindTexture(gl.TEXTURE_2D,earth_cloud_texture);
	// 	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
	// 	gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,earth_cloud_texture.image);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.REPEAT);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.REPEAT);				
	// 	gl.bindTexture(gl.TEXTURE_2D,null);
	// }

	// earth_day_texture = gl.createTexture();
	// earth_day_texture.image = new Image();
	// earth_day_texture.image.src="earth2.png";
	// earth_day_texture.image.onload = function()
	// {
	// 	gl.bindTexture(gl.TEXTURE_2D,earth_day_texture);
	// 	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
	// 	gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,earth_day_texture.image);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.REPEAT);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.REPEAT);				
	// 	gl.bindTexture(gl.TEXTURE_2D,null);
	// }

	// earth_night_texture = gl.createTexture();
	// earth_night_texture.image = new Image();
	// earth_night_texture.image.src="earth1.png";
	// earth_night_texture.image.onload = function()
	// {
	// 	gl.bindTexture(gl.TEXTURE_2D,earth_night_texture);
	// 	gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
	// 	gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,earth_night_texture.image);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_T,gl.REPEAT);
	// 	gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_WRAP_S,gl.REPEAT);				
	// 	gl.bindTexture(gl.TEXTURE_2D,null);
	// }


	
	// EarthCloud = gl.getUniformLocation(shaderProgramObject,"EarthCloud");

	// EarthNight = gl.getUniformLocation(shaderProgramObject, "EarthNight");

	// EarthDay = gl.getUniformLocation(shaderProgramObject, "EarthDay");
	
	
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);
	//gl.enable(gl.TEXTURE_2D);

	gl.clearColor(0.0,0.0,0.0,1.0);


	perspectiveProjectionMatrix = mat4.create();

	
}


function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{

	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	if(bLKeyPressed == true)
	{
		gl.uniform1i(LkeyPressedUnifrom,1);

		gl.uniform3fv(laUniform,light_ambient);
		gl.uniform3fv(ldUniform,light_diffuse);
		gl.uniform3fv(lsUniform,light_specular);
		gl.uniform4fv(lightpositionuniform,light_position);

		gl.uniform3fv(kaUniform,material_ambient);
		gl.uniform3fv(kdUniform,material_diffuse);
		gl.uniform3fv(ksUniform,material_specular);
		gl.uniform1f(matrialShininessuniform,material_shininess);		
	}
	else{
		gl.uniform1i(LkeyPressedUnifrom,0);
	}


	// var modelMatrix = mat4.create();
	// var modelViewMatrix = mat4.create();
	// var modelViewProjectionMatrix = mat4.create();

	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();

	mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-6.0]);
	
	 // mat4.rotateX(modelMatrix,modelMatrix,degToRad(-90.0));
	 // mat4.rotateY(modelMatrix,modelMatrix,degToRad(angle_sphere));
	// mat4.rotateZ(modelViewMatrix,modelViewMatrix,degToRad(angle_cube));

	// mat4.multiply(modelViewMatrix,modelViewMatrix,modelMatrix);
	// mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	
	 gl.uniformMatrix4fv(modelMatrixUniform,false,modelMatrix);
	 gl.uniformMatrix4fv(viewMatrixUniform,false,viewMatrix);
	 gl.uniformMatrix4fv(projectionMatrixUniform,false,perspectiveProjectionMatrix);
	 // gl.uniformMatrix4fv(mvMatrix,false,modelViewMatrix);
	 // gl.uniformMatrix4fv(mvpMatrix,false,modelViewProjectionMatrix);


	// //bind texture
	// gl.bindTexture(gl.TEXTURE_2D,cube_texture);
	// gl.uniform1i(EarthCloud,0);

	// gl.bindVertexArray(vao_cube);
	
	// gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	// gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	// gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	// gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	// gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	// gl.drawArrays(gl.TRIANGLE_FAN,20,4);

	// gl.bindVertexArray(null);

	// gl.activeTexture(gl.TEXTURE0);
 //    gl.bindTexture(gl.TEXTURE_2D, earth_cloud_texture);
 //    gl.uniform1i(EarthCloud, 0);

 //    gl.activeTexture(gl.TEXTURE2);
 //    gl.bindTexture(gl.TEXTURE_2D, earth_day_texture);
 //    gl.uniform1i(EarthDay, 2);

 //    gl.activeTexture(gl.TEXTURE1);
 //    gl.bindTexture(gl.TEXTURE_2D, earth_night_texture);
 //    gl.uniform1i(EarthNight, 1);

 //    gl.uniform3f(lightPosition, -20.0,0.0, -7.0);

    sphere.draw();
    //gl.bindTexture(gl.TEXTURE_2D, null);

	update();
	requestAnimationFrame(draw,canvas);
//	angle_sphere = angle_sphere + 0.3;

}

function update()
{
	

	// angle_cube = angle_cube + 0.2;
	// 	if(angle_cube>=360.0)
	// 	{
	// 		angle_cube = angle_cube -360.0;
	// 	}
}

function uninitialize()
{
	
	
	// if(cube_texture)
	// {
	// 	gl.deleteTexture(cube_texture);
	// 	cube_texture =0;
	// }
	
	// if(vao_cube)
	// {
	// 	gl.deleteBuffer(vao_cube);
	// 	vao_cube = null;	
	// }
	// if(vbo_position_cube)
	// {
	// 	gl.deleteBuffer(vbo_position_cube);
	// 	vbo_position_cube = null;	
	// }
	// if(vbo_texture_cube)
	// {
	// 	gl.deleteBuffer(vbo_texture_cube);
	// 	vbo_texture_cube = null;	
	// }

	if(sphere)
	{
		sphere.deallocate();
		sphere=null;
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
			 break;
		case 76:
		if(bLKeyPressed == false)
		{
			bLKeyPressed =true;
		}else{
			bLKeyPressed =false;
		}

	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
//global variable

var canvas = null;
var context = null;

//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	//get 2d context
	context = canvas.getContext("2d");
	if(!context)
		console.log("Obtaining 2d context failed!" );
	else	
		console.log("Obtaining 2d context succeeded!" );
	
	//fill canvas with black color
	context.fillStyle = "black";
	context.fillRect(0,0,canvas.width,canvas.height);
	
	
	
	//draw text
	drawText("Hello World!!!");
	
	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
}

function drawText(text)
{
	//center the text
	context.textAlign = "center";
	context.textBaseline="middle";
	
	//text font
	context.font = "48px sans-serif";
	
	//text color
	context.fillStyle = "white"; 
	
	//display the text in center
	context.fillText(text,canvas.width/2,canvas.height/2);
}

function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
	}
}
function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 70:
		 toggleFullScreen();
		 
		 //draw text
		drawText("Hello World!!!");
		break;
	}
}

function mouseDown()
{
	alert("Mouse is clicked");
}
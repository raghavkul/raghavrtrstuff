//global variable

var canvas = null;
var gl = null; //webgl context
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;


const WebGLMacros = {

	AMC_ATTRIBUTE_VERTEX:0,
	AMC_ATTRIBUTE_COLOR:1,
	AMC_ATTRIBUTE_NORMAL:2,
	AMC_ATTRIBUTE_TEXTURE0:3
};



var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;


var vao_pyramid;//vertex array object for tri
var vao_cube;
var vbo_position_pyramid;//vertex buffer object for tri
var vbo_texture_pyramid;//vertex buffer object for tri
var vbo_position_cube;//vertex buffer object for tri
var vbo_texture_cube;//vertex buffer object for tri
var mvpUniform;
var u_texture_sampler;
var perspectiveProjectionMatrix;


var pyramid_texture=0;
var cube_texture=0;

var angle_pyramid =0.0;
var angle_cube =0.0;

var requestAnimationFrame = 
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame ||
	null;


var cancleAnimationFrame = 
	window.cancleAnimationFrame ||
	window.webkitCancleRequestAnimationFrame || window.webkitCancleAnimationFrame ||
	window.mozCancleRequestAnimationFrame || window.mozCancleAnimationFrame ||
	window.oCancleRequestAnimationFrame || window.oCancleAnimationFrame ||
	window.msCancleRequestAnimationFrame || window.msCancleAnimationFrame ||
	null;


//onload function

function main()
{
	//get canvas element
	canvas = document.getElementById("AMC");
	if(!canvas)
		console.log("Obtaining canvas failed!" );
	else	
		console.log("Obtaining canvas succeeded!" );
	
	//print canvas width and height on console
	console.log("Canvas Width:"+canvas.width+" and Canvas height:"+canvas.height);
	
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;


	//register keyborad and mouse event handler
	
	window.addEventListener("keydown" , keyDown , false);
	window.addEventListener("click" , mouseDown , false);
	window.addEventListener("resize" , resize , false);

	init();
	resize();
	draw();
}



function toggleFullScreen()
{
	//code
	var fullscreen_element = document.fullscreenElement || 
							 document.webkitFullscreenElement ||
							 document.mozFullScreenElement ||
							 document.msFullscreenElement ||
							 null;
							 
	if(fullscreen_element == null)
	{
		if(canvas.requestFullscreen)
			canvas.requestFullscreen();
		else if(canvas.mozRequestFullScreen)
			canvas.mozRequestFullScreen();
		else if(canvas.webkitRequestFullscreen)
			canvas.webkitRequestFullscreen();
		else if(canvas.msRequestFullscreen)
			canvas.msRequestFullscreen();
		bFullScreen = true;
	}
	else{
		if(document.exitFullscreen)
			document.exitFullscreen();
		else if(document.mozCancelFullScreen)
			document.mozCancelFullScreen();
		else if(document.webkitExitFullscreen)
			document.webkitExitFullscreen();
		else if(document.msExitFullscreen)
			document.msExitFullscreen();
		bFullScreen = false;
	}
}


function init()
{

	
	gl = canvas.getContext("webgl2");

	if(gl == null)
		console.log("Obtaining  context failed!" );
	else	
		console.log("Obtaining context succeeded!" );

	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	var vertexShaderSourceObject =
		"#version 300 es" +
		"\n" +
		"in vec4 vPosition;" +
		"in vec2 vTex_coord;" +
		"uniform mat4 u_mvp_matrix;" +
		"out vec2 out_texture_coord;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
		"out_texture_coord = vTex_coord;" +
		"}" ;

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject,vertexShaderSourceObject);
	gl.compileShader(vertexShaderObject);

	if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if(error.length > 0)
		{
			alert(error);

		}
	}

	var fragmentShaderSourceObject =
	"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 out_texture_coord;" +
		"uniform highp sampler2D u_texture_sampler;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = texture(u_texture_sampler,out_texture_coord);" +
		"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject,fragmentShaderSourceObject);
	gl.compileShader(fragmentShaderObject);

	if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if(error.length > 0)
		{
			alert(error);

		}
	}

	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject,vertexShaderObject);
	gl.attachShader(shaderProgramObject,fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_VERTEX,"vPosition");

	gl.bindAttribLocation(shaderProgramObject,WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,"vTex_coord");

	gl.linkProgram(shaderProgramObject);

	if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if(error.length > 0)
		{
			alert(error);
			
		}
	}

	//load texture

	pyramid_texture = gl.createTexture();
	pyramid_texture.image = new Image();
	pyramid_texture.image.src="stone.png";
	pyramid_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D,pyramid_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,pyramid_texture.image);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);				
		gl.bindTexture(gl.TEXTURE_2D,null);
	}

	cube_texture = gl.createTexture();
	cube_texture.image = new Image();
	cube_texture.image.src="kundali.png";
	cube_texture.image.onload = function()
	{
		gl.bindTexture(gl.TEXTURE_2D,cube_texture);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,true);
		gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,cube_texture.image);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MAG_FILTER,gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D,gl.TEXTURE_MIN_FILTER,gl.NEAREST);				
		gl.bindTexture(gl.TEXTURE_2D,null);
	}

	mvpUniform = gl.getUniformLocation(shaderProgramObject,"u_mvp_matrix");
	u_texture_sampler = gl.getUniformLocation(shaderProgramObject,"u_texture_sampler");

	var pyramidVertices = new Float32Array([
			0.0,1.0,0.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,

			0.0,1.0,0.0,
			1.0,-1.0,1.0,
			1.0,-1.0,-1.0,
			
			0.0,1.0,0.0,
			1.0,-1.0,-1.0,
			-1.0,-1.0,-1.0,

			0.0,1.0,0.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0,
	]);

	var pyramidTextcoord = new Float32Array([
			0.5,1.0,
			0.0,0.0,
			1.0,0.0,
			
			0.5,1.0,
			1.0,0.0,
			0.0,0.0,
			
			0.5,1.0,
			1.0,0.0,
			0.0,0.0,
			
			0.5,1.0,
			0.0,0.0,
			1.0,0.0,
	]);

	var cubeVertices = new Float32Array([
			1.0,1.0,-1.0,
			-1.0,1.0,-1.0,
			-1.0,1.0,1.0,
			1.0,1.0,1.0,

			1.0,-1.0,1.0,
			-1.0,-1.0,1.0,
			-1.0,-1.0,-1.0,
			1.0,-1.0,-1.0,

			1.0,1.0,1.0,
			-1.0,1.0,1.0,
			-1.0,-1.0,1.0,
			1.0,-1.0,1.0,

			1.0,-1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,1.0,-1.0,
			1.0,1.0,-1.0,

			-1.0,1.0,1.0,
			-1.0,1.0,-1.0,
			-1.0,-1.0,-1.0,
			-1.0,-1.0,1.0,

			1.0,1.0,-1.0,
			1.0,1.0,1.0,
			1.0,-1.0,1.0,
			1.0,-1.0,-1.0,
	]);


	for(var i =0;i<72;i++)
	{
		if(cubeVertices[i]<0.0)
		{
			cubeVertices[i] = cubeVertices[i]+0.25;
		}else if(cubeVertices[i]>0.0)
		{
			cubeVertices[i] = cubeVertices[i]-0.25;
		}else{
			cubeVertices[i] = cubeVertices[i];
		}
	}
	var cubeTextcoord = new Float32Array([
		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,

		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,

		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,

		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,

		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,

		0.0,0.0,
		1.0,0.0,
		1.0,1.0,
		0.0,1.0,
	]);
	
	vao_pyramid=gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);


	//pyramid
	vbo_position_pyramid = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_pyramid);
	gl.bufferData(gl.ARRAY_BUFFER,pyramidVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);


	vbo_texture_pyramid = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture_pyramid);
	gl.bufferData(gl.ARRAY_BUFFER,pyramidTextcoord,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);


	//cube
	vao_cube=gl.createVertexArray();
	gl.bindVertexArray(vao_cube);


	vbo_position_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_position_cube);
	gl.bufferData(gl.ARRAY_BUFFER,cubeVertices,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,3,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);


	vbo_texture_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER,vbo_texture_cube);
	gl.bufferData(gl.ARRAY_BUFFER,cubeTextcoord,gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,2,gl.FLOAT,false,0,0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER,null);
	
	gl.bindVertexArray(null);
	

	gl.clearColor(0.0,0.0,0.0,1.0);
	
	gl.enable(gl.DEPTH_TEST);
	gl.enable(gl.CULL_FACE);



	perspectiveProjectionMatrix = mat4.create();

	
}


function resize()
{
	if(bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	gl.viewport(0,0,canvas.width,canvas.height);


	mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width)/parseFloat(canvas.height),
		0.1,
		100.0);
	console.log("perspective projection succeefull");
}


function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	
	// var modelMatrix = mat4.create();
	// var traslationMatrix = mat4.create();
	// var rotationMatrix = mat4.create();


	mat4.translate(modelViewMatrix,modelViewMatrix,[-1.5,0.0,-5.0]);
	mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(angle_pyramid));
	//mat4.multiply(modelViewMatrix,modelViewMatrix,modelMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

	//bind texture
	gl.bindTexture(gl.TEXTURE_2D,pyramid_texture);
	gl.uniform1i(u_texture_sampler,0);

	gl.bindVertexArray(vao_pyramid);
	
	gl.drawArrays(gl.TRIANGLES,0,3);
	gl.drawArrays(gl.TRIANGLES,3,3);
	gl.drawArrays(gl.TRIANGLES,6,3);
	gl.drawArrays(gl.TRIANGLES,9,3);

	gl.bindVertexArray(null);


	//cube
	
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);
	// mat4.identity(modelMatrix);
	// mat4.identity(traslationMatrix);
	// mat4.identity(rotationMatrix);

	mat4.translate(modelViewMatrix,modelViewMatrix,[1.5,0.0,-5.0]);
	
	mat4.rotateX(modelViewMatrix,modelViewMatrix,degToRad(angle_cube));
	mat4.rotateY(modelViewMatrix,modelViewMatrix,degToRad(angle_cube));
	mat4.rotateZ(modelViewMatrix,modelViewMatrix,degToRad(angle_cube));

	//mat4.multiply(modelViewMatrix,modelViewMatrix,modelMatrix);
	mat4.multiply(modelViewProjectionMatrix,perspectiveProjectionMatrix,modelViewMatrix);
	
	gl.uniformMatrix4fv(mvpUniform,false,modelViewProjectionMatrix);

	//bind texture
	gl.bindTexture(gl.TEXTURE_2D,cube_texture);
	gl.uniform1i(u_texture_sampler,0);

	gl.bindVertexArray(vao_cube);
	
	gl.drawArrays(gl.TRIANGLE_FAN,0,4);
	gl.drawArrays(gl.TRIANGLE_FAN,4,4);
	gl.drawArrays(gl.TRIANGLE_FAN,8,4);
	gl.drawArrays(gl.TRIANGLE_FAN,12,4);
	gl.drawArrays(gl.TRIANGLE_FAN,16,4);
	gl.drawArrays(gl.TRIANGLE_FAN,20,4);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	update();
	requestAnimationFrame(draw,canvas);

}

function update()
{
	angle_pyramid = angle_pyramid + 0.2;
		if(angle_pyramid>=360.0)
		{
			angle_pyramid = angle_pyramid -360.0;
		}

	angle_cube = angle_cube + 0.2;
		if(angle_cube>=360.0)
		{
			angle_cube = angle_cube -360.0;
		}
}

function uninitialize()
{
	
	if(pyramid_texture)
	{
		gl.deleteTexture(pyramid_texture);
		pyramid_texture =0;
	}
	if(cube_texture)
	{
		gl.deleteTexture(cube_texture);
		cube_texture =0;
	}
	if(vao_pyramid)
	{
		gl.deleteBuffer(vao_pyramid);
		vao_pyramid = null;	
	}
	if(vao_cube)
	{
		gl.deleteBuffer(vao_cube);
		vao_cube = null;	
	}
	if(vbo_position)
	{
		gl.deleteBuffer(vbo_position);
		vbo_position = null;	
	}
	if(vbo_texture)
	{
		gl.deleteBuffer(vbo_texture);
		vbo_texture = null;	
	}

	if(shaderProgramObject)
	{
		if(fragmentShaderObject)
		{
			gl.detachShader(shaderProgramObject,fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		if(vertexShaderObject)
		{
			gl.detachShader(shaderProgramObject,vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event)
{
	//code
	switch(event.keyCode)
	{
		case 27:
		//	uninitialize();
			window.close();
			break;
		case 70:
		 toggleFullScreen();
			 break;

	}
}


function mouseDown()
{
	//code
}

function degToRad(degrees)
{
	return(degrees * Math.PI/180);
}
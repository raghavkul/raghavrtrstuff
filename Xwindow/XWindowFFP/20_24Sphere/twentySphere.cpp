#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

//X11 header files

#include<X11/Xlib.h> //For All XServer Api
#include<X11/Xutil.h> // For XVisualInfo Structure
#include<X11/XKBlib.h> // For Keybord
#include<X11/keysym.h> // For KeyCode and Symbols Relationship


//#include<GL/glew.h>

#include<GL/glu.h>

#include<GL/glx.h>

using namespace std;

//global variables
static GLfloat angle = 0.0f;

bool bFullScreen = false;
Display *gpDisplay = NULL; // For Display
XVisualInfo *gpXVisualInfo = NULL;
static  GLXContext gGLXContext;
//XVisualInfo gXVisualInfo; // Like pixelFormatDescriptor ..which store visual info...
Colormap gColormap; // it is structure and contains the color cells.. like pixelType
Window gWindow; // Like wndClass .. it is structure

//Light Varbales
bool bLight = false;
GLfloat lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };
GLUquadric *quadric[24];

GLfloat light_model_ambiant[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat light_model_local_viewer[] = {0.0f};

GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;

GLint keyPress = 0;


int giWindowWidth = 800; // For Window Width
int giWindowHeight = 600;  // For Window Height

//function
void update(void);

//entry point function
int main()
{
	//function declerations

	void CreateWindow();
	void ToggleFullScreen();
	void uninitialize();
	int initialize();
	void resize(int,int);
	void display();

	// Local variable 

	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	bool bDone = false;
	//code 

	CreateWindow();

	initialize();	
	//varibales requried in message loop
	char keys[26];

			
	XEvent event;
	KeySym keysym;
	
	//Message Loop
		while(bDone == false)
		{
			while(XPending(gpDisplay))
			{
				XNextEvent(gpDisplay,&event);  //address of event .. like &Msg
				switch(event.type)  //event.type takes all the event ..like (WM_) messages
				{	
					case MapNotify: // Like WM_CREATE .. it occures only onces..
						break;
					case KeyPress: // Like WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0); // This fun is in XKBlib.h...Convert this member event.xkey.keycode into keycode .. 0:by defaut english(Which code page want to use), 0: if press Shift key or not...
						switch(keysym)
						{
							case XK_Escape:
									printf("Escape key is pressed\n");
									//  uninitialize();
									//  exit(0);
									bDone = true;
								break;
							
						}
						
						XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
						switch(keys[0])
						{
						case 'F':
						case 'f':
							printf("F key is pressed\n");
							if(bFullScreen == false)
							{
								ToggleFullScreen();
								bFullScreen = true;
							}else
							{
								ToggleFullScreen();
								bFullScreen = false;
							}
						break;
						case 'L':
						case 'l':
							if (bLight == false)
							{
								bLight = true;
								glEnable(GL_LIGHTING);
							}
							else
							{
								bLight = false;
								glDisable(GL_LIGHTING);
							}
							break;
						case'x':
						case'X':
								keyPress = 1;
								angleOfXRotation = 0.0f;
								break;
						case'y':
						case'Y':
								keyPress = 2;
								angleOfXRotation = 0.0f;
								break; 
						case'z':
						case'Z':
								keyPress = 3;
								angleOfXRotation = 0.0f;
								break;
							}
						break;	
						case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1: // left button
									printf("Left Button Preesed\n");
								break;
							case 2: // Middle button
									printf("Middle Button Preesed\n");
								break;
							case 3: // Right button
									printf("Right Button Preesed\n");
								break;
							case 4: // Mouse Wheel up
									printf("Mouse Wheel Up\n");
								break;
							case 5: // Mouse Wheel Down
									printf("Mouse Wheel Down\n");
								break;
						}
						break;
						case MotionNotify: // WM_MOUSEMOVE
							break;

						case ConfigureNotify: // WM_SIZE	
								winWidth = event.xconfigure.width;
								winHeight = event.xconfigure.height;
								resize(winWidth,winHeight);
							break;
						case Expose: // WM_PAINT
							break;
							case DestroyNotify: // WM_DESTROY
							break;
						case 33: // it is constant window manager message for close button or close menu
								printf("Close button is pressed\n");
							 // uninitialize();
							  //exit(0);
							
								bDone = true;
							break;
				}			
			}
		update();		
		display();
				
		}
		
	return(0);
			
}


// Functions Definations

void  CreateWindow()
{
	//function decleration
	void uninitialize();

	// variables 
	XSetWindowAttributes winAttribs; // For set the Attributes

	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]={
		GLX_RGBA,
		GLX_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None

	};
	//code 
	// Step-1 :Connect with XServer and To get Display
	gpDisplay = XOpenDisplay(NULL);// Start the connection with default display structure 
	
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	//Step-2: To Get Default Screen
	defaultScreen = XDefaultScreen(gpDisplay);


	//Step-3: To Get Default Depth
//	defaultDepth = DefaultDepth(gpDisplay,defaultScreen); // we receving depth from ChooseVisual

	//Step-4: To Get Matching Visual Info
	// gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

	// if(gpXVisualInfo == NULL)
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }  

	// XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);

	// if(gpXVisualInfo == NULL)
	// {
	// 	printf("ERROR : Unable To Get Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }

	gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Get Visual Info. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}  

	// Status status = XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,&gXVisualInfo);

	// if(status == 0)
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),
		gpXVisualInfo->visual,AllocNone);

	
	gColormap = winAttribs.colormap;

	
	winAttribs.event_mask = VisibilityChangeMask | ExposureMask | ButtonPressMask | KeyPressMask | 
		PointerMotionMask | StructureNotifyMask ;

	
	//Step-6: To Fill Window Style
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//Step-7: Create Actual Window To Using All Above Things
	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,
		giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,
		gpXVisualInfo->visual,styleMask,&winAttribs);


	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	
	//Step-8: Give Name To Window
	XStoreName(gpDisplay,gWindow,"First XWindow");


	//Step-9: This is Constant 33 message
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	//Step-10: Map Our Window On Default Window Given By XServer
	XMapWindow(gpDisplay,gWindow);
}

int initialize()
{
	void uninitialize();
	

	GLenum result;


	gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	// result = glewInit();
	// if (result != GLEW_OK)
	// {
	// 	printf("glewInit Failed!!\n");
	// 	uninitialize();
	// }
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambiant);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	glEnable(GL_LIGHT0);

	for (int i = 0; i < 24; i++)
	{
		quadric[i] = gluNewQuadric();
	}
	return(0);

}


void resize(int width, int height)
{
	
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width<=height)
	{
		glOrtho(0.0f, 15.5f, 0.0f, 15.5f*((GLfloat)height / (GLfloat)width), -10.0f, 10.0f);
	}
	else
	{
		glOrtho(0.0f, 15.5f*((GLfloat)width / (GLfloat)height), 0.0f,15.5f, -10.0f, 10.0f);

	}
}

void display()
{
	

	void draw24Sphers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	if (keyPress == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = angleOfXRotation;
	}else if (keyPress == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);
		lightPosition[2] = angleOfYRotation;
		
	}else if (keyPress == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = angleOfZRotation;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	draw24Sphers();
	

	glXSwapBuffers(gpDisplay,gWindow);

}

void draw24Sphers()
{

	GLfloat MaterialAmbiant[4]; 
	GLfloat MaterialDiffuse[4]; 
	GLfloat MaterialSpecular[4];
	GLfloat MaterialShininess[1]; 

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//GEMS
	//1 sphere on 1st column - emerald
	MaterialAmbiant[0] = 0.0215f;
	MaterialAmbiant[1] = 0.1745f;
	MaterialAmbiant[2] = 0.0215f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);
	
	MaterialDiffuse[0] = 0.07568f;
	MaterialDiffuse[1] = 0.61424f;
	MaterialDiffuse[2] = 0.0215f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	
	MaterialSpecular[0] = 0.633f;
	MaterialSpecular[1] = 0.727811f;
	MaterialSpecular[2] = 0.633f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);
	
	MaterialShininess[0] = 0.6f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(3.5f, 14.0f, 0.0f); // Y increment = +2.5
	gluSphere(quadric[0], 1.0f, 30, 30);

	//2 sphere on 1st column - jade
	MaterialAmbiant[0] = 0.135f;
	MaterialAmbiant[1] = 0.2225f;
	MaterialAmbiant[2] = 0.1575f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.54f;
	MaterialDiffuse[1] = 0.89f;
	MaterialDiffuse[2] = 0.63f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.316228f;
	MaterialSpecular[1] = 0.316228f;
	MaterialSpecular[2] = 0.316228f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.1f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(3.5f, 11.5f, 0.0f);
	gluSphere(quadric[1], 1.0f, 30, 30);

	//3 sphere on 1st column - obsidian
	MaterialAmbiant[0] = 0.5375f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.06625f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.18275f;
	MaterialDiffuse[1] = 0.17f;
	MaterialDiffuse[2] = 0.22525f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.332741f;
	MaterialSpecular[1] = 0.328634f;
	MaterialSpecular[2] = 0.346435f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.3f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(3.5f, 9.0f, 0.0f);
	gluSphere(quadric[2], 1.0f, 30, 30);

	//4 sphere on 1st column - pearl
	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.20725f;
	MaterialAmbiant[2] = 0.20725f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 1.0f;
	MaterialDiffuse[1] = 0.829f;
	MaterialDiffuse[2] = 0.829f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.296648f;
	MaterialSpecular[1] = 0.296648f;
	MaterialSpecular[2] = 0.296648f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.088f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(3.5f, 6.5f, 0.0f);
	gluSphere(quadric[3], 1.0f, 30, 30);

	//5 sphere on 1st column - ruby
	MaterialAmbiant[0] = 0.1745f;
	MaterialAmbiant[1] = 0.01175f;
	MaterialAmbiant[2] = 0.01175f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.61424f;
	MaterialDiffuse[1] = 0.04136f;
	MaterialDiffuse[2] = 0.04136f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.727811f;
	MaterialSpecular[1] = 0.626959f;
	MaterialSpecular[2] = 0.626959f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.6f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(3.5f, 4.0f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	//6 sphere on 1st column - turquoise
	MaterialAmbiant[0] = 0.1f;
	MaterialAmbiant[1] = 0.18725f;
	MaterialAmbiant[2] = 0.1745f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.396f;
	MaterialDiffuse[1] = 0.74151f;
	MaterialDiffuse[2] = 0.69102f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.297254f;
	MaterialSpecular[1] = 0.30829f;
	MaterialSpecular[2] = 0.306678f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.1f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(3.5f, 1.5f, 0.0f);
	gluSphere(quadric[5], 1.0f, 30, 30);

	//METALS
	//1 sphere in 2nd column - brass
	MaterialAmbiant[0] = 0.329412f;
	MaterialAmbiant[1] = 0.223529f;
	MaterialAmbiant[2] = 0.27451f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.780392f;
	MaterialDiffuse[1] = 0.568627f;
	MaterialDiffuse[2] = 0.113725f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.992157f;
	MaterialSpecular[1] = 0.941176f;
	MaterialSpecular[2] = 0.807843f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.21794872f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 14.0f, 0.0f);//X increment = +6
	gluSphere(quadric[6], 1.0f, 30, 30);


	//2 sphere in 2nd column - bronze
	MaterialAmbiant[0] = 0.2125f;
	MaterialAmbiant[1] = 0.1275f;
	MaterialAmbiant[2] = 0.054f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.714f;
	MaterialDiffuse[1] = 0.4284f;
	MaterialDiffuse[2] = 0.18144f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.393548f;
	MaterialSpecular[1] = 0.271906f;
	MaterialSpecular[2] = 0.166721f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.2f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 11.5f, 0.0f);//X increment = +6
	gluSphere(quadric[7], 1.0f, 30, 30);


	//3 sphere in 2nd column - chrome
	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.25f;
	MaterialAmbiant[2] = 0.25f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.774597f;
	MaterialSpecular[1] = 0.774597f;
	MaterialSpecular[2] = 0.774597f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.6f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 9.0f, 0.0f);//X increment = +6
	gluSphere(quadric[8], 1.0f, 30, 30);



	//4 sphere in 2nd column - copper
	MaterialAmbiant[0] = 0.19125f;
	MaterialAmbiant[1] = 0.0735f;
	MaterialAmbiant[2] = 0.02225f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.7038f;
	MaterialDiffuse[1] = 0.27048f;
	MaterialDiffuse[2] = 0.0828f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.256777f;
	MaterialSpecular[1] = 0.137622f;
	MaterialSpecular[2] = 0.086014f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.1f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 6.5f, 0.0f);//X increment = +6
	gluSphere(quadric[9], 1.0f, 30, 30);


	//5 sphere in 2nd column - gold
	MaterialAmbiant[0] = 0.24725f;
	MaterialAmbiant[1] = 0.1995f;
	MaterialAmbiant[2] = 0.0745f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.75164f;
	MaterialDiffuse[1] = 0.60648f;
	MaterialDiffuse[2] = 0.22648f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.628281f;
	MaterialSpecular[1] = 0.555802f;
	MaterialSpecular[2] = 0.366065f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.4f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 4.0f, 0.0f);//X increment = +6
	gluSphere(quadric[10], 1.0f, 30, 30);


	//6 sphere in 2nd column - silver
	MaterialAmbiant[0] = 0.19225f;
	MaterialAmbiant[1] = 0.19225f;
	MaterialAmbiant[2] = 0.19225f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.50754f;
	MaterialDiffuse[1] = 0.50754f;
	MaterialDiffuse[2] = 0.50754f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.508273f;
	MaterialSpecular[1] = 0.508273f;
	MaterialSpecular[2] = 0.508273f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.4f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 1.5f, 0.0f);//X increment = +6
	gluSphere(quadric[11], 1.0f, 30, 30);


	//PLASTIC
	//1 sphere in 3rd column - black
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50f;
	MaterialSpecular[1] = 0.50f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(15.5f, 14.0f, 0.0f);//X increment = +6
	gluSphere(quadric[12], 1.0f, 30, 30);


	//2 sphere in 3rd column - cyan
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.1f;
	MaterialAmbiant[2] = 0.06f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.0f;
	MaterialDiffuse[1] = 0.50980392f;
	MaterialDiffuse[2] = 0.50980392f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50196078f;
	MaterialSpecular[1] = 0.50196078f;
	MaterialSpecular[2] = 0.50196078f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(15.5f, 11.5f, 0.0f);//X increment = +6
	gluSphere(quadric[13], 1.0f, 30, 30);


	//3 sphere in 3rd column - green
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.1f;
	MaterialDiffuse[1] = 0.35f;
	MaterialDiffuse[2] = 0.1f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.45f;
	MaterialSpecular[1] = 0.55f;
	MaterialSpecular[2] = 0.45f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(15.5f, 9.0f, 0.0f);//X increment = +6
	gluSphere(quadric[14], 1.0f, 30, 30);


	//4 sphere in 3rd column - red
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.0f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.6f;
	MaterialSpecular[2] = 0.6f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(15.5f, 6.5f, 0.0f);//X increment = +6
	gluSphere(quadric[15], 1.0f, 30, 30);


	//5 sphere in 3rd column - white
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.55f;
	MaterialDiffuse[1] = 0.55f;
	MaterialDiffuse[2] = 0.55f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.70f;
	MaterialSpecular[1] = 0.70f;
	MaterialSpecular[2] = 0.70f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(15.5f, 4.0f, 0.0f);//X increment = +6
	gluSphere(quadric[16], 1.0f, 30, 30);


	//6 sphere in 3rd column - yellow
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.60f;
	MaterialSpecular[1] = 0.60f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(15.5f, 1.5f, 0.0f);//X increment = +6
	gluSphere(quadric[17], 1.0f, 30, 30);

	//RUBBER
	//1 sphere in 4th column - black
	MaterialAmbiant[0] = 0.02f;
	MaterialAmbiant[1] = 0.02f;
	MaterialAmbiant[2] = 0.02f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.4f;
	MaterialSpecular[1] = 0.4f;
	MaterialSpecular[2] = 0.4f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(21.5f, 14.0f, 0.0f);//X increment = +6
	gluSphere(quadric[18], 1.0f, 30, 30);


	//2 sphere in 4th column - cyan
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(21.5f, 11.5f, 0.0f);//X increment = +6
	gluSphere(quadric[19], 1.0f, 30, 30);


	//3 sphere in 4th column - green
	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(21.5f, 9.0f, 0.0f);//X increment = +6
	gluSphere(quadric[20], 1.0f, 30, 30);


	//4 sphere in 4th column - red
	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.04f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(21.5f, 6.5f, 0.0f);//X increment = +6
	gluSphere(quadric[21], 1.0f, 30, 30);


	//5 sphere in 4th column - white
	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(21.5f, 4.0f, 0.0f);//X increment = +6
	gluSphere(quadric[12], 1.0f, 30, 30);


	//6 sphere in 4th column - yellow
	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);

	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125f * 128.0f;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(21.5f, 1.5f, 0.0f);//X increment = +6
	gluSphere(quadric[23], 1.0f, 30, 30);

}

void update(void)
{
	

	angleOfXRotation = angleOfXRotation + 0.3f;
	if (angleOfXRotation >= 360.0f)
	{
		angleOfXRotation = 0.0f;
	}
	angleOfYRotation = angleOfYRotation + 0.3f;
	if (angleOfYRotation >= 360.0f)
	{
		angleOfYRotation = 0.0f;
	}
	angleOfZRotation = angleOfZRotation + 0.3f;
	if (angleOfZRotation >= 360.0f)
	{
		angleOfZRotation = 0.0f;
	}
}

void ToggleFullScreen()
{
	//variables declerations

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False); // Network protocol ...which is used to do window fullscreen remoetly and locally both
 	memset(&xev,0,sizeof(xev)); // All Members set to zero

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen? 0 : 1;

	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); // To Save New State Of Window
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen), // Pass This Event to XServer
		False,StructureNotifyMask,&xev);

}


void uninitialize()
{
	GLXContext CurrentGLXContext = glXGetCurrentContext();
	if(CurrentGLXContext!=NULL && CurrentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	} 
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}

}
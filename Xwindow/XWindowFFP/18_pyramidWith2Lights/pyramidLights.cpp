#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

//X11 header files

#include<X11/Xlib.h> //For All XServer Api
#include<X11/Xutil.h> // For XVisualInfo Structure
#include<X11/XKBlib.h> // For Keybord
#include<X11/keysym.h> // For KeyCode and Symbols Relationship


//#include<GL/glew.h>

#include<GL/glu.h>

#include<GL/glx.h>

using namespace std;

//global variables
static GLfloat angle = 0.0f;

bool bFullScreen = false;
Display *gpDisplay = NULL; // For Display
XVisualInfo *gpXVisualInfo = NULL;
static  GLXContext gGLXContext;
//XVisualInfo gXVisualInfo; // Like pixelFormatDescriptor ..which store visual info...
Colormap gColormap; // it is structure and contains the color cells.. like pixelType
Window gWindow; // Like wndClass .. it is structure

//Light Varbales
bool bLight = false;

struct Light
{
	GLfloat ambiant[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position[4];
};

struct Light lights[2];

GLfloat MaterialAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialShininess[] = { 50.0f };

GLUquadric *quadric = NULL;


int giWindowWidth = 800; // For Window Width
int giWindowHeight = 600;  // For Window Height

//function
void update(void);

//entry point function
int main()
{
	//function declerations

	void CreateWindow();
	void ToggleFullScreen();
	void uninitialize();
	void initialize();
	void resize(int,int);
	void display();

	// Local variable 

	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	bool bDone = false;
	//code 

	CreateWindow();

	initialize();	
	//varibales requried in message loop
	char keys[26];

			
	XEvent event;
	KeySym keysym;
	
	//Message Loop
		while(bDone == false)
		{
			while(XPending(gpDisplay))
			{
				XNextEvent(gpDisplay,&event);  //address of event .. like &Msg
				switch(event.type)  //event.type takes all the event ..like (WM_) messages
				{	
					case MapNotify: // Like WM_CREATE .. it occures only onces..
						break;
					case KeyPress: // Like WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0); // This fun is in XKBlib.h...Convert this member event.xkey.keycode into keycode .. 0:by defaut english(Which code page want to use), 0: if press Shift key or not...
						switch(keysym)
						{
							case XK_Escape:
									printf("Escape key is pressed\n");
									//  uninitialize();
									//  exit(0);
									bDone = true;
								break;
							
						}
						
						XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
						switch(keys[0])
						{
						case 'F':
						case 'f':
							printf("F key is pressed\n");
							if(bFullScreen == false)
							{
								ToggleFullScreen();
								bFullScreen = true;
							}else
							{
								ToggleFullScreen();
								bFullScreen = false;
							}
						break;
						case 'L':
						case 'l':
							if (bLight == false)
							{
								bLight = true;
								glEnable(GL_LIGHTING);
							}
							else
							{
								bLight = false;
								glDisable(GL_LIGHTING);
							}
							break;
						}
						break;	
						case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1: // left button
									printf("Left Button Preesed\n");
								break;
							case 2: // Middle button
									printf("Middle Button Preesed\n");
								break;
							case 3: // Right button
									printf("Right Button Preesed\n");
								break;
							case 4: // Mouse Wheel up
									printf("Mouse Wheel Up\n");
								break;
							case 5: // Mouse Wheel Down
									printf("Mouse Wheel Down\n");
								break;
						}
						break;
						case MotionNotify: // WM_MOUSEMOVE
							break;

						case ConfigureNotify: // WM_SIZE	
								winWidth = event.xconfigure.width;
								winHeight = event.xconfigure.height;
								resize(winWidth,winHeight);
							break;
						case Expose: // WM_PAINT
							break;
							case DestroyNotify: // WM_DESTROY
							break;
						case 33: // it is constant window manager message for close button or close menu
								printf("Close button is pressed\n");
							 // uninitialize();
							  //exit(0);
							
								bDone = true;
							break;
				}			
			}
		update();		
		display();
				
		}
		
	return(0);
			
}


// Functions Definations

void  CreateWindow()
{
	//function decleration
	void uninitialize();

	// variables 
	XSetWindowAttributes winAttribs; // For set the Attributes

	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]={
		GLX_RGBA,
		GLX_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None

	};
	//code 
	// Step-1 :Connect with XServer and To get Display
	gpDisplay = XOpenDisplay(NULL);// Start the connection with default display structure 
	
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	//Step-2: To Get Default Screen
	defaultScreen = XDefaultScreen(gpDisplay);


	//Step-3: To Get Default Depth
//	defaultDepth = DefaultDepth(gpDisplay,defaultScreen); // we receving depth from ChooseVisual

	//Step-4: To Get Matching Visual Info
	// gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

	// if(gpXVisualInfo == NULL)
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }  

	// XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);

	// if(gpXVisualInfo == NULL)
	// {
	// 	printf("ERROR : Unable To Get Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }

	gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Get Visual Info. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}  

	// Status status = XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,&gXVisualInfo);

	// if(status == 0)
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),
		gpXVisualInfo->visual,AllocNone);

	
	gColormap = winAttribs.colormap;

	
	winAttribs.event_mask = VisibilityChangeMask | ExposureMask | ButtonPressMask | KeyPressMask | 
		PointerMotionMask | StructureNotifyMask ;

	
	//Step-6: To Fill Window Style
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//Step-7: Create Actual Window To Using All Above Things
	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,
		giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,
		gpXVisualInfo->visual,styleMask,&winAttribs);


	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	
	//Step-8: Give Name To Window
	XStoreName(gpDisplay,gWindow,"First XWindow");


	//Step-9: This is Constant 33 message
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	//Step-10: Map Our Window On Default Window Given By XServer
	XMapWindow(gpDisplay,gWindow);
}

void initialize()
{
	void uninitialize();
	

	GLenum result;


	gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	// result = glewInit();
	// if (result != GLEW_OK)
	// {
	// 	printf("glewInit Failed!!\n");
	// 	uninitialize();
	// }
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//peace meal array initialization
	//for light 0
	lights[0].ambiant[0] = { 0.0f };
	lights[0].ambiant[1] = { 0.0f };
	lights[0].ambiant[2] = { 0.0f };
	lights[0].ambiant[3] = { 1.0f };

	lights[0].diffuse[0] = { 1.0f };
	lights[0].diffuse[1] = { 0.0f };
	lights[0].diffuse[2] = { 0.0f };
	lights[0].diffuse[3] = { 1.0f };

	lights[0].specular[0] = {1.0f};
	lights[0].specular[1] = { 0.0f };
	lights[0].specular[2] = { 0.0f };
	lights[0].specular[3] = { 1.0f };

	lights[0].position[0] = { -2.0f };
	lights[0].position[1] = { 0.0f };
	lights[0].position[2] = { 0.0f };
	lights[0].position[3] = { 1.0f };

	//for light 1
	lights[1].ambiant[0] = { 0.0f };
	lights[1].ambiant[1] = { 0.0f };
	lights[1].ambiant[2] = { 0.0f };
	lights[1].ambiant[3] = { 1.0f };

	lights[1].diffuse[0] = { 0.0f };
	lights[1].diffuse[1] = { 0.0f };
	lights[1].diffuse[2] = { 1.0f };
	lights[1].diffuse[3] = { 1.0f };

	lights[1].specular[0] = {0.0f };
	lights[1].specular[1] = { 0.0f };
	lights[1].specular[2] = { 1.0f };
	lights[1].specular[3] = { 1.0f };

	lights[1].position[0] = { 2.0f };
	lights[1].position[1] = { 0.0f };
	lights[1].position[2] = { 0.0f };
	lights[1].position[3] = { 1.0f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, lights[0].ambiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lights[0].diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lights[0].specular);
	glLightfv(GL_LIGHT0, GL_POSITION, lights[0].position);
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT1, GL_AMBIENT, lights[1].ambiant);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lights[1].diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lights[1].specular);
	glLightfv(GL_LIGHT1, GL_POSITION, lights[1].position);
	glEnable(GL_LIGHT1);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbiant);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

}


void resize(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

}

void display()
{
	

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);


	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -4.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_TRIANGLES);

	//front
	glNormal3f(0.0f, 0.447214f, 0.89442f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glNormal3f(0.0f, 0.447214f, 0.89442f);

	glVertex3f(-1.0f, -1.0f, 1.0f);
	glNormal3f(0.0f, 0.447214f, 0.89442f);

	glVertex3f(1.0f, -1.0f, 1.0f);

	//right
	glNormal3f(0.894427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glNormal3f(0.894427f, 0.447214f, 0.0f);

	glVertex3f(1.0f, -1.0f, 1.0f);
	glNormal3f(0.894427f, 0.447214f, 0.0f);

	glVertex3f(1.0f, -1.0f, -1.0f);


	//back
	glNormal3f(0.0f, 0.447214f, -0.89442f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glNormal3f(0.0f, 0.447214f, -0.89442f);

	glVertex3f(1.0f, -1.0f, -1.0f);
	glNormal3f(0.0f, 0.447214f, -0.89442f);

	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left
	glNormal3f(-0.894427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glNormal3f(-0.894427f, 0.447214f, 0.0f);

	glVertex3f(-1.0f, -1.0f, -1.0f);
	glNormal3f(-0.894427f, 0.447214f, 0.0f);

	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();



	glXSwapBuffers(gpDisplay,gWindow);

}


void update(void)
{
	

angle = angle + 0.3f;
	if (angle >= 360)
	{
		angle = 0.0f;
	}

}

void ToggleFullScreen()
{
	//variables declerations

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False); // Network protocol ...which is used to do window fullscreen remoetly and locally both
 	memset(&xev,0,sizeof(xev)); // All Members set to zero

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen? 0 : 1;

	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); // To Save New State Of Window
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen), // Pass This Event to XServer
		False,StructureNotifyMask,&xev);

}


void uninitialize()
{
	GLXContext CurrentGLXContext = glXGetCurrentContext();
	if(CurrentGLXContext!=NULL && CurrentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	} 
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}

}
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

//X11 header files

#include<X11/Xlib.h> //For All XServer Api
#include<X11/Xutil.h> // For XVisualInfo Structure
#include<X11/XKBlib.h> // For Keybord
#include<X11/keysym.h> // For KeyCode and Symbols Relationship

using namespace std;

//global variables

bool bFullScreen = false;
Display *gpDisplay = NULL; // For Display
XVisualInfo *gpXVisualInfo = NULL;
//XVisualInfo gXVisualInfo; // Like pixelFormatDescriptor ..which store visual info...
Colormap gColormap; // it is structure and contains the color cells.. like pixelType
Window gWindow; // Like wndClass .. it is structure

int giWindowWidth = 800; // For Window Width
int giWindowHeight = 600;  // For Window Height


//entry point function
int main()
{
	//function declerations

	void CreateWindow();
	void ToggleFullScreen();
	void uninitialize();

	// Local variable 

	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;


	//code 

	CreateWindow();

	//varibales requried in message loop
	
	XEvent event;
	KeySym keysym;
	
	//Message Loop

	while(1)
	{
		XNextEvent(gpDisplay,&event);  //address of event .. like &Msg
		switch(event.type)  //event.type takes all the event ..like (WM_) messages
		{	
			case MapNotify: // Like WM_CREATE .. it occures only onces..
				break;
			case KeyPress: // Like WM_KEYDOWN
				keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0); // This fun is in XKBlib.h...Convert this member event.xkey.keycode into keycode .. 0:by defaut english(Which code page want to use), 0: if press Shift key or not...
					switch(keysym)
					{
						case XK_Escape:
								printf("Escape key is pressed\n");
								uninitialize();
								exit(0);
							break;
						case XK_F:
						case XK_f:
								printf("F key is pressed\n");
								if(bFullScreen == false)
								{
									ToggleFullScreen();
									bFullScreen = true;
								}else
								{
									ToggleFullScreen();
									bFullScreen = false;
								}
							break;	
					}	
				break;
			case ButtonPress:
				switch(event.xbutton.button)
				{
					case 1: // left button
							printf("Left Button Preesed\n");
						break;
					case 2: // Middle button
							printf("Middle Button Preesed\n");
						break;
					case 3: // Right button
							printf("Right Button Preesed\n");
						break;
					case 4: // Mouse Wheel up
							printf("Mouse Wheel Up\n");
						break;
					case 5: // Mouse Wheel Down
							printf("Mouse Wheel Down\n");
						break;
				}
				break;

			case MotionNotify: // WM_MOUSEMOVE
				break;

			case ConfigureNotify: // WM_SIZE	
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
				break;
			case Expose: // WM_PAINT
				break;
			case DestroyNotify: // WM_DESTROY
				break;
			case 33: // it is constant window manager message for close button or close menu
					printf("Close button is pressed\n");
					uninitialize();
					exit(0);
				break;
		}
	}

	uninitialize();
	return(0);
}


// Functions Definations

void  CreateWindow()
{
	//function decleration
	void uninitialize();

	// variables 
	XSetWindowAttributes winAttribs; // For set the Attributes
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	//code 
	// Step-1 :Connect with XServer and To get Display
	gpDisplay = XOpenDisplay(NULL);// Start the connection with default display structure 
	
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	//Step-2: To Get Default Screen
	defaultScreen = XDefaultScreen(gpDisplay);


	//Step-3: To Get Default Depth
	defaultDepth = DefaultDepth(gpDisplay,defaultScreen);

	//Step-4: To Get Matching Visual Info
	gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}  

	XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);

	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Get Visual Info. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}  

	// Status status = XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,&gXVisualInfo);

	// if(status == 0)
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	//Step-5: To Fill Window Attribute
	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),
		gpXVisualInfo->visual,AllocNone);

	
	gColormap = winAttribs.colormap;

	
	winAttribs.event_mask = VisibilityChangeMask | ExposureMask | ButtonPressMask | KeyPressMask | 
		PointerMotionMask | StructureNotifyMask ;

	
	//Step-6: To Fill Window Style
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//Step-7: Create Actual Window To Using All Above Things
	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,
		giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,
		gpXVisualInfo->visual,styleMask,&winAttribs);


	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	
	//Step-8: Give Name To Window
	XStoreName(gpDisplay,gWindow,"First XWindow");


	//Step-9: This is Constant 33 message
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	//Step-10: Map Our Window On Default Window Given By XServer
	XMapWindow(gpDisplay,gWindow);
}


void ToggleFullScreen()
{
	//variables declerations

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False); // Network protocol ...which is used to do window fullscreen remoetly and locally both
 	memset(&xev,0,sizeof(xev)); // All Members set to zero

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen? 0 : 1;

	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); // To Save New State Of Window
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen), // Pass This Event to XServer
		False,StructureNotifyMask,&xev);

}


void uninitialize()
{
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}	
}
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

//X11 header files

#include<X11/Xlib.h> //For All XServer Api
#include<X11/Xutil.h> // For XVisualInfo Structure
#include<X11/XKBlib.h> // For Keybord
#include<X11/keysym.h> // For KeyCode and Symbols Relationship


//#include<GL/glew.h>

#include<GL/glu.h>

#include<GL/glx.h>

#define _USE_MATH_DEFINES 1
#include<math.h>

//Audio files
#include<AL/al.h>
#include<AL/alut.h>

using namespace std;

//global variables

bool bFullScreen = false;
Display *gpDisplay = NULL; // For Display
XVisualInfo *gpXVisualInfo = NULL;
static  GLXContext gGLXContext;

//XVisualInfo gXVisualInfo; // Like pixelFormatDescriptor ..which store visual info...
Colormap gColormap; // it is structure and contains the color cells.. like pixelType
Window gWindow; // Like wndClass .. it is structure

int giWindowWidth = 800; // For Window Width
int giWindowHeight = 600;  // For Window Height
static int winWidth = giWindowWidth;
static int winHeight = giWindowHeight;


//Audio Pars
ALCcontext *context;
ALCdevice *device;
char*     alBuffer;             //data for the buffer
ALenum alFormatBuffer;    //buffer format
ALsizei   alFreqBuffer;       //frequency
long       alBufferLen;        //bit depth
ALboolean    alLoop;         //loop
unsigned int alSource;      //source
unsigned int alSampleSet;


//Functions
void leftI();
void N();
void D();
void rightI();
void A();
void topPlane();
void middlePlane();
void bottomPlane();
void WholeFlag();
void AFlag();

void createAudioContext(void);
void PlaySound();
void StopSound();
void deleteAudioContext();
//static flags
static bool ifLI = false;
static bool ifA = false;
static bool ifN = false;
static bool ifRI = false;
static bool ifD = false;
static bool movingPlane = false;
static bool Airostopper = false;
static bool splitter = false;


//global functions
void update(void);

//entry point function
int main()
{
	//function declerations

	void CreateWindow();
	void ToggleFullScreen();
	void uninitialize();
	void initialize();
	void resize(int,int);
	void display();

	// Local variable 
	
	bool bDone = false;
	//code 

	CreateWindow();

	initialize();	
	//varibales requried in message loop
	char keys[26];

			
	XEvent event;
	KeySym keysym;
	
	//Message Loop
		while(bDone == false)
		{
			while(XPending(gpDisplay))
			{
				XNextEvent(gpDisplay,&event);  //address of event .. like &Msg
				switch(event.type)  //event.type takes all the event ..like (WM_) messages
				{	
					case MapNotify: // Like WM_CREATE .. it occures only onces..
						break;
					case KeyPress: // Like WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0); // This fun is in XKBlib.h...Convert this member event.xkey.keycode into keycode .. 0:by defaut english(Which code page want to use), 0: if press Shift key or not...
						switch(keysym)
						{
							case XK_Escape:
									printf("Escape key is pressed\n");
									//  uninitialize();
									//  exit(0);
									bDone = true;
								break;
							
						}
						
						XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
						switch(keys[0])
						{
						case 'F':
						case 'f':
							printf("F key is pressed\n");
									if(bFullScreen == false)
									{
										ToggleFullScreen();
										bFullScreen = true;
									}else
									{
										ToggleFullScreen();
										bFullScreen = false;
									}
						break;
					
						}
						break;	
						case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1: // left button
									printf("Left Button Preesed\n");
								break;
							case 2: // Middle button
									printf("Middle Button Preesed\n");
								break;
							case 3: // Right button
									printf("Right Button Preesed\n");
								break;
							case 4: // Mouse Wheel up
									printf("Mouse Wheel Up\n");
								break;
							case 5: // Mouse Wheel Down
									printf("Mouse Wheel Down\n");
								break;
						}
						break;
						case MotionNotify: // WM_MOUSEMOVE
							break;

						case ConfigureNotify: // WM_SIZE	
								winWidth = event.xconfigure.width;
								winHeight = event.xconfigure.height;
								resize(winWidth,winHeight);
							break;
						case Expose: // WM_PAINT
							break;
							case DestroyNotify: // WM_DESTROY
							break;
						case 33: // it is constant window manager message for close button or close menu
								printf("Close button is pressed\n");
							 // uninitialize();
							  //exit(0);
							
								bDone = true;
							break;
				}			
			}
		update();		
		display();
				
		}
		
	return(0);
			
}


// Functions Definations

void  CreateWindow()
{
	//function decleration
	void uninitialize();

	// variables 
	XSetWindowAttributes winAttribs; // For set the Attributes

	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]={
		GLX_RGBA,
		GLX_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None

	};
	//code 
	// Step-1 :Connect with XServer and To get Display
	gpDisplay = XOpenDisplay(NULL);// Start the connection with default display structure 
	
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	//Step-2: To Get Default Screen
	defaultScreen = XDefaultScreen(gpDisplay);


	//Step-3: To Get Default Depth
//	defaultDepth = DefaultDepth(gpDisplay,defaultScreen); // we receving depth from ChooseVisual

	//Step-4: To Get Matching Visual Info
	// gpXVisualInfo = (XVisualInfo *)malloc(sizeof(XVisualInfo));

	// if(gpXVisualInfo == NULL)
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }  

	// XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);

	// if(gpXVisualInfo == NULL)
	// {
	// 	printf("ERROR : Unable To Get Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }

	gpXVisualInfo = glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);

	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Get Visual Info. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}  

	// Status status = XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,&gXVisualInfo);

	// if(status == 0)
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info. \n Exitting Now..\n");
	// 	uninitialize();
	// 	exit(1);
	// }
	
	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),
		gpXVisualInfo->visual,AllocNone);

	
	gColormap = winAttribs.colormap;

	
	winAttribs.event_mask = VisibilityChangeMask | ExposureMask | ButtonPressMask | KeyPressMask | 
		PointerMotionMask | StructureNotifyMask ;

	
	//Step-6: To Fill Window Style
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//Step-7: Create Actual Window To Using All Above Things
	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,
		giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,
		gpXVisualInfo->visual,styleMask,&winAttribs);


	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	
	//Step-8: Give Name To Window
	XStoreName(gpDisplay,gWindow,"First XWindow");


	//Step-9: This is Constant 33 message
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	//Step-10: Map Our Window On Default Window Given By XServer
	XMapWindow(gpDisplay,gWindow);
}

void initialize()
{
	void uninitialize();
	void resize(int, int);


	GLenum result;


	gGLXContext = glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	// glClearDepth(1.0f);
	// glEnable(GL_DEPTH_TEST);
	// glDepthFunc(GL_LEQUAL);
	createAudioContext();
	PlaySound();		
		
	
}

void createAudioContext(void) { //Call this in Initialize
    void uninitialize(void);

    device = alcOpenDevice(NULL);
    if (device == NULL)
    {
        printf(" ERROR : Cannot Open Audio Context../n Exiting NOw!\n");
        uninitialize();
        exit(0);
    }
 
    //Create a context
    context=alcCreateContext(device,NULL);
 
    //Set active context
    alcMakeContextCurrent(context);
    
     //load the wave file
    alutLoadWAVFile((ALbyte*)"watan.wav",&alFormatBuffer, (void **) &alBuffer,(ALsizei*)&alBufferLen, &alFreqBuffer, &alLoop);
    
    //create a source
    alGenSources(1, &alSource);
    
    //create  buffer
    alGenBuffers(1, &alSampleSet);
    
    //put the data into our sampleset buffer
    alBufferData(alSampleSet, alFormatBuffer, alBuffer, alBufferLen, alFreqBuffer);
    
    //assign the buffer to this source
    alSourcei(alSource, AL_BUFFER, alSampleSet);
    
    //release the data
    alutUnloadWAV(alFormatBuffer, alBuffer, alBufferLen, alFreqBuffer);
}

void PlaySound() {  //Call in display but once

    alSourcei(alSource,AL_LOOPING,AL_TRUE);    
    //play
    alSourcePlay(alSource);
}


void StopSound() { 
    alSourceStop(alSource);
}
void resize(int width, int height)
{
	
if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display()
{
	
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	if (ifLI == false)
	{

		leftI();
			}
	if (ifA == true)
	{
		A();
	}
	if (ifN == true)
	{
		N();
	}
	 if (ifRI == true)
	{
		rightI();
	}
	if (ifD == true)
	{
		D();
	}
	 if (movingPlane == true)
	{
		
		topPlane();
		middlePlane();
		bottomPlane();
	}
	 WholeFlag();

	glXSwapBuffers(gpDisplay,gWindow);
}

void leftI()
{
	static GLfloat Litx = -3.0f, Lity = -0.7f, Litz = -8.0f;

	////Left Leter I
	glLineWidth(5.0f);

	glLoadIdentity();
	glTranslatef(Litx, Lity, Litz);

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(-2.5f, 1.5f);
	glVertex2f(-3.5f, 1.5f);

	glVertex2f(-3.0f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-3.0f, 0.0f);

	glVertex2f(-2.5f, 0.0f);
	glVertex2f(-3.5f, 0.0f);

	glEnd();
	if ((Litx < 0.0f) || (Litx == 0.0f))
	{
		Litx = Litx + 0.009f;
	}
	if (Litx >= 0.0f)
	{
		A();
	}
}
void N()
{
	static GLfloat ntx = 0.0f, nty = 4.0f, ntz = -8.0f;
	glLineWidth(5.0f);

	// Letter N
	glLoadIdentity();
	glTranslatef(ntx, nty, ntz);

	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-2.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);

	glEnd();
	if ((nty >= -0.7f) || (nty == -0.7f))
	{
		nty = nty - 0.02f;

	}
	if (nty <= -0.7f)
	{
		rightI();
	}
}
	

void D()
{
	static GLfloat dtx = 0.0f, dty = -0.7f, dtz = -8.0f;
	static GLfloat i = 0.0f, j = 0.0f, k = 0.0f, l = 0.0f, m = 0.0f, n = 0.0f;  //For D

	glLineWidth(5.0f);

	// Letter D

	glLoadIdentity();
	glTranslatef(dtx, dty, dtz);

	glBegin(GL_LINES);

	glColor3f(i, j, k);
	glVertex2f(-0.5f, 1.5f);

	glColor3f(l, m, n);
	glVertex2f(-0.5f, 0.0f);


	glColor3f(i, j, k);
	glVertex2f(-0.5f, 1.5f);
	glVertex2f(0.5f, 1.2f);


	glColor3f(i, j, k);
	glVertex2f(0.5f, 1.2f);

	glColor3f(l, m, n);
	glVertex2f(0.5f, 0.3f);

	glColor3f(l, m, n);
	glVertex2f(-0.5f, 0.0f);
	glVertex2f(0.5f, 0.3f);

	glEnd();
	if (i <= 1.0f && j < 0.6f && k < 0.2f && l< 0.07f && m < 0.533f && n < 0.027f)
	{
		i = i + 0.001f;
		j = j + 0.0006f;
		k = k + 0.0002f;

		l = l + 0.00007f;
		m = m + 0.000533f;
		n = n + 0.000027f;
	}
	if (n >= 0.027f)
	{
		topPlane();
		middlePlane();
		bottomPlane();
	}
	
	
}
void rightI()
{
	static GLfloat Ritx = 0.0f, Rity = -6.0f, Ritz = -8.0f;
	glLineWidth(5.0f);

	//Right Letter I

	glLoadIdentity();
	glTranslatef(Ritx, Rity, Ritz);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(1.0f, 1.5f);
	glVertex2f(2.0f, 1.5f);

	glVertex2f(1.5f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(1.5f, 0.0f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(2.0f, 0.0f);
	glEnd();
	 if ((Rity <= -0.7f) || (Rity == -0.7f))
	{
		Rity = Rity + 0.02f;

	}
	 if (Rity >= -0.7f)
	 {
		 D();
	 }
	


}
void A()
{
	static GLfloat x = 0.0f, y = 0.0f, z = 0.0f, a = 0.0f, b = 0.0f, c = 0.0f, d = 0.0f, e = 0.0f, f = 0.0f; //For  middle color of A
	
	static GLfloat atx = 4.0f, aty = -0.7f, atz = -8.0f, aty1 = 0.01f;
	
	glLineWidth(5.0f);

	//Letter A

	glLoadIdentity();
	glTranslatef(atx, aty, atz);

	glBegin(GL_LINES);

	glColor3f(0.07f, 0.533f, 0.027f);

	glVertex2f(2.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(3.0f, 1.5f);

	glVertex2f(3.0f, 1.5f);
	//glVertex2f(0.95f, 0.3f);

	//glVertex2f(0.95f, 0.3f);
	//glVertex2f(1.05f, 0.18f);

	//glVertex2f(1.05f, 0.18f);

	glColor3f(0.07f, 0.533f, 0.027f);

	glVertex2f(3.5f, 0.0f);


	glEnd();
	//middle flag of A

	 if ((atx > 0.0) || (atx == 0.0f))
	{
		atx = atx - 0.009f;

	}
	 if (atx <= 0.0f)
	 {
		 N();
	 }
	 


}
void topPlane()
{
	static GLfloat t2x = -18.0f, t2y = 6.0f, t2z = -22.0f, Airoangale2 = -90.0f, r2x = 0.0f, r2y = 0.0f, r2z = 1.0f;
	static bool  splitter = false;

	//top plane
	glColor3f(0.7265f, 0.8828f, 0.9296f);

	glLoadIdentity();
	glTranslatef(t2x, t2y, t2z);
	glRotatef(Airoangale2, r2x, r2y, r2z);
	glLineWidth(3.0f);
	glBegin(GL_QUADS);

	glVertex2f(0.4f, 0.2f);
	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-0.6f, -0.2f);
	glVertex2f(0.4f, -0.2f);

	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-1.0f, 0.4f);
	glVertex2f(-1.0f, -0.4f);
	glVertex2f(-0.6f, -0.2f);

	glVertex2f(0.2f, 0.2f);
	glVertex2f(-0.3f, 1.0f);
	glVertex2f(-0.3f, -1.0f);
	glVertex2f(0.2f, -0.2f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(0.4f, 0.2f);
	glVertex2f(0.4f, -0.2f);
	glVertex2f(1.0f, 0.0f);

	glEnd();
	//IAF for Top Plane

	glLoadIdentity();
	glTranslatef(t2x, t2y, t2z);
	glRotatef(Airoangale2, r2x, r2y, r2z);
	

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.3f, 0.0f);
	glVertex2f(0.1f, 0.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(-0.1f, 0.05f);
	glVertex2f(-0.1f, -0.05f);

	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.5f, 0.1f);
	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.3f, -0.1f);
	glVertex2f(-0.4f, 0.1f);
	glVertex2f(-0.4f, -0.05f);



	glEnd();
	//Back Flag Of Top plane
	glLoadIdentity();
	glTranslatef(t2x, t2y, t2z);
	glRotatef(Airoangale2, r2x, r2y, r2z);

	glBegin(GL_LINES);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.05f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 0.05f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(-1.0f, 0.0f);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, -0.05f);
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, -0.05f);
	glEnd();
	if (splitter == false)
	{

		Airoangale2 = Airoangale2 + 0.14f;
		if (Airoangale2 >= 0.0f)
		{
			Airoangale2 = 0.0f;

		}

	}
	
	if (splitter == false)
	{

		t2y = t2y - 0.009f;
		if (t2y <= 0.0f)
		{
			t2y = 0.0f;
			if (t2y <= 0.0f)
			{
				splitter = true;
			}
		}

	}

	t2x = t2x + 0.009f;
	if (t2x >= 18.0f)
	{
		t2x = 18.0f;
	}

	if (t2x >= 11.0f)
	{
		Airoangale2 = Airoangale2 + 0.13f;
		if (Airoangale2 >= 90.0f)
		{
			Airoangale2 = 90.0f;
		}
	}


	if (t2x >= 11.0f)
	{
		t2y = t2y + 0.009f;
		if (t2y >= 7.0f)
		{
			t2y = 7.0f;
		}
	}
	
}

void middlePlane()
{
	static GLfloat t1x = -18.0f, t1y = 0.0f, t1z = -22.0f, Airoangale1, r1x = 0.0f, r1y = 0.0f, r1z = 1.0f;
	//middle plane
//glColor3f(1.0f, 1.0f, 1.0f);
	glColor3f(0.7265f, 0.8828f, 0.9296f);

	glLoadIdentity();
	glTranslatef(t1x, t1y, t1z);

	glLineWidth(3.0f);
	glBegin(GL_QUADS);

	glVertex2f(0.4f, 0.2f);
	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-0.6f, -0.2f);
	glVertex2f(0.4f, -0.2f);

	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-1.0f, 0.4f);
	glVertex2f(-1.0f, -0.4f);
	glVertex2f(-0.6f, -0.2f);

	glVertex2f(0.2f, 0.2f);
	glVertex2f(-0.3f, 1.0f);
	glVertex2f(-0.3f, -1.0f);
	glVertex2f(0.2f, -0.2f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(0.4f, 0.2f);
	glVertex2f(0.4f, -0.2f);
	glVertex2f(1.0f, 0.0f);

	glEnd();
	//IAF for Middle Plane
	glLoadIdentity();
	glTranslatef(t1x, t1y, t1z);

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.3f, 0.0f);
	glVertex2f(0.1f, 0.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(-0.1f, 0.05f);
	glVertex2f(-0.1f, -0.05f);

	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.5f, 0.1f);
	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.3f, -0.1f);
	glVertex2f(-0.4f, 0.1f);
	glVertex2f(-0.4f, -0.05f);



	glEnd();

	//Back Flag Of Middle plane
	glLoadIdentity();
	glTranslatef(t1x, t1y, t1z);
	glBegin(GL_LINES);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.05f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 0.05f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(-1.0f, 0.0f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, -0.05f);
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, -0.05f);


	glEnd();
	t1x = t1x + 0.009f;
	if (t1x >= 22.0f)
	{
		t1x = 22.0f;
	}
	if (t1x >= 15.0f)
	{
		Airostopper = true ;
	//	AFlag();
	}
}
void bottomPlane()
{
	static GLfloat t3x = -18.0f, t3y = -6.0f, t3z = -22.0f, Airoangale3 = 90.0f, r3x = 0.0f, r3y = 0.0f, r3z = 1.0f;
	static bool Airostopper = false;
	static bool splitter = false;

	//bottom plane
	glColor3f(0.7265f, 0.8828f, 0.9296f);

	glLoadIdentity();
	glTranslatef(t3x, t3y, t3z);
	glRotatef(Airoangale3, r3x, r3y, r3z);
	glLineWidth(3.0f);
	glBegin(GL_QUADS);

	glVertex2f(0.4f, 0.2f);
	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-0.6f, -0.2f);
	glVertex2f(0.4f, -0.2f);

	glVertex2f(-0.6f, 0.2f);
	glVertex2f(-1.0f, 0.4f);
	glVertex2f(-1.0f, -0.4f);
	glVertex2f(-0.6f, -0.2f);

	glVertex2f(0.2f, 0.2f);
	glVertex2f(-0.3f, 1.0f);
	glVertex2f(-0.3f, -1.0f);
	glVertex2f(0.2f, -0.2f);

	glVertex2f(1.0f, 0.0f);
	glVertex2f(0.4f, 0.2f);
	glVertex2f(0.4f, -0.2f);
	glVertex2f(1.0f, 0.0f);

	glEnd();
	//IAF for Bottom Plane
	glLoadIdentity();
	glTranslatef(t3x, t3y, t3z);
	glRotatef(Airoangale3, r3x, r3y, r3z);

	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);

	glVertex2f(0.3f, 0.0f);
	glVertex2f(0.1f, 0.0f);

	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.0f, 0.0f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(-0.1f, 0.05f);
	glVertex2f(-0.1f, -0.05f);

	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.5f, 0.1f);
	glVertex2f(-0.3f, 0.1f);
	glVertex2f(-0.3f, -0.1f);
	glVertex2f(-0.4f, 0.1f);
	glVertex2f(-0.4f, -0.05f);



	glEnd();

	//Back Flag Of Bottom plane
	glLoadIdentity();
	glTranslatef(t3x, t3y, t3z);
	glRotatef(Airoangale3, r3x, r3y, r3z);

	glBegin(GL_LINES);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.05f);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 0.05f);

	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, 0.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(-1.0f, 0.0f);
	glLineWidth(15.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-2.5f, -0.05f);
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, -0.05f);


	glEnd();

	
	if (splitter == false)
	{

		Airoangale3 = Airoangale3 - 0.14f;
		if (Airoangale3 <= 0.0f)
		{
			Airoangale3 = 0.0f;

		}
	}
	

	if (splitter == false)
	{


		t3y = t3y + 0.009f;
		if (t3y >= 0.0f)
		{
			t3y = 0.0f;
			if (t3y >= 0.0f)
			{
				splitter = true;
			}
		}
	}


	t3x = t3x + 0.009f;
	if (t3x >= 18.0f)
	{
		t3x = 18.0f;
	}


	if (t3x >= 11.0f)
	{
		Airoangale3 = Airoangale3 - 0.13f;
		if (Airoangale3 <= -90.0f)
		{
			Airoangale3 = -90.0f;
		}
	}

	if (t3x >= 11.0f)
	{
		t3y = t3y - 0.009f;
		if (t3y <= -7.0f)
		{
			t3y = -7.0f;
		}
	}
}

void WholeFlag()
{
	static GLfloat x = 1.0f, y = 0.6f, z = 0.2f, a = 1.0f, b = 1.0f, c = 1.0f, d = 0.07f, e = 0.533f, f = 0.027f; //For  middle color flag
	static GLfloat t = -11.0f;
	static GLfloat tx = -4.0f,ty=0.0f, tz=-9.0f;
	static GLfloat p = -1.0f, q = 0.03f, p1 = -1.0f, q1 = 0.0f, p2 = -1.0f, q2 = -0.03f;
	static GLfloat atx = 4.0f, aty = -0.7f, atz = -8.0f, aty1 = 0.01f;

	glLoadIdentity();
	glTranslatef(tx, ty, tz);
	glLineWidth(2.5f);

	glBegin(GL_LINES);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.03f);

	glColor3f(x, y, z);
	glVertex2f(p, q);//-2.0, 0.025
	//glVertex2f(2.77f, 0.025f);
	//glVertex2f(3.23f, 0.025f);

	//glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);

	glColor3f(a, b, c);
	glVertex2f(p1, q1);//-2.0 , 0.0

	//glVertex2f(2.76f, 0.0f);
	//glVertex2f(3.24f, 0.0f);

	//glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex2f(-1.0f, -0.03f);

	glColor3f(d, e, f);
	glVertex2f(p2, q2);//-2.0f,  -0.02f

	//glVertex2f(2.75f, -0.02f);
	//glVertex2f(3.23f, -0.02f);
	glEnd();

	
	
	t = t + 0.0095f;
	if (t >= 50.0f)
	{
		t = 50.0f;
	}

	if (t >= 19.7f)
	{
		p = p + 0.0039f;
		p1 = p1 + 0.0039f;
		p2 = p2 + 0.0039f;
		if (p2 >= 8.0f)
		{
			p = 8.0f;
			p1 = 8.0f;
			p2 = 8.0f;
		}

	}
	
	
		if(t>=42.0f)
		{
			x = x - 0.001f;
			y = y - 0.0006f;
			z = z - 0.0002f;
			a = a - 0.001f;
			b = b - 0.001f;
			c = c - 0.001f;
			d = d - 0.00007f;
			e = e - 0.000533f;
			f = f - 0.000027f;
			if (f <= 0.00f)
			{
				x = 0.0f;
				y = 0.0f;
				z = 0.0f;
				a = 0.0f;
				b = 0.0f;
				c = 0.0f;
				d = 0.0f;
				e = 0.0f;
				f = 0.0f;
			}
		}

		if (t >= 42)
		{
			AFlag();
		}
	
}
void AFlag()
{
	glLoadIdentity();
	glTranslatef(-0.97f,0.0f,-3.0f);
	glLineWidth(2.5f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(1.97f,0.01f);
	glVertex2f(2.23f,0.01f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex2f(1.97f,0.0f);
	glVertex2f(2.23f,0.0f);
	
	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(1.97f,-0.01f);
	glVertex2f(2.23f,-0.01f);
	glEnd();
	//LeftI
	glLineWidth(5.0f);

	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);

	glVertex2f(-3.0f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-3.0f, 0.0f);
	glEnd();
	//RightI
	glLineWidth(5.0f);
	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);

	
	glVertex2f(1.5f, 1.5f);


	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(1.5f, 0.0f);
	glEnd();
	//D
	glLineWidth(5.0f);
	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-0.5f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-0.5f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(0.5f, 1.2f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(0.5f, 0.3f);
	glEnd();
	//N
	glLineWidth(5.0f);
	glLoadIdentity();
	glTranslatef(0.0f, -0.7f, -8.0f);

	glBegin(GL_LINES);
	
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-2.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-1.0f, 1.5f);

	glColor3f(0.07f, 0.533f, 0.027f);
	glVertex2f(-1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(-2.0f, 1.5f);


	glEnd();
}
void update(void)
{
	//code

}

void ToggleFullScreen()
{
	//variables declerations

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False); // Network protocol ...which is used to do window fullscreen remoetly and locally both
 	memset(&xev,0,sizeof(xev)); // All Members set to zero

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen? 0 : 1;

	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); // To Save New State Of Window
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen), // Pass This Event to XServer
		False,StructureNotifyMask,&xev);

}


void uninitialize()
{
	deleteAudioContext();
	StopSound();
	
	GLXContext CurrentGLXContext = glXGetCurrentContext();
	if(CurrentGLXContext!=NULL && CurrentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	} 
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}

}
void deleteAudioContext() { //Call it in uninitialize.

    alDeleteSources(1,&alSource);
    
    //delete our buffer
    alDeleteBuffers(1,&alSampleSet);
    
    context=alcGetCurrentContext();
    
    //Get device for active context
    device=alcGetContextsDevice(context);
    
    //Disable context
    alcMakeContextCurrent(NULL);
    
    //Release context(s)
    alcDestroyContext(context);
    
    //Close device
    alcCloseDevice(device);
}

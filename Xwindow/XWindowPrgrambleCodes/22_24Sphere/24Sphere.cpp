#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

//X11 header files

#include<X11/Xlib.h> //For All XServer Api
#include<X11/Xutil.h> // For XVisualInfo Structure
#include<X11/XKBlib.h> // For Keybord
#include<X11/keysym.h> // For KeyCode and Symbols Relationship


#include<GL/glew.h>

#include<GL/glu.h>

#include<GL/glx.h>

#include"vmath.h"

#include"Sphere.h"

using namespace vmath;
using namespace std;

//global variables

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];

//light values
//light values
GLfloat lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };


bool bFullScreen = false;
bool gbLighting = false;

Display *gpDisplay = NULL; // For Display
XVisualInfo *gpXVisualInfo = NULL;

//XVisualInfo gXVisualInfo; // Like pixelFormatDescriptor ..which store visual info...
Colormap gColormap; // it is structure and contains the color cells.. like pixelType
Window gWindow; // Like wndClass .. it is structure

//variables for PP
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_sphere;//vertex array object for rect
GLuint vbo_position_sphere;//vertex buffer object(position) for rect
GLuint vbo_normal_sphere;//vertex buffer object(color) for rect
GLuint vbo_elements_sphere;

//matrices
GLuint mUniform; // model matrix
GLuint vUniform; // view matrix
GLuint pUniform; //projection matrix

//lights
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;

//material
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShinynessUniform;


GLuint lightPositionUniform;
GLuint lKeyIsPressedUniform;

//sphere variables
unsigned int gNumVertices;
unsigned int gNumElements;

mat4 perspectiveProjectionMatrix;//This is from vmath

GLint keyPress = 0;

static GLfloat angleOfXRotation = 0.0f;
static GLfloat angleOfYRotation = 0.0f;
static GLfloat angleOfZRotation = 0.0f;


//enum decleration
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};



//function
void update(void);
void vertexShaderCode();
void fragmentShaderCode();
void createProgramCode();
void geomentryCode();
void uninitialize(void);

//function pointer
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

int giWindowWidth = 800; // For Window Width
int giWindowHeight = 600;  // For Window Height

static int winWidth = giWindowWidth;
static int winHeight = giWindowHeight;


//global functions
void update(void);

//entry point function
int main()
{
	//function declerations

	void CreateWindow();
	void ToggleFullScreen();
	void uninitialize();
	void initialize();
	void resize(int,int);
	void display();

	// Local variable 
	
	bool bDone = false;
	//code 

	CreateWindow();

	initialize();	
	//varibales requried in message loop
	char keys[26];

			
	XEvent event;
	KeySym keysym;
	
	//Message Loop
		while(bDone == false)
		{
			while(XPending(gpDisplay))
			{
				XNextEvent(gpDisplay,&event);  //address of event .. like &Msg
				switch(event.type)  //event.type takes all the event ..like (WM_) messages
				{	
					case MapNotify: // Like WM_CREATE .. it occures only onces..
						break;
					case KeyPress: // Like WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0); // This fun is in XKBlib.h...Convert this member event.xkey.keycode into keycode .. 0:by defaut english(Which code page want to use), 0: if press Shift key or not...
						switch(keysym)
						{
							case XK_Escape:
									printf("Escape key is pressed\n");
									//  uninitialize();
									//  exit(0);
									bDone = true;
								break;
							
						}
						
						XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
						switch(keys[0])
						{
						case 'F':
						case 'f':
							printf("F key is pressed\n");
									if(bFullScreen == false)
									{
										ToggleFullScreen();
										bFullScreen = true;
									}else
									{
										ToggleFullScreen();
										bFullScreen = false;
									}
						break;
						case 'L':
						case 'l':
							if (gbLighting == false)
							{
								gbLighting = true;
							}
							else {
								gbLighting = false;
							}
						break;
						case'x':
						case'X':
							keyPress = 1;
							angleOfXRotation = 0.0f;
							break;
						case'y':
						case'Y':
							keyPress = 2;
							angleOfXRotation = 0.0f;
							break;
						case'z':
						case'Z':
							keyPress = 3;
							angleOfXRotation = 0.0f;
							break;
						}
						break;	
						case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1: // left button
									printf("Left Button Preesed\n");
								break;
							case 2: // Middle button
									printf("Middle Button Preesed\n");
								break;
							case 3: // Right button
									printf("Right Button Preesed\n");
								break;
							case 4: // Mouse Wheel up
									printf("Mouse Wheel Up\n");
								break;
							case 5: // Mouse Wheel Down
									printf("Mouse Wheel Down\n");
								break;
						}
						break;
						case MotionNotify: // WM_MOUSEMOVE
							break;

						case ConfigureNotify: // WM_SIZE	
								winWidth = event.xconfigure.width;
								winHeight = event.xconfigure.height;
								resize(winWidth,winHeight);
							break;
						case Expose: // WM_PAINT
							break;
							case DestroyNotify: // WM_DESTROY
							break;
						case 33: // it is constant window manager message for close button or close menu
								printf("Close button is pressed\n");
							 // uninitialize();
							  //exit(0);
							
								bDone = true;
							break;
				}			
			}
		update();		
		display();
				
		}
		
	return(0);
			
}


// Functions Definations

void  CreateWindow()
{
	//local variables
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNoFBConfigs = 0;
	

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int wrostFrameBufferConfig = -1;
	int wrostNumberOfSamples = 999;


	//function decleration
	void uninitialize();

	// variables 
	XSetWindowAttributes winAttribs; // For set the Attributes

	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None

	};
	//code 
	// Step-1 :Connect with XServer and To get Display
	gpDisplay = XOpenDisplay(NULL);// Start the connection with default display structure 
	
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	//Step-2: To Get Default Screen
	defaultScreen = XDefaultScreen(gpDisplay);


	//Retrive all FbConfigs fronm drivers
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&iNoFBConfigs);

	for(int i = 0; i < iNoFBConfigs; i++)
	{
		//To check the capability.. here we take one temp variable
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);

		if(pTempXVisualInfo)
		{
			//variables
			int sampleBuffers,samples;
			//Get Number of samples buffers from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);

			//Get Number of samples  from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			//Here we serach best and wrost FBConfig from driver
			
			//for best
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}
			//for wrost
			if(wrostFrameBufferConfig < 0 || (!sampleBuffers) || samples < wrostNumberOfSamples)
			{
				wrostFrameBufferConfig = i;
				wrostNumberOfSamples = samples;
			} 
		}
		//free
		XFree(pTempXVisualInfo);
	}

	//Assign the best one 
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	//Assign same best to global FBConfig
	gGLXFBConfig = bestGLXFBConfig;

	//free gGlXFBConfig Array
	XFree(gGLXFBConfig);

	//Here we get BEST Visual info

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);



	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),
		gpXVisualInfo->visual,AllocNone);

	
	gColormap = winAttribs.colormap;

	
	winAttribs.event_mask = VisibilityChangeMask | ExposureMask | ButtonPressMask | KeyPressMask | 
		PointerMotionMask | StructureNotifyMask ;

	
	//Step-6: To Fill Window Style
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//Step-7: Create Actual Window To Using All Above Things
	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,
		giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,
		gpXVisualInfo->visual,styleMask,&winAttribs);


	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	
	//Step-8: Give Name To Window
	XStoreName(gpDisplay,gWindow,"First XWindow");


	//Step-9: This is Constant 33 message
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	//Step-10: Map Our Window On Default Window Given By XServer
	XMapWindow(gpDisplay,gWindow);
}

void initialize()
{
	void uninitialize();
	void resize(int, int);


	GLenum result;

	//Getting Context steps
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB"); 
	if(glXGetProcAddressARB == NULL)
	{
		printf("Can not get context address\n");
		uninitialize();	
	}

	// now get context 
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,0,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	
	if(!gGLXContext)
	{
			GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
		
 	}

 	//check wheather the context is h/w rendering context or not 

 	if(!glXIsDirect(gpDisplay,gGLXContext))
 	{
 		printf("The context is not hardware rendering\n");
 	}
 	else
 	{
 		printf("The context is hardware rendering\n");	
 	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("glewInit Failed!!\n");
		uninitialize();
	}

	//GLEW initialization Code for GLSL (IMPORTANT : It must be here Means After 
	// creating OpenGL context But before using	any OpenGL function

	GLenum glew_error = glewInit();
	printf("GL_Version : %s \n", (char*)glGetString(GL_VERSION));

	printf("GL_Shading_Language_Version : %s \n", (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	
	//call vertex shader function 
	vertexShaderCode();

	//call fragment shader function
	fragmentShaderCode();

	//call create Program code function
	createProgramCode();
	
	//Postlinking Retriving Uniform locations
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_materialShine");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	
	//call geomentry code function
	geomentryCode();
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.25f, 0.25f, 0.25f, 0.0f);

	//Give identity toorthographicProjectMatrix
	perspectiveProjectionMatrix = mat4::identity();

}

void vertexShaderCode()
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec4 u_lightPosition;" \
		"out vec3 tNorm;" \
		"out vec3 lightDirection;" \
		"out vec3 viwerVector;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
		"tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" \
		"lightDirection = vec3(u_lightPosition - eye_Coordinate);" \
		"viwerVector = -eye_Coordinate.xyz;" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}"
	};


	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

}

void fragmentShaderCode()
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 lightDirection;" \
		"in vec3 viwerVector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_materialShine;" \
		"uniform vec4 u_lightPosition;" \
		"uniform int u_lKeyIsPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec3 normalizeTNorm = normalize(tNorm);" \
		"vec3 normalizeLightDirection = normalize(lightDirection);" \
		"vec3 normalizeViwerVector = normalize(viwerVector);" \
		"float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" \
		"vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" \
		"vec3 ambiant = vec3(u_la * u_ka);" \
		"vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" \
		"vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" \
		"vec3 phong_ads_light = ambiant + diffuse + specular;" \
		"FragColor = vec4(phong_ads_light,1.0f);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
		"}" \
		"}"
	};
	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

}




void createProgramCode()
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding of vertex shader
	
	//----- Position
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	// ---- Normal
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking For Program Object

	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

}


void geomentryCode()
{
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

	//****************************** CUBE ****************************
	//Create vao for rect
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//################### POSITION ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and arra for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//################### NORMAL ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);


	//########################## Elements ###############
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_elements_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	
	////how many slots my array is break
	//glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	////Enabling the position
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);

}

void resize(int width, int height)
{
	int xWindowCenter = winWidth / 2;
	int yWindowCenter = winHeight / 2;
	
	int distorasionX = winWidth/6;
	int distorasionY = winHeight/8;
	
	
	if (height == 0)
	{
		height = 1;
	}

	
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)distorasionX / (GLfloat)distorasionY,
		0.1f,
		100.0f);
}

void display()
{
	
	void draw24Sphers();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Using program object 
	glUseProgram(gShaderProgramObject);
	
	if (gbLighting == true)
	{
		glUniform1i(lKeyIsPressedUniform, 1);
		glUniform3fv(laUniform, 1, lightAmbiant);
		glUniform3fv(ldUniform, 1, lightDiffuse);
		glUniform3fv(lsUniform, 1, lightSpecular);
		if (keyPress == 1)
		{
			lightPosition[1] = cos(angleOfXRotation) * 100.0f;
			lightPosition[2] = -sin(angleOfXRotation) * 100.0f;
			
			glUniform4fv(lightPositionUniform, 1, lightPosition);
		}else if(keyPress == 2)
		{
			lightPosition[0] = cos(angleOfYRotation) * 100.0f;
			lightPosition[2] = -sin(angleOfYRotation) * 100.0f;
		
			glUniform4fv(lightPositionUniform, 1, lightPosition);
		}else if(keyPress==3) {

			lightPosition[0] = cos(angleOfZRotation) * 100.0f;
			lightPosition[1] = sin(angleOfZRotation) * 100.0f;
			
			glUniform4fv(lightPositionUniform, 1, lightPosition);
		}
	}
	else {
		glUniform1i(lKeyIsPressedUniform, 0);
	}

	draw24Sphers();

	//Unused Program
	glUseProgram(0);
	glXSwapBuffers(gpDisplay,gWindow);
}

void draw24Sphers()
{
	int xWindowCenter = winWidth / 2;
	int yWindowCenter = winHeight / 2;


	int distorasionX = winWidth / 6;
	int distorasionY = winHeight / 8;

	int xTransOffset = distorasionX;
	int yTransOffset = distorasionY;

	int currentViewportX;
	int currentViewportY;

	mat4 viewMatrix;
	mat4 modelMatrix;
	mat4 translationMatrix;
	/*mat4 rotationMatrix;
	mat4 scaleMatrix;*/
	mat4 projectionMatrix;


	GLfloat MaterialAmbiant[4];
	GLfloat MaterialDiffuse[4];
	GLfloat MaterialSpecular[4];
	GLfloat MaterialShininess[1];

	//*********************************** SPHERE 1 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	
	

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;

	
	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);

	
	MaterialAmbiant[0] = 0.0215f;
	MaterialAmbiant[1] = 0.1745f;
	MaterialAmbiant[2] = 0.0215f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.07568f;
	MaterialDiffuse[1] = 0.61424f;
	MaterialDiffuse[2] = 0.0215f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.633f;
	MaterialSpecular[1] = 0.727811f;
	MaterialSpecular[2] = 0.633f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);


	//*********************************** SPHERE 2 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);

	
	MaterialAmbiant[0] = 0.135f;
	MaterialAmbiant[1] = 0.2225f;
	MaterialAmbiant[2] = 0.1575f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.54f;
	MaterialDiffuse[1] = 0.89f;
	MaterialDiffuse[2] = 0.63f;
	MaterialDiffuse[3] = 1.0f;
	

	MaterialSpecular[0] = 0.316228f;
	MaterialSpecular[1] = 0.316228f;
	MaterialSpecular[2] = 0.316228f;
	MaterialSpecular[3] = 1.0f;
	

	MaterialShininess[0] = 0.1f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 3 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.5375f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.06625f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.18275f;
	MaterialDiffuse[1] = 0.17f;
	MaterialDiffuse[2] = 0.22525f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.332741f;
	MaterialSpecular[1] = 0.328634f;
	MaterialSpecular[2] = 0.346435f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.3f * 128.0f;
	
	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);


	//*********************************** SPHERE 4 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.20725f;
	MaterialAmbiant[2] = 0.20725f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 1.0f;
	MaterialDiffuse[1] = 0.829f;
	MaterialDiffuse[2] = 0.829f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.296648f;
	MaterialSpecular[1] = 0.296648f;
	MaterialSpecular[2] = 0.296648f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.088f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 5 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.1745f;
	MaterialAmbiant[1] = 0.01175f;
	MaterialAmbiant[2] = 0.01175f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.61424f;
	MaterialDiffuse[1] = 0.04136f;
	MaterialDiffuse[2] = 0.04136f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.727811f;
	MaterialSpecular[1] = 0.626959f;
	MaterialSpecular[2] = 0.626959f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.6f * 128.0f;
	

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 6 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 2.5;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.1f;
	MaterialAmbiant[1] = 0.18725f;
	MaterialAmbiant[2] = 0.1745f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.396f;
	MaterialDiffuse[1] = 0.74151f;
	MaterialDiffuse[2] = 0.69102f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.297254f;
	MaterialSpecular[1] = 0.30829f;
	MaterialSpecular[2] = 0.306678f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.1f * 128.0f;
	

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 7 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.329412f;
	MaterialAmbiant[1] = 0.223529f;
	MaterialAmbiant[2] = 0.27451f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.780392f;
	MaterialDiffuse[1] = 0.568627f;
	MaterialDiffuse[2] = 0.113725f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.992157f;
	MaterialSpecular[1] = 0.941176f;
	MaterialSpecular[2] = 0.807843f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.21794872f * 128.0f;
	

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 8 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.2125f;
	MaterialAmbiant[1] = 0.1275f;
	MaterialAmbiant[2] = 0.054f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.714f;
	MaterialDiffuse[1] = 0.4284f;
	MaterialDiffuse[2] = 0.18144f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.393548f;
	MaterialSpecular[1] = 0.271906f;
	MaterialSpecular[2] = 0.166721f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.2f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 9 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.25f;
	MaterialAmbiant[1] = 0.25f;
	MaterialAmbiant[2] = 0.25f;
	MaterialAmbiant[3] = 1.0f;

	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;

	MaterialSpecular[0] = 0.774597f;
	MaterialSpecular[1] = 0.774597f;
	MaterialSpecular[2] = 0.774597f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.6f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 10 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.19125f;
	MaterialAmbiant[1] = 0.0735f;
	MaterialAmbiant[2] = 0.02225f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.7038f;
	MaterialDiffuse[1] = 0.27048f;
	MaterialDiffuse[2] = 0.0828f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.256777f;
	MaterialSpecular[1] = 0.137622f;
	MaterialSpecular[2] = 0.086014f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.1f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 11 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.24725f;
	MaterialAmbiant[1] = 0.1995f;
	MaterialAmbiant[2] = 0.0745f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.75164f;
	MaterialDiffuse[1] = 0.60648f;
	MaterialDiffuse[2] = 0.22648f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.628281f;
	MaterialSpecular[1] = 0.555802f;
	MaterialSpecular[2] = 0.366065f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.4f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 12 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * 1.1;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.19225f;
	MaterialAmbiant[1] = 0.19225f;
	MaterialAmbiant[2] = 0.19225f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.50754f;
	MaterialDiffuse[1] = 0.50754f;
	MaterialDiffuse[2] = 0.50754f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.508273f;
	MaterialSpecular[1] = 0.508273f;
	MaterialSpecular[2] = 0.508273f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.4f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 13 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.50f;
	MaterialSpecular[1] = 0.50f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 14 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.1f;
	MaterialAmbiant[2] = 0.06f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.0f;
	MaterialDiffuse[1] = 0.50980392f;
	MaterialDiffuse[2] = 0.50980392f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.50196078f;
	MaterialSpecular[1] = 0.50196078f;
	MaterialSpecular[2] = 0.50196078f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 15 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.1f;
	MaterialDiffuse[1] = 0.35f;
	MaterialDiffuse[2] = 0.1f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.45f;
	MaterialSpecular[1] = 0.55f;
	MaterialSpecular[2] = 0.45f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 16 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.0f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.6f;
	MaterialSpecular[2] = 0.6f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;
	
	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 17 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.55f;
	MaterialDiffuse[1] = 0.55f;
	MaterialDiffuse[2] = 0.55f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.70f;
	MaterialSpecular[1] = 0.70f;
	MaterialSpecular[2] = 0.70f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 18 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -0.1;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.0f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.60f;
	MaterialSpecular[1] = 0.60f;
	MaterialSpecular[2] = 0.50f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.25f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 19 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.02f;
	MaterialAmbiant[1] = 0.02f;
	MaterialAmbiant[2] = 0.02f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.01f;
	MaterialDiffuse[1] = 0.01f;
	MaterialDiffuse[2] = 0.01f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.4f;
	MaterialSpecular[1] = 0.4f;
	MaterialSpecular[2] = 0.4f;
	MaterialSpecular[3] = 1.0f;

	MaterialShininess[0] = 0.078125f * 128.0f;
	
	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 20 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 21 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * 0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.0f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.4f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.04f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 22 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -0.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.0f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.4f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.04f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 23 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -1.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.05f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.5f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.7f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

	//*********************************** SPHERE 24 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	//rotationMatrix = mat4::identity();
	//scaleMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(0.0f, 0.0f, -1.8f);
	//rotationMatrix = rotate(0.0f, angleCube, 0.0f);

	//Do neccessary Matrix Multilication
	modelMatrix = translationMatrix;
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	glUniformMatrix4fv(mUniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(vUniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);

	currentViewportX = xWindowCenter - xTransOffset * -1.5;
	currentViewportY = yWindowCenter + yTransOffset * -2.5;


	glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


	MaterialAmbiant[0] = 0.05f;
	MaterialAmbiant[1] = 0.05f;
	MaterialAmbiant[2] = 0.0f;
	MaterialAmbiant[3] = 1.0f;
	
	MaterialDiffuse[0] = 0.5f;
	MaterialDiffuse[1] = 0.5f;
	MaterialDiffuse[2] = 0.4f;
	MaterialDiffuse[3] = 1.0f;
	
	MaterialSpecular[0] = 0.7f;
	MaterialSpecular[1] = 0.7f;
	MaterialSpecular[2] = 0.04f;
	MaterialSpecular[3] = 1.0f;
	
	MaterialShininess[0] = 0.078125f * 128.0f;

	glUniform3fv(kaUniform, 1, MaterialAmbiant);
	glUniform3fv(kdUniform, 1, MaterialDiffuse);
	glUniform3fv(ksUniform, 1, MaterialSpecular);
	glUniform1fv(materialShinynessUniform, 1, MaterialShininess);


	glBindVertexArray(vao_sphere);

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLE_STRIP, gNumElements, GL_UNSIGNED_SHORT, 0);
	//Unbind vao of rectangle
	glBindVertexArray(0);

}
void update(void)
{
	angleOfXRotation = angleOfXRotation + 0.03f;
	if (angleOfXRotation >= 360.0f)
	{
		angleOfXRotation = 0.0f;
	}
	angleOfYRotation = angleOfYRotation + 0.03f;
	if (angleOfYRotation >= 360.0f)
	{
		angleOfYRotation = 0.0f;
	}
	angleOfZRotation = angleOfZRotation + 0.03f;
	if (angleOfZRotation >= 360.0f)
	{
		angleOfZRotation = 0.0f;
	}


}

void ToggleFullScreen()
{
	//variables declerations

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False); // Network protocol ...which is used to do window fullscreen remoetly and locally both
 	memset(&xev,0,sizeof(xev)); // All Members set to zero

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen? 0 : 1;

	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); // To Save New State Of Window
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen), // Pass This Event to XServer
		False,StructureNotifyMask,&xev);

}


void uninitialize()
{
	if (vbo_elements_sphere)
	{
		glDeleteBuffers(1, &vbo_elements_sphere);
		vbo_elements_sphere = 0;
	}

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}

	if (vbo_position_sphere)
	{
		glDeleteVertexArrays(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	GLXContext CurrentGLXContext = glXGetCurrentContext();
	if(CurrentGLXContext!=NULL && CurrentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	} 
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}

}

#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

//X11 header files

#include<X11/Xlib.h> //For All XServer Api
#include<X11/Xutil.h> // For XVisualInfo Structure
#include<X11/XKBlib.h> // For Keybord
#include<X11/keysym.h> // For KeyCode and Symbols Relationship


#include<GL/glew.h>

#include<GL/glu.h>

#include<GL/glx.h>

#include"vmath.h"

using namespace vmath;
using namespace std;

//global variables

bool bFullScreen = false;
Display *gpDisplay = NULL; // For Display
XVisualInfo *gpXVisualInfo = NULL;

//XVisualInfo gXVisualInfo; // Like pixelFormatDescriptor ..which store visual info...
Colormap gColormap; // it is structure and contains the color cells.. like pixelType
Window gWindow; // Like wndClass .. it is structure

//variables foe FBConfig

//function pointer
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

int giWindowWidth = 800; // For Window Width
int giWindowHeight = 600;  // For Window Height

static int winWidth = giWindowWidth;
static int winHeight = giWindowHeight;
static GLfloat Rotateangle = 0.0f;

mat4 perspectiveProjectionMatrix;//This is from vmath


//global functions
void update(void);

//entry point function
int main()
{
	//function declerations

	void CreateWindow();
	void ToggleFullScreen();
	void uninitialize();
	void initialize();
	void resize(int,int);
	void display();

	// Local variable 
	
	bool bDone = false;
	//code 

	CreateWindow();

	initialize();	
	//varibales requried in message loop
	char keys[26];

			
	XEvent event;
	KeySym keysym;
	
	//Message Loop
		while(bDone == false)
		{
			while(XPending(gpDisplay))
			{
				XNextEvent(gpDisplay,&event);  //address of event .. like &Msg
				switch(event.type)  //event.type takes all the event ..like (WM_) messages
				{	
					case MapNotify: // Like WM_CREATE .. it occures only onces..
						break;
					case KeyPress: // Like WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0); // This fun is in XKBlib.h...Convert this member event.xkey.keycode into keycode .. 0:by defaut english(Which code page want to use), 0: if press Shift key or not...
						switch(keysym)
						{
							case XK_Escape:
									printf("Escape key is pressed\n");
									//  uninitialize();
									//  exit(0);
									bDone = true;
								break;
							
						}
						
						XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
						switch(keys[0])
						{
						case 'F':
						case 'f':
							printf("F key is pressed\n");
									if(bFullScreen == false)
									{
										ToggleFullScreen();
										bFullScreen = true;
									}else
									{
										ToggleFullScreen();
										bFullScreen = false;
									}
						break;
					
						}
						break;	
						case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1: // left button
									printf("Left Button Preesed\n");
								break;
							case 2: // Middle button
									printf("Middle Button Preesed\n");
								break;
							case 3: // Right button
									printf("Right Button Preesed\n");
								break;
							case 4: // Mouse Wheel up
									printf("Mouse Wheel Up\n");
								break;
							case 5: // Mouse Wheel Down
									printf("Mouse Wheel Down\n");
								break;
						}
						break;
						case MotionNotify: // WM_MOUSEMOVE
							break;

						case ConfigureNotify: // WM_SIZE	
								winWidth = event.xconfigure.width;
								winHeight = event.xconfigure.height;
								resize(winWidth,winHeight);
							break;
						case Expose: // WM_PAINT
							break;
							case DestroyNotify: // WM_DESTROY
							break;
						case 33: // it is constant window manager message for close button or close menu
								printf("Close button is pressed\n");
							 // uninitialize();
							  //exit(0);
							
								bDone = true;
							break;
				}			
			}
		update();		
		display();
				
		}
		
	return(0);
			
}


// Functions Definations

void  CreateWindow()
{
	//local variables
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNoFBConfigs = 0;
	

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int wrostFrameBufferConfig = -1;
	int wrostNumberOfSamples = 999;


	//function decleration
	void uninitialize();

	// variables 
	XSetWindowAttributes winAttribs; // For set the Attributes

	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None

	};
	//code 
	// Step-1 :Connect with XServer and To get Display
	gpDisplay = XOpenDisplay(NULL);// Start the connection with default display structure 
	
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	//Step-2: To Get Default Screen
	defaultScreen = XDefaultScreen(gpDisplay);


	//Retrive all FbConfigs fronm drivers
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&iNoFBConfigs);

	for(int i = 0; i < iNoFBConfigs; i++)
	{
		//To check the capability.. here we take one temp variable
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);

		if(pTempXVisualInfo)
		{
			//variables
			int sampleBuffers,samples;
			//Get Number of samples buffers from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);

			//Get Number of samples  from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			//Here we serach best and wrost FBConfig from driver
			
			//for best
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}
			//for wrost
			if(wrostFrameBufferConfig < 0 || (!sampleBuffers) || samples < wrostNumberOfSamples)
			{
				wrostFrameBufferConfig = i;
				wrostNumberOfSamples = samples;
			} 
		}
		//free
		XFree(pTempXVisualInfo);
	}

	//Assign the best one 
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	//Assign same best to global FBConfig
	gGLXFBConfig = bestGLXFBConfig;

	//free gGlXFBConfig Array
	XFree(gGLXFBConfig);

	//Here we get BEST Visual info

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);



	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),
		gpXVisualInfo->visual,AllocNone);

	
	gColormap = winAttribs.colormap;

	
	winAttribs.event_mask = VisibilityChangeMask | ExposureMask | ButtonPressMask | KeyPressMask | 
		PointerMotionMask | StructureNotifyMask ;

	
	//Step-6: To Fill Window Style
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//Step-7: Create Actual Window To Using All Above Things
	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,
		giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,
		gpXVisualInfo->visual,styleMask,&winAttribs);


	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	
	//Step-8: Give Name To Window
	XStoreName(gpDisplay,gWindow,"First XWindow");


	//Step-9: This is Constant 33 message
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	//Step-10: Map Our Window On Default Window Given By XServer
	XMapWindow(gpDisplay,gWindow);
}

void initialize()
{
	void uninitialize();
	void resize(int, int);


	GLenum result;

	//Getting Context steps
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB"); 
	if(glXGetProcAddressARB == NULL)
	{
		printf("Can not get context address\n");
		uninitialize();	
	}

	// now get context 
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,0,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	
	if(!gGLXContext)
	{
			GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
		
 	}

 	//check wheather the context is h/w rendering context or not 

 	if(!glXIsDirect(gpDisplay,gGLXContext))
 	{
 		printf("The context is not hardware rendering\n");
 	}
 	else
 	{
 		printf("The context is hardware rendering\n");	
 	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("glewInit Failed!!\n");
		uninitialize();
	}
	glClearColor(0.0f, 0.0f, 1.0f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	


}


void resize(int width, int height)
{
	
if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
												width / height,
												0.1f,
												100.0f);
}

void display()
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	glXSwapBuffers(gpDisplay,gWindow);
}

void update(void)
{
	//code

}

void ToggleFullScreen()
{
	//variables declerations

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False); // Network protocol ...which is used to do window fullscreen remoetly and locally both
 	memset(&xev,0,sizeof(xev)); // All Members set to zero

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen? 0 : 1;

	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); // To Save New State Of Window
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen), // Pass This Event to XServer
		False,StructureNotifyMask,&xev);

}


void uninitialize()
{
	GLXContext CurrentGLXContext = glXGetCurrentContext();
	if(CurrentGLXContext!=NULL && CurrentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	} 
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}

}
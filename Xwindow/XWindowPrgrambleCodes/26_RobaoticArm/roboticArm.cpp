#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

//X11 header files

#include<X11/Xlib.h> //For All XServer Api
#include<X11/Xutil.h> // For XVisualInfo Structure
#include<X11/XKBlib.h> // For Keybord
#include<X11/keysym.h> // For KeyCode and Symbols Relationship


#include<GL/glew.h>

#include<GL/glu.h>

#include<GL/glx.h>

#include"vmath.h"

#include"Sphere.h"

using namespace vmath;
using namespace std;

//global variables

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];

bool bFullScreen = false;
bool gbLighting = false;

Display *gpDisplay = NULL; // For Display
XVisualInfo *gpXVisualInfo = NULL;

//XVisualInfo gXVisualInfo; // Like pixelFormatDescriptor ..which store visual info...
Colormap gColormap; // it is structure and contains the color cells.. like pixelType
Window gWindow; // Like wndClass .. it is structure

//variables for PP
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_sphere;//vertex array object for rect
GLuint vbo_position_sphere;//vertex buffer object(position) for rect
GLuint vbo_normal_sphere;//vertex buffer object(color) for rect
GLuint vbo_elements_sphere;

GLuint mvUniform; // model view matrix
GLuint pUniform; //projection matrix
GLuint ldUniform;
GLuint kdUniform;
GLuint lightPositionUniform;
GLuint lKeyIsPressedUniform;

unsigned int gNumVertices;
unsigned int gNumElements;

mat4 perspectiveProjectionMatrix;//This is from vmath


//Animation varibales
GLfloat angleCube = 0.0f;
int shoulder = 0;
int elbow = 0;
//enum decleration
enum {
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};



//function
void update(void);
void vertexShaderCode();
void fragmentShaderCode();
void createProgramCode();
void geomentryCode();
void uninitialize(void);

//function pointer
typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glXCreateContextAttribsARBProc glXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;
GLXContext gGLXContext;

int giWindowWidth = 800; // For Window Width
int giWindowHeight = 600;  // For Window Height

static int winWidth = giWindowWidth;
static int winHeight = giWindowHeight;
static GLfloat Rotateangle = 0.0f;

//global functions
void update(void);

//entry point function
int main()
{
	//function declerations

	void CreateWindow();
	void ToggleFullScreen();
	void uninitialize();
	void initialize();
	void resize(int,int);
	void display();

	// Local variable 
	
	bool bDone = false;
	//code 

	CreateWindow();

	initialize();	
	//varibales requried in message loop
	char keys[26];

			
	XEvent event;
	KeySym keysym;
	
	//Message Loop
		while(bDone == false)
		{
			while(XPending(gpDisplay))
			{
				XNextEvent(gpDisplay,&event);  //address of event .. like &Msg
				switch(event.type)  //event.type takes all the event ..like (WM_) messages
				{	
					case MapNotify: // Like WM_CREATE .. it occures only onces..
						break;
					case KeyPress: // Like WM_KEYDOWN
					keysym = XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0); // This fun is in XKBlib.h...Convert this member event.xkey.keycode into keycode .. 0:by defaut english(Which code page want to use), 0: if press Shift key or not...
						switch(keysym)
						{
							case XK_Escape:
									printf("Escape key is pressed\n");
									//  uninitialize();
									//  exit(0);
									bDone = true;
								break;
							
						}
						
						XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);
						switch(keys[0])
						{
						case 'F':
						case 'f':
							printf("F key is pressed\n");
									if(bFullScreen == false)
									{
										ToggleFullScreen();
										bFullScreen = true;
									}else
									{
										ToggleFullScreen();
										bFullScreen = false;
									}
						break;
						case 'L':
						case 'l':
							if (gbLighting == false)
							{
								gbLighting = true;
							}
							else {
								gbLighting = false;
							}
						break;
						case 'S':
							shoulder = (shoulder + 3) % 360;
							break;
						case 's':
							shoulder = (shoulder - 3) % 360;
							break;
						case 'E':
							elbow = (elbow + 3) % 360;
							break;
						case 'e':
							elbow = (elbow - 3) % 360;
							break;
						}
						break;	
						case ButtonPress:
						switch(event.xbutton.button)
						{
							case 1: // left button
									printf("Left Button Preesed\n");
								break;
							case 2: // Middle button
									printf("Middle Button Preesed\n");
								break;
							case 3: // Right button
									printf("Right Button Preesed\n");
								break;
							case 4: // Mouse Wheel up
									printf("Mouse Wheel Up\n");
								break;
							case 5: // Mouse Wheel Down
									printf("Mouse Wheel Down\n");
								break;
						}
						break;
						case MotionNotify: // WM_MOUSEMOVE
							break;

						case ConfigureNotify: // WM_SIZE	
								winWidth = event.xconfigure.width;
								winHeight = event.xconfigure.height;
								resize(winWidth,winHeight);
							break;
						case Expose: // WM_PAINT
							break;
							case DestroyNotify: // WM_DESTROY
							break;
						case 33: // it is constant window manager message for close button or close menu
								printf("Close button is pressed\n");
							 // uninitialize();
							  //exit(0);
							
								bDone = true;
							break;
				}			
			}
		update();		
		display();
				
		}
		
	return(0);
			
}


// Functions Definations

void  CreateWindow()
{
	//local variables
	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNoFBConfigs = 0;
	

	int bestFrameBufferConfig = -1;
	int bestNumberOfSamples = -1;
	int wrostFrameBufferConfig = -1;
	int wrostNumberOfSamples = 999;


	//function decleration
	void uninitialize();

	// variables 
	XSetWindowAttributes winAttribs; // For set the Attributes

	int defaultScreen;
	int defaultDepth;
	int styleMask;
	
	static int frameBufferAttributes[]={
		GLX_X_RENDERABLE,True,
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_STENCIL_SIZE,8,
		GLX_DOUBLEBUFFER,True,
		GLX_RED_SIZE,8,
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None

	};
	//code 
	// Step-1 :Connect with XServer and To get Display
	gpDisplay = XOpenDisplay(NULL);// Start the connection with default display structure 
	
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	//Step-2: To Get Default Screen
	defaultScreen = XDefaultScreen(gpDisplay);


	//Retrive all FbConfigs fronm drivers
	pGLXFBConfig = glXChooseFBConfig(gpDisplay,defaultScreen,frameBufferAttributes,&iNoFBConfigs);

	for(int i = 0; i < iNoFBConfigs; i++)
	{
		//To check the capability.. here we take one temp variable
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,pGLXFBConfig[i]);

		if(pTempXVisualInfo)
		{
			//variables
			int sampleBuffers,samples;
			//Get Number of samples buffers from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);

			//Get Number of samples  from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			//Here we serach best and wrost FBConfig from driver
			
			//for best
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNumberOfSamples = samples;
			}
			//for wrost
			if(wrostFrameBufferConfig < 0 || (!sampleBuffers) || samples < wrostNumberOfSamples)
			{
				wrostFrameBufferConfig = i;
				wrostNumberOfSamples = samples;
			} 
		}
		//free
		XFree(pTempXVisualInfo);
	}

	//Assign the best one 
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];

	//Assign same best to global FBConfig
	gGLXFBConfig = bestGLXFBConfig;

	//free gGlXFBConfig Array
	XFree(gGLXFBConfig);

	//Here we get BEST Visual info

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay,bestGLXFBConfig);



	winAttribs.border_pixel = 0;
	winAttribs.border_pixmap = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.background_pixel = BlackPixel(gpDisplay,defaultScreen);

	winAttribs.colormap = XCreateColormap(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),
		gpXVisualInfo->visual,AllocNone);

	
	gColormap = winAttribs.colormap;

	
	winAttribs.event_mask = VisibilityChangeMask | ExposureMask | ButtonPressMask | KeyPressMask | 
		PointerMotionMask | StructureNotifyMask ;

	
	//Step-6: To Fill Window Style
	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	//Step-7: Create Actual Window To Using All Above Things
	gWindow = XCreateWindow(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen),0,0,
		giWindowWidth,giWindowHeight,0,gpXVisualInfo->depth,InputOutput,
		gpXVisualInfo->visual,styleMask,&winAttribs);


	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now..\n");
		uninitialize();
		exit(1);
	}

	
	//Step-8: Give Name To Window
	XStoreName(gpDisplay,gWindow,"First XWindow");


	//Step-9: This is Constant 33 message
	Atom windowManagerDelete = XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	//Step-10: Map Our Window On Default Window Given By XServer
	XMapWindow(gpDisplay,gWindow);
}


int MAXSIZE = 8;
mat4 stack[20];
static int top = -1;

int isempty() {

	if (top == -1)
		return 1;
	else
		return 0;
}

int isfull() {

	if (top == MAXSIZE)
		return 1;
	else
		return 0;
}
mat4 pop() {
	mat4 data;

	if (!isempty()) {
		data = stack[top];
		top = top - 1;
		return data;
	}
	else {
		printf("Could not retrieve data, Stack is empty.\n");
	}
}

void push(mat4 data) {

	if (!isfull()) {
		top = top + 1;
		stack[top] = data;
		//return stack[top];
	}
	else {
		printf("Could not insert data, Stack is full.\n");
	}
}

void initialize()
{
	void uninitialize();
	void resize(int, int);


	GLenum result;

	//Getting Context steps
	glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB"); 
	if(glXGetProcAddressARB == NULL)
	{
		printf("Can not get context address\n");
		uninitialize();	
	}

	// now get context 
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,0,
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
	
	if(!gGLXContext)
	{
			GLint attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,
			None
		};
		gGLXContext = glXCreateContextAttribsARB(gpDisplay,gGLXFBConfig,0,True,attribs);
		
 	}

 	//check wheather the context is h/w rendering context or not 

 	if(!glXIsDirect(gpDisplay,gGLXContext))
 	{
 		printf("The context is not hardware rendering\n");
 	}
 	else
 	{
 		printf("The context is hardware rendering\n");	
 	}
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);

	result = glewInit();
	if (result != GLEW_OK)
	{
		printf("glewInit Failed!!\n");
		uninitialize();
	}

	//GLEW initialization Code for GLSL (IMPORTANT : It must be here Means After 
	// creating OpenGL context But before using	any OpenGL function

	GLenum glew_error = glewInit();
	printf("GL_Version : %s \n", (char*)glGetString(GL_VERSION));

	printf("GL_Shading_Language_Version : %s \n", (char*)glGetString(GL_SHADING_LANGUAGE_VERSION));
	
	
	//call vertex shader function 
	vertexShaderCode();

	//call fragment shader function
	fragmentShaderCode();

	//call create Program code function
	createProgramCode();
	
	//Postlinking Retriving Uniform locations
	mvUniform = glGetUniformLocation(gShaderProgramObject, "u_mv_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosition");
	lKeyIsPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyIsPressed");
	
	//call geomentry code function
	geomentryCode();
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	//Give identity toorthographicProjectMatrix
	perspectiveProjectionMatrix = mat4::identity();

}

void vertexShaderCode()
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define vertex shader object

	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	//write vertex shader code
	const GLchar *vertexShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_mv_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform int u_lKeyIsPressed;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_kd;" \
		"uniform vec4 u_lightPosition;" \
		"out vec3 diffuseColor;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"vec4 eye_Coordinate = u_mv_matrix * vPosition;" \
		"mat3 normalMatrix = mat3(transpose(inverse(u_mv_matrix)));" \
		"vec3 tNorm = normalize(normalMatrix * vNormal);" \
		"vec3 s = vec3(normalize(u_lightPosition - eye_Coordinate));" \
		"diffuseColor = u_ld * u_kd * dot(s,tNorm);" \
		"}" \
		"gl_Position = u_p_matrix * u_mv_matrix * vPosition;" \
		"}"
	};


	//specify source code to vertex shader
	glShaderSource(gVertexShaderObject, 1,
		(const GLchar **)& vertexShaderSourceCode,
		NULL);

	//compile the vertex shader
	glCompileShader(gVertexShaderObject);

	//Error Checking For Vertex Shader Object

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Vertex shader comiplation error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

}

void fragmentShaderCode()
{
	GLenum result;
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	//Define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	//write fragment shader code

	const GLchar *fragmentShaderSourceCode =
	{ "#version 400 core" \
		"\n" \
		"in vec3 diffuseColor;" \
		"out vec4 FragColor;" \
		"uniform int u_lKeyIsPressed;" \
		"void main(void)" \
		"{" \
		"if(u_lKeyIsPressed == 1)" \
		"{" \
		"FragColor = vec4(diffuseColor,1.0f);" \
		"}" \
		"else" \
		"{" \
		"FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
		"}" \
		"}"
	};
	//specify source code to fragment shader object

	glShaderSource(gFragmentShaderObject, 1,
		(const GLchar**)&fragmentShaderSourceCode,
		NULL);

	//compile fragment shader 

	glCompileShader(gFragmentShaderObject);

	//Error checking For Fragment Shader Object

	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);

	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				printf("Fragment shader comiplation error: ");
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

}




void createProgramCode()
{
	GLenum result;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;
	GLint iProgramLinkStatus = 0;

	//Create shader program object 
	gShaderProgramObject = glCreateProgram();

	//Attach Vertex Shader to Program Object
	glAttachShader(gShaderProgramObject, gVertexShaderObject);


	//Attach Fragment Shader to Program Object
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	//Prelinking binding of vertex shader
	
	//----- Position
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	// ---- Normal
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	//Link Shader Program
	glLinkProgram(gShaderProgramObject);

	//Error Checking For Program Object

	iInfoLogLength = 0;
	//*szInfoLog = NULL;

	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);

	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);

		if (iInfoLogLength > 0)
		{
			szInfoLog = (GLchar*)malloc(iInfoLogLength);

			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				printf("Program Object Linking  error: %s", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

}


void geomentryCode()
{
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

	//****************************** CUBE ****************************
	//Create vao for rect
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	//################### POSITION ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	//Unbinding buffer and arra for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//################### NORMAL ###################3
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
	//how many slots my array is break
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	//Enabling the position
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);


	//########################## Elements ###############
	//Generating Buffer for rect
	glGenBuffers(1, &vbo_elements_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere);
	//push data into buffers immediate
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	
	////how many slots my array is break
	//glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	////Enabling the position
	//glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	//Unbinding buffer for rectangle
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbinding Array for rectangle
	glBindVertexArray(0);

}

void resize(int width, int height)
{
	
if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	perspectiveProjectionMatrix = perspective(45.0f,
												(GLfloat)width / (GLfloat)height,
												0.1f,
												100.0f);
}

void display()
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Using program object 
	glUseProgram(gShaderProgramObject);

	//declerations of matrix
	mat4 modelViewMatrix;
	mat4 translationMatrix;
	mat4 rotationMatrix1;
	mat4 rotationMatrix2;
	mat4 scaleMatrix;
	mat4 projectionMatrix;
	mat4 lookAtMatrix;
	//*********************************** SPHERE1 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	rotationMatrix1 = mat4::identity();
	scaleMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	projectionMatrix = mat4::identity();
	lookAtMatrix = mat4::identity();
	//Do neccessary transformation
	//lookAtMatrix = vmath::lookat(vec3(0.0f,0.0f,-7.0f),vec3(0.0f,0.0f,0.0f),vec3(0.0f,1.0f,0.0f));

	
	translationMatrix = translate(1.0f, 0.0f, -13.0f);
	scaleMatrix = scale(2.0f, 0.6f, 1.0f);
	rotationMatrix1= rotate((GLfloat)shoulder,0.0f, 0.0f, 1.0f);

	//Do neccessary Matrix Multilication
	modelViewMatrix = rotationMatrix1 * translationMatrix * scaleMatrix ;
	
	push(modelViewMatrix);
	//modelViewMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	//Send neccessary matrices to shader in respective to uniforms-
	glUniformMatrix4fv(mvUniform, 1, GL_FALSE, modelViewMatrix);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);


	if (gbLighting == true)
	{
		glUniform1i(lKeyIsPressedUniform, 1);
		glUniform3f(ldUniform, 0.5f, 0.35f, 0.05f);
		glUniform3f(kdUniform, 0.1f, 0.1f, 0.1f);
		glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
	}
	else {
		glUniform1i(lKeyIsPressedUniform, 0);
	}



	//Bind with vao of rectangle
	glBindVertexArray(vao_sphere);

	//Bind texture if any

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//Unbind vao of rectangle
	glBindVertexArray(0);
	mat4 shoulderMat = pop();

	//*********************************** SPHERE2 *****************************
	//Initialize above matrix to identity
	translationMatrix = mat4::identity();
	mat4 translationMatrix1 = mat4::identity();
	rotationMatrix1 = mat4::identity();
	rotationMatrix2 = mat4::identity();
	scaleMatrix = mat4::identity();
	mat4 modelViewMatrix1 = mat4::identity();
	projectionMatrix = mat4::identity();
	//Do neccessary transformation

	translationMatrix = translate(1.5f, 0.0f, 0.0f);
	scaleMatrix = scale(2.0f, 1.0f, 1.0f);
	rotationMatrix1 = rotate((GLfloat)elbow, 0.0f, 0.0f, 1.0f);
	translationMatrix1 = translate(1.0f, 0.0f, 0.0f);
	
	//Do neccessary Matrix Multilication
	modelViewMatrix1 =  modelViewMatrix * rotationMatrix1 *translationMatrix  *scaleMatrix * rotationMatrix1 ;
	push(modelViewMatrix1);
	projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;

	//Send neccessary matrices to shader in respective to uniforms-
	glUniformMatrix4fv(mvUniform, 1, GL_FALSE, modelViewMatrix1);
	glUniformMatrix4fv(pUniform, 1, GL_FALSE, projectionMatrix);
	

	if (gbLighting == true)
	{
		glUniform1i(lKeyIsPressedUniform, 1);
		glUniform3f(ldUniform, 0.5f, 0.35f, 0.05f);
		glUniform3f(kdUniform, 0.1f, 0.1f, 0.1f);
		glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
	}
	else {
		glUniform1i(lKeyIsPressedUniform, 0);
	}



	//Bind with vao of rectangle
	glBindVertexArray(vao_sphere);

	//Bind texture if any

	//Draw function
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	//Unbind vao of rectangle
	glBindVertexArray(0);

	mat4 elbowMat = pop();
	//Unused Program
	glUseProgram(0);
	glXSwapBuffers(gpDisplay,gWindow);
}

void update(void)
{
	//code
	// angleCube = angleCube + 0.3f;
	// if (angleCube >= 360)
	// {
	// 	angleCube = 0.0f;
	// }


}

void ToggleFullScreen()
{
	//variables declerations

	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	//code 

	wm_state = XInternAtom(gpDisplay,"_NET_WM_STATE",False); // Network protocol ...which is used to do window fullscreen remoetly and locally both
 	memset(&xev,0,sizeof(xev)); // All Members set to zero

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullScreen? 0 : 1;

	fullscreen = XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False); // To Save New State Of Window
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,RootWindow(gpDisplay,gpXVisualInfo->screen), // Pass This Event to XServer
		False,StructureNotifyMask,&xev);

}


void uninitialize()
{
	if (vbo_elements_sphere)
	{
		glDeleteBuffers(1, &vbo_elements_sphere);
		vbo_elements_sphere = 0;
	}

	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}

	if (vbo_position_sphere)
	{
		glDeleteVertexArrays(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);
		vao_sphere = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;
		GLsizei shaderNumber;
		glUseProgram(gShaderProgramObject);

		//Ask shader how many shaders are attached to you
		glGetProgramiv(gShaderProgramObject, GL_ATTACHED_SHADERS, &shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint)*shaderCount);

		if (pShaders)
		{
			glGetAttachedShaders(gShaderProgramObject, shaderCount, &shaderCount, pShaders);
			for (shaderNumber = 0; shaderNumber < shaderCount; shaderNumber++)
			{
				//Dettach shaders
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);

				//Delete Shaders
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		glDeleteProgram(gShaderProgramObject);
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	GLXContext CurrentGLXContext = glXGetCurrentContext();
	if(CurrentGLXContext!=NULL && CurrentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	} 
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
	}

}

//
//  SceneDelegate.h
//  PerspectiveTriRK
//
//  Created by Akshay Apte on 28/03/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


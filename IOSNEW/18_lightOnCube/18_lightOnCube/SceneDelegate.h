//
//  SceneDelegate.h
//  18_lightOnCube
//
//  Created by Akshay Apte on 16/05/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


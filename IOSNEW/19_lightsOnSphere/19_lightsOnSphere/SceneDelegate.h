//
//  SceneDelegate.h
//  19_lightsOnSphere
//
//  Created by Akshay Apte on 17/05/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


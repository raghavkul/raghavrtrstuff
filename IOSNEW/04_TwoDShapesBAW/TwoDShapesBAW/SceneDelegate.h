//
//  SceneDelegate.h
//  TwoDShapesBAW
//
//  Created by Akshay Apte on 28/03/20.
//  Copyright © 2020 Akshay Apte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


//
//  GLESView.mm
//  OrthoTriangle
//
//  Created by Akshay Apte on 21/02/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"
#import "Sphere.h"

enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXURE0,
};

int MAXSIZE = 4;
int shoulder = 0;
int elbow = 0;

@implementation GLESView
{
    EAGLContext *eaglContext;
       
       GLuint defaultFramebuffer;
       GLuint colorRenderbuffer;
       GLuint depthRenderbuffer;
       
       id displayLink;
       NSInteger animationFrameInterval;
       
       BOOL isAnimation;
       
       GLuint vertexShaderObject;
       GLuint fragmentShaderObject;
       GLuint shaderProgramObject;
       
           int angleMoon;
           int top;
           float sphere_vertices[1146];
           float  sphere_normals[1146];
           float sphere_texture[764];
           unsigned short sphere_elements[2280];
           
           GLuint vao_sphere_raghav;//vertex array object for rect
           GLuint vbo_position_sphere_raghav;//vertex buffer object(position) for rect
           GLuint vbo_normal_sphere_raghav;//vertex buffer object(color) for rect
           GLuint vbo_elements_sphere_raghav;
           bool gbLighting;
           int gNumVertices;
           int gNumElements;
           
           int year;
           int day;
           
           
        
           vmath::mat4 stack[20];

        GLuint mvUniform;
        GLuint pUniform;
        GLuint ldUniform;
        GLuint kdUniform;
        GLuint lightPositionUniform;
        GLuint lKeyIsPressedUniform;
       
       vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frame
{
    
 

    self=[super initWithFrame:frame];
    if(self)
    {
           printf("In Self");
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER, colorRenderbuffer);
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        



        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed TO Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteFramebuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            return(nil);
        }
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        
        isAnimation = NO;
        animationFrameInterval=60; //default since iOS 8.2
        
        //***VERTEX SHADER***
        //Create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //provide source code to shader
        const GLchar *vertexShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_mv_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform int u_lKeyIsPressed;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_kd;" \
        "uniform vec4 u_lightPosition;" \
        "out vec3 diffuseColor;" \
        "void main(void)" \
        "{" \
            "if(u_lKeyIsPressed == 1)" \
            "{" \
                "vec4 eye_Coordinate = u_mv_matrix * vPosition;" \
                "mat3 normalMatrix = mat3(transpose(inverse(u_mv_matrix)));" \
                "vec3 tNorm = normalize(normalMatrix * vNormal);" \
                "vec3 s = vec3(u_lightPosition) - vec3(eye_Coordinate.xyz);" \
                "diffuseColor = u_ld * u_kd * dot(s,tNorm);" \
            "}" \
        "gl_Position = u_p_matrix * u_mv_matrix * vPosition;" \
        "}\n";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus =0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        //*** FRAGMENT SHADER ***
        //re-initialize
        iInfoLogLength =0;
        iShaderCompiledStatus=0;
        szInfoLog=NULL;
        
        //create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "precision highp float;\n" \
        "precision highp int;\n" \
        "in vec3 diffuseColor;" \
        "out vec4 FragColor;" \
        "uniform int u_lKeyIsPressed;" \
        "void main(void)" \
        "{" \
            "if(u_lKeyIsPressed == 1)" \
            "{" \
                "FragColor = vec4(diffuseColor,1.0f);" \
            "}" \
            "else" \
            "{" \
                "FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
            "}" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //*** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //attach fragment shader to shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //pre-linking binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
         glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");
        //link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus =0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        mvUniform = glGetUniformLocation(shaderProgramObject,"u_mv_matrix");
        pUniform=glGetUniformLocation(shaderProgramObject,"u_p_matrix");
        ldUniform=glGetUniformLocation(shaderProgramObject,"u_ld");
        kdUniform=glGetUniformLocation(shaderProgramObject,"u_kd");
        lightPositionUniform=glGetUniformLocation(shaderProgramObject,"u_lightPosition");
        lKeyIsPressedUniform=glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
        //*** Vertices , colors, shader attribs, vbo,vao initializations ***
       getSphereVertexData(sphere_vertices,sphere_normals,sphere_texture,sphere_elements);
       
       gNumVertices = getNumberOfSphereVertices();
       gNumElements = getNumberOfSphereElements();
            
        glGenVertexArrays(1,&vao_sphere_raghav);
          glBindVertexArray(vao_sphere_raghav);
          
          glGenBuffers(1,&vbo_position_sphere_raghav);
          glBindBuffer(GL_ARRAY_BUFFER,vbo_position_sphere_raghav);
          glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
          
          glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

          //Enabling the position
          glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

          //Unbinding buffer and arra for rectangle
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          
          
         glGenBuffers(1, &vbo_normal_sphere_raghav);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere_raghav);
          //push data into buffers immediate
          glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
          //how many slots my array is break
          glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

          //Enabling the position
          glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

          //Unbinding buffer for rectangle
          glBindBuffer(GL_ARRAY_BUFFER, 0);

          //Unbinding Array for rectangle
          glBindVertexArray(0);
          
          
         glGenBuffers(1, &vbo_elements_sphere_raghav);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere_raghav);
          //push data into buffers immediate
          glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
          glBindBuffer(GL_ARRAY_BUFFER, 0);

          //Unbinding Array for rectangle
          glBindVertexArray(0);
        
        
        glClearDepthf(1.0f);
        //enable Depth testing
        glEnable(GL_DEPTH_TEST);
        
        //depth test to do
        glDepthFunc(GL_LEQUAL);
        //We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        //set background color
        glClearColor(0.0f, 0.0f,0.0f, 1.0f);
        
        //set Projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        //GESTURE RECOGNITION
        //Tap gesture code
        
        UITapGestureRecognizer * singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
               [singleTapGestureRecognizer setNumberOfTapsRequired:1];
               [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
               
               [singleTapGestureRecognizer setDelegate:self];
               [self addGestureRecognizer:singleTapGestureRecognizer];
               
               UITapGestureRecognizer *doubleTapGestureRecognizer =
               [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
               [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
               [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
               
               [doubleTapGestureRecognizer setDelegate:self];
               [self addGestureRecognizer:doubleTapGestureRecognizer];
               
               //this will allow to differentiate between single tap and double tap
               [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
               
               //swipe gesture
               UISwipeGestureRecognizer *swipeGestureRecognizer =
               [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
               [self addGestureRecognizer:swipeGestureRecognizer];
               
               //long-press gesture
               UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
               [self addGestureRecognizer:longPressGestureRecognizer];
               
        
    }
    return(self);
}
- (int) isEmpty
{
    if(top == -1)
        return 1;
    else
        return 0;
}
- (vmath::mat4) pop
{
    vmath::mat4 data;
    
    //error={0.0f,0.0f,0.0f,0.0f};
    if(![self isEmpty]        )
    {
        data = stack[top];
        top = top -1;
        return data;
    }
    else
    {
        
        printf("\nCould not retrieve data, Stack is empty.\n");
        return (vmath::mat4(0.0f,0.0f,0.0f,0.0f));
    }
}
-(int) isFull
{
    if (top == MAXSIZE)
        return 1;
    else
        return 0;
}
-(void) push:(vmath::mat4)data
{
    
    if(![self isFull])
    {
        top = top +1;
        stack[top] = data;
        printf("Data Inserted Successfully.\n");
    }
    else
    {
        printf("Could not insert data, Stack is full.\n");
    }
}
/*
-(void)drawRect:(CGRect)rect
{
    
}
*/


+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
      glUseProgram(shaderProgramObject);
        
        vmath::mat4 modelViewMatrix = vmath::mat4::identity();
        vmath::mat4 translationMatrix = vmath::mat4::identity();
        vmath::mat4 projectionMatrix = vmath::mat4::identity();
        
        vmath::mat4 rotationMatrix1=vmath::mat4::identity();
        rotationMatrix1 = vmath::rotate((GLfloat)shoulder,0.0f,0.0f,1.0f);
        
        translationMatrix= vmath::translate(-0.5f, 0.0f, -13.0f);
        vmath::mat4 scaleMatrix = vmath::scale(2.0f,0.6f,1.0f);
        
        modelViewMatrix = rotationMatrix1 * translationMatrix * scaleMatrix;
        
        [self push:modelViewMatrix];
        projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;
        glUniformMatrix4fv(mvUniform,1,GL_FALSE,modelViewMatrix);
        
        glUniformMatrix4fv(pUniform,1,GL_FALSE,projectionMatrix);
        
        if (gbLighting == true)
        {
            glUniform1i(lKeyIsPressedUniform, 1);
            glUniform3f(ldUniform, 0.5f, 0.35f, 0.05f);
            glUniform3f(kdUniform, 0.1f, 0.1f, 0.1f);
            glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
        }
        else
        {
            glUniform1i(lKeyIsPressedUniform, 0);
        }

        
        glBindVertexArray(vao_sphere_raghav);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_elements_sphere_raghav);
        glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
        glBindVertexArray(0);
        vmath::mat4 shoulderMatrix = [self pop];
        
        
        //elblow
        vmath::mat4 modelViewMatrix1 = vmath::mat4::identity();
        translationMatrix = vmath::mat4::identity();
        projectionMatrix = vmath::mat4::identity();
        vmath::mat4 scaleMatrix1 = vmath::scale(2.0f,2.1f,1.0f);
        vmath::mat4 rotationMatrix2 = vmath::rotate((GLfloat)elbow,0.0f,0.0f,1.0f);
        translationMatrix = vmath::translate(1.6f,0.0f,-13.0f);
        
        
        modelViewMatrix1 = rotationMatrix2 * shoulderMatrix * translationMatrix * scaleMatrix1;
        [self push:modelViewMatrix1];
        
        
        projectionMatrix = perspectiveProjectionMatrix * projectionMatrix;
        glUniformMatrix4fv(mvUniform,1,GL_FALSE,modelViewMatrix1);
           
        glUniformMatrix4fv(pUniform,1,GL_FALSE,projectionMatrix);
           
         if (gbLighting == true)
           {
               glUniform1i(lKeyIsPressedUniform, 1);
               glUniform3f(ldUniform, 0.5f, 0.35f, 0.05f);
               glUniform3f(kdUniform, 0.1f, 0.1f, 0.1f);
               glUniform4f(lightPositionUniform, 0.0f, 0.0f, 2.0f, 1.0f);
           }
           else
           {
               glUniform1i(lKeyIsPressedUniform, 0);
           }
           
           glBindVertexArray(vao_sphere_raghav);
           glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,vbo_elements_sphere_raghav);
           glDrawElements(GL_TRIANGLES,gNumElements,GL_UNSIGNED_SHORT,0);
           glBindVertexArray(0);
           vmath::mat4 elbow = [self pop];

        
        
       
        glUseProgram(0);

    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)layoutSubviews
{
    
    //code
    GLint width;
    GLint height;
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    glViewport(0,0,(GLsizei)fwidth,(GLsizei)fheight);
       perspectiveProjectionMatrix= vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimation)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimation=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimation)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimation = NO;
    }
}

-(BOOL)acceptsFirstResponder
{
    return(YES);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(gbLighting==true)
        gbLighting=false;
    else
        gbLighting=true;
    
    shoulder=(shoulder-3) % 360;
    
}
-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    elbow = (elbow + 3) % 360;
}
-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    shoulder=(shoulder+3) % 360;
}
-(void)dealloc
{
   if(vao_sphere_raghav)
   {
       glDeleteVertexArrays(1,&vao_sphere_raghav);
       vao_sphere_raghav=0;
   }
   if(vbo_elements_sphere_raghav)
   {
       glDeleteBuffers(1,&vbo_elements_sphere_raghav);
       vbo_elements_sphere_raghav=0;
   }
   if(vbo_normal_sphere_raghav)
   {
       glDeleteBuffers(1,&vbo_normal_sphere_raghav);
       vbo_normal_sphere_raghav=0;
   }
   
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}
@end

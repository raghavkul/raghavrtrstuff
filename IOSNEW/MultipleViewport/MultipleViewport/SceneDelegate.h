//
//  SceneDelegate.h
//  MultipleViewport
//
//  Created by Akshay Apte on 01/06/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


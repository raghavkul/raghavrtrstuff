//
//  SceneDelegate.h
//  StaticSmilyOnRect
//
//  Created by Akshay Apte on 05/04/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


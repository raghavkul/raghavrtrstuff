//
//  GLESView.mm
//  OrthoTriangle
//
//  Created by Akshay Apte on 21/02/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"

enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXURE0,
};



@implementation GLESView
{
    EAGLContext *eaglContext;
       
       GLuint defaultFramebuffer;
       GLuint colorRenderbuffer;
       GLuint depthRenderbuffer;
       
       id displayLink;
       NSInteger animationFrameInterval;
       
       BOOL isAnimation;
       
       GLuint vertexShaderObject;
       GLuint fragmentShaderObject;
       GLuint shaderProgramObject;
       
       GLuint vao_LeftI_raghav;
       GLuint vao_N_raghav;
       GLuint vao_D_raghav;
       GLuint vao_RightI_raghav;
       GLuint vao_A_raghav;

       GLuint vbo_position_LeftI_raghav;//vertex buffer object for tri
       GLuint vbo_color_LeftI_raghav;

       GLuint vbo_position_N_raghav;//vertex buffer object for tri
       GLuint vbo_color_N_raghav;

       GLuint vbo_position_D_raghav;//vertex buffer object for tri
       GLuint vbo_color_D_raghav;

       GLuint vbo_position_RightI_raghav;//vertex buffer object for tri
       GLuint vbo_color_RightI_raghav;

       GLuint vbo_position_A_raghav;//vertex buffer object for tri
       GLuint vbo_color_A_raghav;
    
       GLuint mvpUniform;
       
       vmath::mat4 perspectivecProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frame
{
    
 

    self=[super initWithFrame:frame];
    if(self)
    {
           printf("In Self");
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER, colorRenderbuffer);
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        



        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed TO Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteFramebuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            return(nil);
        }
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        
        isAnimation = NO;
        animationFrameInterval=60; //default since iOS 8.2
        
        //***VERTEX SHADER***
        //Create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //provide source code to shader
        const GLchar *vertexShaderSourceCode =
        " #version 300 es " \
        "\n" \
         "in vec4 vPosition;" \
               "in vec4 vColor;" \
               "uniform mat4 u_mvp_matrix;" \
               "out vec4 out_color;" \
               "void main(void)" \
               "{" \
               "gl_Position = u_mvp_matrix * vPosition;" \
               "out_color = vColor;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus =0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        //*** FRAGMENT SHADER ***
        //re-initialize
        iInfoLogLength =0;
        iShaderCompiledStatus=0;
        szInfoLog=NULL;
        
        //create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "precision highp float;\n" \
        "in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //*** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //attach fragment shader to shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //pre-linking binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");
        
        //link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus =0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
        //*** Vertices , colors, shader attribs, vbo,vao initializations ***
       const GLfloat leftIVertices[] = {
            -1.30f,0.70f,0.0f,
            -1.30f,-0.70f,0.0f
        };
        const GLfloat leftIColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat nVertices[] = {
            -1.10f,0.70f,0.0f,
            -1.10f,-0.70f,0.0f,
            -1.10f,0.70f,0.0f,
            -0.60f,-0.70f,0.0f,
            -0.60f,0.70f,0.0f,
            -0.60f,-0.70f,0.0f
        };
        const GLfloat nColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat dVertices[] = {
            -0.4f,0.70f,0.0f,
            -0.4f,-0.70f,0.0f,
            -0.50f,0.70f,0.0f,
            0.1f,0.70f,0.0f,
            0.1f,0.70f,0.0f,
            0.1f,-0.70f,0.0f,
            -0.50f,-0.70f,0.0f,
            0.1f,-0.70f,0.0f
        };
        const GLfloat dColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            1.0f, 0.6f, 0.2f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            0.07f, 0.533f, 0.027f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat rightIVertices[] = {
            0.3f,0.70f,0.0f,
            0.3f,-0.70f,0.0f
        };
        const GLfloat rightIColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f
        };
        const GLfloat aVertices[] = {
            0.8f,0.70f,0.0f,
            0.50f,-0.70f,0.0f,
            0.80f,0.70f,0.0f,
            1.1f,-0.70f,0.0f,
            0.66f,0.02f,0.0f,
            0.94f,0.02f,0.0f,
            0.66f,0.0f,0.0f,
            0.94f,0.0f,0.0f,
            0.66f,-0.02f,0.0f,
            0.94f,-0.02f,0.0f
        };
        const GLfloat aColor[] = {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            0.07f, 0.533f, 0.027f,
            1.0f, 0.6f, 0.2f,
            1.0f, 0.6f, 0.2f,
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            0.07f, 0.533f, 0.027f,
            0.07f, 0.533f, 0.027f
        };


        //leftI
        glGenVertexArrays(1, &vao_LeftI_raghav);
        glBindVertexArray(vao_LeftI_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_LeftI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_LeftI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(leftIVertices), leftIVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_LeftI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_LeftI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(leftIColor), leftIColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);


        //N
        //Create vao for triangle
        glGenVertexArrays(1, &vao_N_raghav);
        glBindVertexArray(vao_N_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_N_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_N_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(nVertices), nVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_N_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_N_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(nColor), nColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);

        //D
        //Create vao for triangle
        glGenVertexArrays(1, &vao_D_raghav);
        glBindVertexArray(vao_D_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_D_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_D_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(dVertices), dVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_D_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(dColor), dColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);

        //RightI
        //Create vao for triangle
        glGenVertexArrays(1, &vao_RightI_raghav);
        glBindVertexArray(vao_RightI_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_RightI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_RightI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(rightIVertices), rightIVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_RightI_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_RightI_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(rightIColor), rightIColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);

        //A
        //Create vao for triangle
        glGenVertexArrays(1, &vao_A_raghav);
        glBindVertexArray(vao_A_raghav);

        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_position_A_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_A_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(aVertices), aVertices, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        //Generating Buffer for triangle
        glGenBuffers(1, &vbo_color_A_raghav);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_A_raghav);
        //push data into buffers immediate
        glBufferData(GL_ARRAY_BUFFER, sizeof(aColor), aColor, GL_STATIC_DRAW);
        //how many slots my array is break
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        //Enabling the position
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);

        //Unbinding buffer and array for triangle
        glBindBuffer(GL_ARRAY_BUFFER, 0);


        glBindVertexArray(0);
        
        //enable Depth testing
        glEnable(GL_DEPTH_TEST);
        
        //depth test to do
        glDepthFunc(GL_LEQUAL);
        //We will always cull back faces for better performance
        glEnable(GL_CULL_FACE);
        
        //set background color
        glClearColor(0.0f, 0.0f,0.0f, 1.0f);
        
        //set Projection matrix to identity matrix
        perspectivecProjectionMatrix = vmath::mat4::identity();
        
        //GESTURE RECOGNITION
        //Tap gesture code
        
        UITapGestureRecognizer * singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer =
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer =
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}
/*
-(void)drawRect:(CGRect)rect
{
    
}
*/


+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glUseProgram(shaderProgramObject);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translateMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    
      //leftI
     //Initialize above matrix to identity
     modelViewMatrix =  vmath::mat4::identity();
     modelViewProjectionMatrix =  vmath::mat4::identity();
     translateMatrix =  vmath::mat4::identity();
     
     //Do neccessary transformation

     translateMatrix =  vmath::translate(0.0f, 0.00f, -3.0f);
     

     //Do neccessary Matrix Multilication
     modelViewMatrix = translateMatrix ;
     modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

     //Send neccessary matrices to shader in respective to uniforms
     glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

     //Bind with vao of triangle
     glBindVertexArray(vao_LeftI_raghav);

     
     //Draw function
     glDrawArrays(GL_LINES, 0, 2);
     
     //Unbind vao of triangle
     glBindVertexArray(0);

     //N
     //Initialize above matrix to identity
     modelViewMatrix = vmath::mat4::identity();
     modelViewProjectionMatrix = vmath::mat4::identity();
     translateMatrix = vmath::mat4::identity();

     //Do neccessary transformation

     translateMatrix = vmath::translate(0.0f, 0.00f, -3.0f);


     //Do neccessary Matrix Multilication
     modelViewMatrix = translateMatrix;
     modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

     //Send neccessary matrices to shader in respective to uniforms
     glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

     //Bind with vao of triangle
     glBindVertexArray(vao_N_raghav);


     //Draw function
     glDrawArrays(GL_LINES, 0, 6);

     //Unbind vao of triangle
     glBindVertexArray(0);

     //D
     //Initialize above matrix to identity
     modelViewMatrix = vmath::mat4::identity();
     modelViewProjectionMatrix = vmath::mat4::identity();
     translateMatrix = vmath::mat4::identity();

     //Do neccessary transformation

     translateMatrix = vmath::translate(0.0f, 0.00f, -3.0f);


     //Do neccessary Matrix Multilication
     modelViewMatrix = translateMatrix;
     modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

     //Send neccessary matrices to shader in respective to uniforms
     glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

     //Bind with vao of triangle
     glBindVertexArray(vao_D_raghav);


     //Draw function
     glDrawArrays(GL_LINES, 0, 8);

     //Unbind vao of triangle
     glBindVertexArray(0);

     //rightI
     //Initialize above matrix to identity
     modelViewMatrix = vmath::mat4::identity();
     modelViewProjectionMatrix = vmath::mat4::identity();
     translateMatrix = vmath::mat4::identity();

     //Do neccessary transformation

     translateMatrix = vmath::translate(0.0f, 0.00f, -3.0f);


     //Do neccessary Matrix Multilication
     modelViewMatrix = translateMatrix;
     modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

     //Send neccessary matrices to shader in respective to uniforms
     glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

     //Bind with vao of triangle
     glBindVertexArray(vao_RightI_raghav);


     //Draw function
     glDrawArrays(GL_LINES, 0, 2);

     //Unbind vao of triangle
     glBindVertexArray(0);

     //A
     //Initialize above matrix to identity
     modelViewMatrix = vmath::mat4::identity();
     modelViewProjectionMatrix = vmath::mat4::identity();
     translateMatrix = vmath::mat4::identity();

     //Do neccessary transformation

     translateMatrix = vmath::translate(0.0f, 0.00f, -3.0f);


     //Do neccessary Matrix Multilication
     modelViewMatrix = translateMatrix;
     modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

     //Send neccessary matrices to shader in respective to uniforms
     glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

     //Bind with vao of triangle
     glBindVertexArray(vao_A_raghav);


     //Draw function
     glDrawArrays(GL_LINES, 0, 10);

     //Unbind vao of triangle
     glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)layoutSubviews
{
    
    //code
    GLint width;
    GLint height;
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
        perspectivecProjectionMatrix = vmath::perspective(45.0f,
                                                    fwidth / fheight,
                                                    0.1f,
                                                    100.0f);
    printf("In resize");
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimation)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimation=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimation)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimation = NO;
    }
}

-(BOOL)acceptsFirstResponder
{
    return(YES);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}
-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}
-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}
-(void)dealloc
{

    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}
@end

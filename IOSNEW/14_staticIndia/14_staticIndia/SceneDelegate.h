//
//  SceneDelegate.h
//  14_staticIndia
//
//  Created by Akshay Apte on 21/05/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


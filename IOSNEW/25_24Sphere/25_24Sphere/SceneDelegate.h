//
//  SceneDelegate.h
//  25_24Sphere
//
//  Created by Akshay Apte on 19/05/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


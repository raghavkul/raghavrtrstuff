//
//  GLESView.mm
//  OrthoTriangle
//
//  Created by Akshay Apte on 21/02/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"
#import "Sphere.h"

enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXURE0,
};

float sphere_vertices[1146];
   float sphere_normals[1146];
   float sphere_texture[764];
   unsigned short sphere_elements[2280];

unsigned int gNumVertices_raghav;
      unsigned int gNumElements_raghav;

//light values
static float angleOfXRotation = 0.0f;
static float angleOfYRotation = 0.0f;
static float angleOfZRotation = 0.0f;

GLfloat width =0.0f;
GLfloat height=0.0f;

int keyPress = 0,keyPressEvent=0;
//light values
GLfloat lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };

@implementation GLESView
{
    EAGLContext *eaglContext;
       
       GLuint defaultFramebuffer;
       GLuint colorRenderbuffer;
       GLuint depthRenderbuffer;
       
       id displayLink;
       NSInteger animationFrameInterval;
       
       BOOL isAnimation;
       bool gbLighting ;
    
       GLuint vertexShaderObject;
       GLuint fragmentShaderObject;
       GLuint shaderProgramObject;
       
      GLuint vao_sphere_raghav;//vertex array object for rect
         GLuint vbo_position_sphere_raghav;//vertex buffer object(position) for rect
         GLuint vbo_normal_sphere_raghav;//vertex buffer object(color) for rect
         GLuint vbo_elements_sphere_raghav;
         
         GLuint mUniform_raghav; // model view matrix
         GLuint vUniform_raghav;
         GLuint pUniform_raghav; //projection matrix
         
         GLuint laUniform_raghav;
         GLuint ldUniform_raghav;
         GLuint lsUniform_raghav;
         
         GLuint kaUniform_raghav;
         GLuint kdUniform_raghav;
         GLuint ksUniform_raghav;
         GLuint materialShinynessUniform_raghav;
         
         GLuint lightPositionUniform_raghav;
         GLuint lKeyIsPressedUniform_raghav;

    
    
       vmath::mat4 perspectiveProjectionmatrix;
}

-(id) initWithFrame:(CGRect)frame
{
    
 

    self=[super initWithFrame:frame];
    if(self)
    {
           printf("In Self");
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER, colorRenderbuffer);
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        



        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed TO Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteFramebuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            return(nil);
        }
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        
        isAnimation = NO;
        animationFrameInterval=60; //default since iOS 8.2
        
        //***VERTEX SHADER***
        //Create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //provide source code to shader
        const GLchar *vertexShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_m_matrix;" \
        "uniform mat4 u_v_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform int u_lKeyIsPressed;" \
        "uniform vec4 u_lightPosition;" \
        "out vec3 tNorm;" \
        "out vec3 lightDirection;" \
        "out vec3 viwerVector;" \
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
        "vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
        "tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" \
        "lightDirection = vec3(u_lightPosition - eye_Coordinate);" \
        "viwerVector = -eye_Coordinate.xyz;" \
        "}" \
        "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus =0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        //*** FRAGMENT SHADER ***
        //re-initialize
        iInfoLogLength =0;
        iShaderCompiledStatus=0;
        szInfoLog=NULL;
        
        //create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "precision highp float;\n" \
        "precision highp int;\n" \
        "in vec3 tNorm;" \
        "in vec3 lightDirection;" \
        "in vec3 viwerVector;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_ls;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_materialShine;" \
        "uniform vec4 u_lightPosition;" \
        "uniform int u_lKeyIsPressed;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
        "vec3 normalizeTNorm = normalize(tNorm);" \
        "vec3 normalizeLightDirection = normalize(lightDirection);" \
        "vec3 normalizeViwerVector = normalize(viwerVector);" \
        "float tn_dot_ld = max(dot(normalizeLightDirection,normalizeTNorm),0.0f);" \
        "vec3 reflectionVector = reflect(-normalizeLightDirection , normalizeTNorm);" \
        "vec3 ambiant = vec3(u_la * u_ka);" \
        "vec3 diffuse = vec3(u_ld * u_kd * tn_dot_ld);" \
        "vec3 specular = vec3(u_ls * u_ks * pow(max(dot(reflectionVector , normalizeViwerVector),0.0f),u_materialShine));" \
        "vec3 phong_ads_light = ambiant + diffuse + specular;" \
        "FragColor = vec4(phong_ads_light,1.0f);" \
        "}" \
        "else" \
        "{" \
        "FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
        "}" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //*** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //attach fragment shader to shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //pre-linking binding of shader program object with vertex shader position attribute
            glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        // ---- color
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
        
        //link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus =0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
         mUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
         vUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
         pUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
         laUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_la");
         ldUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_ld");
         lsUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_ls");
         kaUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_ka");
         kdUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_kd");
         ksUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_ks");
         materialShinynessUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_materialShine");
         lightPositionUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_lightPosition");
         lKeyIsPressedUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
        
          getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
            gNumVertices_raghav = getNumberOfSphereVertices();
            gNumElements_raghav = getNumberOfSphereElements();

            //****************************** CUBE ****************************
            //Create vao for rect
            glGenVertexArrays(1, &vao_sphere_raghav);
            glBindVertexArray(vao_sphere_raghav);

            //################### POSITION ###################3
            //Generating Buffer for rect
            glGenBuffers(1, &vbo_position_sphere_raghav);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere_raghav);
            //push data into buffers immediate
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
            //how many slots my array is break
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            //Enabling the position
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

            //Unbinding buffer and arra for rectangle
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            //################### NORMAL ###################3
            //Generating Buffer for rect
            glGenBuffers(1, &vbo_normal_sphere_raghav);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere_raghav);
            //push data into buffers immediate
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
            //how many slots my array is break
            glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            //Enabling the position
            glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

            //Unbinding buffer for rectangle
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            //Unbinding Array for rectangle
            glBindVertexArray(0);


            //########################## Elements ###############
            //Generating Buffer for rect
            glGenBuffers(1, &vbo_elements_sphere_raghav);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere_raghav);
            //push data into buffers immediate
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
            
            ////how many slots my array is break
            //glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            ////Enabling the position
            //glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

            //Unbinding buffer for rectangle
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        //
            //Unbinding Array for rectangle
            glBindVertexArray(0);
        //enable Depth testing
        glEnable(GL_DEPTH_TEST);
        
        //depth test to do
        glClearDepthf(1.0f);
        glDepthFunc(GL_LEQUAL);
        //We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        //set background color
        glClearColor(0.0f, 0.0f,0.0f, 1.0f);
        
        //set Projection matrix to identity matrix
        perspectiveProjectionmatrix = vmath::mat4::identity();
        
        //GESTURE RECOGNITION
        //Tap gesture code
        
        UITapGestureRecognizer * singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer =
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer =
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}
/*
-(void)drawRect:(CGRect)rect
{
    
}
*/

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{

    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glUseProgram(shaderProgramObject);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    
        if (gbLighting == true)
    {
        glUniform1i(lKeyIsPressedUniform_raghav, 1);
        glUniform3fv(laUniform_raghav, 1, lightAmbiant);
        glUniform3fv(ldUniform_raghav, 1, lightDiffuse);
        glUniform3fv(lsUniform_raghav, 1, lightSpecular);
        if (keyPress == 1)
        {
            lightPosition[1] = cos(angleOfXRotation) * 100.0f;
            lightPosition[2] = -sin(angleOfXRotation) * 100.0f;
            
            glUniform4fv(lightPositionUniform_raghav, 1, lightPosition);
        }else if(keyPress == 2)
        {
            lightPosition[0] = cos(angleOfYRotation) * 100.0f;
            lightPosition[2] = -sin(angleOfYRotation) * 100.0f;
        
            glUniform4fv(lightPositionUniform_raghav, 1, lightPosition);
        }else if(keyPress==3) {

            lightPosition[0] = cos(angleOfZRotation) * 100.0f;
            lightPosition[1] = sin(angleOfZRotation) * 100.0f;
            
            glUniform4fv(lightPositionUniform_raghav, 1, lightPosition);
        }
        
        
    }
    else {
        glUniform1i(lKeyIsPressedUniform_raghav, 0);
    }
    
    
    
    
    [self draw24Sphere];

    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
      angleOfXRotation =angleOfXRotation+0.03f;
      angleOfYRotation=angleOfYRotation+0.03f;
      angleOfZRotation =angleOfZRotation+0.03f;
}

-(void)draw24Sphere
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    GLint screenWidth = screenRect.size.width;
    GLint screenHeight = screenRect.size.height;
    
    
    int xWindowCenter = screenWidth / 2;
    int yWindowCenter = screenHeight / 2;


    int distorasionX = screenWidth / 6;
    int distorasionY = screenHeight / 8;

    int xTransOffset = distorasionX;
    int yTransOffset = distorasionY;

    int currentViewportX;
    int currentViewportY;
    
    //declerations of matrix
        vmath::mat4 modelMatrix;
        vmath::mat4 viewMatrix;
        vmath::mat4 translationMatrix;
        vmath::mat4 rotationMatrix;
        vmath::mat4 projectionMatrix;
    
    
        GLfloat MaterialAmbiant[4];
        GLfloat MaterialDiffuse[4];
        GLfloat MaterialSpecular[4];
        GLfloat MaterialShininess[1];
    
        translationMatrix = vmath::mat4::identity();
        rotationMatrix = vmath::mat4::identity();
        modelMatrix = vmath::mat4::identity();
        viewMatrix = vmath::mat4::identity();
        projectionMatrix = vmath::mat4::identity();
        
        
        //Do neccessary transformation
        translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //    rotationMatrix = vmath::rotate(angleRect, angleRect, angleRect);
        //Do neccessary Matrix Multilication
        modelMatrix = translationMatrix;// * rotationMatrix;
        projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

        //Send neccessary matrices to shader in respective to uniforms
        glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
        glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
        glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    
    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;
    
    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);    //Bind with vao of rectangle
    
    MaterialAmbiant[0] = 0.0215f;
    MaterialAmbiant[1] = 0.1745f;
    MaterialAmbiant[2] = 0.0215f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 0.07568f;
    MaterialDiffuse[1] = 0.61424f;
    MaterialDiffuse[2] = 0.0215f;
    MaterialDiffuse[3] = 1.0f;

    MaterialSpecular[0] = 0.633f;
    MaterialSpecular[1] = 0.727811f;
    MaterialSpecular[2] = 0.633f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.6f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);        glBindVertexArray(vao_sphere_raghav);

        //Draw function
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
        glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT,0);    //Bind texture if any
    
        //Unbind vao of rectangle
    glBindVertexArray(0);
    
    
    //*********************************** SPHERE 2 *****************************
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);

    
    MaterialAmbiant[0] = 0.135f;
    MaterialAmbiant[1] = 0.2225f;
    MaterialAmbiant[2] = 0.1575f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 0.54f;
    MaterialDiffuse[1] = 0.89f;
    MaterialDiffuse[2] = 0.63f;
    MaterialDiffuse[3] = 1.0f;
    

    MaterialSpecular[0] = 0.316228f;
    MaterialSpecular[1] = 0.316228f;
    MaterialSpecular[2] = 0.316228f;
    MaterialSpecular[3] = 1.0f;
    

    MaterialShininess[0] = 0.1f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 3 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.5375f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.06625f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.18275f;
    MaterialDiffuse[1] = 0.17f;
    MaterialDiffuse[2] = 0.22525f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.332741f;
    MaterialSpecular[1] = 0.328634f;
    MaterialSpecular[2] = 0.346435f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.3f * 128.0f;
    
    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);


    //*********************************** SPHERE 4 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.25f;
    MaterialAmbiant[1] = 0.20725f;
    MaterialAmbiant[2] = 0.20725f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 1.0f;
    MaterialDiffuse[1] = 0.829f;
    MaterialDiffuse[2] = 0.829f;
    MaterialDiffuse[3] = 1.0f;

    MaterialSpecular[0] = 0.296648f;
    MaterialSpecular[1] = 0.296648f;
    MaterialSpecular[2] = 0.296648f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.088f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 5 *****************************
  translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.1745f;
    MaterialAmbiant[1] = 0.01175f;
    MaterialAmbiant[2] = 0.01175f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.61424f;
    MaterialDiffuse[1] = 0.04136f;
    MaterialDiffuse[2] = 0.04136f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.727811f;
    MaterialSpecular[1] = 0.626959f;
    MaterialSpecular[2] = 0.626959f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.6f * 128.0f;
    

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 6 *****************************
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 2.5;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.1f;
    MaterialAmbiant[1] = 0.18725f;
    MaterialAmbiant[2] = 0.1745f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.396f;
    MaterialDiffuse[1] = 0.74151f;
    MaterialDiffuse[2] = 0.69102f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.297254f;
    MaterialSpecular[1] = 0.30829f;
    MaterialSpecular[2] = 0.306678f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.1f * 128.0f;
    

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 7 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.329412f;
    MaterialAmbiant[1] = 0.223529f;
    MaterialAmbiant[2] = 0.27451f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.780392f;
    MaterialDiffuse[1] = 0.568627f;
    MaterialDiffuse[2] = 0.113725f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.992157f;
    MaterialSpecular[1] = 0.941176f;
    MaterialSpecular[2] = 0.807843f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.21794872f * 128.0f;
    

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 8 *****************************
 translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.2125f;
    MaterialAmbiant[1] = 0.1275f;
    MaterialAmbiant[2] = 0.054f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.714f;
    MaterialDiffuse[1] = 0.4284f;
    MaterialDiffuse[2] = 0.18144f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.393548f;
    MaterialSpecular[1] = 0.271906f;
    MaterialSpecular[2] = 0.166721f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.2f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 9 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.25f;
    MaterialAmbiant[1] = 0.25f;
    MaterialAmbiant[2] = 0.25f;
    MaterialAmbiant[3] = 1.0f;

    MaterialDiffuse[0] = 0.4f;
    MaterialDiffuse[1] = 0.4f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;

    MaterialSpecular[0] = 0.774597f;
    MaterialSpecular[1] = 0.774597f;
    MaterialSpecular[2] = 0.774597f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.6f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 10 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.19125f;
    MaterialAmbiant[1] = 0.0735f;
    MaterialAmbiant[2] = 0.02225f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.7038f;
    MaterialDiffuse[1] = 0.27048f;
    MaterialDiffuse[2] = 0.0828f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.256777f;
    MaterialSpecular[1] = 0.137622f;
    MaterialSpecular[2] = 0.086014f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.1f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 11 *****************************
    //Initialize above matrix to identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.24725f;
    MaterialAmbiant[1] = 0.1995f;
    MaterialAmbiant[2] = 0.0745f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.75164f;
    MaterialDiffuse[1] = 0.60648f;
    MaterialDiffuse[2] = 0.22648f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.628281f;
    MaterialSpecular[1] = 0.555802f;
    MaterialSpecular[2] = 0.366065f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.4f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 12 *****************************
    //Initialize above matrix to identity
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * 1.1;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.19225f;
    MaterialAmbiant[1] = 0.19225f;
    MaterialAmbiant[2] = 0.19225f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.50754f;
    MaterialDiffuse[1] = 0.50754f;
    MaterialDiffuse[2] = 0.50754f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.508273f;
    MaterialSpecular[1] = 0.508273f;
    MaterialSpecular[2] = 0.508273f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.4f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 13 *****************************
    //Initialize above matrix to identity
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.01f;
    MaterialDiffuse[1] = 0.01f;
    MaterialDiffuse[2] = 0.01f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.50f;
    MaterialSpecular[1] = 0.50f;
    MaterialSpecular[2] = 0.50f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 14 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.1f;
    MaterialAmbiant[2] = 0.06f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.0f;
    MaterialDiffuse[1] = 0.50980392f;
    MaterialDiffuse[2] = 0.50980392f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.50196078f;
    MaterialSpecular[1] = 0.50196078f;
    MaterialSpecular[2] = 0.50196078f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 15 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.1f;
    MaterialDiffuse[1] = 0.35f;
    MaterialDiffuse[2] = 0.1f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.45f;
    MaterialSpecular[1] = 0.55f;
    MaterialSpecular[2] = 0.45f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 16 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.0f;
    MaterialDiffuse[2] = 0.0f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.6f;
    MaterialSpecular[2] = 0.6f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;
    
    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 17 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.55f;
    MaterialDiffuse[1] = 0.55f;
    MaterialDiffuse[2] = 0.55f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.70f;
    MaterialSpecular[1] = 0.70f;
    MaterialSpecular[2] = 0.70f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 18 *****************************
   translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -0.1;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.0f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.60f;
    MaterialSpecular[1] = 0.60f;
    MaterialSpecular[2] = 0.50f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.25f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 19 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * 2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.02f;
    MaterialAmbiant[1] = 0.02f;
    MaterialAmbiant[2] = 0.02f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.01f;
    MaterialDiffuse[1] = 0.01f;
    MaterialDiffuse[2] = 0.01f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.4f;
    MaterialSpecular[1] = 0.4f;
    MaterialSpecular[2] = 0.4f;
    MaterialSpecular[3] = 1.0f;

    MaterialShininess[0] = 0.078125f * 128.0f;
    
    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 20 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * 1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.05f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.4f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.5f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.04f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.7f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 21 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * 0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.0f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.4f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.04f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.04f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 22 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * -0.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.05f;
    MaterialAmbiant[1] = 0.0f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.4f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.04f;
    MaterialSpecular[2] = 0.04f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 23 *****************************
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * -1.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.05f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.05f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.5f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.7f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);

    //*********************************** SPHERE 24 *****************************
 translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    projectionMatrix = vmath::mat4::identity();
    //Do neccessary transformation

    translationMatrix = vmath::translate(0.0f, 0.0f, -1.8f);
    //rotationMatrix = rotate(0.0f, angleCube, 0.0f);

    //Do neccessary Matrix Multilication
    modelMatrix = translationMatrix;
    //modelViewMatrix = perspectiveProjectionmatrix * modelViewMatrix;
    projectionMatrix = perspectiveProjectionmatrix * projectionMatrix;

    glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
    glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
    glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

    currentViewportX = xWindowCenter - xTransOffset * -1.5;
    currentViewportY = yWindowCenter + yTransOffset * -2.5;


    glViewport(currentViewportX, currentViewportY, distorasionX, distorasionY);


    MaterialAmbiant[0] = 0.05f;
    MaterialAmbiant[1] = 0.05f;
    MaterialAmbiant[2] = 0.0f;
    MaterialAmbiant[3] = 1.0f;
    
    MaterialDiffuse[0] = 0.5f;
    MaterialDiffuse[1] = 0.5f;
    MaterialDiffuse[2] = 0.4f;
    MaterialDiffuse[3] = 1.0f;
    
    MaterialSpecular[0] = 0.7f;
    MaterialSpecular[1] = 0.7f;
    MaterialSpecular[2] = 0.04f;
    MaterialSpecular[3] = 1.0f;
    
    MaterialShininess[0] = 0.078125f * 128.0f;

    glUniform3fv(kaUniform_raghav, 1, MaterialAmbiant);
    glUniform3fv(kdUniform_raghav, 1, MaterialDiffuse);
    glUniform3fv(ksUniform_raghav, 1, MaterialSpecular);
    glUniform1fv(materialShinynessUniform_raghav, 1, MaterialShininess);


    glBindVertexArray(vao_sphere_raghav);

    //Draw function
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
    glDrawElements(GL_TRIANGLE_STRIP, numElements, GL_UNSIGNED_SHORT, 0);
    //Unbind vao of rectangle
    glBindVertexArray(0);}
-(void)layoutSubviews
{
    
    //code
//    GLint width;
//    GLint height;
    
   CGRect screenRect = [[UIScreen mainScreen] bounds];
   GLint screenWidth = screenRect.size.width;
   GLint screenHeight = screenRect.size.height;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &screenWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &screenHeight);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, screenWidth, screenHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
//   glViewport(0, 0, width, height);

    printf("in screenWidth %d\n",screenWidth);
     printf("in screenHeight %d\n",screenHeight);
    int distorationX = screenWidth/6;
    int distorationY =screenHeight/8;
   glViewport(0,0,(GLsizei)screenWidth,(GLsizei)screenHeight);

    printf("in distorationX %d\n",distorationX);
        printf("in distorationY %d\n",distorationY);
        perspectiveProjectionmatrix = vmath::perspective(45.0f,
                                                    (GLfloat)distorationX / (GLfloat)distorationY,
                                                    0.1f,
                                                    100.0f);
    printf("In resize");
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimation)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimation=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimation)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimation = NO;
    }
}

-(BOOL)acceptsFirstResponder
{
    return(YES);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(gbLighting==true)
        gbLighting=false;
    else
        gbLighting=true;
}
-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    keyPressEvent ++;
    if(keyPressEvent == 1)
    {
        keyPress=1;
        angleOfXRotation=0.0f;
    }
    else if(keyPressEvent == 2)
    {
        keyPress=2;
        angleOfYRotation=0.0f;
    }
        
    else if(keyPressEvent == 3)
    {
        keyPress=3;
        angleOfZRotation = 0.0f;
    }
    if(keyPressEvent >3)
    {
        keyPress=1;
        
        keyPressEvent=1;
    }
}
-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}
-(void)dealloc
{
    if(vao_sphere_raghav)
    {
        glDeleteVertexArrays(1, &vao_sphere_raghav);
        vao_sphere_raghav=0;
    }

    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}
@end

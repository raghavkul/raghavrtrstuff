//
//  SceneDelegate.h
//  10_Checkerboard
//
//  Created by Akshay Apte on 15/05/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


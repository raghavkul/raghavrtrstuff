//
//  GLESView.mm
//  OrthoTriangle
//
//  Created by Akshay Apte on 21/02/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"

#define MESH_HEIGHT 1
#define MESH_WIDTH 1
#define MESH_ROW 10
#define MESH_COLUMN 10

enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXURE0,
};


GLfloat graphVertices[MESH_ROW * MESH_COLUMN * 3];
GLfloat xDiff = (GLfloat)MESH_WIDTH / (GLfloat)MESH_COLUMN;
GLfloat yDiff = (GLfloat)MESH_HEIGHT / (GLfloat)MESH_ROW;
GLfloat circleVertices[1100];
static GLfloat angale;

static GLfloat triTrans = -4.0f;
static GLfloat cirTrans = 4.0f;
static GLfloat lineTrans = 2.0f;
static GLfloat triAng = 0.0f,cirAng = 0.0f;//interface decleration

@implementation GLESView
{
    EAGLContext *eaglContext;
       
       GLuint defaultFramebuffer;
       GLuint colorRenderbuffer;
       GLuint depthRenderbuffer;
       
       id displayLink;
       NSInteger animationFrameInterval;
       
       BOOL isAnimation;
       
       GLuint vertexShaderObject;
       GLuint fragmentShaderObject;
       GLuint shaderProgramObject;
       
       GLuint vao_triangle_raghav;//vertex array object for tri
         GLuint vao_circle_raghav;
         GLuint vao_line_raghav;
         GLuint vbo_position_triangle_raghav;//vertex buffer object for tri
         GLuint vbo_position_circle_raghav;//vertex buffer object for rect
         GLuint vbo_position_line_raghav;
    
       GLuint mvpUniform;
       
       vmath::mat4 perspectivecProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frame
{
    
    self=[super initWithFrame:frame];
    if(self)
    {
           printf("In Self");
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER, colorRenderbuffer);
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        



        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed TO Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteFramebuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            return(nil);
        }
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        
        isAnimation = NO;
        animationFrameInterval=60; //default since iOS 8.2
        
        //***VERTEX SHADER***
        //Create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //provide source code to shader
        const GLchar *vertexShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "in vec4 vPosition;" \
        "uniform mat4 u_mvp_matrix;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus =0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        //*** FRAGMENT SHADER ***
        //re-initialize
        iInfoLogLength =0;
        iShaderCompiledStatus=0;
        szInfoLog=NULL;
        
        //create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "precision highp float;\n" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = vec4(1.0,0.0,0.0,1.0);" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //*** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //attach fragment shader to shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //pre-linking binding of shader program object with vertex shader position attribute
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        
        //link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus =0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        mvpUniform = glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        
      GLfloat fx1, fx2, fx3, fy1, fy2, fy3;
          GLfloat fArea, fRad, fPer;
          GLfloat fdAB, fdBC, fdAC;
          GLfloat fxCord, fyCord;
          
          fx1 = 0.0;
          fy1 = 1.0;
          fx2 = -1.0;
          fy2 = -1.0;
          fx3 = 1.0;
          fy3 = -1.0;
          
          const GLfloat triangleVertices[] = {
                              fx1, fy1, 0.0,
                             fx2, fy2, 0.0,
                             fx3, fy3, 0.0,
                             fx1, fy1, 0.0,
                              fx2,fy2,0.0,
                              fx3,fy3,0.0
          };


          const GLfloat lineVertices[] = {
              0.0f,1.0f,0.0f,
              0.0f,-1.0f,0.0f,
          };
          

          //****************************** TRIANGLE ****************************
          //Create vao for triangle
          glGenVertexArrays(1, &vao_triangle_raghav);
          glBindVertexArray(vao_triangle_raghav);

          //Generating Buffer for triangle
          glGenBuffers(1, &vbo_position_triangle_raghav);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_position_triangle_raghav);
          //push data into buffers immediate
          glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertices), triangleVertices, GL_STATIC_DRAW);
          //how many slots my array is break
          glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

          //Enabling the position
          glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

          //Unbinding buffer and array for triangle
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          glBindVertexArray(0);

          //****************************** LINES ****************************
          //Create vao for triangle
          glGenVertexArrays(1, &vao_line_raghav);
          glBindVertexArray(vao_line_raghav);

          //Generating Buffer for triangle
          glGenBuffers(1, &vbo_position_line_raghav);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_position_line_raghav);
          //push data into buffers immediate
          glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), lineVertices, GL_STATIC_DRAW);
          //how many slots my array is break
          glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

          //Enabling the position
          glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

          //Unbinding buffer and array for triangle
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          glBindVertexArray(0);



          //****************************** CRICLE ****************************
          //// /  CIRCLE


          fdAB = sqrt(((fx2 - fx1)*(fx2 - fx1)) + ((fy2 - fy1)*(fy2 - fy1)));
          fdBC = sqrt(((fx3 - fx2)*(fx3 - fx2)) + ((fy3 - fy2)*(fy3 - fy2)));
          fdAC = sqrt(((fx3 - fx1)*(fx3 - fx1)) + ((fy3 - fy1)*(fy3 - fy1)));
          
          fPer = ((fdAB + fdAB + fdBC) / 2);

          fArea = sqrt(fPer*(fPer - fdAB)*(fPer - fdBC)*(fPer - fdAC));

          fRad = (fArea / fPer);

          fxCord = (((fdBC*fx1) + (fx2*fdAC) + (fx3*fdAB)) / (fPer * 2));
          fyCord = (((fdBC*fy1) + (fy2*fdAC) + (fy3*fdAB)) / (fPer * 2));
          
          GLfloat circleVert[500000];
          GLint i, j;
          float circleSteps = 0.0;
          for(i = 0; i < 6290; i++) {
              for(j = 0; j < 2; j++) {
                  if(j==0)
                      circleVert[ (i*2) + j] =  fxCord + cos(circleSteps)*fRad;
                  else
                      circleVert[ (i*2) + j] =  fyCord + sin(circleSteps)*fRad;
              }
              circleSteps += 0.01;
          }
          
          //Create vao for rect
          glGenVertexArrays(1, &vao_circle_raghav);
          glBindVertexArray(vao_circle_raghav);

          //Generating Buffer for rect
          glGenBuffers(1, &vbo_position_circle_raghav);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_position_circle_raghav);
          //push data into buffers immediate
          glBufferData(GL_ARRAY_BUFFER, sizeof(circleVert), circleVert, GL_STATIC_DRAW);
          //how many slots my array is break
          glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

          //Enabling the position
          glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

          //Unbinding buffer and arra for rectangle
          glBindBuffer(GL_ARRAY_BUFFER, 0);
          glBindVertexArray(0);
        //enable Depth testing
        glEnable(GL_DEPTH_TEST);
        
        //depth test to do
        glDepthFunc(GL_LEQUAL);
        //We will always cull back faces for better performance
        glEnable(GL_CULL_FACE);
        
        //set background color
        glClearColor(0.0f, 0.0f,0.0f, 1.0f);
        
        //set Projection matrix to identity matrix
        perspectivecProjectionMatrix = vmath::mat4::identity();
        
        //GESTURE RECOGNITION
        //Tap gesture code
        
        UITapGestureRecognizer * singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer =
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer =
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}
/*
-(void)drawRect:(CGRect)rect
{
    
}
*/


+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    
    //code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glUseProgram(shaderProgramObject);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    vmath::mat4 modelViewMatrix = vmath::mat4::identity();
    vmath::mat4 translationMatrix = vmath::mat4::identity();
    vmath::mat4 modelViewProjectionMatrix = vmath::mat4::identity();
    vmath::mat4 rotateMatrix = vmath::mat4::identity();
    
  //Initialize above matrix to identity
         translationMatrix = vmath::mat4::identity();
         modelViewMatrix = vmath::mat4::identity();
         modelViewProjectionMatrix = vmath::mat4::identity();

         //Do neccessary transformation

         translationMatrix = vmath::translate(triTrans, 0.0f, -6.0f);
         rotateMatrix = vmath::rotate(triAng, 0.0f, 1.0f, 0.0f);
         
         
         //Do neccessary Matrix Multilication
         modelViewMatrix = translationMatrix * rotateMatrix;
         modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

         //Send neccessary matrices to shader in respective to uniforms
         glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

         //Bind with vao of triangle
         glBindVertexArray(vao_triangle_raghav);

         glDrawArrays(GL_LINES, 0, 2);
         glDrawArrays(GL_LINES, 2, 2);
         glDrawArrays(GL_LINES, 4, 2);
     
         glBindVertexArray(0);

     modelViewMatrix = vmath::mat4::identity();
     modelViewProjectionMatrix = vmath::mat4::identity();


     modelViewMatrix = vmath::translate(0.0f, lineTrans, -6.0f);
     //Do neccessary Matrix Multilication
     modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

     //Send neccessary matrices to shader in respective to uniforms
     glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);

     //Bind with vao of triangle
     glBindVertexArray(vao_line_raghav);

     //Bind texture if any

     //Draw function
     //glDrawArrays(GL_TRIANGLES, 0, 3);
     glDrawArrays(GL_LINES, 0, 2);
     //glDrawArrays(GL_LINE_STRIP, 9, 3);

     //glDrawArrays(GL_LINES, 0, 3);

     //Unbind vao of triangle
     glBindVertexArray(0);
     
     //Initialize above matrix to identity
    translationMatrix = vmath::mat4::identity();
     modelViewMatrix = vmath::mat4::identity();
     modelViewProjectionMatrix = vmath::mat4::identity();
     rotateMatrix = vmath::mat4::identity();
     //Do neccessary transformation

     
     translationMatrix = vmath::translate(cirTrans, 0.0f, -6.0f);
     rotateMatrix = vmath::rotate(cirAng, 0.0f, 1.0f, 0.0f);

     
     //Do neccessary Matrix Multilication
     modelViewMatrix = translationMatrix * rotateMatrix;
     modelViewProjectionMatrix = perspectivecProjectionMatrix * modelViewMatrix;

     //Send neccessary matrices to shader in respective to uniforms
     glUniformMatrix4fv(mvpUniform, 1, GL_FALSE, modelViewProjectionMatrix);
     
     glBindVertexArray(vao_circle_raghav);
     glDrawArrays(GL_POINTS, 0, 6280);
     glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    triTrans = triTrans + 0.009f;
       if (triTrans >= 0.0f)
       {
           triTrans = 0.0f;
       }
       cirTrans = cirTrans - 0.009f;
       if (cirTrans <= 0.0f)
       {
           cirTrans = 0.0f;
       }
       lineTrans = lineTrans - 0.005f;
       if (lineTrans <= 0.0f)
       {
           lineTrans = 0.0f;
       }
       triAng = triAng + 0.3f;
       if (triAng >= 360.0f)
       {
           triAng = 0.0f;
       }
       cirAng = cirAng + 0.3f;
       if (cirAng >= 360.0f)
       {
           cirAng = 0.0f;
       }
}

-(void)layoutSubviews
{
    
    //code
    GLint width;
    GLint height;
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
        perspectivecProjectionMatrix = vmath::perspective(45.0f,
                                                    fwidth / fheight,
                                                    0.1f,
                                                    100.0f);
    printf("In resize");
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimation)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimation=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimation)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimation = NO;
    }
}

-(BOOL)acceptsFirstResponder
{
    return(YES);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}
-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}
-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}
-(void)dealloc
{
    if(vao_circle_raghav)
    {
        glDeleteVertexArrays(1, &vao_circle_raghav);
        vao_circle_raghav=0;
    }
    if(vao_line_raghav)
    {
        glDeleteVertexArrays(1, &vao_line_raghav);
        vao_line_raghav=0;
    }
    if(vbo_position_line_raghav)
    {
        glDeleteBuffers(1,&vbo_position_line_raghav);
        vbo_position_line_raghav=0;
    }
    if(vbo_position_circle_raghav)
    {
        glDeleteBuffers(1,&vbo_position_circle_raghav);
        vbo_position_circle_raghav=0;
    }
    if(vbo_position_triangle_raghav)
    {
        glDeleteBuffers(1,&vbo_position_triangle_raghav);
        vbo_position_triangle_raghav=0;
    }
    if(vao_triangle_raghav)
    {
        glDeleteVertexArrays(1, &vao_triangle_raghav);
        vao_triangle_raghav=0;
    }
    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}
@end

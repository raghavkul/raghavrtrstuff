//
//  SceneDelegate.h
//  13_DeathlyHallows
//
//  Created by Akshay Apte on 21/05/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


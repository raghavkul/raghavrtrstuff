//
//  GLESView.mm
//  OrthoTriangle
//
//  Created by Akshay Apte on 21/02/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"
#import "Sphere.h"

enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXURE0,
};

float sphere_vertices[1146];
   float sphere_normals[1146];
   float sphere_texture[764];
   unsigned short sphere_elements[2280];

unsigned int gNumVertices_raghav;
      unsigned int gNumElements_raghav;

//light values
float lightAmbiantRed[] = { 0.0f,0.0f,0.0f,1.0f };
float lightDiffuseRed[] = { 1.0f,0.0f,0.0f,1.0f };
float lightSpecularRed[] = { 1.0f,0.0f,0.0f,1.0f };
float lightPositionRed[] = { 0.0f,0.0f,0.0f,1.0f };

float lightAmbiantBlue[] = { 0.0f,0.0f,0.0f,1.0f };
float lightDiffuseBlue[] = { 0.0f,1.0f,0.0f,1.0f };
float lightSpecularBlue[] = { 0.0f,1.0f,0.0f,1.0f };
float lightPositionBlue[] = { 0.0f,0.0f,0.0f,1.0f };

float lightAmbiantGreen[] = { 0.0f,0.0f,0.0f,1.0f };
float lightDiffuseGreen[] = { 0.0f,0.0f,1.0f,1.0f };
float lightSpecularGreen[] = { 0.0f,0.0f,1.0f,1.0f };
float lightPositionGreen[] = { 0.0f,0.0f,0.0f,1.0f };
//material values
float materialAmbiant[4] = { 0.0f,0.0f,0.0f,0.0f };
float materialDiffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialSpecular[4] = { 1.0f,1.0f,1.0f,1.0f };
float materialShinyness = 128.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;
       
       GLuint defaultFramebuffer;
       GLuint colorRenderbuffer;
       GLuint depthRenderbuffer;
       
       id displayLink;
       NSInteger animationFrameInterval;
       
       BOOL isAnimation;
       bool gbLighting ;
    
       GLuint vertexShaderObject;
       GLuint fragmentShaderObject;
       GLuint shaderProgramObject;
       
      GLuint vao_sphere_raghav;//vertex array object for rect
         GLuint vbo_position_sphere_raghav;//vertex buffer object(position) for rect
         GLuint vbo_normal_sphere_raghav;//vertex buffer object(color) for rect
         GLuint vbo_elements_sphere_raghav;
         
         GLuint mUniform_raghav; // model view matrix
         GLuint vUniform_raghav;
         GLuint pUniform_raghav; //projection matrix
         
           //lights
           GLuint laUniformRed_raghav;
           GLuint ldUniformRed_raghav;
           GLuint lsUniformRed_raghav;


           GLuint laUniformBlue_raghav;
           GLuint ldUniformBlue_raghav;
           GLuint lsUniformBlue_raghav;

           GLuint laUniformGreen_raghav;
           GLuint ldUniformGreen_raghav;
           GLuint lsUniformGreen_raghav;
           
           GLuint kaUniform_raghav;
           GLuint kdUniform_raghav;
           GLuint ksUniform_raghav;
           GLuint materialShinynessUniform_raghav;
           
           GLuint lightPositionUniformRed_raghav;
           GLuint lightPositionUniformBlue_raghav;
           GLuint lightPositionUniformGreen_raghav;
           
           GLuint lKeyIsPressedUniform_raghav;

    
    
       vmath::mat4 perspectivecProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frame
{
    
 

    self=[super initWithFrame:frame];
    if(self)
    {
           printf("In Self");
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER, colorRenderbuffer);
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        



        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed TO Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteFramebuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            return(nil);
        }
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        
        isAnimation = NO;
        animationFrameInterval=60; //default since iOS 8.2
        
        //***VERTEX SHADER***
        //Create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //provide source code to shader
        const GLchar *vertexShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_m_matrix;" \
        "uniform mat4 u_v_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform int u_lKeyIsPressed;" \
        "uniform vec4 u_lightPosition_red;" \
        "uniform vec4 u_lightPosition_blue;" \
        "uniform vec4 u_lightPosition_green;" \
        "out vec3 tNorm;" \
        "out vec3 lightDirectionRed;" \
        "out vec3 lightDirectionBlue;" \
        "out vec3 lightDirectionGreen;" \
        "out vec3 viwerVector;" \
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
        "vec4 eye_Coordinate = u_v_matrix * u_m_matrix * vPosition;" \
        "tNorm = mat3(u_v_matrix * u_m_matrix ) * vNormal;" \
        "lightDirectionRed = vec3(u_lightPosition_red - eye_Coordinate);" \
        "lightDirectionBlue = vec3(u_lightPosition_blue - eye_Coordinate);" \
        "lightDirectionGreen = vec3(u_lightPosition_green - eye_Coordinate);" \
        "viwerVector = -eye_Coordinate.xyz;" \
        "}" \
        "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
        "}";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus =0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        //*** FRAGMENT SHADER ***
        //re-initialize
        iInfoLogLength =0;
        iShaderCompiledStatus=0;
        szInfoLog=NULL;
        
        //create shader
        fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        
        //provide source code to shader
        const GLchar *fragmentShaderSourceCode =
        " #version 300 es " \
        "\n" \
        "precision highp float;\n" \
        "precision highp int;\n" \
        "in vec3 tNorm;" \
        "in vec3 lightDirectionRed;" \
        "in vec3 lightDirectionBlue;" \
        "in vec3 lightDirectionGreen;" \
        "in vec3 viwerVector;" \
        "uniform vec3 u_la_red;" \
        "uniform vec3 u_ld_red;" \
        "uniform vec3 u_ls_red;" \
        "uniform vec3 u_la_blue;" \
        "uniform vec3 u_ld_blue;" \
        "uniform vec3 u_ls_blue;" \
        "uniform vec3 u_la_green;" \
        "uniform vec3 u_ld_green;" \
        "uniform vec3 u_ls_green;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_materialShine;" \
        "uniform vec4 u_lightPosition_red;" \
        "uniform vec4 u_lightPosition_blue;" \
        "uniform vec4 u_lightPosition_green;" \
        "uniform int u_lKeyIsPressed;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed == 1)" \
        "{" \
        "vec3 normalizeTNorm = normalize(tNorm);" \
        "vec3 normalizeLightDirectionRed = normalize(lightDirectionRed);" \
        "vec3 normalizeLightDirectionBlue = normalize(lightDirectionBlue);" \
        "vec3 normalizeLightDirectionGreen = normalize(lightDirectionGreen);" \
        "vec3 normalizeViwerVector = normalize(viwerVector);" \
        "float tn_dot_ldRed = max(dot(normalizeLightDirectionRed,normalizeTNorm),0.0f);" \
        "float tn_dot_ldBlue = max(dot(normalizeLightDirectionBlue,normalizeTNorm),0.0f);" \
        "float tn_dot_ldGreen = max(dot(normalizeLightDirectionGreen,normalizeTNorm),0.0f);" \
        "vec3 reflectionVectorRed = reflect(-normalizeLightDirectionRed , normalizeTNorm);" \
        "vec3 reflectionVectorBlue = reflect(-normalizeLightDirectionBlue , normalizeTNorm);" \
        "vec3 reflectionVectorGreen = reflect(-normalizeLightDirectionGreen , normalizeTNorm);" \
        "vec3 ambiantRed = vec3(u_la_red * u_ka);" \
        "vec3 ambiantBlue = vec3(u_la_blue * u_ka);" \
        "vec3 ambiantGreen = vec3(u_la_green * u_ka);" \
        "vec3 diffuseRed = vec3(u_ld_red * u_kd * tn_dot_ldRed);" \
        "vec3 diffuseBlue = vec3(u_ld_blue * u_kd * tn_dot_ldBlue);" \
        "vec3 diffuseGreen = vec3(u_ld_green * u_kd * tn_dot_ldGreen);" \
        "vec3 specularRed = vec3(u_ls_red * u_ks * pow(max(dot(reflectionVectorRed , normalizeViwerVector),0.0f),u_materialShine));" \
        "vec3 specularBlue = vec3(u_ls_blue * u_ks * pow(max(dot(reflectionVectorBlue , normalizeViwerVector),0.0f),u_materialShine));" \
        "vec3 specularGreen = vec3(u_ls_green * u_ks * pow(max(dot(reflectionVectorGreen , normalizeViwerVector),0.0f),u_materialShine));" \
        "vec3 phong_ads_light_red = ambiantRed + diffuseRed + specularRed;" \
        "vec3 phong_ads_light_blue = ambiantBlue + diffuseBlue + specularBlue;" \
        "vec3 phong_ads_light_green = ambiantGreen + diffuseGreen + specularGreen;" \
        "FragColor = vec4(phong_ads_light_red + phong_ads_light_blue + phong_ads_light_green ,1.0f);" \
        "}" \
        "else" \
        "{" \
        "FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
        "}" \
        "}";
        
        glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(fragmentShaderObject);
        glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(fragmentShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Fragment Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        //*** SHADER PROGRAM ***
        // create
        shaderProgramObject = glCreateProgram();
        
        //attach vertex shader to shader program
        glAttachShader(shaderProgramObject, vertexShaderObject);
        
        //attach fragment shader to shader Program
        glAttachShader(shaderProgramObject, fragmentShaderObject);
        
        //pre-linking binding of shader program object with vertex shader position attribute
            glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
        // ---- color
        glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");
        
        //link shader
        glLinkProgram(shaderProgramObject);
        GLint iShaderProgramLinkStatus =0;
        glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
        if(iShaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetProgramInfoLog(shaderProgramObject, iInfoLogLength, &written, szInfoLog);
                    printf("Shader Program Link Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
         mUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_m_matrix");
           vUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_v_matrix");
           pUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_p_matrix");
           laUniformRed_raghav = glGetUniformLocation(shaderProgramObject, "u_la_red");
           ldUniformRed_raghav = glGetUniformLocation(shaderProgramObject, "u_ld_red");
           lsUniformRed_raghav = glGetUniformLocation(shaderProgramObject, "u_ls_red");
           lightPositionUniformRed_raghav = glGetUniformLocation(shaderProgramObject, "u_lightPosition_red");
           laUniformBlue_raghav = glGetUniformLocation(shaderProgramObject, "u_la_blue");
           ldUniformBlue_raghav = glGetUniformLocation(shaderProgramObject, "u_ld_blue");
           lsUniformBlue_raghav = glGetUniformLocation(shaderProgramObject, "u_ls_blue");
           lightPositionUniformBlue_raghav = glGetUniformLocation(shaderProgramObject, "u_lightPosition_blue");
           laUniformGreen_raghav = glGetUniformLocation(shaderProgramObject, "u_la_green");
           ldUniformGreen_raghav = glGetUniformLocation(shaderProgramObject, "u_ld_green");
           lsUniformGreen_raghav = glGetUniformLocation(shaderProgramObject, "u_ls_green");
           lightPositionUniformGreen_raghav = glGetUniformLocation(shaderProgramObject, "u_lightPosition_green");
           kaUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_ka");
           kdUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_kd");
           ksUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_ks");
           materialShinynessUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_materialShine");
           lKeyIsPressedUniform_raghav = glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
        
        
          getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
            gNumVertices_raghav = getNumberOfSphereVertices();
            gNumElements_raghav = getNumberOfSphereElements();

            //****************************** CUBE ****************************
            //Create vao for rect
            glGenVertexArrays(1, &vao_sphere_raghav);
            glBindVertexArray(vao_sphere_raghav);

            //################### POSITION ###################3
            //Generating Buffer for rect
            glGenBuffers(1, &vbo_position_sphere_raghav);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere_raghav);
            //push data into buffers immediate
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
            //how many slots my array is break
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            //Enabling the position
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

            //Unbinding buffer and arra for rectangle
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            //################### NORMAL ###################3
            //Generating Buffer for rect
            glGenBuffers(1, &vbo_normal_sphere_raghav);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere_raghav);
            //push data into buffers immediate
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
            //how many slots my array is break
            glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            //Enabling the position
            glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

            //Unbinding buffer for rectangle
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            //Unbinding Array for rectangle
            glBindVertexArray(0);


            //########################## Elements ###############
            //Generating Buffer for rect
            glGenBuffers(1, &vbo_elements_sphere_raghav);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_elements_sphere_raghav);
            //push data into buffers immediate
            glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
            
            ////how many slots my array is break
            //glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

            ////Enabling the position
            //glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

            //Unbinding buffer for rectangle
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        //
            //Unbinding Array for rectangle
            glBindVertexArray(0);
        //enable Depth testing
        glEnable(GL_DEPTH_TEST);
        
        //depth test to do
        glClearDepthf(1.0f);
        glDepthFunc(GL_LEQUAL);
        //We will always cull back faces for better performance
        //glEnable(GL_CULL_FACE);
        
        //set background color
        glClearColor(0.0f, 0.0f,0.0f, 1.0f);
        
        //set Projection matrix to identity matrix
        perspectivecProjectionMatrix = vmath::mat4::identity();
        
        //GESTURE RECOGNITION
        //Tap gesture code
        
        UITapGestureRecognizer * singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer =
        [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        //this will allow to differentiate between single tap and double tap
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
        
        //swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer =
        [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        //long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
        
    }
    return(self);
}
/*
-(void)drawRect:(CGRect)rect
{
    
}
*/

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{
    static float lightAngleRed =0.0f;
    static float lightAngleBlue =0.0f;
    static float lightAngleGreen =0.0f;
    
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
    glUseProgram(shaderProgramObject);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
    
    //declerations of matrix
    vmath::mat4 modelMatrix;
    vmath::mat4 viewMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    vmath::mat4 projectionMatrix;
    
    //************************** CUBE ************************
    translationMatrix = vmath::mat4::identity();
        rotationMatrix = vmath::mat4::identity();
        modelMatrix = vmath::mat4::identity();
        viewMatrix = vmath::mat4::identity();
        projectionMatrix = vmath::mat4::identity();
        
        
        //Do neccessary transformation
        translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    //    rotationMatrix = vmath::rotate(angleRect, angleRect, angleRect);
        //Do neccessary Matrix Multilication
        modelMatrix = translationMatrix;// * rotationMatrix;
        projectionMatrix = perspectivecProjectionMatrix * projectionMatrix;

      //Send neccessary matrices to shader in respective to uniforms
      glUniformMatrix4fv(mUniform_raghav, 1, GL_FALSE, modelMatrix);
      glUniformMatrix4fv(vUniform_raghav, 1, GL_FALSE, viewMatrix);
      glUniformMatrix4fv(pUniform_raghav, 1, GL_FALSE, projectionMatrix);

      
      
     if (gbLighting == true)
     {
         lightPositionRed[1] = cos(lightAngleRed) * 100.0f;
         lightPositionRed[2] = sin(lightAngleRed) * 100.0f;
         
         lightPositionBlue[0] = cos(lightAngleBlue) * 100.0f;
         lightPositionBlue[2] = sin(lightAngleBlue) * 100.0f;
         
         lightPositionGreen[0] = cos(lightAngleGreen) * 100.0f;
         lightPositionGreen[1] = sin(lightAngleGreen) * 100.0f;

         glUniform1i(lKeyIsPressedUniform_raghav, 1);

         glUniform3fv(laUniformRed_raghav, 1, lightAmbiantRed);
         glUniform3fv(ldUniformRed_raghav, 1, lightDiffuseRed);
         glUniform3fv(lsUniformRed_raghav, 1, lightSpecularRed);
         glUniform4fv(lightPositionUniformRed_raghav, 1, lightPositionRed);

         glUniform3fv(laUniformBlue_raghav, 1, lightAmbiantBlue);
         glUniform3fv(ldUniformBlue_raghav, 1, lightDiffuseBlue);
         glUniform3fv(lsUniformBlue_raghav, 1, lightSpecularBlue);
         glUniform4fv(lightPositionUniformBlue_raghav, 1, lightPositionBlue);

         glUniform3fv(laUniformGreen_raghav, 1, lightAmbiantGreen);
         glUniform3fv(ldUniformGreen_raghav, 1, lightDiffuseGreen);
         glUniform3fv(lsUniformGreen_raghav, 1, lightSpecularGreen);
         glUniform4fv(lightPositionUniformGreen_raghav, 1, lightPositionGreen);

         glUniform3fv(kaUniform_raghav, 1, materialAmbiant);
         glUniform3fv(kdUniform_raghav, 1, materialDiffuse);
         glUniform3fv(ksUniform_raghav, 1, materialSpecular);
         glUniform1f(materialShinynessUniform_raghav, materialShinyness);

         
     }
     else {
         glUniform1i(lKeyIsPressedUniform_raghav, 0);
     }
       
     glBindVertexArray(vao_sphere_raghav);

           //Draw function
           glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_elements_sphere_raghav);
           glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT,0);    //Bind texture if any
       
       
       glBindVertexArray(0);
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
    lightAngleRed=lightAngleRed+0.03f;
    lightAngleBlue=lightAngleBlue+0.03f;
    lightAngleGreen=lightAngleGreen+0.03f;
}

-(void)layoutSubviews
{
    
    //code
    GLint width;
    GLint height;
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
        perspectivecProjectionMatrix = vmath::perspective(45.0f,
                                                    fwidth / fheight,
                                                    0.1f,
                                                    100.0f);
    printf("In resize");
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimation)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimation=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimation)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimation = NO;
    }
}

-(BOOL)acceptsFirstResponder
{
    return(YES);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(gbLighting==true)
        gbLighting=false;
    else
        gbLighting=true;
}
-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}
-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}
-(void)dealloc
{
    if(vao_sphere_raghav)
    {
        glDeleteVertexArrays(1, &vao_sphere_raghav);
        vao_sphere_raghav=0;
    }

    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}
@end

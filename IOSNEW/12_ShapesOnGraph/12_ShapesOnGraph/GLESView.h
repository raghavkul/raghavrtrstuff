//
//  GLESView.h
//  Ortho
//
//  Created by Admin on 04/03/20.
//

#import <UIKit/UIKit.h>

 @interface GLESView : UIView<UIGestureRecognizerDelegate>
-(void)startAnimation;
-(void)stopAnimation;
@end


//
//  GLESView.mm
//  OrthoTriangle
//
//  Created by Akshay Apte on 21/02/20.
//

#import "GLESView.h"
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>
#import "vmath.h"
#import "Sphere.h"


enum
{
        AMC_ATTRIBUTE_POSITION=0,
        AMC_ATTRIBUTE_COLOR,
        AMC_ATTRIBUTE_NORMAL,
        AMC_ATTRIBUTE_TEXURE0,
};

bool gbLighting = false;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_texture[764];
unsigned short sphere_elements[2280];
static int keyPress=0;
static int key=1;
GLuint u_model,u_View, u_Projection,  lightPositionUniform, lKeyPressed;
GLuint vbo_sphere_position, vbo_sphere_normal, vbo_sphere_elements;
GLfloat LightAmbient[] = {0.0f,0.0f,0.0f,1.0f};
GLfloat LIghtDiifuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = {1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = {100.0f,100.0f,100.0f,1.0f};
GLfloat MaterialAmbient[] = { 0.1f,0.1f,0.1f,0.0f };
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
float MaterialShineness = 128.0f;
GLuint u_la, u_ld, u_ls, u_ka, u_kd, u_ks,u_materialShineness;
GLuint vertexShaderObject;
GLuint fragmentShaderObject;
GLuint shaderProgramObject;
unsigned int gNumVertices, gNumElements;

@implementation GLESView
{
    EAGLContext *eaglContext;
       
       GLuint defaultFramebuffer;
       GLuint colorRenderbuffer;
       GLuint depthRenderbuffer;
       
       id displayLink;
       NSInteger animationFrameInterval;
       
       BOOL isAnimation;
       
       GLuint vao_sphere;
        GLuint vbo_sphere_position, vbo_sphere_normal, vbo_sphere_elements;
       vmath::mat4 perspectiveProjectionMatrix;
}

-(id) initWithFrame:(CGRect)frame
{
    
 

    self=[super initWithFrame:frame];
    if(self)
    {
           printf("In Self");
        
        CAEAGLLayer *eaglLayer=(CAEAGLLayer *)super.layer;
        eaglLayer.opaque=YES;
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:FALSE],kEAGLDrawablePropertyRetainedBacking,kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat,nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext==nil)
        {
            [self release];
            return(nil);
        }
        [EAGLContext setCurrentContext:eaglContext];
        glGenFramebuffers(1, &defaultFramebuffer);
        glGenRenderbuffers(1, &colorRenderbuffer);
        glBindFramebuffer(GL_FRAMEBUFFER,defaultFramebuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_RENDERBUFFER, colorRenderbuffer);
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1,&depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_RENDERBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        



        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed TO Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1,&defaultFramebuffer);
            glDeleteFramebuffers(1, &colorRenderbuffer);
            glDeleteRenderbuffers(1,&depthRenderbuffer);
            return(nil);
        }
        printf("Renderer : %s | GL Version : %s | GLSL Version : %s \n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        //hard coded initializations
        
        isAnimation = NO;
        animationFrameInterval=60; //default since iOS 8.2
        
        //***VERTEX SHADER***
        //Create shader
        vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        
        //provide source code to shader
        const GLchar *vertexShaderSourceCode =
        " #version 300 es " \
        "\n" \
       "in vec4 vPosition;" \

        "in vec3 vNormal;" \
        "uniform mat4 u_model;" \
        "uniform mat4 u_view;" \
        "uniform mat4 u_Projection;" \
        "uniform int u_lKeyIsPressed;" \
        "uniform vec4 u_light_position;" \
        "out vec3 transformedNormed;" \
        "out vec3 lightdirection;" \
        "out vec3 viewervector;" \
        "out vec4 eye_coordinates;" \
        "void main(void)" \
        "{" \
        "if(u_lKeyIsPressed==1)" \
        "{" \
                "eye_coordinates = u_view * u_model * vPosition;" \
                "transformedNormed = mat3(u_view * u_model )* vNormal;" \
                "lightdirection = vec3(u_light_position) - vec3(eye_coordinates);" \
                "viewervector = vec3(-eye_coordinates.xyz);" \
        "}" \
            "gl_Position = u_Projection * u_view * u_model * vPosition;" \
        "}\n";
        
        glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
        
        //compile Shader
        glCompileShader(vertexShaderObject);
        GLint iInfoLogLength = 0;
        GLint iShaderCompiledStatus =0;
        char *szInfoLog = NULL;
        
        glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
        if(iShaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH,&iInfoLogLength);
            if(iInfoLogLength > 0)
            {
                szInfoLog = (char *)malloc(iInfoLogLength);
                if(szInfoLog != NULL)
                {
                    GLsizei written;
                    glGetShaderInfoLog(vertexShaderObject, iInfoLogLength, &written, szInfoLog);
                    printf("Vertex Shader Compilation Log : %s\n",szInfoLog);
                    free(szInfoLog);
                    [self release];
                }
            }
        }
        
        iInfoLogLength = 0;
        iShaderCompiledStatus =0;
        char *szInfoLog1 = NULL;
       
       fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
       
       const GLchar *fragmentShaderSourceCode=
       " #version 300 es " \
       "\n" \
    "precision highp float;\n" \
    "precision highp int;\n" \
       "uniform vec3 u_ld;" \
       "uniform vec3 u_ls;" \
       "uniform vec3 u_la;" \
       "uniform vec3 u_ka;" \
       "uniform vec3 u_kd;" \
       "uniform vec3 u_ks;" \
       "uniform float u_matrialShineness;" \
       "in vec3 transformedNormed;" \
       "in vec3 lightdirection;" \
       "vec3 ambient;" \
       "in vec3 viewervector;" \
       "vec3 phong_ADS_lighting;" \
       "uniform int u_lKeyIsPressed;" \
       "out vec4 fragColor;" \
       "void main(void)" \
       "{" \
       "if(u_lKeyIsPressed==1)" \
       "{" \
                   "vec3 transformedNormalized = normalize(transformedNormed);" \
                   "vec3 normalizedlightdirection = normalize(lightdirection);" \
                   "float tn_dot_ld = max(dot(normalizedlightdirection,transformedNormalized),0.0);" \
                   "vec3 reflectionVector = reflect(-normalizedlightdirection,transformedNormalized);" \
                   "vec3 normalizedViewerVector = normalize(viewervector);" \
                   "ambient = u_la * u_ka;" \
                   "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
                   "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector,normalizedViewerVector),0.0),u_matrialShineness);" \
                   "phong_ADS_lighting = ambient + diffuse + specular;" \
                   "fragColor = vec4(phong_ADS_lighting,1.0);" \
       "}" \
       "else" \
       "{" \
                   "phong_ADS_lighting = vec3(1.0,1.0,1.0);" \
                   "fragColor = vec4(phong_ADS_lighting,1.0);" \
       "}" \
       "}";
       
       glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
       
       glCompileShader(fragmentShaderObject);
       glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
       if(iShaderCompiledStatus==GL_FALSE)
       {
           glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iInfoLogLength);
           if(iInfoLogLength>0)
           {
               szInfoLog1=(char*) malloc(iInfoLogLength);
               if(szInfoLog1!=NULL)
               {
                   GLsizei written;
                   glGetShaderInfoLog(fragmentShaderObject,iInfoLogLength,&written,szInfoLog1);
                   
                   printf("Fragment Shader Compilation Log %s\n",szInfoLog1);
                   free(szInfoLog);
                   [self release];
                   
               }
           }
       }
        GLint iINfoLogLength = 0;
        
           char *szInfoLog3 = NULL;
            shaderProgramObject = glCreateProgram();
          
          glAttachShader(shaderProgramObject,vertexShaderObject);
          glAttachShader(shaderProgramObject,fragmentShaderObject);
          
          
          
          glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
          glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");
          
          glLinkProgram(shaderProgramObject);
          GLint iShaderProgramLinkStatus=0;
          glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
          
          if(iShaderProgramLinkStatus==GL_FALSE)
          {
              glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
              if(iINfoLogLength>0)
              {
                  szInfoLog=(char*)malloc(iINfoLogLength);
                  if(szInfoLog3!=NULL)
                  {
                      GLsizei written;
                      glGetProgramInfoLog(shaderProgramObject,iINfoLogLength,&written,szInfoLog3);
                      printf("Shader Program Link Log : %s\n",szInfoLog3);
                      free(szInfoLog);
                      [self release];
                      
                  }
              }
          }
          
          u_model= glGetUniformLocation(shaderProgramObject, "u_model");
          u_View = glGetUniformLocation(shaderProgramObject, "u_view");
          u_Projection = glGetUniformLocation(shaderProgramObject, "u_Projection");
          lKeyPressed = glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
          u_la= glGetUniformLocation(shaderProgramObject, "u_la");
          u_ld = glGetUniformLocation(shaderProgramObject, "u_ld");
          u_ls = glGetUniformLocation(shaderProgramObject, "u_ls");
          u_ka = glGetUniformLocation(shaderProgramObject, "u_ka");
          u_kd = glGetUniformLocation(shaderProgramObject, "u_kd");
          u_ks = glGetUniformLocation(shaderProgramObject, "u_ks");
          u_materialShineness = glGetUniformLocation(shaderProgramObject, "u_matrialShineness");
          lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");

        getSphereVertexData(sphere_vertices, sphere_normals, sphere_texture, sphere_elements);
        gNumVertices = getNumberOfSphereVertices();
        gNumElements = getNumberOfSphereElements();
            
         glGenVertexArrays(1, &vao_sphere);
          glBindVertexArray(vao_sphere);

          glGenBuffers(1, &vbo_sphere_position);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);

          glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
          glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

          glBindBuffer(GL_ARRAY_BUFFER, 0);

          glGenBuffers(1, &vbo_sphere_normal);
          glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);

          glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
          glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
          glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

          glBindBuffer(GL_ARRAY_BUFFER, 0);


          glGenBuffers(1, &vbo_sphere_elements);
          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);


          glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);

          glBindBuffer(GL_ARRAY_BUFFER, 0);

          glBindVertexArray(0);
           
           
           glClearDepthf(1.0f);
           
           glEnable(GL_DEPTH_TEST);
           
           glDepthFunc(GL_LEQUAL);
           
        
        //set background color
        glClearColor(0.0f, 0.0f,0.0f, 1.0f);
        
        //set Projection matrix to identity matrix
        perspectiveProjectionMatrix = vmath::mat4::identity();
        
        //GESTURE RECOGNITION
        //Tap gesture code
        
        UITapGestureRecognizer * singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
               [singleTapGestureRecognizer setNumberOfTapsRequired:1];
               [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
               
               [singleTapGestureRecognizer setDelegate:self];
               [self addGestureRecognizer:singleTapGestureRecognizer];
               
               UITapGestureRecognizer *doubleTapGestureRecognizer =
               [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
               [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
               [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
               
               [doubleTapGestureRecognizer setDelegate:self];
               [self addGestureRecognizer:doubleTapGestureRecognizer];
               
               //this will allow to differentiate between single tap and double tap
               [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer];
               
               //swipe gesture
               UISwipeGestureRecognizer *swipeGestureRecognizer =
               [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
               [self addGestureRecognizer:swipeGestureRecognizer];
               
               //long-press gesture
               UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
               [self addGestureRecognizer:longPressGestureRecognizer];
               
        
    }
    return(self);
}

-(void) VertexShader_PerFragment
{
    
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode=
    " #version 300 es " \
    "\n" \
    
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model;" \
    "uniform mat4 u_view;" \
    "uniform mat4 u_Projection;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec4 u_light_position;" \
    "out vec3 transformedNormed;" \
    "out vec3 lightdirection;" \
    "out vec3 viewervector;" \

    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed==1)" \
    "{" \
            "vec4 eye_coordinates = u_view * u_model * vPosition;" \
            "transformedNormed = mat3(u_view * u_model )* vNormal;" \
            "lightdirection = vec3(u_light_position) - vec3(eye_coordinates);" \
            "viewervector = vec3(-eye_coordinates.xyz);" \
    "}" \
    "gl_Position = u_Projection * u_view * u_model * vPosition;" \
    "}\n";
    
    glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    
    glCompileShader(vertexShaderObject);
    
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength > 0)
        {
            szInfoLog = (char *) malloc(iINfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iINfoLogLength,&written,szInfoLog);
                
                printf("Vertex Shader Per Vertex Compilation Error %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }

}
-(void) FragmentShader_PerFragment
{
     GLint iINfoLogLength = 0;
     GLint iShaderCompiledStatus =0;
     char *szInfoLog = NULL;
    
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode=
    " #version 300 es " \
    "\n" \
    "precision highp float;\n" \
    "precision highp int;\n" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_matrialShineness;" \
    "in vec3 transformedNormed;" \
    "in vec3 lightdirection;" \
    "vec3 ambient;" \
    "in vec3 viewervector;" \
    "vec3 phong_ADS_lighting;" \
    "uniform int u_lKeyIsPressed;" \
    "out vec4 fragColor;" \
    "void main(void)" \
    "{" \
    "if(u_lKeyIsPressed==1)" \
    "{" \
                "vec3 transformedNormalized = normalize(transformedNormed);" \
                "vec3 normalizedlightdirection = normalize(lightdirection);" \
                "float tn_dot_ld = max(dot(normalizedlightdirection,transformedNormalized),0.0);" \
                "vec3 reflectionVector = reflect(-normalizedlightdirection,transformedNormalized);" \
                "vec3 normalizedViewerVector = normalize(viewervector);" \
                "ambient = u_la * u_ka;" \
                "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
                "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector,normalizedViewerVector),0.0),u_matrialShineness);" \
                "phong_ADS_lighting = ambient + diffuse + specular;" \
                "fragColor = vec4(phong_ADS_lighting,1.0);" \
    "}" \
    "else" \
    "{" \
                "phong_ADS_lighting = vec3(1.0,1.0,1.0);" \
                "fragColor = vec4(phong_ADS_lighting,1.0);" \
    "}" \
    "}";
    
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*) malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iINfoLogLength,&written,szInfoLog);
                
                printf("Fragment Shader Compilation Log %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }
}
-(void) vertexShader_PerVertex
{
    vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaderSourceCode=
    " #version 300 es " \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_model;" \
    "uniform mat4 u_view;" \
    "uniform mat4 u_Projection;" \
    "uniform int u_lKeyIsPressed;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_matrialShineness;" \
    "uniform vec4 u_light_position;" \
    "out vec3 phong_ADS_lighting;" \
    "void main(void)" \
    "{" \
            "if(u_lKeyIsPressed==1)" \
            "{" \
                    "vec4 eye_coordinates = u_view * u_model * vPosition;" \
                    "vec3 transformedNormed = normalize(mat3(u_view * u_model )* vNormal);" \
                    "vec3 lightdirection = normalize(vec3(u_light_position) - vec3(eye_coordinates));" \
                    "float tn_dot_ld = max(dot(lightdirection,transformedNormed),0.0);" \
                    "vec3 reflectionVector = reflect(-lightdirection,transformedNormed);" \
                    "vec3 viewervector = normalize(vec3(-eye_coordinates.xyz));" \
                    "vec3 ambient = u_la * u_ka;" \
                    "vec3 diffuse = u_ld * u_kd * tn_dot_ld;" \
                    "vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector,viewervector),0.0),u_matrialShineness);" \
                    "phong_ADS_lighting = ambient + diffuse + specular;" \
            "}" \
            "else" \
            "{" \
                    "phong_ADS_lighting = vec3(1.0,1.0,1.0);" \
            "}" \
                "gl_Position = u_Projection * u_view * u_model * vPosition;" \
    "}\n";
    
    glShaderSource(vertexShaderObject,1,(const GLchar **)&vertexShaderSourceCode,NULL);
    
    glCompileShader(vertexShaderObject);
    
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    glGetShaderiv(vertexShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(vertexShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength > 0)
        {
            szInfoLog = (char *) malloc(iINfoLogLength);
            if(szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(vertexShaderObject,iINfoLogLength,&written,szInfoLog);
                
                printf("Vertex Shader Per Vertex Compilation Error %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }

}
-(void) fragmentShader_PerVertex
{
    GLint iINfoLogLength = 0;
    GLint iShaderCompiledStatus =0;
    char *szInfoLog = NULL;
    
    fragmentShaderObject=glCreateShader(GL_FRAGMENT_SHADER);
    
    const GLchar* fragmentShaderSourceCode=
    " #version 300 es " \
    "\n" \
    "precision highp float;\n" \
   
   "in vec3 phong_ADS_lighting;" \
    "out vec4 fragColor;" \
    "void main(void)" \
    "{" \
            "fragColor = vec4(phong_ADS_lighting,1.0);" \
    "}";
    
    glShaderSource(fragmentShaderObject,1,(const GLchar**)&fragmentShaderSourceCode,NULL);
    
    glCompileShader(fragmentShaderObject);
    glGetShaderiv(fragmentShaderObject,GL_COMPILE_STATUS,&iShaderCompiledStatus);
    if(iShaderCompiledStatus==GL_FALSE)
    {
        glGetShaderiv(fragmentShaderObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
        if(iINfoLogLength>0)
        {
            szInfoLog=(char*) malloc(iINfoLogLength);
            if(szInfoLog!=NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fragmentShaderObject,iINfoLogLength,&written,szInfoLog);
                
                printf("Fragment Shader Per Vertex Compilation Log %s\n",szInfoLog);
                free(szInfoLog);
                [self release];
                
            }
        }
    }

}
-(void) ShaderProgramLinking
{
    GLint iINfoLogLength = 0;
    
       char *szInfoLog = NULL;
        shaderProgramObject = glCreateProgram();
      
      glAttachShader(shaderProgramObject,vertexShaderObject);
      glAttachShader(shaderProgramObject,fragmentShaderObject);
      
      
      
      glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_POSITION,"vPosition");
      glBindAttribLocation(shaderProgramObject,AMC_ATTRIBUTE_NORMAL,"vNormal");
      
      glLinkProgram(shaderProgramObject);
      GLint iShaderProgramLinkStatus=0;
      glGetProgramiv(shaderProgramObject,GL_LINK_STATUS,&iShaderProgramLinkStatus);
      
      if(iShaderProgramLinkStatus==GL_FALSE)
      {
          glGetProgramiv(shaderProgramObject,GL_INFO_LOG_LENGTH,&iINfoLogLength);
          if(iINfoLogLength>0)
          {
              szInfoLog=(char*)malloc(iINfoLogLength);
              if(szInfoLog!=NULL)
              {
                  GLsizei written;
                  glGetProgramInfoLog(shaderProgramObject,iINfoLogLength,&written,szInfoLog);
                  printf("Shader Program Link Log : %s\n",szInfoLog);
                  free(szInfoLog);
                  [self release];
                  
              }
          }
      }
      
      u_model= glGetUniformLocation(shaderProgramObject, "u_model");
      u_View = glGetUniformLocation(shaderProgramObject, "u_view");
      u_Projection = glGetUniformLocation(shaderProgramObject, "u_Projection");
      lKeyPressed = glGetUniformLocation(shaderProgramObject, "u_lKeyIsPressed");
      u_la= glGetUniformLocation(shaderProgramObject, "u_la");
      u_ld = glGetUniformLocation(shaderProgramObject, "u_ld");
      u_ls = glGetUniformLocation(shaderProgramObject, "u_ls");
      u_ka = glGetUniformLocation(shaderProgramObject, "u_ka");
      u_kd = glGetUniformLocation(shaderProgramObject, "u_kd");
      u_ks = glGetUniformLocation(shaderProgramObject, "u_ks");
      u_materialShineness = glGetUniformLocation(shaderProgramObject, "u_matrialShineness");
      lightPositionUniform = glGetUniformLocation(shaderProgramObject, "u_light_position");

    

}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}

-(void)drawView:(id)sender
{


    //code
    [EAGLContext setCurrentContext:eaglContext];
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
   
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
      glUseProgram(shaderProgramObject);
         
         vmath::mat4 modelMatrix = vmath::mat4::identity();
         vmath::mat4 viewMatrix = vmath::mat4::identity();
         vmath::mat4 projectMatrix = vmath::mat4::identity();

         
         
         modelMatrix= vmath::translate(0.0f, 0.0f, -3.0f);
      
         
         
           
    projectMatrix = perspectiveProjectionMatrix * modelMatrix;

           //send necessary matrices to shader in respective uniform
           glUniformMatrix4fv(u_model, 1, GL_FALSE, modelMatrix);
           glUniformMatrix4fv(u_View, 1, GL_FALSE, viewMatrix);
           glUniformMatrix4fv(u_Projection, 1, GL_FALSE, projectMatrix);

             if (gbLighting == true)
             {
                 glUniform1i(lKeyPressed, 1);
                 glUniform3fv(u_la, 1, LightAmbient);
                 glUniform3fv(u_ld, 1, LIghtDiifuse);
                 glUniform3fv(u_ls, 1, LightSpecular);
                 glUniform3fv(lightPositionUniform, 1, LightPosition);
                 glUniform3fv(u_ka, 1, MaterialAmbient);
                 glUniform3fv(u_kd, 1, MaterialDiffuse);
                 glUniform3fv(u_ks, 1, MaterialSpecular);
                 glUniform1f(u_materialShineness,  MaterialShineness);

             }

             else
             {
                 glUniform1i(lKeyPressed, 0);
             }




         //Bind with vao
         //(This will avoid repetative steps)

         glBindVertexArray(vao_sphere);

          glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_elements);
         glDrawElements(GL_TRIANGLES, numElements, GL_UNSIGNED_SHORT,0);
         

         glBindVertexArray(0);
         
         glUseProgram(0);
         if(keyPress==1)
         {
             [self VertexShader_PerFragment];
             [self FragmentShader_PerFragment];
             [self ShaderProgramLinking];
         }
         else if(keyPress==2)
         {
             [self vertexShader_PerVertex];
             [self fragmentShader_PerVertex];
             [self ShaderProgramLinking];
         }
         
    
    glBindRenderbuffer(GL_RENDERBUFFER,colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
    
}

-(void)layoutSubviews
{
    
    //code
    GLint width;
    GLint height;
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    glViewport(0, 0, width, height);
    
    GLfloat fwidth = (GLfloat)width;
    GLfloat fheight = (GLfloat)height;
    
    glViewport(0,0,(GLsizei)fwidth,(GLsizei)fheight);
       perspectiveProjectionMatrix= vmath::perspective(45.0f, (GLfloat)fwidth / (GLfloat)fheight, 0.1f, 100.0f);
    [self drawView:nil];
}

-(void)startAnimation
{
    if(!isAnimation)
    {
        displayLink = [NSClassFromString(@"CADisplayLink")displayLinkWithTarget:self selector:@selector(drawView:)];
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        
        isAnimation=YES;
    }
}

-(void)stopAnimation
{
    if(isAnimation)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimation = NO;
    }
}

-(BOOL)acceptsFirstResponder
{
    return(YES);
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if(gbLighting==true)
        gbLighting=false;
    else
        gbLighting=true;
}
-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
     
    
    if(key==1)
    {
        keyPress=1;
        key=2;
        printf("Key is %d",key);
    }
    
    else if(key==2)
    {
        keyPress=2;
        key=1;
        printf("Key is %d",key);
    }
    
  
}
-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}
-(void)dealloc
{
  if (vbo_sphere_elements)
   {
       glDeleteBuffers(1, &vbo_sphere_elements);
       vbo_sphere_elements = 0;
   }
   if (vao_sphere)
   {
       glDeleteVertexArrays(1, &vao_sphere);
       vao_sphere = 0;
   }

   if (vbo_sphere_normal)
   {
       glDeleteBuffers(1, &vbo_sphere_normal);
       vbo_sphere_normal = 0;
   }

   if (vbo_sphere_position)
   {
       glDeleteBuffers(1, &vbo_sphere_position);
       vbo_sphere_position = 0;
   }
   
    
    glDetachShader(shaderProgramObject, vertexShaderObject);
    
    glDetachShader(shaderProgramObject, fragmentShaderObject);
    
    glDeleteShader(vertexShaderObject);
    vertexShaderObject=0;
    
    glDeleteShader(fragmentShaderObject);
    fragmentShaderObject=0;
    
    glDeleteProgram(shaderProgramObject);
    shaderProgramObject=0;
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1,&depthRenderbuffer);
        depthRenderbuffer=0;
    }
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1,&colorRenderbuffer);
        colorRenderbuffer=0;
    }
    if(defaultFramebuffer)
    {
        glDeleteFramebuffers(1, &defaultFramebuffer);
        defaultFramebuffer=0;
    }
    if([EAGLContext currentContext] == eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext = nil;
    
    [super dealloc];
}
@end

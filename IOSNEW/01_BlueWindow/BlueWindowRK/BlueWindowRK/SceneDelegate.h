//
//  SceneDelegate.h
//  BlueWindowRK
//
//  Created by Admin on 06/03/20.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

